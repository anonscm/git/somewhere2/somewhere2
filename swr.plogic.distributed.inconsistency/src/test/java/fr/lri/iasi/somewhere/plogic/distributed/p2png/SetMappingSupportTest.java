/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.p2png;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.MappingSupport;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.SetMappingSupport;

public class SetMappingSupportTest {
	
	@Test
	public void testEquals1() {	
		SetMappingSupport sms = new SetMappingSupport();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("mapping2");
		ms1.add("mapping1");
		MappingSupport ms2 = new MappingSupport();
		ms2.add("mapping3");
		ms2.add("mapping4");
		sms.add(ms1);
		sms.add(ms2);
		
		SetMappingSupport sms2 = new SetMappingSupport();
		MappingSupport ms3 = new MappingSupport();
		ms3.add("mapping2");
		ms3.add("mapping1");
		MappingSupport ms4 = new MappingSupport();
		ms4.add("mapping3");
		ms4.add("mapping4");
		sms2.add(ms3);
		sms2.add(ms4);
		
		assertEquals(sms, sms2);
	}
	
	@Test
	public void testEquals2() {
		
		Set<SetMappingSupport> set = new HashSet<SetMappingSupport>();
		
		SetMappingSupport sms = new SetMappingSupport();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("mapping2");
		ms1.add("mapping1");
		MappingSupport ms2 = new MappingSupport();
		ms2.add("mapping3");
		ms2.add("mapping4");
		sms.add(ms1);
		sms.add(ms2);
		
		set.add(sms);
		
		SetMappingSupport sms2 = new SetMappingSupport();
		MappingSupport ms3 = new MappingSupport();
		ms3.add("mapping2");
		ms3.add("mapping1");
		MappingSupport ms4 = new MappingSupport();
		ms4.add("mapping3");
		ms4.add("mapping4");
		sms2.add(ms3);
		sms2.add(ms4);
		
		assertTrue(set.contains(sms2));
	}

	@Test
	public void testAdd1() {
		SetMappingSupport sms = new SetMappingSupport();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("0");
		ms1.add("1");
		ms1.add("2");
		
		MappingSupport ms2 = new MappingSupport();
		ms2.add("1");
		ms2.add("2");
		
		assertTrue(sms.add(ms2));
		assertFalse(sms.add(ms1));
	}
	
	@Test
	public void testUnion1() {
		SetMappingSupport sms1 = new SetMappingSupport();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("0");
		ms1.add("1");
		ms1.add("2");
		
		MappingSupport ms2 = new MappingSupport();
		ms2.add("1");
		ms2.add("2");
		ms2.add("3");
		
		sms1.add(ms1);
		sms1.add(ms2);
		
		SetMappingSupport sms2 = new SetMappingSupport();
		MappingSupport ms3 = new MappingSupport();
		ms3.add("0");
		ms3.add("1");
		
		MappingSupport ms4 = new MappingSupport();
		ms4.add("1");
		ms4.add("2");
		ms4.add("3");
		ms4.add("4");
		
		sms2.add(ms3);
		sms2.add(ms4);
		
		SetMappingSupport result = sms1.union(sms2);
		
		assertThat(result.size(), is(2));
		assertTrue(result.contains(ms3));
		assertTrue(result.contains(ms2));
		assertFalse(result.contains(ms1));
		assertFalse(result.contains(ms4));
		
		result.remove(ms3);
		assertTrue(sms2.contains(ms3));
	}
	
	@Test
	public void testProduct1() {
		SetMappingSupport sms1 = new SetMappingSupport();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("0");
		ms1.add("1");
		
		MappingSupport ms2 = new MappingSupport();
		ms2.add("2");
		ms2.add("3");
		
		sms1.add(ms1);
		sms1.add(ms2);
		
		SetMappingSupport sms2 = new SetMappingSupport();
		MappingSupport ms3 = new MappingSupport();
		ms3.add("4");
		ms3.add("5");
		
		MappingSupport ms4 = new MappingSupport();
		ms4.add("6");
		ms4.add("7");
		
		sms2.add(ms3);
		sms2.add(ms4);
		
		SetMappingSupport result = sms1.product(sms2);
		
		assertThat(result.size(), is(4));
	}

}
