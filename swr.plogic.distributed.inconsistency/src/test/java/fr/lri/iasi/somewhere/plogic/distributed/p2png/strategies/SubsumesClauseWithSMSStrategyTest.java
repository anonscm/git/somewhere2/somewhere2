/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.p2png.strategies;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses.ResultPair;
import fr.lri.iasi.libs.plogic.impl.ClauseElementsFactory;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryImpl;
import fr.lri.iasi.somewhere.plogic.distributed.MappingBuilder;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.MappingSupport;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.SetMappingSupport;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.annotations.SetMappingSupportAnnotation;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies.ResolveClauseWithSMSStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies.SubsumesClauseWithSMSStrategy;

public class SubsumesClauseWithSMSStrategyTest {
	
	@BeforeClass
	public static void prepare() {
		ClauseElementsFactory.getInstance().addAnnotations(new SetMappingSupportAnnotation());
		
		ClauseElementsFactory.getInstance().setResolveStrategy(new ResolveClauseWithSMSStrategy());
		ClauseElementsFactory.getInstance().setSubsumesStrategy(new SubsumesClauseWithSMSStrategy());
	}

	@Test
	public void testSubsumes1() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.addLiteral("1");
		builder1.addLiteral("2");
		builder1.addNegativeLiteral("3");
		Clause clause1 = builder1.create();
		
		SetMappingSupport sms1 = (SetMappingSupport) clause1.getAnnotations().get("sms").getContent();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("mapping2");
		ms1.add("mapping1");
		sms1.add(ms1);
		
		MappingBuilder builder2 = new MappingBuilder();
		builder2.addLiteral("1");
		builder2.addLiteral("2");
		Clause clause2 = builder2.create();
		
		SetMappingSupport sms2 = (SetMappingSupport) clause2.getAnnotations().get("sms").getContent();
		MappingSupport ms2 = new MappingSupport();
		ms2.add("mapping2");
		sms2.add(ms2);
		
		assertFalse(clause1.subsumes(clause2));
		assertTrue(clause2.subsumes(clause1));
	}
	
	@Test
	public void testSubsumes2() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.addLiteral("1");
		builder1.addLiteral("2");
		builder1.addNegativeLiteral("3");
		Clause clause1 = builder1.create();
		
		SetMappingSupport sms1 = (SetMappingSupport) clause1.getAnnotations().get("sms").getContent();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("mapping2");
		sms1.add(ms1);
		
		MappingBuilder builder2 = new MappingBuilder();
		builder2.addLiteral("1");
		builder2.addLiteral("2");
		Clause clause2 = builder2.create();
		
		SetMappingSupport sms2 = (SetMappingSupport) clause2.getAnnotations().get("sms").getContent();
		MappingSupport ms2 = new MappingSupport();
		ms2.add("mapping2");
		ms2.add("mapping1");
		sms2.add(ms2);
		
		assertFalse(clause1.subsumes(clause2));
		assertFalse(clause2.subsumes(clause1));
	}
		
	@Test
	public void testSubsumes3() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.addLiteral("H1");
		builder1.addNegativeLiteral("F1");
		Clause clause1 = builder1.create();
		
		SetMappingSupport sms1 = (SetMappingSupport) clause1.getAnnotations().get("sms").getContent();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("mapping1");
		ms1.add("mapping2");
		ms1.add("mapping3");
		ms1.add("mapping4");
		ms1.add("mapping5");
		ms1.add("mapping6");
		sms1.add(ms1);
		
		MappingBuilder builder2 = new MappingBuilder();
		builder2.addLiteral("H1");
		builder2.addNegativeLiteral("F1");
		builder2.addNegativeLiteral("A1");
		Clause clause2 = builder2.create();
		
		SetMappingSupport sms2 = (SetMappingSupport) clause2.getAnnotations().get("sms").getContent();
		MappingSupport ms2 = new MappingSupport();
		ms2.add("mapping1");
		ms2.add("mapping2");
		ms2.add("mapping3");
		ms2.add("mapping4");
		ms2.add("mapping5");
		ms2.add("mapping6");
		sms2.add(ms2);
		
		assertTrue(clause1.subsumes(clause2));
	}
	
	@Test
	public void testSubsumes4() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.addLiteral("G1");
		builder1.addLiteral("H1");
		Clause clause1 = builder1.create();
		
		SetMappingSupport sms1 = (SetMappingSupport) clause1.getAnnotations().get("sms").getContent();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("P0:m0");
		ms1.add("P1:m1");
		ms1.add("P98:m0");
		ms1.add("P97:m1");
		ms1.add("P3:m1");
		ms1.add("P2:m0");
		ms1.add("P1:m0");
		sms1.add(ms1);
		
		MappingBuilder builder2 = new MappingBuilder();
		builder2.addLiteral("G1");
		builder2.addLiteral("H1");
		Clause clause2 = builder2.create();
		
		SetMappingSupport sms2 = (SetMappingSupport) clause2.getAnnotations().get("sms").getContent();
		MappingSupport ms2 = new MappingSupport();
		ms2.add("P1:m1");
		ms2.add("P98:m0");
		ms2.add("P97:m1");
		ms2.add("P3:m1");
		ms2.add("P2:m0");
		ms2.add("P1:m0");
		sms2.add(ms2);
		
		assertTrue(clause2.subsumes(clause1));
	}
	
	@Test
	public void testSubsumes5() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.addLiteral("G1");
		builder1.addLiteral("H1");
		Clause clause1 = builder1.create();
		
		SetMappingSupport sms1 = (SetMappingSupport) clause1.getAnnotations().get("sms").getContent();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("P0:m0");
		ms1.add("P1:m0");
		ms1.add("P98:m0");
		ms1.add("P97:m1");
		ms1.add("P3:m1");
		ms1.add("P2:m0");
		sms1.add(ms1);
		
		MappingBuilder builder2 = new MappingBuilder();
		builder2.addLiteral("G1");
		builder2.addLiteral("H1");
		builder2.addNegativeLiteral("A1");
		Clause clause2 = builder2.create();
		
		SetMappingSupport sms2 = (SetMappingSupport) clause2.getAnnotations().get("sms").getContent();
		MappingSupport ms2 = new MappingSupport();
		ms2.add("P0:m0");
		ms2.add("P1:m0");
		ms2.add("P98:m0");
		ms2.add("P97:m1");
		ms2.add("P3:m1");
		ms2.add("P2:m0");
		sms2.add(ms2);
		
		assertTrue(clause1.subsumes(clause2));
	}
	
	@Test
	public void testSubsumes6() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.addLiteral("G1");
		builder1.addLiteral("H1");
		Clause clause1 = builder1.create();
		
		SetMappingSupport sms1 = (SetMappingSupport) clause1.getAnnotations().get("sms").getContent();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("P0:m0");
		ms1.add("P1:m0");
		ms1.add("P98:m0");
		ms1.add("P97:m1");
		ms1.add("P3:m1");
		ms1.add("P2:m0");
		sms1.add(ms1);
		
		MappingBuilder builder2 = new MappingBuilder();
		builder2.addLiteral("G1");
		builder2.addLiteral("H1");
		builder2.addNegativeLiteral("A1");
		Clause clause2 = builder2.create();
		
		SetMappingSupport sms2 = (SetMappingSupport) clause2.getAnnotations().get("sms").getContent();
		MappingSupport ms2 = new MappingSupport();
		ms2.add("P0:m0");
		ms2.add("P1:m0");
		ms2.add("P98:m0");
		ms2.add("P97:m1");
		ms2.add("P3:m1");
		ms2.add("P2:m0");
		sms2.add(ms2);
		
		MappingSupport ms3 = new MappingSupport();
		ms3.add("P0:m0");
		ms3.add("P1:m1");
		ms3.add("P98:m0");
		ms3.add("P97:m1");
		ms3.add("P3:m1");
		ms3.add("P2:m0");
		sms2.add(ms3);
		
		assertFalse(clause1.subsumes(clause2));
	}
	
	@Test
	public void testSubsumes7() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.addLiteral("G1");
		builder1.addLiteral("H1");
		Clause clause1 = builder1.create();
		
		SetMappingSupport sms1 = (SetMappingSupport) clause1.getAnnotations().get("sms").getContent();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("P0:m0");
		ms1.add("P1:m0");
		ms1.add("P98:m0");
		ms1.add("P97:m1");
		ms1.add("P3:m1");
		ms1.add("P2:m0");
		sms1.add(ms1);
		
		MappingBuilder builder2 = new MappingBuilder();
		builder2.addLiteral("G1");
		builder2.addLiteral("H1");
		builder2.addNegativeLiteral("A1");
		Clause clause2 = builder2.create();
		
		SetMappingSupport sms2 = (SetMappingSupport) clause2.getAnnotations().get("sms").getContent();
		MappingSupport ms2 = new MappingSupport();
		ms2.add("P0:m0");
		ms2.add("P1:m0");
		ms2.add("P98:m0");
		ms2.add("P97:m1");
		ms2.add("P3:m1");
		ms2.add("P2:m0");
		sms2.add(ms2);
		MappingSupport ms3 = new MappingSupport();
		ms3.add("P0:m0");
		ms3.add("P1:m1");
		ms3.add("P2:m1");
		ms3.add("P98:m0");
		ms3.add("P97:m1");
		ms3.add("P3:m1");
		ms3.add("P2:m0");
		sms1.add(ms3);
		
		assertTrue(clause1.subsumes(clause2));
	}
	
	@Test
	public void testSubsumes8() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.addLiteral("G1");
		builder1.addLiteral("H1");
		Clause clause1 = builder1.create();
		
		SetMappingSupport sms1 = (SetMappingSupport) clause1.getAnnotations().get("sms").getContent();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("P0:m0");
		ms1.add("P1:m0");
		ms1.add("P98:m0");
		ms1.add("P97:m1");
		ms1.add("P3:m1");
		ms1.add("P2:m0");
		sms1.add(ms1);
		MappingSupport ms3 = new MappingSupport();
		ms3.add("P0:m0");
		ms3.add("P1:m1");
		ms3.add("P98:m0");
		ms3.add("P97:m1");
		ms3.add("P3:m1");
		ms3.add("P2:m0");
		sms1.add(ms3);
		
		MappingBuilder builder2 = new MappingBuilder();
		builder2.addLiteral("G1");
		builder2.addLiteral("H1");
		builder2.addNegativeLiteral("A1");
		Clause clause2 = builder2.create();
		
		assertFalse(clause1.subsumes(clause2));
	}
	
	@Test
	public void testSubsumes9() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.addLiteral("G1");
		builder1.addLiteral("H1");
		Clause clause1 = builder1.create();
		
		SetMappingSupport sms1 = (SetMappingSupport) clause1.getAnnotations().get("sms").getContent();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("P1:m1");
		ms1.add("P2:m0");
		ms1.add("P3:m1");
		ms1.add("P97:m1");
		ms1.add("P98:m0");
		sms1.add(ms1);
		MappingSupport ms3 = new MappingSupport();
		ms3.add("P1:m0");
		ms3.add("P2:m0");
		ms3.add("P3:m1");
		ms3.add("P97:m1");
		ms3.add("P98:m0");
		sms1.add(ms3);
		
		MappingBuilder builder2 = new MappingBuilder();
		builder2.addLiteral("G1");
		builder2.addLiteral("H1");
		builder2.addNegativeLiteral("A1");
		Clause clause2 = builder2.create();
		
		SetMappingSupport sms2 = (SetMappingSupport) clause2.getAnnotations().get("sms").getContent();
		MappingSupport ms2 = new MappingSupport();
		ms2.add("P1:m0");
		ms2.add("P2:m0");
		ms2.add("P3:m1");
		ms2.add("P97:m1");
		ms2.add("P98:m0");
		sms2.add(ms2);
		
		assertTrue(clause1.subsumes(clause2));
	}	
	
	@Test
	public void testSubsumes10() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.addLiteral("G1");
		builder1.addLiteral("H1");
		Clause clause1 = builder1.create();
		
		SetMappingSupport sms1 = (SetMappingSupport) clause1.getAnnotations().get("sms").getContent();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("P97:m1");
		ms1.add("P98:m0");
		sms1.add(ms1);
		
		MappingBuilder builder2 = new MappingBuilder();
		builder2.addLiteral("Empty");
		Clause clause2 = builder2.create();
		
		SetMappingSupport sms2 = (SetMappingSupport) clause2.getAnnotations().get("sms").getContent();
		MappingSupport ms2 = new MappingSupport();
		ms2.add("P0:m0");
		ms2.add("P1:m1");
		ms2.add("P97:m1");
		ms2.add("P98:m0");
		sms2.add(ms2);
		
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		theory.add(clause2);
		
		assertFalse(theory.subsumes(clause1));
	}
	
	@Test
	public void testSubsumes11() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.addLiteral("G1");
		builder1.addLiteral("H1");
		Clause clause1 = builder1.create();
		
		SetMappingSupport sms1 = (SetMappingSupport) clause1.getAnnotations().get("sms").getContent();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("P2:m0");
		sms1.add(ms1);

		MappingBuilder builder2 = new MappingBuilder();
		builder2.addLiteral("Empty");
		Clause clause2 = builder2.create();
		
		SetMappingSupport sms2 = (SetMappingSupport) clause2.getAnnotations().get("sms").getContent();
		MappingSupport ms2 = new MappingSupport();
		ms2.add("P0:m0");
		ms2.add("P1:m1");
		ms2.add("P2:m0");
		sms2.add(ms2);
		
		MappingBuilder builder3 = new MappingBuilder();
		builder3.addLiteral("G1");
		builder3.addLiteral("H1");
		Clause clause3 = builder3.create();
		
		SetMappingSupport sms3 = (SetMappingSupport) clause3.getAnnotations().get("sms").getContent();
		MappingSupport ms3 = new MappingSupport();
		ms3.add("P2:m0");
		sms3.add(ms3);
		
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		theory.add(clause3);
		theory.add(clause2);
		
		assertFalse(theory.subsumes(clause1));
	}
	
	
	@Test
	public void testSubsumes12() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.addLiteral("G1");
		builder1.addLiteral("H1");
		Clause clause1 = builder1.create();

		MappingBuilder builder2 = new MappingBuilder();
		builder2.addLiteral("Empty");
		Clause clause2 = builder2.create();
		
		SetMappingSupport sms2 = (SetMappingSupport) clause2.getAnnotations().get("sms").getContent();
		MappingSupport ms2 = new MappingSupport();
		ms2.add("P0:m0");
		ms2.add("P1:m1");
		sms2.add(ms2);
		
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		theory.add(clause2);
		ResultPair result = theory.addSubsuming(clause1);
		
		assertTrue(result.getFirst());
	}

}
