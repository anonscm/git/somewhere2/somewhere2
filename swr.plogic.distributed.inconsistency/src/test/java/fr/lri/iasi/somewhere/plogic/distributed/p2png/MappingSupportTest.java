/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.p2png;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.MappingSupport;

public class MappingSupportTest {
	
	@Test
	public void testEquals1() {
		MappingSupport ms1 = new MappingSupport();
		ms1.add("1");
		ms1.add("2");
		
		MappingSupport ms2 = new MappingSupport();
		ms2.add("1");
		ms2.add("2");
		
		assertTrue(ms1.equals(ms2));
	}
	
	@Test
	public void testEquals2() {
		MappingSupport ms1 = new MappingSupport();
		ms1.add("1");
		ms1.add("2");
		
		Set<MappingSupport> set = new HashSet<MappingSupport>();
		set.add(ms1);
		
		MappingSupport ms2 = new MappingSupport();
		ms2.add("1");
		ms2.add("2");
		
		assertTrue(set.contains(ms2));
	}

	@Test
	public void testUnion1() {
		MappingSupport ms1 = new MappingSupport();
		ms1.add("0");
		ms1.add("1");
		
		MappingSupport ms2 = new MappingSupport();
		ms2.add("1");
		ms2.add("2");
		
		MappingSupport result = ms1.union(ms2);
		
		assertTrue(result.contains("0"));
		assertTrue(result.contains("1"));
		assertTrue(result.contains("2"));
		assertThat(result.size(), is(3));
	}
	
	@Test
	public void testSubsumes1() {
		MappingSupport ms1 = new MappingSupport();
		ms1.add("1");
		ms1.add("2");
		
		MappingSupport ms2 = new MappingSupport();
		ms2.add("1");
		ms2.add("2");
		ms2.add("3");
		
		assertTrue(ms1.subsumes(ms2));
		assertTrue(ms1.subsumes(ms1));
		assertFalse(ms2.subsumes(ms1));
		assertTrue(ms2.subsumes(ms2));
	}

}
