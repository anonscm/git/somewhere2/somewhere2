/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.p2png.strategies;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.impl.ClauseBuilder;
import fr.lri.iasi.libs.plogic.impl.ClauseElementsFactory;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryElementsFactory;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryImpl;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.MappingSupport;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.SetMappingSupport;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.annotations.SetMappingSupportAnnotation;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies.ResolveClauseWithSMSStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies.SubsumesClauseWithSMSStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies.TheoryAddFormulaWithSMSStrategy;

public class TheoryAddFormulaWithSMSStrategyTest {
	
	@BeforeClass
	public static void prepare() {
		ClauseElementsFactory.getInstance().addAnnotations(new SetMappingSupportAnnotation());
		
		PropositionalClausalTheoryElementsFactory.getInstance().setAddFormulaStrategy(new TheoryAddFormulaWithSMSStrategy());
		ClauseElementsFactory.getInstance().setResolveStrategy(new ResolveClauseWithSMSStrategy());
		ClauseElementsFactory.getInstance().setSubsumesStrategy(new SubsumesClauseWithSMSStrategy());
	}

	@Test
	public void testSMSUpdate1() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		// Adding clause (!q v p v r) 
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("q");
		builder1.addLiteral("p");
		builder1.addLiteral("r");
		Clause clause1 = builder1.create();
		
		SetMappingSupport sms = (SetMappingSupport) clause1.getAnnotations().remove("sms").getContent();
		MappingSupport ms = new MappingSupport();
		ms.add("mapping2");
		ms.add("mapping1");
		sms.add(ms);
		SetMappingSupportAnnotation annotation = new SetMappingSupportAnnotation(sms);
		clause1.getAnnotations().put("sms", annotation);
		
		theory.add(clause1);
		
		// Adding clause (!q v p v r) 
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("q");
		builder2.addLiteral("p");
		builder2.addLiteral("r");
		Clause clause2 = builder2.create();
		
		SetMappingSupport sms2 = (SetMappingSupport) clause2.getAnnotations().remove("sms").getContent();
		MappingSupport ms2 = new MappingSupport();
		ms2.add("mapping3");
		ms2.add("mapping4");
		sms2.add(ms2);
		SetMappingSupportAnnotation annotation2 = new SetMappingSupportAnnotation(sms2);
		clause2.getAnnotations().put("sms", annotation2);
		
		theory.add(clause2);
		
		// Building expected theory
		PropositionalClausalTheory expected = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addNegativeLiteral("q");
		builder3.addLiteral("p");
		builder3.addLiteral("r");
		Clause clause3 = builder3.create();
		
		SetMappingSupport sms3 = (SetMappingSupport) clause3.getAnnotations().remove("sms").getContent();
		MappingSupport ms3 = new MappingSupport();
		ms3.add("mapping2");
		ms3.add("mapping1");
		MappingSupport ms4 = new MappingSupport();
		ms4.add("mapping3");
		ms4.add("mapping4");
		sms3.add(ms3);
		sms3.add(ms4);
		SetMappingSupportAnnotation annotation3 = new SetMappingSupportAnnotation(sms3);
		clause3.getAnnotations().put("sms", annotation3);
		
		expected.add(clause3);
		
		assertEquals(expected, theory);
	}

}
