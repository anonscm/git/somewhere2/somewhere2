/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.p2png.strategies;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.impl.ClauseElementsFactory;
import fr.lri.iasi.libs.plogic.impl.LiteralImpl;
import fr.lri.iasi.somewhere.plogic.distributed.DistantLiteralImpl;
import fr.lri.iasi.somewhere.plogic.distributed.MappingBuilder;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.MappingSupport;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.SetMappingSupport;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.annotations.SetMappingSupportAnnotation;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies.ResolveClauseWithSMSStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies.SubsumesClauseWithSMSStrategy;

public class ResolveClauseWithSMSStrategyTest {
	
	@BeforeClass
	public static void prepare() {
		ClauseElementsFactory.getInstance().addAnnotations(new SetMappingSupportAnnotation());
		
		ClauseElementsFactory.getInstance().setResolveStrategy(new ResolveClauseWithSMSStrategy());
		ClauseElementsFactory.getInstance().setSubsumesStrategy(new SubsumesClauseWithSMSStrategy());
	}

	@Test
	public void testSMSUpdate1() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.setLabel("mapping1");
		builder1.addLiteral("peer1", "1");
		builder1.addLiteral("2");
		builder1.addNegativeLiteral("3");
		Clause clause1 = builder1.create();
		
		MappingBuilder builder2 = new MappingBuilder();
		builder2.setLabel("mapping2");
		builder2.addLiteral("peer2", "4");
		builder2.addLiteral("3");
		Clause clause2 = builder2.create();
		
		Clause result = clause1.resolve(clause2, new LiteralImpl("3", true));
		assertNotNull(result.getAnnotations().get("sms"));
		
		SetMappingSupport sms = (SetMappingSupport) result.getAnnotations().get("sms").getContent();
		MappingSupport ms = new MappingSupport();
		ms.add("mapping2");
		ms.add("mapping1");
		
		assertTrue(sms.contains(ms));
	}
	
	@Test
	public void testSMSUpdate2() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.setLabel("mapping1");
		builder1.addLiteral("1");
		builder1.addLiteral("2");
		builder1.addNegativeLiteral("peer1", "3");
		Clause clause1 = builder1.create();
		
		MappingBuilder builder2 = new MappingBuilder();
		builder2.setLabel("mapping2");
		builder2.addLiteral("4");
		builder2.addLiteral("peer1", "3");
		Clause clause2 = builder2.create();
		
		Clause result = clause1.resolve(clause2, new DistantLiteralImpl("peer1", "3", true));
		
		SetMappingSupport sms = (SetMappingSupport) result.getAnnotations().get("sms").getContent();
		MappingSupport ms = new MappingSupport();
		ms.add("mapping1");
		ms.add("mapping2");
		
		assertTrue(sms.contains(ms));
	}
	
	@Test
	public void testSMSUpdate3() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.setLabel("mapping1");
		builder1.addLiteral("1");
		builder1.addLiteral("2");
		builder1.addNegativeLiteral("peer1", "3");
		builder1.addLiteral("peer1", "7");
		Clause clause1 = builder1.create();
		
		MappingBuilder builder2 = new MappingBuilder();
		builder2.setLabel("mapping2");
		builder2.addLiteral("4");
		builder2.addLiteral("peer1", "3");
		Clause clause2 = builder2.create();
		
		Clause firstResult = clause1.resolve(clause2, new DistantLiteralImpl("peer1", "3", true));
				
		MappingBuilder builder3 = new MappingBuilder();
		builder3.setLabel("mapping3");
		builder3.addLiteral("8");
		builder3.addNegativeLiteral("peer1", "7");
		builder3.addLiteral("peer1", "5");
		Clause clause3 = builder3.create();
		
		MappingBuilder builder4 = new MappingBuilder();
		builder4.setLabel("mapping4");
		builder4.addLiteral("6");
		builder4.addLiteral("peer1", "3");
		builder4.addNegativeLiteral("peer1", "5");
		Clause clause4 = builder4.create();
		
		Clause secondResult = clause3.resolve(clause4, new DistantLiteralImpl("peer1", "5", true));
		
		Clause result = firstResult.resolve(secondResult, new DistantLiteralImpl("peer1", "7", true));
		SetMappingSupport sms = (SetMappingSupport) result.getAnnotations().get("sms").getContent();
		
		MappingSupport ms1 = new MappingSupport();
		ms1.add("mapping1");
		ms1.add("mapping2");
		ms1.add("mapping3");
		ms1.add("mapping4");
		
		assertTrue(sms.contains(ms1));
		assertThat(sms.size(), is(1));
		assertThat(ms1.size(), is(4));
	}
	
	@Test
	public void testSMSUpdate4() {
		MappingBuilder builder1 = new MappingBuilder();
		builder1.setLabel("mapping1");
		builder1.addLiteral("1");
		builder1.addNegativeLiteral("2");
		Clause clause1 = builder1.create();
		
		MappingBuilder builder2 = new MappingBuilder();
		builder2.setLabel("mapping2");
		builder2.addLiteral("2");
		builder2.addLiteral("peer1", "3");
		Clause clause2 = builder2.create();
		
		Clause result = clause1.resolve(clause2, new LiteralImpl("2", true));
		
		SetMappingSupport sms = (SetMappingSupport) result.getAnnotations().get("sms").getContent();
		MappingSupport ms1 = new MappingSupport();
		ms1.add("mapping1");
		
		MappingSupport ms2 = new MappingSupport();
		ms2.add("mapping2");
		
		assertFalse(sms.contains(ms1));
		assertTrue(sms.contains(ms2));
	}

}
