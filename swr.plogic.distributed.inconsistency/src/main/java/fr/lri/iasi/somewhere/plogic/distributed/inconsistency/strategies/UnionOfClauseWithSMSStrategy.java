/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.strategies.impl.DefaultUnionStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.SetMappingSupport;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.annotations.SetMappingSupportAnnotation;

/**
 * Handles the union of clauses considering the SMS information.
 * 
 * @author Andre Fonseca
 *
 */
public class UnionOfClauseWithSMSStrategy extends DefaultUnionStrategy<Clause, Literal> {
	static Logger logger = LoggerFactory.getLogger(UnionOfClauseWithSMSStrategy.class);

	@Override
	public Clause performUnion(Clause formula, Clause otherFormula) {
		Clause result = null;
		Clause toRetrieveAnnotations = null;
		
		if (!formula.isSat()) {
			result = otherFormula.copy();
			toRetrieveAnnotations = formula;
		} else if (!otherFormula.isSat()) {
			result = formula.copy();
			toRetrieveAnnotations = otherFormula;
		} else {
			result = formula.copy();
			result.addAll(otherFormula);
			toRetrieveAnnotations = otherFormula;
		}
		
		final SetMappingSupportAnnotation resultSMSAnnotation = (SetMappingSupportAnnotation) result.getAnnotations().get("sms");
		final SetMappingSupportAnnotation toRetrieveAnnotationsSMSAnnotation = (SetMappingSupportAnnotation) toRetrieveAnnotations.getAnnotations().get("sms");
		
		SetMappingSupport resultSMS = resultSMSAnnotation.getContent();
		SetMappingSupport toRetrieveAnnotationsSMS = toRetrieveAnnotationsSMSAnnotation.getContent();
		resultSMS = resultSMS.product(toRetrieveAnnotationsSMS);
		
		final SetMappingSupportAnnotation finalResultSMSAnnotation = new SetMappingSupportAnnotation(resultSMS);	
		result.getAnnotations().put("sms", finalResultSMSAnnotation);
		
		return result;
	}

}
