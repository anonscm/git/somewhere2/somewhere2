/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.inconsistency;

import java.lang.ref.WeakReference;
import java.util.concurrent.ConcurrentMap;

import com.google.common.collect.Maps;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageAdapter;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.MessageType;
import fr.lri.iasi.somewhere.api.communication.model.MessageTypeSingletonFactory;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicAnswerMessageType;

/**
 * Updates the SNG information before sending or receiving a message.
 * 
 * @author Andre Fonseca
 *
 */
public class SNGUpdater extends MessageAdapter {
	
	/* --- Properties --- */
	/** The manager class */
	private final InconsistencyManager p2pNGManager;
	
	/** The accepted type */
	private final MessageType answerType;
	
	/** Maps noGood by session */
	private final ConcurrentMap<WeakReference<MessageSession>, SetNoGood> noGoodMap = Maps.newConcurrentMap();
	
	public SNGUpdater(InconsistencyManager p2pNGManager) {
		if (p2pNGManager == null)
            throw new IllegalArgumentException("p2pNGManager");

		this.answerType = MessageTypeSingletonFactory.create(PLogicAnswerMessageType.class);
		
        this.p2pNGManager = p2pNGManager;
        this.p2pNGManager.getPLogicManager().getCommunicationManager().addMessageListener(this);
	}
	
	/* --- Methods --- */
	/**
	 * {@inheritDoc}
	 */
	@Override
    public void messageIsToBeSent(Message message) 
    		throws CommunicationException {
		
		// Creates the sng information for answers
		if (message.getType().equals(this.answerType)) {		
			// Getting refereed session
			MessageSession session = this.p2pNGManager.getPLogicManager().getCommunicationManager()
					.getSessionHandler().getCreatorSessions().get(message.getId());
			
			// Returns if session is already over
			if (session == null)
				return;
			
			// The message's sng needs to be united with all sngs of messages that arrived for the current session.
			SetNoGood sng = this.noGoodMap.get(session);
			if (sng != null)
				InconsistencyManager.NO_GOODS.addContent(message, sng);
		}
    }
	
	/**
	 * {@inheritDoc}
	 */
	@Override
    public void messageIsToBeReceived(Message message) 
    		throws CommunicationException {
		
		// Only for answer messages...
		if (message.getType().equals(this.answerType)) {
			
			// It will be created a new sng information ...
			SetNoGood sng = new SetNoGood(this.p2pNGManager.getNoGoods());
			SetNoGood messageSNG = InconsistencyManager.NO_GOODS.getContent(message);
			if (messageSNG != null)
				sng.union(messageSNG);
			
			// Getting refereed session
			MessageSession session = this.p2pNGManager.getPLogicManager().getCommunicationManager()
					.getSessionHandler().getMessageSessions().get(message.getId());
			
			// Returns if session is already over
			if (session == null)
				return;
			
			WeakReference<MessageSession> sessionReference = new WeakReference<MessageSession>(session);
			SetNoGood previousSNG = this.noGoodMap.putIfAbsent(sessionReference, sng);
			if (previousSNG != null) {
				// The message's sng needs to be united with all sngs of messages that arrived for the current session.
				sng = new SetNoGood(sng.union(previousSNG));
				SetNoGood oldVal = null;
				SetNoGood newVal = null;
				
				do {
					oldVal = this.noGoodMap.get(sessionReference);
					newVal = (oldVal == null) ? sng : new SetNoGood(sng.union(oldVal));
				} while (!this.noGoodMap.replace(sessionReference, oldVal, newVal));
			}
			
			// Update sng on the message
			if (!sng.isEmpty())
				InconsistencyManager.NO_GOODS.addContent(message, sng);
		}
    }

}
