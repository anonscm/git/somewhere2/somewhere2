/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.inconsistency;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.somewhere.api.communication.model.FinalAnswer;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageCollector;
import fr.lri.iasi.somewhere.plogic.distributed.PLogicManager;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicTimeoutMessageType;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicAnswerMessageType;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicFinishMessageType;
import fr.lri.iasi.somewhere.ui.CommandListener;

/**
 * Message listener used to output answers and its informations to the user interface.
 * Responsible for combining sms.
 * 
 * @author Andre Fonseca
 *
 */
public class InconsistencyAnswersCollector extends AbstractMessageCollector {
	static Logger logger = LoggerFactory.getLogger(InconsistencyAnswersCollector.class);
	
	/* --- Properties --- */
	/** The manager class */
	private final InconsistencyManager p2pNGManager;
	
	/** Command listener used to inform tha
	 * t all messages have been sent */
	private final CommandListener listener;
	
	/** Answer messages counter */
	private AtomicInteger answersReceived = new AtomicInteger();

	public InconsistencyAnswersCollector(InconsistencyManager p2pNGManager,
			CommandListener listener) {
		super(p2pNGManager.getPLogicManager().getCommunicationManager());
		
		// Preparing the message receiver    
        addAcceptedTypes(PLogicAnswerMessageType.class);
        
        this.p2pNGManager = p2pNGManager;
        this.listener = listener;
	}
	
	/* --- Accessors --- */
	public int getReceivedAnswersNum() {
		return answersReceived.get();
	}
	
	/* --- Methods --- */
	/**
     * {@inheritDoc}
     */
	@Override
	public void messageHasBeenSent(Message message) {
		
		if (!message.getDestination().getName().equals("User"))
			return;
		
		if (this.acceptedTypes.contains(message.getType())) {
			FinalAnswer<Clause> finalAnswer = PLogicManager.FINAL_ANSWERS.getContent(message);
			
			Clause clause = finalAnswer.getContent();
			if (!clause.isSat()) {
				SetMappingSupport sms = (SetMappingSupport) clause.getAnnotations().get("sms").getContent();
				this.p2pNGManager.getNoGoods().addAll(sms);
				
				Clause query = PLogicManager.QUERY.getContent(message);
				String label = query.getLabel();
				if (!label.isEmpty()) {
					SetMappingSupport smsToProduct = new SetMappingSupport();
					MappingSupport ms = new MappingSupport();
					ms.add(label);
					smsToProduct.add(ms);
					
					SetMappingSupport sng = this.p2pNGManager.getNoGoods().product(smsToProduct);
					SetNoGood newSNG = new SetNoGood(sng);
					this.p2pNGManager.setNoGoods(newSNG);
				}
			}
			
			this.ui.print(message.getType().print(message));

			this.answersReceived.incrementAndGet();
		} else if (message.getType() instanceof PLogicTimeoutMessageType) {
			this.ui.print(message.getType().print(message));
			this.listener.executionCompleted();
		} else if (message.getType() instanceof PLogicFinishMessageType) {
			this.listener.executionCompleted();
		}
    }
	
}
