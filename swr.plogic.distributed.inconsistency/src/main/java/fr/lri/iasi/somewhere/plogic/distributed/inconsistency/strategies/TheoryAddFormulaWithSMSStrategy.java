/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies;

import java.util.Collection;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses.ResultPair;
import fr.lri.iasi.libs.plogic.strategies.impl.TheoryAddFormulaStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.SetMappingSupport;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.annotations.SetMappingSupportAnnotation;

/**
 * Handles the addition of a clause in a theory, considering its SMS annotation.
 * 
 * @author Andre Fonseca
 *
 */
public class TheoryAddFormulaWithSMSStrategy extends TheoryAddFormulaStrategy {
	static Logger logger = LoggerFactory.getLogger(TheoryAddFormulaWithSMSStrategy.class);

	 /**
     * Add a new clause, and update literalHashTable accordingly
     * @param c Clause to add
     * @return true if success
     */
	@Override
	public boolean performAdd(PropositionalClausalTheory collection,
			Clause formula) {
		
		Clause canonicalClause = formula.canonicalCopy();
		Clause equivalentClause = collection.getCanonicalMap().get(canonicalClause);
		
		if (equivalentClause != null) {
			collection.remove(equivalentClause);	
			
			final SetMappingSupportAnnotation formulaSMSAnnotation = (SetMappingSupportAnnotation) formula.getAnnotations().get("sms");
			final SetMappingSupportAnnotation equivalentClauseSMSAnnotation = (SetMappingSupportAnnotation) equivalentClause.getAnnotations().get("sms");
			
			SetMappingSupport formulaSMS = formulaSMSAnnotation.getContent();
			SetMappingSupport equivalentClauseSMS = equivalentClauseSMSAnnotation.getContent();
			
			if (!formulaSMS.justContainEmptyMS()) {
				SetMappingSupport union = equivalentClauseSMS.union(formulaSMS);
			
				final SetMappingSupportAnnotation newFormulaSMSAnnotation = new SetMappingSupportAnnotation(union);
				formula.getAnnotations().put("sms", newFormulaSMSAnnotation);
			}
		}

		return super.performAdd(collection, formula);
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * <p>This operation consider the SMS annotation as follows: if in this theory there is 
	 * a clause that is canonically equals to the clause to be added (with a different SMS), 
	 * the ancient clause will considered as subsumed by the new clause to be added.</p>
	 */
	@Override
	public ResultPair performAddSubsuming(
			PropositionalClausalTheory collection, Clause clause) {
		Collection<Clause> subsumedClauses = Collections.emptyList();
		
		if (collection.subsumes(clause))
			return new ResultPair(false, subsumedClauses);
		
		Clause canonicalClause = clause.canonicalCopy();
		Clause equivalentClause = collection.getCanonicalMap().get(canonicalClause);
		
		subsumedClauses = collection.removeSubsumed(clause);
		if (equivalentClause != null)
			subsumedClauses.add(equivalentClause);
		
		boolean resultAdd = collection.add(clause);
        
		return new ResultPair(resultAdd, subsumedClauses);
	}

}
