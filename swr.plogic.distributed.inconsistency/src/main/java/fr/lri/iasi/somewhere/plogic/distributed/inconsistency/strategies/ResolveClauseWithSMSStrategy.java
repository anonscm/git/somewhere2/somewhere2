/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.strategies.impl.ClauseResolveStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.DistantLiteralImpl;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.MappingSupport;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.SetMappingSupport;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.annotations.SetMappingSupportAnnotation;

/**
 * Handles the SMS information during the clause resolve process.
 * 
 * @author Andre Fonseca
 *
 */
public class ResolveClauseWithSMSStrategy extends ClauseResolveStrategy {
	static Logger logger = LoggerFactory.getLogger(ResolveClauseWithSMSStrategy.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Clause performResolve(Clause targetClause, Clause resolvingClause, Literal literal) {
		Clause result = super.performResolve(targetClause, resolvingClause, literal);
		
		// The result of the resolution was null... there is no need to update anything...
		if (result == null)
			return null;
		
		// Verifies if the targetClause or the resolvingClause are mappings...
		boolean targetIsMapping = false;
		for (Literal targetClauseLit : targetClause) {
			if (targetClauseLit instanceof DistantLiteralImpl) {
				targetIsMapping = true;
				break;
			}
		}
		
		boolean resolvingIsMapping = false;
		for (Literal resolvingClauseLit : resolvingClause) {
			if (resolvingClauseLit instanceof DistantLiteralImpl) {
				resolvingIsMapping = true;
				break;
			}
		}
		
		// If neither the resolvingClause or the targetIsMapping are mappings, leave this method...
		if (!resolvingIsMapping && !targetIsMapping)
			return result;
		
		// ... Otherwise, apply the update on the SMS annotation!
		final SetMappingSupportAnnotation resultSMSAnnotation = (SetMappingSupportAnnotation) result.getAnnotations().get("sms");
		final SetMappingSupportAnnotation targetSMSAnnotation = (SetMappingSupportAnnotation) targetClause.getAnnotations().get("sms");
		final SetMappingSupportAnnotation resolvingSMSAnnotation = (SetMappingSupportAnnotation) resolvingClause.getAnnotations().get("sms");
		
		SetMappingSupport resultSMS = resultSMSAnnotation.getContent();
		SetMappingSupport targetSMS = targetSMSAnnotation.getContent();
		SetMappingSupport resolvingSMS = resolvingSMSAnnotation.getContent();
		
		// The result will be based on the product of the SMSs of the mappings used to create this new clause.
		SetMappingSupport product = resolvingSMS.product(targetSMS);
		
		// Updating the product of mapping supports and including them into the result SMS
		resultSMS.clear();
		for (MappingSupport ms : product) {
			
			// Only the mappings will included on the SMS of the resolving clause!
			if (targetIsMapping && !targetClause.getLabel().isEmpty())
				ms.add(targetClause.getLabel());
			if (resolvingIsMapping && !resolvingClause.getLabel().isEmpty())
				ms.add(resolvingClause.getLabel());
			
			resultSMS.add(ms);
		}
		
		SetMappingSupportAnnotation finalSMSAnnotation = new SetMappingSupportAnnotation(resultSMS);
		result.getAnnotations().put("sms", finalSMSAnnotation);
		
		return result;
	}

}
