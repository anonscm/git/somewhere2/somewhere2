/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.inconsistency;

/**
 * Describes a set of supports that indicates how inconsistencies were produced.
 * 
 * @author Andre Fonseca
 *
 */
public class SetNoGood extends SetMappingSupport {
	private static final long serialVersionUID = 8850104633485454287L;

	public SetNoGood() {
		super();
		this.clear();
	}
	
	public SetNoGood(SetMappingSupport noGoods) {
		this();
		this.addAll(noGoods);
	}

}
