/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.inconsistency.annotations;

import fr.lri.iasi.libs.formallogic.Annotation;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.SetMappingSupport;


/**
 * Clause annotation that wraps the sms information.
 * 
 * @author Andre Fonseca
 *
 */
public class SetMappingSupportAnnotation implements Annotation<SetMappingSupportAnnotation, SetMappingSupport> {
	private static final long serialVersionUID = -960998563004385203L;
	
	/* --- Properties --- */
	/** The wrapped sms */
	private final SetMappingSupport sms;
	
	public SetMappingSupportAnnotation() {
		this.sms = new SetMappingSupport();
	}
	
	public SetMappingSupportAnnotation(SetMappingSupport sms) {
		if (sms == null)
			throw new IllegalArgumentException("sms");
		
		this.sms = new SetMappingSupport(sms);
	}
	
	public SetMappingSupportAnnotation(SetMappingSupportAnnotation annotation) {
		this(annotation.getContent());
	}

	/* --- Accessors --- */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return "sms";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SetMappingSupport getContent() {
		return sms;
	}
	
	/* --- Methods --- */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addContent(Object o) {
		if (o instanceof SetMappingSupport) {
			SetMappingSupport sms = (SetMappingSupport) o;
			this.sms.addAll(sms);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SetMappingSupportAnnotation copy() {
		return new SetMappingSupportAnnotation(this);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + sms.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object o) {
        if (o == this)
            return true;

        if (!(o instanceof Annotation<?,?>))
            return false;
        
        if (o instanceof SetMappingSupportAnnotation) {
        	SetMappingSupportAnnotation s = (SetMappingSupportAnnotation) o;
        	
        	return sms.equals(s.getContent());
        }
        
        return false;
	}
	
	@Override
	public String toString() {
		return sms.toString();
	}

}
