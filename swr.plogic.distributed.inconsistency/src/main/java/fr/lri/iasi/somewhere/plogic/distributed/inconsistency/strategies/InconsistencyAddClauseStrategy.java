/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.libs.formallogic.Restriction;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.restrictions.EmptyClauseRestriction;
import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.InconsistencyAnswersCollector;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.InconsistencyManager;
import fr.lri.iasi.somewhere.plogic.distributed.strategies.LocallyAddClauseStrategy;

/**
 * Controls the inconsistency detection by the addition of a mapping on the current theory.
 * 
 * @author Andre Fonseca
 *
 */
public class InconsistencyAddClauseStrategy extends LocallyAddClauseStrategy {
	static Logger logger = LoggerFactory.getLogger(InconsistencyAddClauseStrategy.class);
	
	/* --- Properties --- */
	/** The manager class */
	private final InconsistencyManager p2pNGManager;
	
	public InconsistencyAddClauseStrategy(InconsistencyManager p2pNGManager) {
		super(p2pNGManager.getPLogicManager());
		
		this.p2pNGManager = p2pNGManager;
	}

	/* --- Methods --- */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void modify(Clause clause, boolean isMapping, long ttl) throws CommunicationException {
		
		try {
			// Creating answer collector (if any answer is needed)
			InconsistencyAnswersCollector collector = new InconsistencyAnswersCollector(this.p2pNGManager, this.commandListener);
	    	
	    	// Creating the required restriction (empty clauses only)
	    	Restriction<Clause> emptyClauseRestriction = new EmptyClauseRestriction();
	    	
	    	// Query the neighbor peers with the clause be added in oder to detect inconsistencies
			this.plogicManager.receiveQueryFromUser(clause, ttl, emptyClauseRestriction);
			
			waitExecutionToComplete();
			collector.finishReceiving();
			
			super.modify(clause, isMapping, ttl);
		} catch (InterruptedException e) {
			Thread.interrupted();
		}
	}
	
	/**
     * Blocks the UI until the execution is completed
     * @throws InterruptedException
     */
    private void waitExecutionToComplete() 
		throws InterruptedException {
		
		while (!this.commandListener.isExecutionCompleted())
    		Thread.sleep(10);
    }

}
