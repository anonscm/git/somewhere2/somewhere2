/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.somewhere.api.communication.model.Message;

import fr.lri.iasi.somewhere.plogic.distributed.PropositionalInstanceWithMapping;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.MappingSupport;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.InconsistencyManager;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.SetMappingSupport;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.SetNoGood;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.annotations.SetMappingSupportAnnotation;
import fr.lri.iasi.somewhere.plogic.distributed.strategies.AnytimeCombinationStrategy;

/**
 * Responsible for the combination of SMSs after the answer combination
 * 
 * @author Andre Fonseca
 *
 */
public class AnytimeCombinationWithSMSStrategy extends AnytimeCombinationStrategy {
	static Logger logger = LoggerFactory.getLogger(AnytimeCombinationWithSMSStrategy.class);
	
	/**
     * {@inheritDoc}
     */
    @Override
	public Collection<Clause> combine(PropositionalInstanceWithMapping instance,
			Clause query, 
			Clause answer, 
			Clause nonTerminal, 
			Map<Clause,Collection<Clause>> answersByQuery, 
			Message message) {
    	
    	Collection<Clause> combinations = super.combine(instance, query, answer, nonTerminal, answersByQuery, message);
		
		// Get the sms annotations
		SetMappingSupportAnnotation answerSMSAnnotation = null;
		SetMappingSupportAnnotation nonTerminalSMSAnnotation = null;
		try {
			answerSMSAnnotation = (SetMappingSupportAnnotation) answer.getAnnotations().get("sms");
			nonTerminalSMSAnnotation = (SetMappingSupportAnnotation) nonTerminal.getAnnotations().get("sms");
		} catch (Exception e) {
			// Fails silently if the parameters do not correspond to this hook...
			return combinations;
		}
		
		// Get the sns information
		SetNoGood noGoods = InconsistencyManager.NO_GOODS.getContent(message);
		if (noGoods == null)
			noGoods = new SetNoGood();
		
		// Calculate common part of sms.
		SetMappingSupport answerSMS = answerSMSAnnotation.getContent();
		SetMappingSupport nonTerminalSMS = nonTerminalSMSAnnotation.getContent();
		SetMappingSupport sms = answerSMS.product(nonTerminalSMS);
		
		Iterator<Clause> it = combinations.iterator();
		while (it.hasNext()) {
			Clause combination = it.next();
			
			// Calculate the sms for the resulting combination.
			SetMappingSupportAnnotation combinationSMSAnnotation = (SetMappingSupportAnnotation) combination.getAnnotations().get("sms");
			SetMappingSupport combinationSMS = sms.product(combinationSMSAnnotation.getContent());
			boolean smsEmptyBeforeVerification = combinationSMS.isEmpty();
			
			// Verify no-goods contained in the new sms
			Iterator<MappingSupport> itMs = combinationSMS.iterator();
			for (MappingSupport noGood : noGoods) {
				
				// If the mapping support contains a no-good it will be removed.
				while (itMs.hasNext()) {
					MappingSupport ms = itMs.next();
					
					if (ms.containsAll(noGood))
						itMs.remove();
				}
			}
			
			// If its sms is empty due the no-good verification, remove the new combination
			if (combinationSMS.isEmpty() && !smsEmptyBeforeVerification) {
				it.remove();
				logger.trace("Combination {} removed since all its sms contains the noGood : {}", 
						combination, noGoods);
			}
			
			SetMappingSupportAnnotation finalSMSAnnotation = new SetMappingSupportAnnotation(combinationSMS);
			combination.getAnnotations().put("sms", finalSMSAnnotation);
		}
		
		return combinations;
	}

}
