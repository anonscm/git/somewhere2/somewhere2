/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.inconsistency.commands;

import java.util.List;

import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.InconsistencyManager;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.SetMappingSupport;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

/**
 * A command to show the non goods stocked on this peer
 * @author Andre Fonseca
 */
public class ShowNoGoodsCommand  implements Command {
    /* --- Properties --- */
    /** Manager of the module */
    protected final InconsistencyManager manager;

    public ShowNoGoodsCommand(InconsistencyManager manager) {
        if (manager == null) {
            throw new IllegalArgumentException("manager");
        }

        this.manager = manager;
    }

    /* --- Accessors --- */
    public InconsistencyManager getManager() {
        return manager;
    }

    /**
     * @return the name of the command
     */
    public String getName() {
        return "showNoGoods";
    }

    /**
     * @return the description of the command
     */
    public String getDescription() {
        return "Show the no goods mappings detected on this instance";
    }

    /**
     * Execute the command
     * @param parameters The parameters of the command
     * @return the return status
     *  NOTE : note that the return status contains only text
     *  information
     */
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;

        try {
        	SetMappingSupport noGoods = manager.getNoGoods();
        	
            String str = noGoods.toString();

            res = new GenericReturnStatus(str, true, null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error showing the " +
                    "propositional instance ...", false, null);
        }

        return res;
    }

}
