/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.inconsistency;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import com.google.common.collect.ForwardingSet;
import com.google.common.collect.Iterables;

/**
 * Representation of a set of mapping support
 * 
 * @author Andre Fonseca
 *
 */
public class SetMappingSupport extends ForwardingSet<MappingSupport> implements Serializable {
	private static final long serialVersionUID = 5663759811354034075L;
	
	/* --- Properties --- */
	/** The delegate data structure object */
	protected Set<MappingSupport> delegate;
	
	public SetMappingSupport() {
		this.delegate = new LinkedHashSet<MappingSupport>();
		this.delegate.add(new MappingSupport());
	}
	
	public SetMappingSupport(SetMappingSupport setMappingSupport) {
		this();
		this.clear();
		
		for (MappingSupport mappingSupport : setMappingSupport) {
			this.delegate.add(mappingSupport.copy());
		}
	}

	/* --- Accessors --- */
	@Override
	protected Set<MappingSupport> delegate() {
		return delegate;
	}
	
	/* --- Mutators --- */
	/**
	 * Add a new mapping support ms to a sms while checking for possible subsumptions
	 * mappings subsumed by ms are removed from the sms. If ms is subsumed by some
	 * mapping support of the sms, the latter is unchanged.
	 * @param ms    the mapping support to be added.
	 */
	@Override
	public boolean add(MappingSupport ms) {
		if (ms.isEmpty() || this.justContainEmptyMS()) {
			this.clear();
			return super.add(ms.copy());
		}
		
		Iterator<MappingSupport> it = this.iterator();
		while (it.hasNext()) {
			MappingSupport mappingSupport = it.next();

			if (mappingSupport.subsumes(ms))
				return false;
			
			if (ms.subsumes(mappingSupport))
				it.remove();
		}

		return super.add(ms.copy());
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addAll(Collection<? extends MappingSupport> set) {
		boolean modified = false;
		
		for (MappingSupport mappingSupport : set) {
			modified |= this.add(mappingSupport);
		}
		
		return modified;
	}
	
	/* --- Methods --- */
	/**
	 * Performs the product of the current sms with another sms while removing subsumed mapping supports
	 *
	 * @param setMappingSupport : the set of mapping support to be distributed.
	 * @return the filtered product of the two sms
	 */
	public SetMappingSupport product(SetMappingSupport setMappingSupport) {
		if (setMappingSupport.isEmpty())
			return new SetMappingSupport(this);
		
		if (this.isEmpty())
			return new SetMappingSupport(setMappingSupport);

		SetMappingSupport result = new SetMappingSupport();

		for (MappingSupport ms : this)
			for (MappingSupport ms2 : setMappingSupport)
				result.add(ms.union(ms2));

		return result;
	}

	/**
	 * Performs the union of the current sms with another sms while removing subsumed mapping supports
	 *
	 * @param setMappingSupport : the set of mapping support to be joined.
	 * @return the filtered union of the two sms
	 */
	public SetMappingSupport union(SetMappingSupport setMappingSupport) {
		SetMappingSupport result = new SetMappingSupport(this);

		for (MappingSupport ms : setMappingSupport)
			result.add(ms);

		return result;
	}
	
	/**
	 * @return true if the current sms only contains an empty mapping support.
	 */
	public boolean justContainEmptyMS() {
		if (this.size() != 1)
			return false;
		
		MappingSupport ms = Iterables.getFirst(this.delegate, null);
		return ms.isEmpty();
	}
	
	/**
	 * Manual serialization of the attributes
	 */
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeObject(delegate);
	}
	
	/**
	 * Manual deserialization
	 */
	private void readObject(ObjectInputStream in) throws IOException,
			ClassNotFoundException, SecurityException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException {

		Field fDelegate = SetMappingSupport.class.getDeclaredField("delegate");
		fDelegate.set(this, in.readObject());
	}
}
