/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.inconsistency;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import fr.lri.iasi.libs.plogic.impl.ClauseElementsFactory;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryElementsFactory;
import fr.lri.iasi.somewhere.AbstractModule;
import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.api.communication.model.MessageContentKey;
import fr.lri.iasi.somewhere.api.communication.model.MessageListener;
import fr.lri.iasi.somewhere.plogic.distributed.PLogicManager;
import fr.lri.iasi.somewhere.plogic.distributed.commands.AddClauseCommand;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.annotations.SetMappingSupportAnnotation;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.commands.ShowNoGoodsCommand;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies.AnytimeCombinationWithSMSStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies.InconsistencyAddClauseStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies.ResolveClauseWithSMSStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies.SubsumesClauseWithSMSStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies.TheoryAddFormulaWithSMSStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies.UnionOfClauseWithSMSStrategy;

import fr.lri.iasi.somewhere.plogic.distributed.strategies.ModifyTheoryStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.strategies.NaiveRemoveClauseStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.strategies.PLogicDistributedStrategiesFactory;
import fr.lri.iasi.somewhere.ui.UI;

/**
 * The main class of the inconsistency handling module
 * 
 * @author Andre Fonseca
 */
public class InconsistencyManager extends AbstractModule {
	static Logger logger = LoggerFactory.getLogger(InconsistencyManager.class);
	
	/* --- Constants --- */
    /** The name of this module */
    protected static final String moduleName = "plogic.dist.p2png";
    
    /* --- Properties --- */
    /** Manager of the module */
    protected PLogicManager plogicManager;
    
    /** The no good mappings detected on this peer */
    protected SetNoGood noGoods;
    
    /** Object responsible for update message's SNG */
    protected MessageListener SNGUpdater;
    
    /** Create/Find no goods content type*/
    public static final MessageContentKey<SetNoGood> NO_GOODS = MessageContentKey.create("noGoods");

	public InconsistencyManager(App app) {
		super(app);
        
        setLoadingOrder(2);
        
        this.noGoods = new SetNoGood();
	}

	/* --- Accessors --- */
	/**
     * {@inheritDoc}
     */
	@Override
	public String getModuleName() {
		return moduleName;
	}
	
	public SetNoGood getNoGoods() {
		return noGoods;
	}
	
	public PLogicManager getPLogicManager() {
		return plogicManager;
	}

	/* --- Mutators --- */
	public void setNoGoods(SetNoGood noGoods) {
		this.noGoods = noGoods;
	}
	
	/* --- Methods --- */	
	/**
     * Return the current P2PNG manager instance
     */
    public static InconsistencyManager getModuleInstance(App app) {
        if (app == null) {
            throw new IllegalArgumentException("app");
        }

        Module m = app.getModule(moduleName);

        if (m == null) {
            return null;
        }

        try {
        	InconsistencyManager res = (InconsistencyManager) m;

            return res;
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * Add p2png commands to the UI
     */
    protected void addBaseCommands() {
        UI ui = app.getUI();
        ui.addCommand(new ShowNoGoodsCommand(this));
    }
    
	/**
     * {@inheritDoc}
     */
	@Override
	public void init() {	
		this.plogicManager = PLogicManager.getModuleInstance(app);
		
		if (this.plogicManager == null) {
            throw new UnsupportedOperationException(
                "The plogic.dist module must be loaded");
        }
		
		// Add message listener
		this.SNGUpdater = new SNGUpdater(this);
		
		// Modify properties on clause builder
		addPropertiesToClauseBuilder();
		
		// Modify strategies
		addSelectedStrategies();
		
		// Add Commands to the UI
        addBaseCommands();
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public void quit() {
		// ...
		
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void importConfigXML(Node config) {
		// ...
		
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public Node exportConfigXML() {
		// ...
		return null;
	}
	
	/**
	 * Add default annotations to the clause builder
	 */
	protected void addPropertiesToClauseBuilder() {
		ClauseElementsFactory.getInstance().addAnnotations(new SetMappingSupportAnnotation());
	}
	
	/**
     * Add strategies to be used on the distributed calculus
     */
	protected void addSelectedStrategies() {
		PropositionalClausalTheoryElementsFactory.getInstance().setAddFormulaStrategy(new TheoryAddFormulaWithSMSStrategy());
		
		ClauseElementsFactory.getInstance().setResolveStrategy(new ResolveClauseWithSMSStrategy());
		ClauseElementsFactory.getInstance().setSubsumesStrategy(new SubsumesClauseWithSMSStrategy());
		ClauseElementsFactory.getInstance().setUnionStrategy(new UnionOfClauseWithSMSStrategy());
		
		PLogicDistributedStrategiesFactory.getInstance().setAnswerCombinationStrategy(new AnytimeCombinationWithSMSStrategy());
		
		AddClauseCommand addClauseCommand = (AddClauseCommand) this.app.getUI().getCommand("addClause");
		ModifyTheoryStrategy addStrategy = new InconsistencyAddClauseStrategy(this);
		ModifyTheoryStrategy removeStrategy = new NaiveRemoveClauseStrategy(this.plogicManager);
		
		addClauseCommand.setModifyTheoryStrategy(addStrategy);
		PLogicDistributedStrategiesFactory.getInstance().setAddFormulaToInstanceStrategy(addStrategy);	
		PLogicDistributedStrategiesFactory.getInstance().setRemoveFormulaFromInstanceStrategy(removeStrategy);
	}
	
}
