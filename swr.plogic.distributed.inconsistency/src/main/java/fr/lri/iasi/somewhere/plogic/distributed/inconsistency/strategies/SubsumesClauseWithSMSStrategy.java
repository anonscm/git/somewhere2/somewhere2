/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.inconsistency.strategies;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.strategies.impl.DefaultSubsumesStrategy;

import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.MappingSupport;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.SetMappingSupport;
import fr.lri.iasi.somewhere.plogic.distributed.inconsistency.annotations.SetMappingSupportAnnotation;

/**
 * Handles the subsumption operation with the SMS annotation.
 * 
 * @author Andre Fonseca
 *
 */
public class SubsumesClauseWithSMSStrategy extends DefaultSubsumesStrategy<Clause> {
	static Logger logger = LoggerFactory.getLogger(SubsumesClauseWithSMSStrategy.class);

	@Override
	public boolean performSubsumes(Clause targetClause, Clause argClause) {
		boolean result = super.performSubsumes(targetClause, argClause);
		
		// The result of the subsumes was false... there is no need to check anything...
		if (!result)
			return false;
		
		// ... Otherwise, check the update on the SMS annotations!
		final SetMappingSupportAnnotation targetSMSAnnotation = (SetMappingSupportAnnotation) targetClause.getAnnotations().get("sms");
		final SetMappingSupportAnnotation argSMSAnnotation = (SetMappingSupportAnnotation) argClause.getAnnotations().get("sms");
		
		SetMappingSupport targetSMS = targetSMSAnnotation.getContent();
		SetMappingSupport argSMS = argSMSAnnotation.getContent();
		
		if (targetSMS.justContainEmptyMS())
			return true;
		
		// After we have to verify if every single mapping support in the target clause is contained by the set of mapping supports 
		// of the arg clause. If yes, remove this mapping support.
		SetMappingSupport argSMSCopy = new SetMappingSupport(argSMS);
		Iterator<MappingSupport> it = argSMSCopy.iterator();
		while (it.hasNext()) {
			MappingSupport mappingSupport = it.next();
		
			for (MappingSupport mappingSupportCurrent : targetSMS) {
				if (mappingSupportCurrent.subsumes(mappingSupport)) {
					it.remove();
					break;
				}
			}	
		}	
		
		// If, in the end, the arg clause does not have any mapping support, the arg clause can be subsumed
		if (argSMSCopy.isEmpty())
			return true;
		
		return false;
	}

}
