/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.inconsistency;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.Set;
import java.util.TreeSet;

import com.google.common.collect.ForwardingSet;

import fr.lri.iasi.libs.plogic.Support;

/**
 * Describes a mapping support (set of labels of mappings used to produce a clause).
 * 
 * @author Andre Fonseca
 *
 */
public class MappingSupport extends ForwardingSet<String> implements Support<MappingSupport> {
	private static final long serialVersionUID = -1175821292084562315L;
	
	/* --- Properties --- */
	/** The delegate data structure object */
	protected Set<String> delegate;
	
	public MappingSupport() {
		this.delegate = new TreeSet<String>();
	}
	
	public MappingSupport(MappingSupport mappingSupport) {
		this();
		this.delegate.addAll(mappingSupport);
	}

	/* --- Accessors --- */
	@Override
	protected Set<String> delegate() {
		return delegate;
	}
	
	/* --- Methods --- */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean subsumes(MappingSupport mappingSupport) {
		if (mappingSupport.containsAll(this))
			return true;

		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public MappingSupport union(MappingSupport mappingSupport) {
		MappingSupport union = this.copy();
		union.addAll(mappingSupport);

		return union;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public MappingSupport copy() {
		return new MappingSupport(this);
	}
	
	@Override
	public String toString() {
		StringBuilder res = new StringBuilder();
		res.append("(");
		
		for (String l : this) {
			res.append(l);
			res.append(",");
		}

		if (res.toString().length() > 1)
			res.delete(res.length() - 1, res.length());
		
		res.append(")");

		return res.toString();
	}
	/**
	 * Generates a XML representation string of the object
	 * 
	 * @return XML representation string
	 */
	public String toXml() {
		StringBuilder res = new StringBuilder();
		res.append("<mappingSupport>");

		for (String l : this) {
			res.append(l);
			res.append(" ");
		}

		res.append("</mappingSupport>");

		return res.toString();
	}
	
	/**
	 * Manual serialization of the attributes
	 */
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeObject(delegate);
	}
	
	/**
	 * Manual deserialization
	 */
	private void readObject(ObjectInputStream in) throws IOException,
			ClassNotFoundException, SecurityException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException {

		Field fDelegate = MappingSupport.class.getDeclaredField("delegate");
		fDelegate.set(this, in.readObject());
	}

}
