/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.ui.console.printer;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import fr.lri.iasi.somewhere.ui.console.PrintStrategy;

/**
 * Printing strategy based on the sending of informations through a output stream
 * @author Andre Fonseca
 *
 */
public class RemotePrintStrategy implements PrintStrategy {
	/* --- Properties --- */
	/** List of current remote connections */
	private final List<Socket> connections;
	
	/** The current output streams */
	private final Map<OutputStream, PrintWriter> outputStreams = new ConcurrentHashMap<OutputStream, PrintWriter>();
	
	public RemotePrintStrategy(List<Socket> connections) {
		if (connections == null)
			throw new IllegalArgumentException("connections");
		
		this.connections = connections;
	}

	/* --- Methods --- */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(String information) {
		PrintWriter output;
		try {
			for (Socket connection : connections) {
				output = getOutput(connection.getOutputStream());
				output.println(information);
			}
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * Wraps the output stream into a print writer object.
	 * @param stream
	 * @return
	 */
	public PrintWriter getOutput(OutputStream stream) {
		for (Entry<OutputStream, PrintWriter> output : outputStreams.entrySet()) {
			if (stream.equals(output.getKey()))
				return output.getValue();
		}
		
		PrintWriter output = new PrintWriter(new OutputStreamWriter(stream), true);
		outputStreams.put(stream, output);
		return output;
	}
}
