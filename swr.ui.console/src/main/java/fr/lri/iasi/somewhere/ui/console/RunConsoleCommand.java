/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.ui.console;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


/**
 * A command to quit the console
 */
public class RunConsoleCommand implements Command {
    static Logger logger = LoggerFactory.getLogger(RunConsoleCommand.class);

    /* --- Properties --- */
    /** The main app module */
    protected final App app;
    
    /** The JLine console object */
    protected final Console console;

    public RunConsoleCommand(App app, Console console) {
        super();

        if (app == null) {
            throw new IllegalArgumentException("app");
        }

        this.app = app;

        if (console == null) {
            throw new IllegalArgumentException("console");
        }

        this.console = console;
    }

    /**
     * @return the name of the command
     */
    public String getName() {
        return "console";
    }

    /**
     * @return the description of the command
     */
    public String getDescription() {
        return "Run a console shell.";
    }

    /**
     * Execute the command
     * @param parameters The parameters of the command
     * @return the return status
     *  NOTE : note that the return status contains only text
     *  information
     */
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;

        try {
            console.start();
            res = new GenericReturnStatus("Console successfully started", true,
                    null);
        } catch (Exception e) {
            console.stop();
            logger.error("Abnormal execution of " + getName() + " : " + e);
            res = new GenericReturnStatus("There was an error launching console...",
                    false, null);
        }

        return res;
    }
}
