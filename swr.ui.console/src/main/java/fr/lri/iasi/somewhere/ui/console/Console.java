/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.ui.console;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.ui.BaseUI;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.ReturnStatus;
import fr.lri.iasi.somewhere.ui.console.printer.DefaultPrintStrategy;

import jline.ConsoleReader;
import jline.History;
import jline.SimpleCompletor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;


/**
 * Main Console class
 * @author Andre Fonseca
 */
public class Console extends BaseUI implements Runnable {
    static Logger logger = LoggerFactory.getLogger(Console.class);

    /* --- Statics --- */
    private static final String UNKNOWN_COMMAND = "Unknown command [%1$s]%n";
    private static final String COMMAND_ERROR = "Command error [%1$s]: [%2$s]%n";

    /* --- Properties --- */
   	/** Indicates when the console is finished */
    private boolean endThreadFlag = true;
    
    /** The JLine console object */
    private final ConsoleFacade console;

    /** The history of previous user-inputs.*/
    private final History consoleHistory = new History();

    /** Jlist Console Main Object */
    private ConsoleReader consoleReader;
    
    /** Print strategy to be used */
    private PrintStrategy printStrategy;
    
    public Console(App app) {
        super(app);

        console = new ConsoleFacade();
        printStrategy = new DefaultPrintStrategy(console);

        addBaseCommands();
        initConsole();
    }

	/* --- Accessors --- */
    public PrintStrategy getPrintStrategy() {
		return printStrategy;
	}

    /* --- Mutators --- */
	public void setPrintStrategy(PrintStrategy printStrategy) {
		if (printStrategy != null)
			this.printStrategy = printStrategy;
	}

	/* --- Methods --- */
	/**
	 * Init Jline console with some default options
	 */
    protected void initConsole() {
        try {
            consoleReader = new ConsoleReader();

            if ((consoleReader == null) ||
                    !consoleReader.getTerminal().isSupported()) {
                throw new RuntimeException();
            }

            // Set the history of the console
            consoleReader.setHistory(consoleHistory);

            // Disable the horrible bell
            consoleReader.setBellEnabled(false);
        } catch (Exception e) {
            logger.error("Unable to correctly initialize Console : " + e);
            throw new RuntimeException("Invalid Console Object", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        while (!endThreadFlag) {
            cmdLine();
        }
    }

    /**
     * Start the console
     */
    public void start() {
        Thread thread = new Thread(this);
        endThreadFlag = false;
        thread.start();
    }

    /**
     * Stop the console
     */
    public void stop() {
        logger.info("Stopping Console...");
        endThreadFlag = true;
    }

    /**
     * @return the prompt for the next command
     */
    protected String getNextPrompt() {
        String res = "";
        Calendar rightNow = Calendar.getInstance();

        res += (rightNow.get(Calendar.HOUR_OF_DAY) + "H ");
        res += (rightNow.get(Calendar.MINUTE) + "M ");
        res += (rightNow.get(Calendar.SECOND) + "S ");
        res += "$ ";

        return res;
    }

    /**
     * Update the history of the console
     */
    @SuppressWarnings("unchecked")
    protected void updateHistory() {
    	List<String> completor = new ArrayList<String>(getCommands().keySet());

        // add old commands as tab completion history
        List<String> oldCommandsAsList = new ArrayList<String>(consoleHistory.getHistoryList());

        // add predefined commands if given
        if ((completor != null) && !completor.isEmpty()) {
            oldCommandsAsList.addAll(completor);
        }

        String[] oldCommands = new String[oldCommandsAsList.size()];
        oldCommandsAsList.toArray(oldCommands);
        consoleReader.addCompletor(new SimpleCompletor(oldCommands));
    }

    /**
     * Try to execute command, if it exists. Throw exception otherwise.
     * @param commandName Name of the command
     * @param param Parameters of the command
     */
    protected void executeCommand(String commandName, List<Object> param) {
        final Command cmd = getCommand(commandName);

        try {
            ReturnStatus ret = cmd.execute(param);
            printStrategy.print(ret.getTextualInfos() + "\n");
        } catch (IllegalArgumentException e) {
            console.printf(UNKNOWN_COMMAND, commandName);
        } catch (Exception e) {
            console.printf(COMMAND_ERROR, commandName, e.getMessage());
        }
    }

    /**
     * Parse and execute commands given by the user
     *  on the console
     */
    protected void cmdLine() {
        String commandLine;

        try {
            updateHistory();
            commandLine = consoleReader.readLine(getNextPrompt());
        } catch (IOException e) {
            logger.error("Unable to correctly use Console");
            throw new RuntimeException("Problems with Console Object", e);
        }

        if (commandLine == null) {
            console.printf("\n");

            Command quitCommand = this.getCommand("quit");
            quitCommand.execute(new ArrayList<Object>());
        } else {
	        commandLine = commandLine.trim();
	
	        Scanner scanner = new Scanner(commandLine);
	
	        // Retreive command infos and execute it, if it exists
	        if (scanner.hasNext()) {
	            final String commandName = scanner.next();
	
	            // Retreive parameters
	            final List<Object> param = new ArrayList<Object>();
	
	            while (scanner.hasNext())
	                param.add(scanner.next());
	
	            // Try to execute command
	            executeCommand(commandName, param);
	        }
	
	        scanner.close();
        }
    }
    
    /**
     * Control the printing of an information on the UI
     */
	public <T> void print(T information) {
		printStrategy.print(information.toString());
	}
	
	/**
     * Set the print strategy back to default (printing on console)
     */
	public void restoreDefaultPrintStrategy() {
		this.printStrategy = new DefaultPrintStrategy(console);
	}

    /**
     * Add base commands to the list of commands
     */
    @Override
    protected void addBaseCommands() {
        super.addBaseCommands();
        addCommand(new RunConsoleCommand(this.app, this));
        addCommand(new QuitConsoleCommand(this.app, this));
    }

    /**
     * This class is used to interface java.io.Console,
     * in case the standard console is not accessible
     * from system.console()
     */
    public final static class ConsoleFacade {
        private java.io.Console console;

        private ConsoleFacade() {
            console = System.console();
        }

        public java.io.Console printf(String format, Object... args) {
            if (console != null) {
                return console.printf(format, args);
            } else {
                return null;
            }
        }
    }
}
