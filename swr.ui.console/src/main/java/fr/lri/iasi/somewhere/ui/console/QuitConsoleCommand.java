/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.ui.console;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.ui.QuitCommand;


/**
 * A command to quit the console
 * 
 * @author Andre Fonseca
 */
public class QuitConsoleCommand extends QuitCommand {
    /* --- Properties --- */
	/** The main app module */
    protected final App app;
    
    /** The JLine console object */
    protected final Console console;

    public QuitConsoleCommand(App app, Console console) {
        super();

        if (app == null) {
            throw new IllegalArgumentException("app");
        }

        this.app = app;

        if (console == null) {
            throw new IllegalArgumentException("console");
        }

        this.console = console;
    }

    /**
     * A routine to send quit signals to other
     *  part of the program
     * @return true if success
     */
    protected boolean quit() {
        try {
            console.stop();
            app.quitApp();

            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
