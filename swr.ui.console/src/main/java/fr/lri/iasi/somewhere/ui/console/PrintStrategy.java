/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.ui.console;

/**
 * Specifies the printing strategy to be used by the module.
 * @author Andre Fonseca
 *
 */
public interface PrintStrategy {
	
	/**
     * Control the printing of an information on the UI
     */
	void print(String information);

}
