/**
 * Somewhere2 ( http://www.somewhere2.org/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.ui.console.printer;

import fr.lri.iasi.somewhere.ui.console.PrintStrategy;
import fr.lri.iasi.somewhere.ui.console.Console.ConsoleFacade;

/**
 * The default print strategy is printing thought the console.
 * @author Andre Fonseca
 *
 */
public class DefaultPrintStrategy implements PrintStrategy {
	/* --- Properties --- */
	/** The JLine console object */
	private final ConsoleFacade console;
	
	public DefaultPrintStrategy(ConsoleFacade console) {
		if (console == null)
			throw new IllegalArgumentException("console");
		
		this.console = console;
	}

	/* --- Methods --- */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(String information) {
		console.printf(information);
	}

}
