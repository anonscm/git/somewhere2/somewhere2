/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.cli;


/**
 * The main class which just calls a Cli object to do all the stuff
 * @author Andre Fonseca
 */
public final class Main {
	
	private Main() {
	}

    public static void main(final String[] args) {
    	Cli cli = new Cli();

        // Load modules in the build
        cli.loadModules();
        
        // Parse the command line arguments
        CmdLineParser clp = new CmdLineParser(args, cli.getUI().getCommands());
        clp.parse();
        
        // Load config files, if any
        clp.loadConfiguration();

        // Initialize selected modules and run chosen commands, if any
        cli.getModulesManager().initAll();
        clp.executeCommands();
    }
}
