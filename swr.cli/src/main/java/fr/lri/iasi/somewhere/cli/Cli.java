/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.cli;

import fr.lri.iasi.somewhere.baseApp.AbstractApp;
import fr.lri.iasi.somewhere.cli.commands.ListenRemoteCmdsCommand;
import fr.lri.iasi.somewhere.cli.commands.RedirectOutputCommand;
import fr.lri.iasi.somewhere.cli.commands.StopListeningRemoteCmdsCommand;
import fr.lri.iasi.somewhere.ui.console.Console;



/**
 * The class which deals with the actions the user wants to execute,
 * and execute them according to the command lines parameters
 * @author Andre Fonseca
 */
public class Cli extends AbstractApp {

    public Cli() {
        this.ui = new Console(this);
        this.addBaseCommands();
    }
    
    @Override
    protected void addBaseCommands() {
    	super.addBaseCommands();
    	ui.addCommand(new ListenRemoteCmdsCommand(this));
    	ui.addCommand(new StopListeningRemoteCmdsCommand(this));
    	ui.addCommand(new RedirectOutputCommand(this));
    }
}
