/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.cli.commands;

import java.util.List;

import fr.lri.iasi.somewhere.cli.Cli;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;
import fr.lri.iasi.somewhere.ui.console.Console;
import fr.lri.iasi.somewhere.ui.console.PrintStrategy;
import fr.lri.iasi.somewhere.ui.console.printer.RemotePrintStrategy;

/**
 * Can redirect the output either to the console, a file or a remote stream.
 * @author Andre Fonseca
 *
 */
public class RedirectOutputCommand implements Command {
	
	/* --- Properties --- */
    /** Base App object */
    private final Cli cli;
    
    public RedirectOutputCommand(Cli cli) {
        if (cli == null) {
            throw new IllegalArgumentException("app");
        }

        this.cli = cli;
    }

    /* --- Accessors --- */
    /**
     * {@inheritDoc}
     */
	public String getName() {
		return "redirectOutput";
	}

	/**
     * {@inheritDoc}
     */
	public String getDescription() {
		return "Redirect the output of the next commands to the console (default), a file or a remote output stream.";
	}

	/* --- Methods --- */
	/**
     * Redirect commands output to the indicated output stream.
     * @param parameters : list containing the name of the selected output stream (console|[file filepath]|remote)
     * @return the return status
     */
	public ReturnStatus execute(List<Object> parameters) {
		ReturnStatus res = null;
		Console console = null;

		try {
			String resMsg = null;
			PrintStrategy strategy = null;
			String redirect = "console";

			console = (Console) cli.getUI();
			
			// Verify if the parameters are good (no parameters in this case...)
	        if (parameters.isEmpty() && parameters.size() > 1) {
	            res = new GenericReturnStatus("Syntax Error! \n Usage :" +
	                    "\t" + getName() + " [console|file|remote]", false, null);
	
	            return res;
	        }
	        		
	        redirect = parameters.get(0).toString();

	        console.restoreDefaultPrintStrategy();

	        switch (Output.getOutput(redirect)) {
	        	case FILE:
	        		// TODO
	        		resMsg = "Next commands output will be redirected to the file output stream.";
	        		
	        		break;
	        	case REMOTE:
	        		resMsg = "Next commands output will be redirected to the remote output stream.";
	    			
	        		ListenRemoteCmdsCommand command = (ListenRemoteCmdsCommand) cli.getUI().getCommand("listenRemoteCommands");
	        		strategy = new RemotePrintStrategy(command.getOpenedConnections());
	    			
	        		break;
	        	default:
	        		resMsg = "Next commands output will be redirected to the standard output stream.";
	        }
	        
	        console.setPrintStrategy(strategy);
	        
	        return new GenericReturnStatus(resMsg, 
	        		true, null);
	        
		} catch (Exception e) {
			console.restoreDefaultPrintStrategy();
			
			return new GenericReturnStatus("There was an error redirecting command outputs...",
                    false, null);
		}
	}
	
	/**
	 * Enumeration describing the possible outputs to redirect messages. 
	 * 
	 * @author Andre Fonseca
	 *
	 */
	public enum Output {
		CONSOLE,
		FILE,
		REMOTE;
		
		public static final Output getOutput(String name) {
			
			if (name.equalsIgnoreCase("console"))
				return CONSOLE;
			else if (name.equalsIgnoreCase("file"))
				return FILE;
			else if (name.equalsIgnoreCase("remote"))
				return REMOTE;
			else
				return CONSOLE;
		}
		
	}

}
