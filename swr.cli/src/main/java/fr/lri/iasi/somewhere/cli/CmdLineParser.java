/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.cli;

import fr.lri.iasi.somewhere.baseApp.BaseConstants;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * Just parse the command line parameters, and save useful infos
 * to accessible properties
 * @author Andre Fonseca
 */
public class CmdLineParser {
    static Logger logger = LoggerFactory.getLogger(CmdLineParser.class);

    /* --- Properties --- */

    /** Command line Arguments */
    private String[] args;

    /** Command line parser */
    private final OptionParser parser = new OptionParser();
    
    /** The options set that will contains all used commands and its arguments */
    private OptionSet options;
    
    /** Commands from the loaded modules */
    private final Map<String, Command> commands;

    public CmdLineParser(String[] args, final Map<String, Command> commands) {
    	if (commands == null) {
    		throw new IllegalArgumentException("commands");
    	}
    	
        if (args == null) {
            this.args = new String[0];
        } else {
        	this.args = Arrays.copyOf(args, args.length);
        }

        this.commands = commands;
    }

    /**
     * Parse the command line parameters
     */
    final void parse() {
        for (final Map.Entry<String, Command> e : commands.entrySet()) {
            String name = e.getKey();
            Command command = e.getValue();

            parser.accepts(name)
            	.withOptionalArg()
            	.ofType(String.class)
                .describedAs(command.getDescription()).defaultsTo("");
        }

        options = parser.parse(args);

        if (options.nonOptionArguments().size() != 0)
            usage();
    }
    
    /**
     * Execute configuration commands
     */
    final void loadConfiguration() {
    	List<Object> param = new ArrayList<Object>();
    	Command command = commands.get("importConfigurationFile");
    	String commandName = command.getName();
    	
        // If there was no config file loaded, load the
        // default config file
		if (options.has(commandName)) {
		    logger.debug("Importing configuration file... ");
		
		    for (String foo : ((String) options.valueOf(commandName)).split(" ")) {
		    	if ((!foo.isEmpty()) && (!foo.equals(" ")))
                    param.add(foo);
		    }
		} else {
			param.add(BaseConstants.DEFAULT_CONFIG_PATH);
		}

	    ReturnStatus ret = command.execute(param);
	
	    if (ret.isSuccessful()) {
	        System.out.println(ret.getTextualInfos());
	    } else {
	        System.err.println("There was an error importing the selected configuration file.");
	    }
    }
    
    /**
     * Execute other commands passed on command line
     */
    final void executeCommands() {
    	for (final Map.Entry<String, Command> e : commands.entrySet()) {
            String name = e.getKey();
            Command command = e.getValue();
            
            // The command 'importConfigurationFile' must be loaded in a different way. Also, 'quit' and 'help' cannot be
            // used as initialization parameters.
            if (name.equals("importConfigurationFile") ||
            	name.equals("quit") || 
            	name.equals("help") ||
            	name.equals("suspend"))
            	continue;

            if (options.has(name)) {
                logger.debug("Options : " + name + " : " +
                    options.valueOf(name));

                List<Object> param = new ArrayList<Object>();

                for (String foo : ((String) options.valueOf(name)).split(" ")) {
                    if ((!foo.isEmpty()) && (!foo.equals(" ")))
                        param.add(foo);
                }

                ReturnStatus ret = command.execute(param);

                if (ret.isSuccessful()) {
                    System.out.println(ret.getTextualInfos());
                } else {
                    System.err.println("There was an error executing " 
                    		+ "option " + name + " : " + ret.getTextualInfos());
                }
            }
        }
    }

    /**
     * Print the usage
     */
    private void usage() {
        try {
            parser.printHelpOn(System.err);
        } catch (Exception e) {
            System.err.println("java -jar somewhere.jar [OPTIONS]");
        }

        System.exit(BaseConstants.GOOD_RESULT);
    }
}
