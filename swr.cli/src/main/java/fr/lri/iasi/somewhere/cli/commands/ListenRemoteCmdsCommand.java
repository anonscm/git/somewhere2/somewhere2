/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.cli.commands;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.somewhere.cli.Cli;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

/**
 * A command to listen and parse commands passed from outside the application execution
 * @author Andre Fonseca
 */
public class ListenRemoteCmdsCommand implements Command {
	static Logger logger = LoggerFactory.getLogger(ListenRemoteCmdsCommand.class);
	
	/* --- Properties --- */
    /** Base App object */
    private final Cli cli;
    
    /** The server socket to receive the messages */
    private ServerSocket serverSocket;
    
    /** All connections opened in a given moment */
    private List<Socket> openedConnections = new CopyOnWriteArrayList<Socket>();
    
    /** The concurrent ExecutorService that will create the server socket used */
    private ExecutorService serverExecutorService;
    
    /** The concurrent ExecutorService that will handle client messages */
    private ExecutorService messageExecutorService;

    public ListenRemoteCmdsCommand(Cli cli) {
        if (cli == null) {
            throw new IllegalArgumentException("app");
        }

        this.cli = cli;
    }
    
    /* --- Accessors --- */
    /**
     * {@inheritDoc}
     */
	public String getName() {
		return "listenRemoteCommands";
	}

	/**
     * {@inheritDoc}
     */
	public String getDescription() {
		return "Listen remote commands";
	}
	
	
	/**
     * @return list of opened connections
     */
	public List<Socket> getOpenedConnections() {
		return Collections.unmodifiableList(openedConnections);
	}
	
	/**
     * Stop the server socket
     */
	public void stopListening() {
		serverExecutorService.shutdown();
		messageExecutorService.shutdown();
		
		// Closes server socket
		try {
			serverSocket.close();
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/* --- Methods --- */
	/**
     * Opens socket to listen external messages.
     * @param parameters : list containing the port number 
     * @return the return status
     */
	public ReturnStatus execute(List<Object> parameters) {
		ReturnStatus res = null;
		
		try {
			int port = 0;
			
			if (this.serverExecutorService != null)
				stopListening();
			
			this.serverExecutorService = Executors.newSingleThreadExecutor();
	        this.messageExecutorService = Executors.newSingleThreadExecutor();
			
			// Verify if the parameters are good (no parameters in this case...)
	        if (parameters != null && parameters.size() > 1) {
	            res = new GenericReturnStatus("Syntax Error! \n Usage :" +
	                    "\t" + getName() + " [PORT_NUMBER]", false, null);
	
	            return res;
	        } else if (parameters != null && parameters.size() == 1) {
	        	port = Integer.valueOf(parameters.get(0).toString());
	        }
	        
	        // Creates server socket and wait messages
			this.serverExecutorService.execute(new CmdServer(port));

			res = new GenericReturnStatus(
	        		"Listening remote commands on port " + serverSocket.getLocalPort() + " ...", 
	        		true,
	                null);
		} catch (Exception e) {
			
			res = new GenericReturnStatus("There was an error listening to remote commands ...",
                    false, null);
		}
		
		return res;
	}
	
	/**
     * Class used to create the server socket a message
     */
    private final class CmdServer implements Runnable {

    	private CmdServer(final int port) {
    		if (port == 0) {
    			while (serverSocket == null) {
	    			try {
		    			serverSocket = new ServerSocket(0);
					} catch (IOException e) {
						// Fails silently, try another port
					}
    			}
    		} else {
	    		try {
	    			serverSocket = new ServerSocket(port);
				} catch (IOException e) {
					logger.error("Unable to open the server socket via port : " + port);
					throw new RuntimeException(e.getMessage(), e);
				}
    		}
        }

		@Override
		public void run() {
			try {
				// Wait for client messages
				while (!serverExecutorService.isShutdown()) {
					Socket socket = serverSocket.accept();
					messageExecutorService.execute(new CommandReceiver(socket));
				}
			} catch (RejectedExecutionException e) {
				logger.warn("Ignoring new remote command received.");
			} catch (SocketException e) {
				logger.error("Unable to handle remote command.");
				throw new RuntimeException(e.getMessage(), e);
			} catch (IOException e) {
				logger.error("Unable to handle remote command.");
				throw new RuntimeException(e.getMessage(), e);
			}
		}  	
    }
	
	/**
     * Class used to create the server socket a message
     */
    private final class CommandReceiver implements Runnable {
    	private Socket socket;

    	private CommandReceiver(final Socket socket) {
    		if (socket == null)
    			throw new IllegalArgumentException("socket");
    		
    		this.socket = socket;
    		openedConnections.add(socket);
        }
    	
    	/**
         * Try to execute command, if it exists. Throw exception otherwise.
         * @param commandName Name of the command
         * @param param Parameters of the command
         */
        private String executeCommand(String commandName, List<Object> param) {
            final Command cmd = cli.getUI().getCommand(commandName);

            try {
                ReturnStatus ret = cmd.execute(param);
                return ret.getTextualInfos();
            } catch (IllegalArgumentException e) {
            	return "Unknown command " + commandName;
            } catch (Exception e) {
            	return "Command error " + commandName + ": " + e.getMessage();
            }
        }

		@Override
		public void run() {
			
			Scanner scanner = null;
			
			try {
				InputStream input = socket.getInputStream();
				String commandStr = new Scanner(input).useDelimiter("\\A").next();
				String commandRes = null;

				// Parse the command line arguments
		        scanner = new Scanner(commandStr.trim());
		
		        // Retreive command infos and execute it, if it exists
		        if (scanner.hasNext()) {
		            final String commandName = scanner.next();
		
		            // Retreive parameters
		            final List<Object> param = new ArrayList<Object>();
		
		            while (scanner.hasNext())
		                param.add(scanner.next());
		
		            // Try to execute command
		            logger.info("Executing new remote command: " + commandName + " with parameters :" + param.toString());
		            commandRes = executeCommand(commandName, param);
		        }
		        
		        cli.getUI().print(commandRes);

			} catch (IOException e) {
				logger.error("Unable to handle remote command.");
				throw new RuntimeException(e.getMessage(), e);
			} finally {

				try {
					openedConnections.remove(socket);
					socket.close();
					
					if (scanner != null)
						scanner.close();
					
				} catch (IOException e) {
					throw new RuntimeException(e.getMessage(), e);
				}
			}
		} 	
    }

}
