/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.cli.commands;

import java.util.List;

import fr.lri.iasi.somewhere.cli.Cli;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

/**
 * A command to cancel the "listen remote commands" action.
 * @author Andre Fonseca
 */
public class StopListeningRemoteCmdsCommand implements Command {
	/* --- Properties --- */
    /** Base App object */
    private final Cli cli;

    public StopListeningRemoteCmdsCommand(Cli cli) {
        if (cli == null) {
            throw new IllegalArgumentException("app");
        }

        this.cli = cli;
    }
    
    /* --- Accessors --- */ 
    /**
     * {@inheritDoc}
     */
	public String getName() {
		return "stopListeningRemoteCommands";
	}

	/**
     * {@inheritDoc}
     */
	public String getDescription() {
		return "Stop Listening to remote commands";
	}

	/* --- Methods --- */ 
	/**
     * Stops listening remote commands.
     * @param parameters : empty list.
     * @return the return status
     */
	public ReturnStatus execute(List<Object> parameters) {
		ReturnStatus res = null;
		
		try {	
			// Verify if the parameters are good (no parameters in this case...)
	        if (parameters != null && parameters.size() != 0) {
	            res = new GenericReturnStatus("Syntax Error! \n Usage :" +
	                    "\t" + getName() , false, null);
	
	            return res;
	        }
	        
	        // Stops to listen the remote commands
	        ListenRemoteCmdsCommand command = (ListenRemoteCmdsCommand) cli.getUI().getCommand("receiveRemoteCommand");
	        command.stopListening();

			res = new GenericReturnStatus(
	        		"Stop listening to remote commands...", 
	        		true,
	                null);
		} catch (Exception e) {
			res = new GenericReturnStatus("There was an error listening to remote commands ...",
                    false, null);
		}
		
		return res;
	}

}
