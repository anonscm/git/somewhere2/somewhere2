/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.iasi.lri.somewhere.jgroups;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.jgroups.JGroupsManager;
import fr.lri.iasi.somewhere.ui.Command;


public class JGroupsManagerIT {
	
	private static JGroupsManager jgroups_first;
	private static JGroupsManager jgroups_second;
	
	@BeforeClass
    public static void initializeFirst() throws InterruptedException {
		initializeSecond();
		
		App app_first = new FakeJGroupsApp();
		CommunicationManager communicationManager = new CommunicationManager(app_first);
		communicationManager.getConfiguration().setPeerName("first");
		communicationManager.getConfiguration().setPeerGroupName("unit_test");
		app_first.getModules().add(communicationManager);
		
		jgroups_first = new JGroupsManager(app_first);
		communicationManager.setCommunicationModule(jgroups_first);
		app_first.getModules().add(jgroups_first);

		jgroups_first.init();
    }
	
    public static void initializeSecond() throws InterruptedException {
    	App app_second = new FakeJGroupsApp();
		CommunicationManager communicationManager = new CommunicationManager(app_second);
		communicationManager.getConfiguration().setPeerName("second");
		communicationManager.getConfiguration().setPeerGroupName("unit_test");
		app_second.getModules().add(communicationManager);
		
		jgroups_second = new JGroupsManager(app_second);
		communicationManager.setCommunicationModule(jgroups_second);
		app_second.getModules().add(jgroups_second);

		jgroups_second.init();
    }
	
	@Test
	public void testAddPeer() {		
		// Adding peer
		Peer peer = jgroups_first.getPeerGroup().retrievePeer("second");
		assertTrue(peer.isConnected());
	}
	
	@Test
	public void sendMessageCorrectly() {
		// Adding peer, if it is not already added
		Peer peer = jgroups_first.getPeerGroup().retrievePeer("second");
		
		// Sending message
		Command sendTextMessage = jgroups_first.getApp().getUI().getCommand("sendTextMessage");
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(peer.getName());
		parameters.add("Hey!");
		
		assertTrue(sendTextMessage.execute(parameters).isSuccessful());
	}
	
	@Test
	public void notifyNotAcessible() throws InterruptedException {
		Peer peer = jgroups_first.getPeerGroup().retrievePeer("second");
		Thread.sleep(100);

		assertTrue(peer.isConnected());
	}
	
	@Test
	public void notifyAcessible() throws InterruptedException {
		Peer peer = jgroups_first.getPeerGroup().retrievePeer("second");
		Thread.sleep(100);

		assertTrue(peer.isConnected());
	}
	
	@Test
	public void testRemovePeer() {		
		// Adding peer, if it is not already added
		Peer peer = jgroups_first.getPeerGroup().retrievePeer("second");
		
		// Removing peer
		jgroups_first.getCommunicationManager().removePeer(peer);	
		assertFalse(peer.isConnected());
	}
}
