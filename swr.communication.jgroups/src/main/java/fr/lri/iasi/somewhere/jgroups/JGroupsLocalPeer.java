/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jgroups;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;

import org.jgroups.JChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The local peer - JGroup version
 * @author Andre Fonseca
 */
public class JGroupsLocalPeer extends JGroupsPeer {
    static Logger logger = LoggerFactory.getLogger(JGroupsLocalPeer.class);
    private static final long serialVersionUID = 345838945847L;

    public JGroupsLocalPeer(final String name, final JGroupsPeerGroup peerGroup,
        final JChannel channel) {
        super(name, peerGroup, channel);
        
        this.addPeerListener(peerGroup);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initConnection() throws CommunicationException {   
    	try {
    		String peerGroupName = this.peerGroup.getName();
    		
    		this.channel.setName(this.name);
    		this.channel.connect(peerGroupName);
        } catch (Exception e) {
            disconnect();
            throw new CommunicationException(e.getMessage());
        }   	
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initDisconnection() {
        // Leave the group gracefully
    	logger.info("Shutting down JGroups local peer... ");
        this.channel.close();
    }

}
