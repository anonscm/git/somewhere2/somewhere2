/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jgroups;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageReceiver;

import org.jgroups.Address;
import org.jgroups.Receiver;
import org.jgroups.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Used to deal with messages reception from the outside
 * 
 * @author Andre Fonseca
 */
public class MessageReceiver extends AbstractMessageReceiver implements Receiver {
    static Logger logger = LoggerFactory.getLogger(MessageReceiver.class);
    static Logger errorLogger = LoggerFactory.getLogger("errorLogger");

    /* --- Properties --- */

    /** The PeerGroup this peer is in */
    protected final JGroupsPeerGroup peerGroup;

    public MessageReceiver(final JGroupsPeerGroup peerGroup) {
    	super(peerGroup.getModuleManager()
                .getCommunicationManager());

        this.peerGroup = peerGroup;
    }

    /* --- Accessors --- */
    public JGroupsPeerGroup getPeerGroup() {
        return peerGroup;
    }

    /**
     * Called when a new message arrive for this peer.
     * @param message signal
     */
    @Override
	public void receive(org.jgroups.Message msgSignal) {
		logger.debug("Received message from " + msgSignal.getSrc());	
		
        try {	
        	Message message = JGroupsMessage.createJGroupsMessage(peerGroup, msgSignal);
			processReceiving(message);
		} catch (final Exception e) {
        	errorLogger.error("Exception occured while dealing with JGroups signal : ", e);
        }
		
	}
    
    /**
     * {@inheritDoc}
     */
    @Override
	public void processReceiving(Message message) throws CommunicationException {
    	communicationManager.receiveMessage(message);
	}


    /**
     * {@inheritDoc}
     */
	@Override
	public void getState(OutputStream output) throws Exception {
		// ...
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void setState(InputStream input) throws Exception {
		// ...
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void viewAccepted(View new_view) {
		Map<String, Address> addressMap = new HashMap<String, Address>();
		for (Address address : new_view) {
			addressMap.put(address.toString(), address);
		}
		
		this.peerGroup.changeAddressMap(addressMap);
		
		// Connects or disconnect peers depending on the new view
		for (Entry<String, JGroupsPeer> entry : peerGroup.getPeers().entrySet()) {
			Peer peer = entry.getValue();
			
			if (!addressMap.containsKey(peer.getName())) {

				if (!peer.equals(peerGroup.getLocalPeer()) && peer.isConnected())
					peer.disconnect();
			} else {
				
				if (!peer.equals(peerGroup.getLocalPeer()) && !peer.isConnected())
					peer.connect();
			}
		}
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void suspect(Address suspected_mbr) {
		// ...
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void block() {
		// ...
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void unblock() {
		// ...
	}

}
