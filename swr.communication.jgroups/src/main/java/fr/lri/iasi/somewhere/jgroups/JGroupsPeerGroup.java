/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jgroups;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import fr.lri.iasi.somewhere.api.communication.configuration.CommunicationConfiguration;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.api.communication.model.PeerGroup;

import org.jgroups.Address;
import org.jgroups.JChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents a peer group, containing:
 *        - a local peer
 *        - its neighbors
 */
public class JGroupsPeerGroup extends PeerGroup<JGroupsManager, JGroupsPeer, JGroupsLocalPeer> {
    static Logger logger = LoggerFactory.getLogger(JGroupsPeerGroup.class);
    
    /* --- Constants --- */
    public static final short HEADER_MAGIC_NUMER = 3095;

    /* --- Properties --- */
    /** JGroups main channel */
    private JChannel channel;

    /** The message receiver */
	private MessageReceiver messageReceiver;
	
	/** Maps peer names into jgroup adresses */
	private volatile Map<String, Address> addressMap = new HashMap<String, Address>();

    public JGroupsPeerGroup(final JGroupsManager moduleManager) {
        super(moduleManager);

        // Register itself as a peer Listener
        this.moduleManager.getCommunicationManager().addPeerListener(this);

        // Create handlers for message sending and reception
        this.moduleManager.getCommunicationManager().setMessageSender(new MessageSender(this));
        this.messageReceiver = new MessageReceiver(this);
        this.moduleManager.getCommunicationManager().setMessageReceiver(this.messageReceiver);
    }

    /* --- Accessors --- */
    public JChannel getChannel() {
        return channel;
    }
    
    public Map<String, Address> getAddressMap() {
        return addressMap;
    }

    public JGroupsManager getModuleManager() {
        return moduleManager;
    }
    
    /* --- Mutators --- */
    synchronized void changeAddressMap(Map<String, Address> addressMap) {
    	this.addressMap = addressMap;
    }
    
    /* --- Methods --- */
    /**
     * Uninitialize our stuffs about the peer group
     */
    @Override
    public void uninit() {
    	this.localPeer.disconnect();
    }

    /**
     * Initialize the peerGroup
     */
    @Override
    public void init() {
        logger.info("Starting JGroups framework...");
        
		try {
			// Get communication manager config
			CommunicationConfiguration communicationConfig = this.moduleManager.getCommunicationManager().getConfiguration();
			
			// Get Jgroups config file
			URL jgroupsURL = getClass().getClassLoader().getResource("META-INF/jgroups-udp.xml");
	        
	        // Get the group name from the configuration
	        this.name = communicationConfig.getPeerGroupName();
	        
	        // Initialize the JGroups Channel
	        this.channel = new JChannel(jgroupsURL);
	        this.channel.setReceiver(this.messageReceiver);
	        this.channel.setDiscardOwnMessages(false);
	        
	        // Create the local peer
	        this.localPeer = new JGroupsLocalPeer(communicationConfig.getPeerName(), this, this.channel);
	        
	        // Connect the local peer
	        this.localPeer.connect();
		} catch (Exception e) {
			logger.error("There were problems starting JGroups framework : {}", e.getMessage());
		}
    }

    /**
     * {@inheritDoc}
     */
    public void peerIsConnecting(Peer peer) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void peerIsLeaving(Peer peer) {
    }

}
