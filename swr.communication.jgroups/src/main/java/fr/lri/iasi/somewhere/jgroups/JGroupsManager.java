/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jgroups;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.api.communication.AbstractCommunicationModule;
import fr.lri.iasi.somewhere.api.communication.model.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.slf4j.bridge.SLF4JBridgeHandler;

import org.w3c.dom.Node;


/**
 * The main class of the jgroups module
 * @author Andre Fonseca
 */
public class JGroupsManager extends AbstractCommunicationModule<JGroupsPeerGroup, JGroupsPeer> {
    static Logger logger = LoggerFactory.getLogger(JGroupsManager.class);

    /* --- Constants --- */
    /** The name of this module */
    protected static final String moduleName = "jgroups";

    /* --- Properties --- */
    

    public JGroupsManager(final App app) {
        super(app);

        // Base init stuffs
        initJGroupsLog();

        // Set this module as the concrete communication module
        communicationManager.setCommunicationModule(this);

        // Create a new peergroup
        peerGroup = new JGroupsPeerGroup(this);

        // Add Commands to the UI
        addBaseCommands();
    }

    /* --- Accessors --- */
    public JGroupsPeerGroup getPeerGroup() {
        return peerGroup;
    }

    /**
     * Set JGroups Logging parameters
     */
    protected void initJGroupsLog() {
        SLF4JBridgeHandler.install();
        
        java.util.logging.Logger ltmp = java.util.logging.Logger.getLogger("JGroupsLogger");

        ltmp.setLevel(java.util.logging.Level.OFF);
    }

    /**
     * @return the current JGroups module instance
     */
    public static JGroupsManager getModuleInstance(final App app) {
        if (app == null) {
            throw new IllegalArgumentException("app");
        }

        final Module m = app.getModule(moduleName);

        if (m == null) {
            return null;
        }

        try {
            final JGroupsManager res = (JGroupsManager) m;

            return res;
        } catch (final Exception e) {
            return null;
        }
    }

    /**
     * Add shoal commands to the UI
     */
    protected void addBaseCommands() {
        // ... No commands by this moment...
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getModuleName() {
        return moduleName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void quit() {
        // Uninit the peer group
        peerGroup.uninit();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void init() {
        try {    	
            // Create the peer group
            peerGroup.init();
        } catch (final Exception e) {
            quit();
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * NOTE : The configuration file depends on JGroups directly
     */
    @Override
    public void importConfigXML(final Node config) {
    	// ...
    }

    /**
     * {@inheritDoc}
     * 
     * NOTE : The configuration file depends on JGroups directly
     */
    @Override
    public Node exportConfigXML() {
    	return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JGroupsPeer createPeer(String peerName) {
        return new JGroupsPeer(peerName, this.getPeerGroup(),
            this.getPeerGroup().getChannel());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Message createMessage(Message msg) {
        return JGroupsMessage.createJGroupsMessage(msg);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
	public void restart() {
		
		if (peerGroup.getLocalPeer() != null) {
			// Stopping shoal
			peerGroup.uninit();
			
			// Restarting shoal
			try {
				peerGroup.init();
			} catch (Exception e) {
				throw new RuntimeException(e.getMessage(), e);
			}
		}
	}
}
