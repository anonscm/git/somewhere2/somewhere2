/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file
 * is part of Somewhere2 Copyright (C) 2012 - IASI - Philippe Chatalic, Andre
 * Fonseca This file must be used under the terms of the CeCILL. This source
 * file is licensed as described in the file LICENSE, which you should have
 * received as part of this distribution. The terms are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jgroups;

import fr.lri.iasi.somewhere.api.communication.model.MessageType;
import fr.lri.iasi.somewhere.api.communication.model.Peer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Describes a JGroups message
 *
 * @author Andre Fonseca
 */
public final class JGroupsMessage extends fr.lri.iasi.somewhere.api.communication.model.Message {

    static Logger logger = LoggerFactory.getLogger(JGroupsMessage.class);
    private static final long serialVersionUID = 745838945840L;

    /* --- Methods --- */
    private JGroupsMessage(final String id, final Peer origin, final Peer destination,
            final MessageType type, final Map<String, Object> contents) {
        super(id, origin, destination, type, contents);
    }

    /**
     * Creates a JGroups message from another message
     *
     * @param msg
     * @return a jGroups message
     */
    public static JGroupsMessage createJGroupsMessage(
            final fr.lri.iasi.somewhere.api.communication.model.Message msg) {
        return new JGroupsMessage(msg.getId(), msg.getOrigin(), msg.getDestination(),
                msg.getType(), msg.getContents());
    }

    /**
     * Unmarshal and creates a JGroups message from a signal
     *
     * @param peerGroup
     * @param msgSignal
     * @return
     * @throws Exception
     */
    public static JGroupsMessage createJGroupsMessage(
            final JGroupsPeerGroup peerGroup, final org.jgroups.Message msgSignal)
            throws Exception {
        if (msgSignal == null) {
            throw new IllegalArgumentException("msg");
        }

        // Deserialization of the msg
        JGroupsMessage msg = null;

        try {
            // Serialize the message
            msg = (JGroupsMessage) org.jgroups.util.Util.objectFromByteBuffer(msgSignal.getBuffer());

        } catch (final Exception e) {
            // Fails silently...
        }

        return msg;
    }

}
