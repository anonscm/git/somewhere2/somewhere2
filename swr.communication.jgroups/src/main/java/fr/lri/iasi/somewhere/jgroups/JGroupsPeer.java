/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jgroups;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.model.Peer;

import org.jgroups.JChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * A peer that can emit or receive messages - JGroups version
 * @author Andre Fonseca
 */
public class JGroupsPeer extends Peer {
    static Logger logger = LoggerFactory.getLogger(JGroupsPeer.class);
    private static final long serialVersionUID = 845838945847L;

    /* --- Properties --- */
    /** The PeerGroup this peer is in */
    protected transient final JGroupsPeerGroup peerGroup;

    /** The JChannel associated with this peer */
    protected transient final JChannel channel;

    public JGroupsPeer(final String name, final JGroupsPeerGroup peerGroup,
        final JChannel channel) {
        super(name);

        if (peerGroup == null) {
            throw new IllegalArgumentException("peerGroup");
        }

        if (channel == null) {
            throw new IllegalArgumentException("channel");
        }

        this.peerGroup = peerGroup;
        this.channel = channel;
    }

    /* --- Accessors --- */
    public JGroupsPeerGroup getPeerGroup() {
        return peerGroup;
    }

    public JChannel getChannel() {
        return channel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initConnection() throws CommunicationException {
    	
        if (!this.peerGroup.getAddressMap().containsKey(name))
            throw new CommunicationException("Peer " + name + " isn't in a valid state! ");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initDisconnection() throws CommunicationException {
        // ...
    }
}
