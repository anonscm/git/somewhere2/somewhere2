/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jgroups;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageSender;

import org.jgroups.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;


/**
 * Used to deal with messages sending
 * 
 * @author Andre Fonseca
 */
public final class MessageSender extends AbstractMessageSender {
    static Logger logger = LoggerFactory.getLogger(MessageSender.class);

    /* --- Properties --- */

    /** The PeerGroup this peer is in */
    private final JGroupsPeerGroup peerGroup;

    /** The concurrent ExecutorService that will execute the different threads/routines used */
    private final ExecutorService messageExecutorService;

    MessageSender(final JGroupsPeerGroup peerGroup) {
    	super(peerGroup.getModuleManager()
                .getCommunicationManager());

        this.peerGroup = peerGroup;
        this.messageExecutorService = Executors.newFixedThreadPool(10);
    }

    /* --- Accessors --- */
    public JGroupsPeerGroup getPeerGroup() {
        return peerGroup;
    }

    /**
     * Uninitialize the message sender.
     */
    public void uninit() {
        messageExecutorService.shutdownNow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processSending(final Message message) 
    	throws CommunicationException {
        JGroupsPeer destination = peerGroup.retrievePeer(message.getDestination().getName());

        if (destination.isConnected()) {
        	Future<Boolean> future = messageExecutorService.submit(new SendMsg(destination, message));
        	
        	try {
				future.get();
			} catch (InterruptedException e) {
				Thread.interrupted();
			} catch (ExecutionException e) {
				throw new CommunicationException(e);
			}
        } else {
            throw new CommunicationException("Cannot connect to " + destination);
        }
    }

    /**
     * Class used to send a message
     * 
     * @author Andre Fonseca
     */
    private final class SendMsg implements Callable<Boolean> {
        /** The message to send */
        private final Message message;
        
        /** The destination peer */
        private final JGroupsPeer peer;

        private SendMsg(final JGroupsPeer peer, final Message message) {
            if (message == null) {
                throw new IllegalArgumentException("message");
            }

            this.message = JGroupsMessage.createJGroupsMessage(message);
            this.peer = peer;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Boolean call() throws CommunicationException{

            try {       	
                // Serializes the message	
            	byte[] serializedObject = org.jgroups.util.Util.objectToByteBuffer(message);
            	Address address = MessageSender.this.peerGroup.getAddressMap().get(peer.getName());
            	org.jgroups.Message msg = new org.jgroups.Message(address, serializedObject);
            	
            	peerGroup.getChannel().send(msg);

            } catch (final Exception e) {
                throw new CommunicationException(e);
            }
            
            return true;
        }
    }
}
