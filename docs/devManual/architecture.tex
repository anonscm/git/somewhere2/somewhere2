\section{Architecture}
\label{sec:architecture}

\textit{Somewhere2} architecture is based on the utilization of several modules, each one of them addressing a specific concern. This fact allows, among others advantages, a greater flexibility when creating extensions and/or selecting different functionalities at compilation time. Also, by separating concerns into modules, the scope of a possible modification/fix is minimized.

Figure \ref{fig:Somewhere2Arch} illustrates the architecture of \textit{Somewhere2}. In this figure, the modules are organized hierarchically (bottom-top) in five components (\textit{Module Manager}, \textit{User interface}, \textit{Transport} and \textit{Distributed Reasoning}). The default configuration of \textit{Somewhere2} also use the a subset of the \textit{IASI Libs} as dependencies, but this component is not really part of \textit{Somewhere2}, being reusable by other applications. These components have very little dependencies between them, represented by its top-bottom adjacencies. For example: the \textit{Distributed Reasoning} depends on the \textit{Transport} and \textit{IASI Libs} components. These components, on the other hand, depends only from the Module \textit{Manager component}. The same logic can applied when analysing the dependencies between modules inside each component.

\begin{figure} [!hbt]
\begin{centering}
      \includegraphics[width=1.0\textwidth]{figures/somewhere2arch}
\end{centering}
\caption{\label{fig:Somewhere2Arch} \emph{Somewhere2's} Architectural Schema.}
\end{figure}

Each component can contains concrete modules (can be instantiated and loaded into baseApp), abstract modules (used to guide the implementation of concrete modules), required modules (concrete modules that are required for any \textit{Somewhere2} execution) and external libraries (in this case, the \textit{IASI Libs} that is the responsible for the propositional calculus) . The baseApp module (in red) is highlighted as the responsible for the instantiation and configuration of the other modules.

Each module on \emph{Somewhere2} address a specific concern of the application and may depend on specific libraries and/or other modules. They may also support its own configuration. These modules will be described in the next sections of this manual.

\subsection{Selecting modules following your needs}

In order to choose the modules to be part of \emph{Somewhere2} during its build, it is necessary to, at compilation time, select the profile by command line using the Maven option \textit{-P}.

If we need to add a new profile to \emph{Somewhere2} we will need to specify the used profile into two different files: the \emph{pom.xml} on the root directory of \emph{Somewhere2} and on the \emph{pom.xml} contained on the \emph{baseApp} module. This is necessary for the following reason: the profile contained on the \emph{pom.xml} file on the root of \emph{Somewhere2} directory specifies which modules to compile, while the profile contained into the \emph{pom.xml} of the \emph{baseApp} module specifies which modules to add in the output jar, in other words, which modules will be seen as dependencies for \emph{baseApp}. Being a dependency for \emph{baseApp} means that a module can be recognized and instantiated by it.

Listing \ref{profileListing} shows an excerpt of the \emph{profiles} section of the main \emph{pom.xml} file. In this listing, there are two build profiles (\emph{inconsistency-jxta} and \emph{inconsistency-jgroups}) specifying a collection of modules to participate of the \emph{Somewhere2} build task. The \emph{inconsistency-jgroups} profile is selected by default using the \emph{<activeByDefault>} option on line 29. \\

\begin{lstlisting}[language=XML, 
		morekeywords={profiles,profile, modules, module, id, activation, property, name, activeByDefault},
		label=profileListing,
		caption={Profiles section of main \emph{pom.xml} file}]
	<profiles>
	
		<profile>
			<id>inconsistency-jxta</id>
			<properties>
				<profileName>inconsistency-jxta</profileName>
			</properties>

			<modules>
				<module>swr.baseI</module>
				<module>swr.baseApp</module>
				<module>swr.cli</module>
				<module>swr.plogic.distributed</module>
				<module>swr.plogic.distributed.inconsistency</module>
				<module>swr.communication</module>
				<module>swr.communication.jxta</module>
				<module>swr.ui</module>
				<module>swr.ui.console</module>
			</modules>
		</profile>

		<profile>
			<id>inconsistency-jgroups</id>
			<properties>
				<profileName>inconsistency-jgroups</profileName>
			</properties>

			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>

			<modules>
				<module>swr.baseI</module>
				<module>swr.baseApp</module>
				<module>swr.cli</module>
				<module>swr.plogic.distributed</module>
				<module>swr.plogic.distributed.inconsistency</module>
				<module>swr.communication</module>
				<module>swr.communication.jgroups</module>
				<module>swr.ui</module>
				<module>swr.ui.console</module>
			</modules>
		</profile>
		
	</profiles>
\end{lstlisting}

In the \emph{baseApp} \emph{pom.xml} the profile containing the same name of the one selected on the main \emph{pom.xml} file will be activated. Listing \ref{profileBaseAppListing} shows an excerpt of the \emph{profiles} section of the \emph{baseApp's} \emph{pom.xml} file. In this listing, there are two build profiles (\emph{inconsistency-jxta} and \emph{inconsistency-jgroups}) specifying a collection of dependencies to be part of the\emph{Somewhere2} output jar. (Obs: since that on \emph{Maven3} profiles are not inherit, the profile activation on the\emph{baseApp's} \emph{pom.xml} is made by creating an empty file at compilation time. This is schedule to be change when \emph{Maven} provides us a better way to manage these profiles.) \\

\begin{lstlisting}[language=XML, 
		morekeywords={profiles,profile, modules, module, id, activation, property, empty file, baseApp},
		label=profileBaseAppListing,
		caption={Profiles section of main \emph{pom.xml} file}]
	<profiles>
	
		<profile>
			<id>inconsistency-jxta</id>
			<activation>
				<file>
					<!-- empty file created to activate the profile -->
					<exists>target/inconsistency-jxta</exists>
				</file>
			</activation>

			<dependencies>
				<dependency>
					<groupId>${project.groupId}</groupId>
					<artifactId>swr.plogic.distributed.inconsistency</artifactId>
					<version>${project.version}</version>
					<scope>runtime</scope>
				</dependency>
				<dependency>
					<groupId>${project.groupId}</groupId>
					<artifactId>swr.communication.jxta</artifactId>
					<version>${project.version}</version>
					<scope>runtime</scope>
				</dependency>
			</dependencies>
		</profile>
		
		<profile>
			<id>inconsistency-jgroups</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>

			<dependencies>
				<dependency>
					<groupId>${project.groupId}</groupId>
					<artifactId>swr.plogic.distributed.inconsistency</artifactId>
					<version>${project.version}</version>
					<scope>runtime</scope>
				</dependency>
				<dependency>
					<groupId>${project.groupId}</groupId>
					<artifactId>swr.communication.jgroups</artifactId>
					<version>${project.version}</version>
					<scope>runtime</scope>
				</dependency>
			</dependencies>
		</profile>
		
	</profiles>
\end{lstlisting}

In order to specify new profiles as he build new modules for \emph{Somewhere2}, the developer must indicate a valid profile that must always include all modules contained in the \textit{Module Manager} component, the \textit{swr.communication module}, one concrete communication module and finally, one or more modules responsible for the distributed reasoning, if necessary. Also, it is desired to include a user interface module for accessibility matters.

By default, the following modules can be enabled by the user using the \textit{-P} Maven option :

\begin{description}
	\item [simple-jxta] the system compiled with this profile will work as the first version of \emph{Somewhere} made by Philippe Adjiman and will use \emph{jxta} as concrete communication module. 
	\item [simple-jgroups] the system compiled with this profile will work as the first version of \emph{Somewhere} made by Philippe Adjiman and will use \emph{jgroups} as concrete communication module. 
	\item [simple-shoal] the system compiled with this profile will work as the first version of \emph{Somewhere} made by Philippe Adjiman and will use \emph{shoal} as concrete communication module. 
	\item [inconsistency-jxta] the system compiled with this profile will work as the of \emph{Somewhere-GHN} version, dealing with inconsistencies. It will also  use \emph{jxta} as concrete communication module. 
	\item [inconsistency-jgroups] the system compiled with this profile will work as the of \emph{Somewhere-GHN} version, dealing with inconsistencies. It will also  use \emph{jgroups} as concrete communication module. This option is set to be the default compilation profile of \emph{Somewhere}
	\item [inconsistency-shoal] the system compiled with this profile will work as the of \emph{Somewhere-GHN} version, dealing with inconsistencies. It will also  use \emph{shoal} as concrete communication module.
\end{description}

That's it! Concerning how the compiled modules are loaded, configured and used by \emph{Somewhere2} let's analyse, on the next chapter, its core component: \textit{Module Manager}.