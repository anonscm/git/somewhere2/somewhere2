\section{{\em Communication} Modules}

The main \emph{communication} module contains an abstract description of the concurrency model to be used by a distributed application. It is designed to be extended by a concrete communication module that normally, defines the utilization of a P2P framework. Any P2P management frameworks could be used in the communication module extension, such as \emph{JGroups}. Also, the communication module defines interfaces to handle network dynamics (e.g: lost of connection of a peer, re-connection of a peer and addition of a peer on the network).

The concurrency model proposed on this module is based on http-like message sessions used to separate different message propagation on a network.

\subsection{Package : {\bf fr.lri.iasi.somewhere.communication}}
Here is a list of the classes of this package and their description :
\begin{description}
	\item[AbstractCommunicationModule] The abstract communication module to be extended by a concrete communication module.
	\item[AlreadyReceivedException] Special type of communication exception that deals with already received messages.
	\item[AlreadySentException]	Special type of communication exception that deals with already sent messages.
	\item[AlreadySetToBeReceivedException] Special type of communication exception that deals with already set to be received messages.
	\item[AlreadySetToBeSentException] Special type of communication exception that deals with already set to be sent messages.
	\item[CommunicationException] Generic communication exception.
	\item[CommunicationManager] The main class of the Communication module. It also acts as the main message and peer listener, delegating tasks  to a collection of other listeners.
\end{description}

\subsection{Package : {\bf fr.lri.iasi.somewhere.communication.model}}
Here is a list of the classes of this package and their description :
\begin{description}
	\item[Peer] A peer that can emit or receive messages.
	\item[PeerGroup] Describes an abstract peer group. A peer group contains the reference of all distant peers that will be part of a computation, including the local peer. This class needs to be extended in a concrete communication module.
	\item[PeerListener] Interface that notifies events on distant peers.
	\item[PeerAdapter] Adapter pattern applied over the \emph{PeerListener} interface.
	\item[Message] Describes a generic message.
	\item[MessageType] Describe a message type interface. A message type classifies the nature and behaviour of a message.
	\item[MessageTypeSingletonFactory] Singleton factory pattern for message types. Message types are unique objects registered stored on a static map.
	\item[MessageListener] Interface that handle message events on local peer.
	\item[MessageAdapter] Message Adapter pattern. Used to adapt the \emph{MessageListener} interface following the needs of different listeners.
	\item[MessageSession] Describes the main entity of the concurrency model. Responsible for treating a specific message propagation, using a collection of processors to treat different message types.
	\item[MessageSessionHandler] Listener responsible for creates and stores message sessions. Also, it addresses messages to be treated by their respective message sessions.
	\item[MessageProcessor] Describes a message processor interface. A message processor is related to the message type and it executes tasks over these messages on their arrival on the local peer.
	\item[MessageContentKey] Alternative implementation of the typesafe heterogeneous container pattern. Creates a type map that will represent the content of a message. Each element of this content, will inserted and retrieved from the map respecting the type indicated on its registration.
	\item[FinalAnswer] Represents a final answer to be sent to the user. A \emph{FinalAnswer} differs from a normal \emph{Message} since it is not meant to be transfer on the network, only to be presented for the user on the local peer.
	\item[FinalAnswerAnnotation] Represents an annotation to be included on the FinalAnswer object.
\end{description}

Here, there are important explanations that need to be reinforced. The \emph{MessageSession} concept is a key one in the communication component since it handles one specific propagation in the local peer. In other words, when a message arrive at a peer, it needs to be processed somehow, and sometimes it needs to address the exact responsible for its processing (since many messages can arrive concurrently to the local peer). To do so, each message needs to indicate the \emph{MessageSession} that will contains information referent to its propagation and the processors that will be used to process it.

For example, in the \emph{Distributed Reasoning} component (explained on \ref{}), among different types of message, there are two that can be used on this explanation: the \emph{Query} message type and the \emph{Answer} message type. Multiple \emph{Query} messages can arrive at a peer simultaneously, and for each one then, several \emph{Answer} messages can be replied for the peer who ask the queries. Let's say that a peer have received and propagated two queries at the same time, and two answers need to be received from the peer where the queries were propagated. The problem is that these answers need to be received properly since each answer must correspond a the query (propagation) that triggered its creation! If two answers arrive at the same time without any indication of which query it corresponds, the results of both propagations could be completely wrong!

Let's say that If each \emph{Query} will be treated by a different \emph{MessageSession}, to solve this problem the answer only need to be replied indicating from which \emph{MessageSession} it corresponds. It is enough to separate informations of different propagations.

\emph{MessageSession} can contains multiple \emph{MessageProcessors}, each one deals with a specific \emph{MessageType}. These may \emph{MessageProcessors} run concurrently, dealing with many messages of the same type at the same time. A peer can have multiple \emph{MessageSessions}, being handled by the \emph{MessageSessionHandler}. Each time a message arrives at a peer the \emph{MessageSessionHandler} will redirect the message to the session that it addresses. This object is also responsible for creating new sessions or destroying then when they are no more useful. 

Figures \ref{fig:message_listener_part}, \ref{fig:peer_listener_part} \ref{fig:message_part} and \ref{fig:communication_manager_part} illustrates the relations between the classes contained in the \textit{fr.lri.iasi.somewhere.communication.model} package (Some classes of \textit{fr.lri.iasi.somewhere.communication} and \textit{fr.lri.iasi.somewhere.communication.model.impl} were included in these diagrams).

\begin{figure} [!hbt]
\begin{centering}
	  \hspace*{-0.6in}
      \includegraphics[width=1.3\textwidth]{figures/message_listener_part}
\end{centering}
\caption{\label{fig:message_listener_part}  \emph{fr.lri.iasi.somewhere.communication.model} class diagram (\emph{MessageListener} relations).}
\end{figure}

\begin{figure} [!hbt]
\begin{centering}
	  \hspace*{-0.6in}
      \includegraphics[width=1.3\textwidth]{figures/peer_listener_part}
\end{centering}
\caption{\label{fig:peer_listener_part} \emph{fr.lri.iasi.somewhere.communication.model} class diagram (\emph{Peer} and \emph{PeerListener} relations).}
\end{figure}

\begin{sidewaysfigure} [!hbt]
\begin{centering}
	  \hspace*{-0.6in}
      \includegraphics[width=1.0\textwidth]{figures/message_part}
\end{centering}
\caption{\label{fig:message_part} \emph{fr.lri.iasi.somewhere.communication.model} class diagram (\emph{Message} relations).}
\end{sidewaysfigure}

\begin{sidewaysfigure} [!hbt]
\begin{centering}
	  \hspace*{-0.6in}
      \includegraphics[width=1.0\textwidth]{figures/communication_manager_part}
\end{centering}
\caption{\label{fig:communication_manager_part} \emph{fr.lri.iasi.somewhere.communication.model} class diagram (\emph{CommunicationManager} relations).}
\end{sidewaysfigure}

\subsection{Package : {\bf fr.lri.iasi.somewhere.communication.model.impl}}
Here is a list of the classes of this package and their description :
\begin{description}
	\item[AbstractMessageCollector] A message collector is a listener that is responsible for collecting messages of an specific type for showing them to the user.
	\item[AbstractMessageProcessor] Describes an abstract message processor.
	\item[AbstractMessageSender] Describes an abstract message sender. It is responsible for sending messages to other peers. To be extent on concrete communication modules.
	\item[AbstractMessageReceiver] Describes an abstract message receiver. It is responsible It is responsible for receiving messages from other peers. To be extent on concrete communication modules.
	\item[AbstractMessageType] Describes an abstract message type.
	\item[SimpleMessageProcessor] Process simple text messages, printing it on the default output stream.
\end{description}

\subsection{Package : {\bf fr.lri.iasi.somewhere.communication.model.types}}
Here is a list of the classes of this package and their description :
\begin{description}
	\item[PingMessageType] Ping message type.
	\item[PongMessageType] Pong message type.
	\item[SimpleMessageType] Simple text message type.
\end{description}

\subsection{Package : {\bf fr.lri.iasi.somewhere.communication.commands}}
Here is a list of the classes of this package and their description :
\begin{description}
	\item[AddPeerCommand] Responsible for adding a new neighbour peer.
	\item[ListPeersCommand] A command to print the list of currently neighbours peers.
	\item[PauseCommunicationCommand]	A command to temporarily pause communication system.
	\item[PingCommand] Responsible for pinging a distant peer.
	\item[RemovePeerCommand] A command to remove a peer from the local peer's neighbour list.
	\item[ResumeCommunicationCommand] A command to resume the communication of the local peer.
	\item[SendTextMessageCommand] Responsible for sending text message to distant peer.
\end{description}

\subsection{Package : {\bf fr.lri.iasi.somewhere.communication.configuration}}
Here is a list of the classes of this package and their description :
\begin{description}
	\item[CommunicationConfiguration] Configuration of communication bundle.
\end{description}

\subsection{Package : {\bf fr.lri.iasi.somewhere.communication.heartbeat}}
Here is a list of the classes of this package and their description :
\begin{description}
	\item[Heartbeat] Responsible for sending, at a specified interval, a heartbeat message to known peers to verify if they are available.
	\item[PingMessageProcessor] Responsible for processing a received ping message.
	\item[PongMessageProcessor] Responsible for processing a received pong message.
\end{description}

\subsection{Package : {\bf fr.lri.iasi.somewhere.communication.logging}}
Here is a list of the classes of this package and their description :
\begin{description}
	\item[CommunicationLoggerLayout] Formats the events messages over the SL4J log.
\end{description}

\subsection{Dependencies}
Here are the dependancies of the \textbf{communication} module :
\begin{description}
	\item[BaseI module] Because it uses lots of interfaces defined there.
	\item[ui module] For implementing some user commands.
\end{description}

\subsection{Common related questions}

\subsubsection{How to create a concrete communication new Module}

In order to create a concrete communication module, the developer needs, at first, to create a new concrete module as it is explained on \ref{subsubsec:createmodule}. Specific configuration and commands may be specified by the user following his needs.

Furthermore, at least 5 classes need to be extent in other to provide all functionalities necessary by a concrete communication module:

\begin{enumerate}
	\item Peer
	\item PeerGroup
	\item Message
	\item AbstractMessageSender
	\item AbstractMessageReceiver
\end{enumerate}

The subclasses of these abstract classes will contains specific P2P frameworks routines related to the sending and reception of messages, detection of failed or reconnected peers and more. In the following subsections it is explained how to extend each one of these abstract classes.

Examples of subclasses of the indicated abstract classes can be found on the following modules: \emph{swr.communication.jxta}, \emph{swr.communication.shoal} and \emph{swr.communication.jgroups}.

\subsubsection{How to create a module specific peer}

For each concrete communication module, a subclass of \emph{Peer} must be implemented in order to handle its initialization. Listing \ref{dummyPeer} exemplifies this task: \\

\begin{lstlisting}[language=java,
	label=dummyPeer,
	caption={The illustrative DummyPeer class.}]
/**
 * The DummyPeer class
 */
public class DummyPeer extends Peer {
    
    /* --- Properties --- */
    /** The PeerGroup this peer is in */
    protected transient final DummyPeerGroup peerGroup;

    public JGroupsPeer(final String name, final DummyPeerGroup peerGroupl) {
        super(name);

        if (peerGroup == null) {
            throw new IllegalArgumentException("peerGroup");
        }

        this.peerGroup = peerGroup;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initConnection() throws CommunicationException {
    	// P2P framework specific code on how to init the connection with the current peer
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initDisconnection() throws CommunicationException {
        //P2P framework specific code on how to init the connection with the current peer
    }
    
}

\end{lstlisting}

In the currently concrete communication modules, these \emph{Peer} subclasses were also extent in order to describe the behaviour of a local peer. So, in that way, instances of the module specific \emph{Peer} class would represent neighbour peers while the instance of the \emph{localPeer} class would corresponds to the user's peer.

It is often a good idea to implement a \emph{LocalPeer} class since some routines may differ regarding if the target peer is a local or a distant peer.

\subsubsection{How to create a module specific peer group}

It is also necessary to implement a subclass of the \emph{PeerGroup} class. In the concrete communication modules, this class normally deals with the initialization of the specific P2P framework and also acts as a peer listener sending events about the local peers to its known neighbour peers. Listing \ref{dummyPeerGroup} illustrates this explanation: \\

\begin{lstlisting}[language=java,
	label=dummyPeerGroup,
	caption={The illustrative DummyPeerGroup class.}]
/**
 * This class represents a peer group, containing:
 *        - a local peer
 *        - its neighbors
 */
public class DummyPeerGroup extends PeerGroup<DummyManager, DummyPeer, DummyLocalPeer> {

    /** The message receiver */
	private MessageReceiver messageReceiver;
	
	/** The message sender */
	private MessageSender messageSender;

    public JGroupsPeerGroup(final DummyManager moduleManager) {
        super(moduleManager);

        // Register itself as a peer Listener
        this.moduleManager.getCommunicationManager()
        								  .addPeerListener(this);

        // Create handlers for message sending and reception
        this.messageSender = new MessageSender(this);
        this.moduleManager.getCommunicationManager()
        								  .setMessageSender(this.messageSender);
        this.messageReceiver = new MessageReceiver(this);
        this.moduleManager.getCommunicationManager()
        								  .setMessageReceiver(this.messageReceiver);
    }
    
    /**
     * Uninitialize our stuffs about the peer group
     */
    public void uninit() {
    	// P2P framework specific uninitialization
    }

    /**
     * Initialize the peerGroup
     */
    public void init() {
       // P2P framework specific initialization
    }

    /**
     * {@inheritDoc}
     */
    public void peerIsNoMoreAccessible(Peer peer) {
    	// Send P2P framework specific message indicating that the local peer is no more accessible
    }

    /**
     * {@inheritDoc}
     */
    public void peerIsAccessible(Peer peer) {
    	// Send P2P framework specific message indicating that the local peer is accessible
    }

    /**
     * {@inheritDoc}
     */
    public void peerIsConnecting(Peer peer) {
    	// Send P2P framework specific message indicating that the local peer is connecting
    }

    /**
     * {@inheritDoc}
     */
    public void peerIsLeaving(Peer peer) {
    	// Send P2P framework specific message indicating that the local peer is disconnecting
    }

}

\end{lstlisting}

\subsubsection{Add and remove peers from the peer group}

Additional information can be given about the peer group concerning adding or removing peers. On each \emph{PeerGroup} the method \emph{retrievePeer} can be called passing the name of a distant peer as parameter. This method will create a instance of the target distant peer if it does not exist, or retrieve it from a peer list if it was already created.

If the distant peer is being created, the same routine will tries to connect to the concerned peer. If successful, it will change the state of the peer to \emph{connected} and \emph{accessible}. The difference between the \emph{connected} and \emph{accessible} is simple: the \emph{connected} attribute infers that the peer is present on the network and the \emph{accessible} attribute infers that the peer can communicate with other peers. Peer listeners can modify these values, changing the distant peers view of the local peer.

All these tasks are handled by the abstract communication layer, buy concrete communication modules need to specify how each P2P framework can detect these events. 

\subsubsection{How to create a module specific message}

A specific message class must be specified for each concrete communication module. This subclass normally indicates how the message will be unmarshalled and received by the local peer.

\subsubsection{How to send and receive messages}

In order to send or receive messages, the user must implement subclasses of \emph{AbstractMessageReceiber} and \emph{AbstractMessageReceiver}. These classes must implement the P2P framework specific routines of sending/receiving information. Listings \ref{dummyMessageReceiver} and \ref{dummyPeerGroup} illustrates this explanation: \\

\begin{lstlisting}[language=java,
	label=dummyMessageReceiver,
	caption={The illustrative DummyMessageReceiver class.}]
/**
 * Used to deal with messages reception from the outside
 */
public class DummyMessageReceiver extends AbstractMessageReceiver implements CallBack {

    public DummyMessageReceiver(final DummyGroup peerGroup) {
    	super(peerGroup.getModuleManager()
                .getCommunicationManager());
    }
    
    // Specific P2P routines that call the processReceiving method
    
    /**
     * {@inheritDoc}
     */
    @Override
	public void processReceiving(Message message) throws CommunicationException {
    	communicationManager.receiveMessage(message);
	}

}
\end{lstlisting}

\begin{lstlisting}[language=java,
	label=dummyMessageSender,
	caption={The illustrative DummyMessageSender class.}]
/**
 * Used to deal with messages sending
 */
public final class DummyMessageSender extends AbstractMessageSender {
    
    /* --- Properties --- */
    MessageSender(final JGroupsPeerGroup peerGroup) {
    	super(peerGroup.getModuleManager()
                .getCommunicationManager());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processSending(final Message message) 
    	throws CommunicationException {
        
        // Use P2P framework to serialize and send the message. You may use an
        // executor service in order to make these tasks concurrent.
    }
    
}

\end{lstlisting}

\subsubsection{How to define a new message type and its correspondent processor}

Different types of message can be sent by \emph{Somewhere2}, being used to perform different tasks. Actually a message type is more related to the \emph{Somewhere2's} application level than its communication level.

Some information the message type is mainly useful to redirect this message to the its specific handler, also called \emph{MessageProcessor}, on each \emph{MessageSession}. Other information that can be useful about a message type is what to do about the current \emph{MessageSession} before or after receiving a message. For instance, we can say that a new \emph{MessageSession} needs to be opened before receiving a specific message kind or a \emph{MessageSession} needs to be destroyed after sending this message somewhere else.

Here we must remind the user that a peer can hold multiple \emph{MessageSessions} at once (one ofr each message propagation) but each one of them will hold only one instance of a \emph{MessageProcessor} for each kind of message.

Listing \ref{pingMessageType} illustrates a \emph{ping} message type, present on the abstract communication module. Listing  \ref{pingMessageProcessor} illustrates its correspondent message processor (sends back a \emph{pong} message). \\

\begin{lstlisting}[language=java,
	label=pingMessageType,
	caption={The PingMessageType class.}]
/**
 * Ping message type
 */
public class PingMessageType extends AbstractMessageType {
	private static final long serialVersionUID = 1839494L;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return "pingMessage";
	}

	/**
	 * The ping message needs a new session
	 * to be created before being sent other peers.
	 */
	@Override
	public boolean needsNewSessionSend() {
		return true;
	}

	/**
	 * The ping message needs a new session
	 * when arriving on distant peers.
	 */
	@Override
	public boolean needsNewSessionReceive() {
		return true;
	}

	/**
	 * The ping is not a returning message.
	 * (The return message would be the 'pong' message type!)
	 */
	@Override
	public boolean isReturningType() {
		return false;
	}

	/**
	 *  The ping message needs to maintain a open
	 * session after being send since the same session
	 * will be need to send the rest of the ping messages.
	 */
	@Override
	public boolean needsToMaintainSessionSend(MessageSession session) {
		return true;
	}

	/**
	 *  The ping message needs to maintain a open
	 * session after being received since the same session
	 * will be need to receive the rest of the ping messages.
	 */
	@Override
	public boolean needsToMaintainSessionReceive(MessageSession session) {
		return true;
	}

	/**
	 * The processor responsible for handling o ping messages will be the 
	 * ping message processor. Basically it will send back pong messages.
	 */
	@Override
	public MessageProcessor createProcessor(MessageSession session) {
		return new PingMessageProcessor(session);
	}

	/**
	 * The ping message don't need to be printed since the interesting
	 * information comes with the pong message.
	 */
	@Override
	public String print(Message message) {
		return null;
	}
}

\end{lstlisting}

\begin{lstlisting}[language=java,
	label=pingMessageProcessor,
	caption={The PingMessageProcessor class.}]
/**
 * Responsible for processing a received ping message.
 */
public class PingMessageProcessor extends AbstractMessageProcessor {
    
    public PingMessageProcessor(MessageSession session) {
        super(session);
    }
    
    /* --- Methods --- */
    /**
     * Process of ping message: whenever a ping message arrives and it is processed, it will
     * generate a pong message to be sent as a answer to the sender peer.
     * 
     * @param message : the message to be processed
     */
    @Override
    public void process(Message message) {
    	try {
    		MessageBuilder builder = new MessageBuilder(session.getCommunicationManager());
    		CommunicationManager.TEXT.addContent(builder, "pong");
    		
			session.sendMessage(builder,
					message.getOrigin().getName(),
					MessageTypeSingletonFactory.create(PongMessageType.class));
			
		} catch (CommunicationException e) {
			logger.error("There were problems responding the ping message: {}", e.getMessage());
		}
    }

}
\end{lstlisting}

\subsubsection{How to create use the MessageContentKey to create type-safe message contents}

The \emph{Message} class was designed to hold different kinds of contents. In order to avoid runtime exceptions related to type casting when inserting or retrieving this contents in both \emph{Message} or \emph{MessageBuilder} classes, \emph{MessageContentKey} was implemented as a mechanism to ensure type-safety of contents following the type-safe heterogeneous container pattern. 

The \emph{MessageContentKey} class holds a map representing the type of each message's content. Each element of this content, will be inserted and retrieved from the map respecting the type indicated on its registration. For example: \\

\begin{lstlisting}[language=java]
public static final MessageContentKey TEXT = MessageContentKey.create("text");
\end{lstlisting}

Will create, a content element named "text" that will be typed as a String. Every operation that tries to read or write this content element, need to respect its type. Listing \ref{createMessageText} shows the creation of a message with the content \emph{"text"}: \\

\begin{lstlisting}[language=java, 
	label=createMessageText]
String content = "hello world!";
MessageBuilder builder = new MessageBuilder(communicationManager);
CommunicationManager.TEXT.addContent(builder, content);
\end{lstlisting}

Listing \ref{readMessageText} shows another example of dealing with message content. There we can observe how to read a message content of index \emph{"text"}. In both listings, if the variable result is not a \emph{String}, a compilation error will occur. \\

\begin{lstlisting}[language=java, 
	label=readMessageText]
String result = CommunicationManager.TEXT.getContent(message);
\end{lstlisting}
