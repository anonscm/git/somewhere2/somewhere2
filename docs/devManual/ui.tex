\section{{\em User Interface} Modules}
\label{sec:ui}

There are two modules that are part of the {\em User Interface} component.

\subsection{ui Module}
This module contains abstract implementation of a \emph{front-end} module. \emph{Front-end} \textbf{ui} modules are modules that give a user interface to the user, and send back \textbf{ui} commands to the \textbf{ui} module, based on the requests of the user. For example, modules \textbf{ui.console} (takes input from user in a shell and sends back associated command to \textbf{ui}) and \textbf{cli} (takes input from command line parameters and sends back associated commands to \textbf{ui}) are \textbf{ui} front-ends.

This module is just a simple module, so it does not have any module manager class (it is just a basic \emph{Maven} module that can be included as a dependency in another module). Also, this module does not have any associated configuration.

It must be noted that base classes and interfaces that represent commands and return status are stored in the \textbf{baseI} module (since a module can have its own commands without being a front-end module).

\subsubsection{Package : {\bf fr.lri.iasi.somewhere.ui}}
Here is a list of the classes of this package, and there description :
\begin{description}
	\item[BaseUI] A base implementation of a UI front-end.
	\item[GenericReturnStatus] A simple command return status.
	\item[HelpCommand] This class implements a command that is meant to print information about others commands. For this, it uses the \verb!description! property of every command.
	\item[HelpReturnStatus] This is a return status for \emph{help} command.
	\item[QuitCommand] This is an abstract class that represents a command that quits the application. It is abstract because the action of quitting depends upon the application. So, the main module (the module which produce the resulting \verb!.jar! file, ie module \textbf{cli}) must implement a new command extending this one.
	\item[QuitReturnStatus] This is a return status for command \textbf{quit}.
\end{description}

Figure \ref{fig:ui} describes the relation between the entities of package \emph{fr.lri.iasi.somewhere.ui}.

\begin{figure} [!hbt]
\begin{centering}
	  \hspace*{-0.6in}
      \includegraphics[width=1.3\textwidth]{figures/ui}
\end{centering}
\caption{\label{fig:ui} \emph{fr.lri.iasi.somewhere.ui} class diagram.}
\end{figure}

\subsubsection{Dependencies}
Here are the dependancies of the \textbf{ui} module :
\begin{description}
	\item[BaseI module] Because it uses lots of interfaces defined there.
\end{description}

\subsection{ui.console Module}
This module contains an implementation of a console shell \textbf{ui} interface. Its extends the \textbf{ui} module being a concrete \emph{Somewhere2} front-end. Additionally, it relay on the utilization of printing strategies to control the console output.

It is just a simple module, so it does not have any module manager class (it is just a basic \emph{Maven} module that can be included as a dependency in another module). Also, this module does not have any associated configuration.

\subsubsection{Package : {\bf fr.lri.iasi.somewhere.ui.console}}
Here is a list of the classes of this package, and there description :
\begin{description}
	\item[Console] This is the base class of the module. It implements interface \textbf{Runnable},
		and thread itself when initializing. It extends the \textbf{BaseUI} class of module \textbf{ui}
		(so this is the console \textbf{ui} front-end). It uses \textbf{Jline} to create a console shell interface,
		and send shell commands back, as \emph{Somewhere2} commands, to module \textbf{ui}. \\
		It also support a small history thanks to \textbf{JLine} history support.
	\item[PrintStrategy] Specifies the console printing strategy to be used by the module.
	\item[QuitConsoleCommand]
		This class is responsible for creating command to quit the console. It extends the \textbf{QuitCommand} class, and just add the console termination.
	\item[RunConsoleCommand]
		This class is responsible for running a new console shell.
\end{description}

Figure \ref{fig:console} describes the relation between the entities of package \emph{fr.lri.iasi.somewhere.ui.console}.

\begin{figure} [!hbt]
\begin{centering}
	  \hspace*{-0.2in}
      \includegraphics[width=1.0\textwidth]{figures/console}
\end{centering}
\caption{\label{fig:console} \emph{fr.lri.iasi.somewhere.ui.console} class diagram.}
\end{figure}

\subsubsection{Package : {\bf fr.lri.iasi.somewhere.ui.console.printer}}
Here is a list of the classes of this package, and there description :
\begin{description}
	\item[DefaultPrintStrategy] Basic console printing strategy : the output is directed to the shell console.
	\item[RemotePrintStrategy] Printing strategy based on the sending of console outputs through a remote output stream (\emph{web socket}.
\end{description}

\subsubsection{Dependencies}
Here are the dependencies of the \textbf{ui.console} module :
\begin{description}
	\item[BaseI module] Because it uses lots of interfaces defined there.
	\item[ui module] Because \textbf{ui.console} is a concrete front-end.
	\item[jLine Framework] This framework is used to have a nice console shell interface.
\end{description}


\subsection{Common related questions}

\subsubsection{How to create a new front-end}

In order to create a new \emph{Somewhere2} front-end, it is necessary to extend the \emph{BaseUI} class of the \textbf{ui} module. Only by extending this class, the sub-class will be able to handle any \emph{Somewhere2} command.

Additionally, the developer needs to indicate to the \textbf{cli} module to use the new front-end during the \emph{Somewhere2} initialization. Normally this is done by creating a command that can be used as an initialization parameter. For example: \\

\begin{lstlisting}[language=java,
	label=runDummyUICommand,
	caption={The illustrative RunDummyUICommand class.}]
/**
 * Runs the DummyUI
 */
public class RunDummyUICommand implements Command {

    public RunConsoleCommand() {
    }

    /**
     * @return the name of the command
     */
    public String getName() {
        return "dummyUI";
    }

    /**
     * @return the description of the command
     */
    public String getDescription() {
        return "Run a dummy UI.";
    }

    /**
     * Runs the dummy UI
     * @param parameters The parameters of the command
     * @return the return status
     */
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;

        try {
            DummyUI ui = new DummyUI();
            ui.start();
            
            res = new GenericReturnStatus("UI successfully started", true,
                    null);
        } catch (Exception e) {
            console.stop();
            logger.error("Abnormal execution of " + getName() + " : " + e);
            res = new GenericReturnStatus("There was an error launching UI...",
                    false, null);
        }

        return res;
    }
    
}  
\end{lstlisting}

\subsubsection{How to create a new print strategy}

A print strategy is used to specify where the answer of a specific command will be printed. To illustrate this explanation, in \emph{Somewhere2} there are two implemented print strategies, the \emph{DefaultPrintStrategy} which prints command returns through the console and the \emph{RemotePrintStrategy} which prints these returns through a socket. It may be easy to imagine another strategy like \emph{FilePrintStrategy} which could print these command returns in a file. These strategy can be changed at runtime using the \emph{RedirectOutputCommand} (more information about this command can be found in the \emph{Somewhere2 User's Manual}). 

In order to implement a new print strategy, the user must create a class that implements the \emph{PrintStrategy}. For example: \\

\begin{lstlisting}[language=java,
	label=dummyPrintStrategy,
	caption={The illustrative DummyPrintStrategy class.}]
/**
 * The dummy print strategy is printing thought the dummy UI.
 *
 */
public class DummyPrintStrategy implements PrintStrategy {
	/* --- Properties --- */
	/** The JLine console object */
	private final DummyUI ui;
	
	public DefaultPrintStrategy(DummyUI ui) {
		if (ui == null)
			throw new IllegalArgumentException("ui");
		
		this.ui = ui;
	}

	/* --- Methods --- */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print(String information) {
		ui.printf(information);
	}

}
\end{lstlisting}


