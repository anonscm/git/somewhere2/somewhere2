
\section{Getting started}
\label{sec:gettingstarted}

In the following subsections is briefly described some general information about the Somewhere2 project. This chapter is intended to be used as a first reading resource of a new Somewhere2 developer. Each of the items will be described further in the following chapters.

\subsection{What this project is about}

Somewhere2 is an application that enables the deployment of networks of independent peers in order to perform decentralized reasoning over these resources. Each Somewhere peer is able to manage autonomously its own local knowledge base and state logical constraints between its own vocabulary and those of the neighbouring peers. Exploiting such links in an appropriate way has enabled the design of fully decentralized algorithms, that can solve reasoning tasks (e.g. consequence finding) with respect to the whole available knowledge of the network. The general schema of such algorithms involves first reasoning on a local basis, then exploiting local results and constraints between peers in order to request neighbours peers that may contribute to obtain further (part of) results, and eventually recombining in an appropriate way the results produced by these neighbours.

\subsection{How to get the source code}
The entire project is stored in a Git repository and it can be downloaded from \url{https://sourcesup.renater.fr/projects/somewhere2/}. \\
The main libraries used by Somewhere, the IASI Libs, can be fetched from \url{https://sourcesup.renater.fr/projects/iasilibs/}. \\

On both projects, the core development is done in the \verb!master! branch .

\subsection{Somewhere2's coding style}
There are several coding style rules that we tried to respect for the project :

\begin{itemize}
 \item No file should contain more than 600 lines (there are a few classes that are exception to this rule)
 \item No method/function should contain more than 200 lines.
 \item There must be comments everywhere (methods, classes, variables, etc\ldots)
 \item Every file must be correctly indented (use of \verb!=! command in {\bf Vim})
 \item There should be no space characters in unused lines - and in the end of lines
	 (use of \verb!:%s/\s*$//g<cr>:noh<cr>''! command in {\bf Vim}).
 \item Use of standard Java Notation for names (variables, methods, etc\ldots)
 \item You should give priority to object-oriented solutions, and use design patterns when  required
\end{itemize}

\subsection{Tools used during the Somewhere2's development}
Here is a list of all the tools used in development :
\begin{itemize}
 \item \textbf{Maven} Project management tool (build tasks).
 \item \textbf{JUnit} For Unit testing
 \item \textbf{Jenkins} For continuous integration
 \item \textbf{Git} As a SCM for the project
 \item \textbf{Sonar} As a static analysis tool
\end{itemize}

\subsection{How the Somewhere2 code is organized - Module System}
As described in the User manual, the Somewhere project is subdivided into several modules (sub-projects), with the following characteristics:

\begin{itemize}
	\item Each module is a Maven module of the Somewhere2 Maven project.
	\item Each module which has a configuration support (cf \verb!Configuration System!) has an associated \verb!modulePackage.configuration! package.
	\item Each module which has some \textbf{UI} module commands (cf Section \textbf{UI} module) has an associated \verb!modulePackage.commands! package.
\end{itemize}

The module \textbf{baseApp} contains a support for adding, deleting, and managing Somewhere modules.
See Section \label{sec:architecture} for more information about the Somewhere2's modular system.

\subsection{How to build Somewhere2 with Maven 3}
The project uses the Maven 3 compilation system. For more details, go to \url{http://maven.apache.org} .

Each \emph{Somewhere2} module (sub-project) contains a \verb!pom.xml! file. Different modules can be chosen at compilation time to be part of \emph{Somewhere2}. This building configuration (also called \textit{profile}) can be selected by the user using a Maven command.

For building \emph{Somewhere2} with the default module profile, enter : \\

\begin{lstlisting}
	mvn clean package
\end{lstlisting}

For compiling a different profile, for example the profile \textit{jxta}, enter :\\

\begin{lstlisting}
	mvn clean package -P simple-jxta
\end{lstlisting}

More explanations about compilation profiles will be given in Section \ref{sec:architecture}.

\subsection{The basics about Somewhere2 configuration system}
The module \textbf{baseApp} contains a support for reading XML configuration files, and forwarding the configuration to each module locally. \\
Each module is associated with one root balise in the XML configuration (for example, to configure module \textbf{jxta}, there must be a root balise named \textbf{jxta} in the configuration file  which contains child balises). \\
See section \label{sec:base} and the \emph{Somewhere2  User's Manual} for more information about the Somewhere2's configuration system.

\subsection{Used testing frameworks and tools}
The {\bf JUnit} framework is used for testing purposes. There are JUnit test classes when required. \\
Integration tests are run daily by Jenkins. However, if is necessary to run integration tests locally, it can be done by using the command : \\

\begin{lstlisting}
	mvn clean install
\end{lstlisting}

\textbf{Attention}: Since these tests can really take a lot of time, the best is to, during Somewhere's build, run only Unit tests (using \textit{mvn clean package}) and let the integration tests to be run by Jenkins.
After every Jenkins build, some reports will be generated on the Sonar server. These reports are useful to analyse code violations, duplications, bugs that can be found statically and many other problems.
These are the debug reports generated by Sonar for every Somewhere module :

\begin{itemize}
 \item \textbf{Cobertura} Checks code coverage of unit tests.
 \item \textbf{Jacoco} Checks code coverage of integration tests.
 \item \textbf{FindBugs Report} Analyse a list of possible bugs
 \item \textbf{PMD Report} Checks dead code, suboptimal code, \textit{copy and paste} code and overcomplicated expressions.
\end{itemize}

\subsection{Somewhere2's Libraries and Dependencies}
Each Somewhere module has several different dependencies upon others java projects (libraries). The main library used by the default configuration of Somewhere are the IASI Libs. \\
Every Somewhere module uses \textbf{slf4j} as facade for logging, \textbf{logback} as the concrete logging library, and \textbf{JUnit} for unit testing.

\subsection{Somewhere2's logging system}
As every module uses \textbf{logback}, a configuration file for the logging is stocked in the path  \verb!src/main/resources/moduleName-logback.xml! inside each module that needs to output some log information.
The standard \textbf{logback} configuration file supplied with module \textbf{swr.cli} is intended to use the standard output channel (ie : the console) to output logging. \\
See the \textbf{logback} manual for information about \textbf{logback} configuration files.

\subsection{Continuous Integration}
The \emph{Somewhere2} project use Jenkins \footnote{\url{http://jenkins-ci.org/}} as its continuous integration server. The main functionality of this server is to assure that the last version of \emph{Somewhere2} in version control system pass on all its tests. Additionally, since some tests (normally, integration tests) require some time to execute and may provoke a heavy CPU load, the are also executed only on this server.

