\section{{\em Module Manager} modules}
\label{sec:base}

There are three modules that are part of the {\em core} of the application.

\subsection{BaseI Module}
This module contains general interfaces (and abstract classes) used in every \emph{Somewhere2} module. Basically, if a module implements the main interface of the \emph{baseI}, it will be considered as a concrete module and loaded into the module manager at runtime. Also, it specifies abstract exceptions and supports an abstract module configuration system that reads configuration parameters and forward them to target modules.

\subsubsection{Package : {\bf fr.lri.iasi.somewhere}}
Here is a list of the classes of this package and their description :
\begin{description}
	\item[App] Describes an application, in other words, the main \emph{Somewhere2} entry point. An application differs from a normal module since the former is responsible to load other modules and forward configuration parameters to them. A user interface and a configuration manager entity are normally related to it.
	\item[Module] Describes a basic \emph{Somewhere2} module. Any class that implements this interface and is part of a build, will be loaded by the concrete \emph{App} class during the \emph{Somewhere2} initialization.
	\item[Configuration]	Describes an abstract module configuration.
	\item[ConfigurationManager] Describes a entity directly responsible to forward configuration parameters to different modules, based on the name the module and the balises indicated on the XML configuration file.
	\item[AbstractModule] Abstract implementation of \emph{Module}. Indicates a generic module loading order and initialize the generic log configuration.
	\item[SomewhereException] Describes the most basic exception on \emph{Somewhere2}.
	\item[XPathReader] Helper class used in order to parse and execute XPath expressions.
\end{description}

Figure \ref{fig:baseI} describes the relation between the entities of package \emph{fr.lri.iasi.somewhere}. \\

\begin{figure} [!hbt]
\begin{centering}
	  \hspace*{-0.6in}
      \includegraphics[width=1.3\textwidth]{figures/baseI}
\end{centering}
\caption{\label{fig:baseI} \emph{fr.lri.iasi.somewhere} class diagram.}
\end{figure}

\subsubsection{Package : {\bf fr.lri.iasi.somewhere.ui}}
Here is a list of the classes of this package, and there description :
\begin{description}
	\item[Command] Describes a \emph{Somewhere2} command. These commands can be indicated by the user during the initialization process, via a command line interface, or passed to the application at runtime using the selected user interface. Each module can contain specific \textbf{ui} commands related to their field of action.
	\item[UI] Describes a user interface. This entity will contain the collection of commands specified by different modules.
	\item[ReturnStatus] Describes the return status of a command execution.
\end{description}

\subsubsection{Dependencies}
The \textbf{BaseI} module only depends on default dependencies (slf4j, logback and JUnit (only for testing)).

\subsection{BaseApp Module}
This module contains an implementation of a general \emph{Somewhere2} application. Basically, it instantiates\emph{Somewhere2} modules and loads its configurations. It implements concepts related to those contained into the \textbf{baseI} module.

\subsubsection{Package : {\bf fr.lri.iasi.somewhere.baseApp}}
Here is a list of the classes of this package and their description :
\begin{description}
	\item[AbstractApp] Abstract implementation of a \emph{Somewhere2} application. On this entity, is implemented tasks like importing and exporting configuration, module loading supporting Java reflection, configuration of log file path and specification of the shut down task.
	\item[BaseConstants] Describe statics constants to be used by a \emph{Somewhere2} application. Among these constants it is specified the default log path and the default configuration file path.
	\item[AbstractConfigurationManager] Abstract implementation of \emph{ConfigurationManager}. Here is specified that the configuration parameters will be forwarded to all modules in sequential way.
	\item[ModuleManager] Responsible for looking inside the compiled (on the application jar file) and identify all classes that implements the \emph{fr.lri.iasi.somewhere.Module} interface. These classes will be loaded as \emph{Somewhere2} concrete modules by the concrete implementation of the \emph{AbstractApp} class.
	\item[Modules] Collection of loaded modules. Specifies that the modules are loaded and unloaded following its loading order property. Triggers the modules initialization.
	\item[XMLConfigurationManager] Specifies a configuration manager based on XML files. Verifies if the XML configuration files are valid following the XML Schema contained a module resource (if any). If valid, parse configuration file and delegates its importation by the target module.
\end{description}

Figure \ref{fig:baseApp} describes the relation between the entities of package \emph{fr.lri.iasi.somewhere.baseApp}.

\begin{figure} [!hbt]
\begin{centering}
	  \hspace*{-0.4in}
      \includegraphics[width=1.2\textwidth]{figures/baseApp}
\end{centering}
\caption{\label{fig:baseApp} \emph{fr.lri.iasi.somewhere.baseApp} class diagram.}
\end{figure}

\subsubsection{Package : {\bf fr.lri.iasi.somewhere.baseApp.commands}}
Here is a list of the classes of this package and their description :
\begin{description}
	\item[ChangeLogLevelCommand] Responsible for changing the application's logger level at runtime.
	\item[ImportConfigurationFileCommand] Import a XML configuration file(s). File(s) must respect XML schemas indicated on every configurable module (these schemas and its description are specified on the \emph{User's manual}).
	\item[ModulesListCommand] Outputs a list a concrete modules currently loaded on the \emph{Somewhere2}.
	\item[ShowConfigurationCommand] Outputs a XML visualization of the current configuration used by all modules.
\end{description}

\subsubsection{Package : {\bf fr.lri.iasi.somewhere.baseApp.configuration}}
Here is a list of the classes of this package and their description :
\begin{description}
	\item[BaseXMLConfiguration] Parse configuration of \emph{baseApp} module. This configuration mainly specify the loading order of each module and the log file path. For more details see the \emph{baseApp} configuration section in the \emph{User's manual}.
\end{description}

\subsubsection{Package : {\bf fr.lri.iasi.somewhere.baseApp.logging}}
Here is a list of the classes of this package and their description :
\begin{description}
	\item[AlertLoggerLayout] Formats the events messages over the SL4J log.
\end{description}

\subsubsection{Dependencies}
Here are the dependencies of the \textbf{BaseApp} module :
\begin{description}
	\item[BaseI module] Since the \emph{baseApp} module uses lots of interfaces defined there.
	\item[Others Somewhere modules] For using them, and adding them to the list of concrete modules to be loaded.
\end{description}

\subsection{CLI Module}
The CLI module extends the \textbf{BaseApp} module being main the entry point of the \emph{Somewhere2} application. Its provides a command line interface, based on library \textbf{JOpt-simple}, to parse initialization parameters to use them as commands for  module \textbf{ui}.

The main idea is that when a command line parameter begin with \verb!--!, it is viewed as a \textbf{ui} command, and following parameters are thus viewed as parameters of this command. The command's attribute "name" indicates the string used to trigger the command execution.
 
For example, executing \emph{Somewhere2} with the following command line parameters, executes commands \textbf{importConfigurationFile} (with parameters \verb!config.xml!) and \textbf{console} (without any parameters): \\
 
\begin{lstlisting}
--importConfigurationFile config.xml --console
\end{lstlisting}

\subsubsection{Package : {\bf fr.lri.iasi.somewhere.cli}}
Here is a list of the classes of this package and their description :
\begin{description}
	\item[Cli] Concrete implementation of \emph{fr.lri.iasi.somewhere.baseApp.AbstractApp}.
	\item[CmdLineParser] Uses \textbf{JOpt-simple} to parse initialization arguments string and trigger correspondent commands.
	\item[Main] Main \emph{Somewhere2} class. Start the execution of the application.
\end{description}

\subsubsection{Package : {\bf fr.lri.iasi.somewhere.cli.commands}}
Here is a list of the classes of this package and their description :
\begin{description}
	\item[ListenRemoteCmdsCommand] Listen and parse commands passed from outside the application execution. Normally a tool like Netcat is used to send the command string to the distant \emph{Somewhere2} application.
	\item[RedirectOutputCommand] Can redirect the output either to the console, a file or a remote stream.
	\item[StopListeningRemoteCmdsCommand] Stop the listen remote commands action.
\end{description}

\subsubsection{Dependencies}
The dependencies of the \textbf{CLI} module are the same of the \textbf{BaseApp} module (since it is a extension of it) and also, the \emph{JOpt-simple} library.

\subsection{Common related questions}

\subsubsection{How to create a new Module}
\label{subsubsec:createmodule}

In order to create a new \emph{Somewhere2} concrete module, it is necessary to create a new class that extends the \emph{AbstractModule} class. Each module must have a name, and it may has associated configuration and commands. Listing \ref{dummyModule} shows a class named \emph{DummyManager}, that illustrates the creation of the main class of a module on \emph{Somewhere2} : \\

\begin{lstlisting}[language=java,
	label=dummyModule,
	caption={The illustrative DummyManager class.}]
/**
 * The main class of the Dummy module
 */
public class DummyManager extends AbstractModule {

    /* --- Constants --- */
    /** The name of this module */
    protected static final String moduleName = "dummy";
    
    /* --- Properties --- */
    /** The configuration for this module */
    protected final DummyXMLConfiguration configuration;
    
    public DummyManager(App app) {
        super(app);
        
        // Init conf
        configuration = new DummyXMLConfiguration(this);

        // Add Commands to the UI
        addBaseCommands();
    }
    
    /* --- Accessors --- */
    public String getModuleName() {
        return moduleName;
    }
    
    public DummyXMLConfiguration getDummyConfiguration() {
    	return configuration;
    }
    
    /* --- Methods --- */
    public void init() {
    	// Nothing by the moment...
    }

    public void quit() {
    	// Nothing by the moment...
    }

    /**
     * Add dummy commands to the UI
     */
    protected void addBaseCommands() {
        UI ui = app.getUI();
        ui.addCommand(new DummyCommand(this));
    }

    /**
     * Import configuration file.
     */
    public void importConfigXML(Node config) {
    	configuration.parseConfig(config);
    }
    
    /**
     * Export configuration file
     */
    public Node exportConfigXML() {
    	try {
            return configuration.export();
        } catch (final Exception e) {
            throw new SomewhereException(e.getMessage(), e);
        }
    }
    
}  
\end{lstlisting}

Some examples of concrete modules classes like the \emph{DummyManager} are: \emph{CommunicationManager}, \emph{BenchmarkManager}, \emph{PLogicManager}, \emph{ShoalManager}, \emph{JGroupsManager} and \emph{JXTAManager}.

For this new module, it is recommended that a new sub-project on the \emph{Somewhere2} root folder would be created. This sub-project need to be considered as a Maven module, having, its own \emph{pom.xml} file referencing the main \emph{Somewhere2} \emph{pom.xml} as its parent \emph{pom}. If you plan to interact with another module, you have to add it as a Maven dependency. Listing \ref{dummyRefParent} illustrates this explanation: \\

\begin{lstlisting}[language=XML, 
		morekeywords={project, modelVersion, artifactId, name, parent, groupId, version, dependencies, dependency},
		label=dummyRefParent,
		caption={Excerpt of the \emph{pom.xml} file on the Dummy module. The \emph{parent} section refers to the parent \emph{pom.xml}.}]
		
<?xml version="1.0"?>
<project
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
	xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<modelVersion>4.0.0</modelVersion>
	<artifactId>swr.dummy</artifactId>
	<name>Somewhere Dummy Module</name>

	<parent>
		<groupId>fr.lri.iasi.somewhere</groupId>
		<artifactId>Somewhere</artifactId>
		<version>1.0</version>
	</parent>
	
	<dependencies>
		<!-- Adding dependency to the swr.ui module -->
		<dependency>
			<groupId>fr.lri.iasi.somewhere</groupId>
			<artifactId>swr.ui</artifactId>
			<version>1.0</version>
		</dependency>
	</dependencies>
	
	...
	
</project>
\end{lstlisting}

Also, every package in the new module must follow the namespace convention of the \emph{Somewhere2} project. For example, for the new \emph{Dummy} module, a project named \emph{swr.dummy} will be created, having its first source package named as \emph{fr.lri.iasi.somewhere.dummy}. Figure \ref{fig:dummyArborescence} illustrates the arborescence of the new \emph{Dummy} module.

\begin{figure} [!hbt]
\begin{centering}
	  \hspace*{0.7in}
      \includegraphics[width=0.6\textwidth]{figures/dummyArborescence}
\end{centering}
\caption{\label{fig:dummyArborescence} \emph{Dummy} module arborescence.}
\end{figure}

For each new module, it will be necessary to either modify or create a new profile on the main \emph{pom.xml} (in the root folder of \emph{Somewhere2}) and in the \emph{pom.xml} of the \emph{baseApp} module. More details about building profiles on Section \ref{sec:architecture}.

\subsubsection{How to change a module's loading order}

When building new a module, the user may decide that this module must be loaded after a specific module. For example, it makes no sense to load a concrete communication module before loading the main communication module. By default, this is the \emph{Somewhere2} module loading order:

\begin{description}
	\item [App class] The main \emph{App} class must be the first module to be loaded. It order is specified as \emph{0} (readonly).
	\item [AbstractModule class] Any subclass of the \emph{AbstractModule} class has \emph{1} as its loading order. It means that a module may never be loaded before the main \emph{App} module.
	\item [AbstractCommunicationModule class] Any subclass of the \emph{AbstractCommunicationModule} class has \emph{3} as its loading order. Normally they are the last ones to be loaded.
\end{description}

When \emph{Somewhere2} is quitting, the modules will be unloaded at the inverse order that they were loaded. 

In order to change the loading order of a new module (only if really necessary), the developer must use the \emph{setLoadingOrder} method of it's module class, on its constructor. For example:

\begin{lstlisting}[language=java,
	label=loadingOrder,
	caption={Changing the DummyManager loading order class.}]
/**
 * The main class of the Dummy module
 */
public class DummyManager extends AbstractModule {
    
    public DummyManager(App app) {
        ...
        setLoadingOrder(2);
        ...
    }
    
    ...
    
}	
\end{lstlisting}

\subsubsection{How to create a new Command}

In order to create a concrete module command, it is necessary to create a new class that implements the \emph{Command} interface. Each command must have a name, description, and it needs be attached to the correspondent module manager class on its \emph{addBaseCommands} method. By convention, all commands classes of a module must be placed on a specific package inside it. It would be desirable that this package would be called \emph{fr.lri.iasi.somewhere.moduleName.commands}.

The command class implementation is mainly based on the overwriting of its \emph{execute} method. This method must trigger an action and use an implementation of the \emph{ReturnStatus} interface to output its result. Some \emph{ReturnStatus} concrete implementations (like \emph{GenericReturnStatus}) resides on the \emph{ui} module.

Listing \ref{dummyCommand} illustrates a command class implementation on \emph{Somewhere2}. \\

\begin{lstlisting}[language=java,
	label=dummyCommand,
	caption={The illustrative DummyCommand class.}]
/**
 * A dummy command of the Dummy module
 */
public class DummyCommand implements Command {

	/* --- Accessors --- */
	@Override
	public String getName() {
		return "doDummy";
	}

	@Override
	public String getDescription() {
		return "Do dummy stuff";
	}
	
	/* --- Methods --- */
	/**
	* Do dummy action, writing parameter on the user output
	*/
	@Override
	public ReturnStatus execute(List<Object> parameters) {
		ReturnStatus res = null;
		
		try {
			// Verify if the parameters are good (no parameters in this case...)
			if (parameters != null && parameters.size() != 1) {
				res = new GenericReturnStatus("Syntax Error! \n Usage :" + "\t" + getName() + " [WORD_TO_WRITE]", false, null);
					
				return res;
			}
			
			String result = "Writing " +  parameters[0] + " on the user default output";
            
            /* Creates generic return status sending back the result string, the value "true" indicating the normal ending of the
            command execution and a null value representing the XML encoding of the result string */
			res = new GenericReturnStatus(
            		result, 
            		true,
            		null);
            		
		} catch (Exception e) {
			res = new GenericReturnStatus("There was an error starting the benchmark ...", false, null);
		}
	}
    
}  
\end{lstlisting}

Some examples of command classes like the \emph{DummyCommand} are: \emph{ImportConfigurationFileCommand}, \emph{ModuleListCommand}, \emph{ShowConfigurationCommand}, \emph{ConsoleCommand} and many other commands that are located on the commands package of different concrete modules.

By indicating the name of the command on specific user interface (CLI or console) the command is triggered and its result is sent to the selected output stream. For example, in order to execute the \emph{importConfigurationFile} command during the \emph{Somewhere2} initialization we would enter: \\

\begin{lstlisting}
	java -jar somewhere2.jar -importConfigurationFile file1.xml
\end{lstlisting}

In order to execute the same command in the console interface we would enter (after the \$ sign): \\

\begin{lstlisting}
	15H 47M 10S \$ importConfigurationFile file1.txt
\end{lstlisting}

\subsubsection{How to create a specify a module Configuration}

\emph{Somewhere2} was design to allow module configuration at runtime. In that way, parameters can be assigned to a module by the user at any moment of \emph{Somewhere2} execution.

In order to create set up a configuration system for a newly created module. A configuration class must extend the abstract \emph{Configuration} class of the \emph{baseI} module. Also, by convention, all configuration classes of a module must be placed on a specific package inside it. It would be desirable that this package would be called \emph{fr.lri.iasi.somewhere.moduleName.configuration}.

A concrete configuration class, normally, is designed to be configurable by XML. However, a XML schema need to be specified in order to the module would be configurable by a XML file. This schema have to be placed into the \emph{resources/META-INF} folder of the sub-project and it must respect the schema name convention (\emph{moduleName\_config.xsd}) and the structure presented on Listing \ref{schemaStructure}. \\

\begin{lstlisting}[language=XML, 
		morekeywords={xsd:element, xsd:complexType, type, name, encoding, xsd:schema, xsd:all, xmlns:xsd},
		label=schemaStructure,
		caption={XML schema configuration structure.}]
		
<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">

	<xsd:element name="somewhere">
		<xsd:complexType>

			<xsd:all>
				<xsd:element name="moduleName" type="moduleName" />
			</xsd:all>

		</xsd:complexType>
	</xsd:element>
	
	<xsd:complexType name="moduleName">

		<!-- Specific module options -->		
		
	</xsd:complexType>
	
</xsd:schema> 
\end{lstlisting}

When read a XML configuration file for a given module (specified by the name of a balise), the \emph{Somewhere2} will try to validate the XML file with the module's schema. If the configuration file is not valid an error will be thrown and the configuration will not be accepted.

If accepted, the configuration class can use some functionalities of the helper class \emph{XPathReader} in order to query XML configuration files and obtain the desired parameters. Listing \ref{dummyConfiguration} illustrates this explanation through the implementation of the \emph{DummyConfiguration} class: \\

\begin{lstlisting}[language=java,
	label=dummyConfiguration,
	caption={The illustrative DummyConfiguration class.}]
/**
 * XML configuration of dummy module
 */
public class DummyXMLConfiguration extends Configuration {

	public DummyXMLConfiguration(Module module) {
		super(module);
	}
	
	/**
	* Parse XML configuration node to obtain corresponding configuration properties
	*/
	public void parseConfig(final Node config) {
		try {
			// Verify if there is a valid config
			if (config == null) {
				return;
			}
            
			final XPathReader reader = new XPathReader(config);
			parseOutputOptions(reader);

		} catch (final Exception e) {
			logger.error("There were errors parsing XML configuration file : " + e.getMessage());
			throw new SomewhereException(e.getMessage(), e);
		}
	}
    
	/**
	* Parse XML configuration file, part 'dummyConfSection' search for the boolean 'dummyOption' option
	*/
    private void parseOutputOptions(XPathReader reader) throws Exception {
    	String showOutputStr = reader.read("dummyConfSection/dummyOption", XPathConstants.STRING).toString();
    	if (!showOutputStr.isEmpty())
    		showOutput = Boolean.parseBoolean(showOutputStr);
    }
    
    ...
}
\end{lstlisting}
