/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;

import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.api.communication.model.PeerListener;

import org.junit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;


public class PeerTest {
    @Before
    public void Init() {
    }

    @Test
    public void cloneAreEquals() {
        Peer p1 = new Peer("p1");
        Peer p2 = new Peer(p1);
        assertEquals(p1, p2);
    }

    @Test
    public void NewlyCreatedEquals() {
        Peer p1 = new Peer("p1");
        Peer p2 = new Peer("p1");
        assertEquals(p1, p2);
    }

    @Test
    public void nameIsCorrect() {
        Peer p1 = new Peer("p1");
        Peer p2 = new Peer("p2");
        assertEquals(p1.getName(), "p1");
        assertEquals(p2.getName(), "p2");
    }

    @Test(expected = Exception.class)
    public void cannotChangePeerListenerWithGetPeerListeners() {
        Peer p1 = new Peer("p1");
        p1.addPeerListener(new FakePeerListener());

        List<PeerListener> l = p1.getPeerListeners();
        l.add(new FakePeerListener());
    }

    @Test
    public void canUpdatePeerListeners() {
        Peer p1 = new Peer("p1");
        FakePeerListener a = new FakePeerListener();
        FakePeerListener b = new FakePeerListener();
        FakePeerListener c = new FakePeerListener();
        p1.addPeerListener(a);
        p1.addPeerListener(b);
        assertTrue(p1.getPeerListeners().contains(a));
        assertTrue(p1.getPeerListeners().contains(b));
        assertFalse(p1.getPeerListeners().contains(c));
        p1.removePeerListener(b);
        assertTrue(p1.getPeerListeners().contains(a));
        assertFalse(p1.getPeerListeners().contains(b));
        assertFalse(p1.getPeerListeners().contains(c));
        p1.removePeerListener(c);
        assertTrue(p1.getPeerListeners().contains(a));
        assertFalse(p1.getPeerListeners().contains(b));
        assertFalse(p1.getPeerListeners().contains(c));
    }

    @Test
    public void launchFirePeerIsConnectingCorrectly() {
        FakePeer p1 = new FakePeer("p1");
        FakePeerListener a = new FakePeerListener();
        FakePeerListener b = new FakePeerListener();
        FakePeerListener c = new FakePeerListener();
        p1.addPeerListener(a);
        p1.addPeerListener(b);
        p1.addPeerListener(c);

        p1.connect();
        assertEquals(a.isConnecting, 1);
        assertEquals(a.isLeaving, 0);
        assertEquals(b.isConnecting, 1);
        assertEquals(b.isLeaving, 0);
        assertEquals(c.isConnecting, 1);
        assertEquals(c.isLeaving, 0);
        assertEquals(p1.connection, 1);
        assertEquals(p1.disconnection, 0);

        p1.connect();
        assertEquals(a.isConnecting, 1);
        assertEquals(a.isLeaving, 0);
        assertEquals(b.isConnecting, 1);
        assertEquals(b.isLeaving, 0);
        assertEquals(c.isConnecting, 1);
        assertEquals(c.isLeaving, 0);
        assertEquals(p1.connection, 1);
        assertEquals(p1.disconnection, 0);
    }

    @Test
    public void launchFirePeerIsDisconnectingCorrectly() {
        FakePeer p1 = new FakePeer("p1");
        FakePeerListener a = new FakePeerListener();
        FakePeerListener b = new FakePeerListener();
        FakePeerListener c = new FakePeerListener();
        p1.addPeerListener(a);
        p1.addPeerListener(b);
        p1.addPeerListener(c);

        p1.connect();

        p1.disconnect();
        assertEquals(a.isConnecting, 1);
        assertEquals(a.isLeaving, 1);
        assertEquals(b.isConnecting, 1);
        assertEquals(b.isLeaving, 1);
        assertEquals(c.isConnecting, 1);
        assertEquals(c.isLeaving, 1);
        assertEquals(p1.connection, 1);
        assertEquals(p1.disconnection, 1);

        p1.disconnect();
        assertEquals(a.isConnecting, 1);
        assertEquals(a.isLeaving, 1);
        assertEquals(b.isConnecting, 1);
        assertEquals(b.isLeaving, 1);
        assertEquals(c.isConnecting, 1);
        assertEquals(c.isLeaving, 1);
        assertEquals(p1.connection, 1);
        assertEquals(p1.disconnection, 1);
    }

    @Test
    public void hashIsNameHash() {
        Peer p1 = new Peer("p1");
        Peer p2 = new Peer("p2");
        assertEquals(p1.hashCode(), "p1".hashCode());
        assertEquals(p2.hashCode(), "p2".hashCode());
    }
}


class FakePeer extends Peer {
    private static final long serialVersionUID = 42L;
    int connection = 0;
    int disconnection = 0;

    public FakePeer(String name) {
        super(name);
    }

    @Override
    protected void initConnection() {
        connection++;
    }

    @Override
    protected void initDisconnection() {
        disconnection++;
    }
}


class FakePeerListener implements PeerListener {
    int isConnecting = 0;
    int isLeaving = 0;

    public void peerIsConnecting(Peer peer) {
        isConnecting++;
    }

    public void peerIsLeaving(Peer peer) {
        isLeaving++;
    }
}
