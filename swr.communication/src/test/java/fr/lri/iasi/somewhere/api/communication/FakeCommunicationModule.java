/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication;

import org.w3c.dom.Node;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.Peer;

public class FakeCommunicationModule extends AbstractCommunicationModule<FakePeerGroup, Peer>{

	public FakeCommunicationModule(App app) {
		super(app);
		
		this.peerGroup = new FakePeerGroup(this);
	}

	@Override
	public void quit() {
	}

	@Override
	public String getModuleName() {
		return null;
	}

	@Override
	public void init() {
	}

	@Override
	public void importConfigXML(Node config) {
	}

	@Override
	public Node exportConfigXML() {
		return null;
	}

	@Override
	public void restart() {
	}

	@Override
	public Peer createPeer(String peerName) {
		return new Peer(peerName);
	}

	@Override
	public Message createMessage(Message msg) {
		return null;
	}

}
