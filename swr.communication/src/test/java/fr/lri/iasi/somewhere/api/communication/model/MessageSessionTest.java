/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.FakeCommunicationApp;
import fr.lri.iasi.somewhere.api.communication.FakeCommunicationModule;
import fr.lri.iasi.somewhere.api.communication.FakeMessageReceiver;
import fr.lri.iasi.somewhere.api.communication.FakeMessageSender;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageProcessor;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageType;

public class MessageSessionTest {
	
	CommunicationManager communicationManager;
	
	@Before
	public void Init() {
		App app = new FakeCommunicationApp();
		communicationManager = new CommunicationManager(app);
		communicationManager.setMessageReceiver(new FakeMessageReceiver(communicationManager));
		communicationManager.setMessageSender(new FakeMessageSender(communicationManager));
		app.getModules().add(communicationManager);
		
		FakeCommunicationModule fakeCommunicationModule = new FakeCommunicationModule(app);
		fakeCommunicationModule.getPeerGroup().setLocalPeer(new Peer("src"));
		communicationManager.setCommunicationModule(fakeCommunicationModule);
	}
	
	public Message createMessageHelper(String id, Class<? extends AbstractMessageType> clazz, String peerNameSrc, String peerNameDst) {
		Peer src = new Peer(peerNameSrc);
        Peer dst = new Peer(peerNameDst);
        AbstractMessageType msgType = MessageTypeSingletonFactory.create(clazz);
        Map<String, Object>msgContents = new HashMap<String, Object>();
		return new Message(id, src, dst, msgType, msgContents);
	}
	
	@Test
	public void sendQueryBehaviorCorrectly() throws Exception {
		Message m1 = createMessageHelper(null, FakeQueryMessageType.class, "src", "dst");
		m1.addMessageListener(communicationManager);
		m1.messageIsToBeSent();
		m1.sent();
		
		assertEquals(communicationManager.getSessionHandler().getCreatorSessions().size(), 1);
		assertEquals(communicationManager.getSessionHandler().getMessageSessions().size(), 1);
	}

	@Test
	public void receiveQueryBehaviorCorrectly() throws Exception {
		Message m1 = createMessageHelper("session1", FakeQueryMessageType.class, "dst", "src");
		m1.addMessageListener(communicationManager);
		m1.messageIsToBeReceived();
		m1.received();
		
		assertNotNull(communicationManager.getSessionHandler().getCreatorSessions().get("session1"));
		assertEquals(communicationManager.getSessionHandler().getCreatorSessions().size(), 1);
		assertEquals(communicationManager.getSessionHandler().getMessageSessions().size(), 1);
	}
	
	@Test
	public void sendAnswerBehaviorCorrectly() throws Exception {
		Message m1 = createMessageHelper("session1", FakeQueryMessageType.class, "dst", "src");
		m1.addMessageListener(communicationManager);
		m1.messageIsToBeReceived();
		m1.received();
		
		MessageSession session = communicationManager.getSessionHandler().getCreatorSessions().get("session1");
		Message m2 = createMessageHelper(session.getSessionId(), FakeAnswerMessageType.class, "src", "dst");
		m2.addMessageListener(communicationManager);
		m2.messageIsToBeSent();
		m2.sent();
		
		
		assertNotNull(communicationManager.getSessionHandler().getCreatorSessions().get("session1"));
		assertEquals(communicationManager.getSessionHandler().getCreatorSessions().size(), 1);
		assertEquals(communicationManager.getSessionHandler().getMessageSessions().size(), 1);
	}
	
	
	@Test
	public void receiveAnswerBehaviorCorrectly() throws Exception {
		Message m1 = createMessageHelper(null, FakeQueryMessageType.class, "src", "dst");
		m1.addMessageListener(communicationManager);
		m1.messageIsToBeSent();
		m1.sent();
		
		Message m2 = createMessageHelper(m1.getId(), FakeAnswerMessageType.class, "dst", "src");
		m2.addMessageListener(communicationManager);
		m2.messageIsToBeReceived();
		m2.received();
		
		assertEquals(communicationManager.getSessionHandler().getCreatorSessions().size(), 1);
		assertEquals(communicationManager.getSessionHandler().getMessageSessions().size(), 1);
	}
	
	@Test
	public void sendFinishBehaviorCorrectly() throws Exception {
		Message m1 = createMessageHelper("session1", FakeQueryMessageType.class, "dst", "src");
		m1.addMessageListener(communicationManager);
		m1.messageIsToBeReceived();
		m1.received();
		
		MessageSession session = communicationManager.getSessionHandler().getCreatorSessions().get("session1");
		Message m2 = createMessageHelper(session.getSessionId(), FakeFinishMessageType.class, "src", "dst");
		m2.addMessageListener(communicationManager);
		m2.messageIsToBeSent();
		m2.sent();
		
		assertEquals(communicationManager.getSessionHandler().getCreatorSessions().size(), 0);
		assertEquals(communicationManager.getSessionHandler().getMessageSessions().size(), 0);
	}
	
	@Test
	public void receiveFinishBehaviorCorrectly() throws Exception {
		Message m1 = createMessageHelper(null, FakeQueryMessageType.class, "src", "dst");
		m1.addMessageListener(communicationManager);
		m1.messageIsToBeSent();
		m1.sent();
		
		Message m2 = createMessageHelper(m1.getId(), FakeFinishMessageType.class, "dst", "src");
		m2.addMessageListener(communicationManager);
		m2.messageIsToBeReceived();
		m2.received();
		
		assertEquals(communicationManager.getSessionHandler().getCreatorSessions().size(), 0);
		assertEquals(communicationManager.getSessionHandler().getMessageSessions().size(), 0);
	}
	
}

@SuppressWarnings("serial")
class FakeQueryMessageType extends AbstractMessageType {

	@Override
	public MessageProcessor createProcessor(MessageSession session) {
		return new FakeProcessor(session);
	}

	@Override
	public boolean needsNewSessionSend() {
		return true;
	}
	
	@Override
	public boolean needsNewSessionReceive() {
		return true;
	}

	@Override
	public boolean needsToMaintainSessionSend(MessageSession session) {
		return true;
	}
	
	@Override
	public boolean needsToMaintainSessionReceive(MessageSession session) {
		return true;
	}

	@Override
	public String getName() {
		return "query";
	}

	@Override
	public boolean isReturningType() {
		return false;
	}

	@Override
	public String print(Message message) {
		return null;
	}
    
}

@SuppressWarnings("serial")
class FakeAnswerMessageType extends AbstractMessageType {

	@Override
	public MessageProcessor createProcessor(MessageSession session) {
		return new FakeProcessor(session);
	}
	
	@Override
	public boolean needsNewSessionSend() {
		return false;
	}
	
	@Override
	public boolean needsNewSessionReceive() {
		return false;
	}

	@Override
	public boolean needsToMaintainSessionSend(MessageSession session) {
		return true;
	}
	
	@Override
	public boolean needsToMaintainSessionReceive(MessageSession session) {
		return true;
	}

	@Override
	public String getName() {
		return "answer";
	}

	@Override
	public boolean isReturningType() {
		return true;
	}

	@Override
	public String print(Message message) {
		return null;
	}
    
}


@SuppressWarnings("serial")
class FakeFinishMessageType extends AbstractMessageType {

	@Override
	public MessageProcessor createProcessor(MessageSession session) {
		return new FakeProcessor(session);
	}
	
	@Override
	public boolean needsNewSessionSend() {
		return false;
	}
	
	@Override
	public boolean needsNewSessionReceive() {
		return false;
	}

	@Override
	public boolean needsToMaintainSessionSend(MessageSession session) {
		return false;
	}
	
	@Override
	public boolean needsToMaintainSessionReceive(MessageSession session) {
		return false;
	}

	@Override
	public String getName() {
		return "finish";
	}

	@Override
	public boolean isReturningType() {
		return true;
	}

	@Override
	public String print(Message message) {
		return null;
	}
    
}

class FakeProcessor extends AbstractMessageProcessor {

	public FakeProcessor(MessageSession session) {
		super(session);
	}

	@Override
	public void process(Message message) {
		//
	}   
}

