/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.api.communication.AlreadyReceivedException;
import fr.lri.iasi.somewhere.api.communication.AlreadySentException;
import fr.lri.iasi.somewhere.api.communication.AlreadySetToBeReceivedException;
import fr.lri.iasi.somewhere.api.communication.AlreadySetToBeSentException;
import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.FakeCommunicationApp;
import fr.lri.iasi.somewhere.api.communication.FakeCommunicationModule;
import fr.lri.iasi.somewhere.api.communication.FakeMessageReceiver;
import fr.lri.iasi.somewhere.api.communication.FakeMessageSender;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageListener;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageType;
import fr.lri.iasi.somewhere.api.communication.model.types.SimpleMessageType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


public class MessageTest {
	CommunicationManager communicationManager;
	String id;
    Peer src;
    Peer dst;
    AbstractMessageType msgType;
    Map<String, Object> msgContents;
    Message m1;
    Message m2;

	@Before
    public void Init() throws CommunicationException {
    	App app = new FakeCommunicationApp();
    	communicationManager = new CommunicationManager(app);
    	communicationManager.setMessageReceiver(new FakeMessageReceiver(communicationManager));
		communicationManager.setMessageSender(new FakeMessageSender(communicationManager));
		app.getModules().add(communicationManager);

        src = new Peer("src");
        dst = new Peer("dst");
        msgType = MessageTypeSingletonFactory.create(SimpleMessageType.class);
        
        FakeCommunicationModule fakeCommunicationModule = new FakeCommunicationModule(app);
		fakeCommunicationModule.getPeerGroup().setLocalPeer(src);
		communicationManager.setCommunicationModule(fakeCommunicationModule);
        
        MessageBuilder builder = new MessageBuilder(communicationManager)
        			.setId("session1")
        			.setOrigin(src)
        			.setDestination(dst)
        			.setType(msgType);
		
		CommunicationManager.TEXT.addContent(builder, "content");
		
        m1 = builder.build();
    }

	@Test
    public void cloneAreEquals() {
        Message m2 = new Message(m1);
        assertEquals(m1, m2);
    }

    @Test
    public void SameParamsEquals() throws CommunicationException {
    	MessageBuilder builder = new MessageBuilder(communicationManager)
    				.setId("session1")
    				.setOrigin(src)
    				.setDestination(dst)
    				.setType(msgType);
		
		CommunicationManager.TEXT.addContent(builder, "content");
		
        Message m2 = builder.build();
        assertEquals(m1, m2);
    }

    @Test(expected = Exception.class)
    public void cannotChangeMessageListenerWithGetMessageListeners() {
        m1.addMessageListener(new FakeMessageListener());

        List<MessageListener> l = m1.getMessageListeners();
        l.add(new FakeMessageListener());
    }

    @Test
    public void canUpdateMessageListener() {
        FakeMessageListener a = new FakeMessageListener();
        FakeMessageListener b = new FakeMessageListener();
        FakeMessageListener c = new FakeMessageListener();
        m1.addMessageListener(a);
        m1.addMessageListener(b);
        assertTrue(m1.getMessageListeners().contains(a));
        assertTrue(m1.getMessageListeners().contains(b));
        assertFalse(m1.getMessageListeners().contains(c));
        m1.removeMessageListener(b);
        assertTrue(m1.getMessageListeners().contains(a));
        assertFalse(m1.getMessageListeners().contains(b));
        assertFalse(m1.getMessageListeners().contains(c));
        m1.removeMessageListener(c);
        assertTrue(m1.getMessageListeners().contains(a));
        assertFalse(m1.getMessageListeners().contains(b));
        assertFalse(m1.getMessageListeners().contains(c));
    }

    @Test
    public void launchFireMessageReceivedCorrectly() throws Exception {
        FakeMessage m2 = new FakeMessage(m1);
        FakeMessageListener a = new FakeMessageListener();
        FakeMessageListener b = new FakeMessageListener();
        FakeMessageListener c = new FakeMessageListener();
        m2.addMessageListener(a);
        m2.addMessageListener(b);
        m2.addMessageListener(c);
        
        m2.messageIsToBeReceived();
        assertEquals(a.hasBeenSent, 0);
        assertEquals(a.isToBeSent, 0);
        assertEquals(a.isToBeReceived, 1);
        assertEquals(a.hasBeenReceived, 0);
        assertEquals(b.hasBeenSent, 0);
        assertEquals(b.isToBeSent, 0);
        assertEquals(b.isToBeReceived, 1);
        assertEquals(b.hasBeenReceived, 0);
        assertEquals(c.hasBeenSent, 0);
        assertEquals(c.isToBeSent, 0);
        assertEquals(c.isToBeReceived, 1);
        assertEquals(c.hasBeenReceived, 0);
        assertEquals(m2.afterSending, 0);
        assertEquals(m2.messageToBeSent, 0);
        assertEquals(m2.messageToBeReceived, 1);
        assertEquals(m2.afterReception, 0);

        m2.received();
        assertEquals(a.hasBeenSent, 0);
        assertEquals(a.isToBeSent, 0);
        assertEquals(a.isToBeReceived, 1);
        assertEquals(a.hasBeenReceived, 1);
        assertEquals(b.hasBeenSent, 0);
        assertEquals(b.isToBeSent, 0);
        assertEquals(b.isToBeReceived, 1);
        assertEquals(b.hasBeenReceived, 1);
        assertEquals(c.hasBeenSent, 0);
        assertEquals(c.isToBeSent, 0);
        assertEquals(c.isToBeReceived, 1);
        assertEquals(c.hasBeenReceived, 1);
        assertEquals(m2.afterSending, 0);
        assertEquals(m2.messageToBeSent, 0);
        assertEquals(m2.messageToBeReceived, 1);
        assertEquals(m2.afterReception, 1);

    }

    @Test
    public void launchFireMessageSentCorrectly()
        throws Exception {
        FakeMessage m2 = new FakeMessage(m1);
        FakeMessageListener a = new FakeMessageListener();
        FakeMessageListener b = new FakeMessageListener();
        FakeMessageListener c = new FakeMessageListener();
        m2.addMessageListener(a);
        m2.addMessageListener(b);
        m2.addMessageListener(c);

        m2.messageIsToBeSent();
        assertEquals(a.hasBeenSent, 0);
        assertEquals(a.isToBeSent, 1);
        assertEquals(a.hasBeenReceived, 0);
        assertEquals(a.isToBeReceived, 0);
        assertEquals(b.hasBeenSent, 0);
        assertEquals(b.isToBeSent, 1);
        assertEquals(b.hasBeenReceived, 0);
        assertEquals(b.isToBeReceived, 0);
        assertEquals(c.hasBeenSent, 0);
        assertEquals(c.isToBeSent, 1);
        assertEquals(c.hasBeenReceived, 0);
        assertEquals(c.isToBeReceived, 0);
        assertEquals(m2.afterSending, 0);
        assertEquals(m2.messageToBeSent, 1);
        assertEquals(m2.afterReception, 0);
        assertEquals(m2.messageToBeReceived, 0);

        m2.sent();
        assertEquals(a.hasBeenSent, 1);
        assertEquals(a.isToBeSent, 1);
        assertEquals(a.isToBeReceived, 0);
        assertEquals(a.hasBeenReceived, 0);
        assertEquals(b.hasBeenSent, 1);
        assertEquals(b.isToBeSent, 1);
        assertEquals(b.hasBeenReceived, 0);
        assertEquals(b.isToBeReceived, 0);
        assertEquals(c.hasBeenSent, 1);
        assertEquals(c.isToBeSent, 1);
        assertEquals(c.hasBeenReceived, 0);
        assertEquals(c.isToBeReceived, 0);
        assertEquals(m2.afterSending, 1);
        assertEquals(m2.messageToBeSent, 1);
        assertEquals(m2.afterReception, 0);
        assertEquals(m2.messageToBeReceived, 0);

    }

    @Test(expected = AlreadySentException.class)
    public void cannotSendMsgTwoTimes() throws Exception {
        FakeMessage m2 = new FakeMessage(m1);
        FakeMessageListener a = new FakeMessageListener();
        FakeMessageListener b = new FakeMessageListener();
        FakeMessageListener c = new FakeMessageListener();
        m2.addMessageListener(a);
        m2.addMessageListener(b);
        m2.addMessageListener(c);

        m2.messageIsToBeSent();
        m2.sent();
        m2.sent();
    }
    
    @Test(expected = AlreadySetToBeSentException.class)
    public void cannotSetMessageToBeSentTwoTimes() throws Exception {
        FakeMessage m2 = new FakeMessage(m1);
        FakeMessageListener a = new FakeMessageListener();
        FakeMessageListener b = new FakeMessageListener();
        FakeMessageListener c = new FakeMessageListener();
        m2.addMessageListener(a);
        m2.addMessageListener(b);
        m2.addMessageListener(c);

        m2.messageIsToBeSent();
        m2.messageIsToBeSent();
    }
    
    @Test(expected = AlreadySetToBeReceivedException.class)
    public void cannotSetMessageToBeReceivedTwoTimes() throws Exception {
        FakeMessage m2 = new FakeMessage(m1);
        FakeMessageListener a = new FakeMessageListener();
        FakeMessageListener b = new FakeMessageListener();
        FakeMessageListener c = new FakeMessageListener();
        m2.addMessageListener(a);
        m2.addMessageListener(b);
        m2.addMessageListener(c);

        m2.messageIsToBeReceived();
        m2.messageIsToBeReceived();
    }
    
    @Test(expected = AlreadyReceivedException.class)
    public void cannotReceiveMsgTwoTimes() throws Exception {
        FakeMessage m2 = new FakeMessage(m1);
        FakeMessageListener a = new FakeMessageListener();
        FakeMessageListener b = new FakeMessageListener();
        FakeMessageListener c = new FakeMessageListener();
        m2.addMessageListener(a);
        m2.addMessageListener(b);
        m2.addMessageListener(c);

        m2.messageIsToBeReceived();
        m2.received();
        m2.received();
    }
}


class FakeMessage extends Message {
    private static final long serialVersionUID = 42L;
    int messageToBeReceived = 0;
    int afterReception = 0;
    int afterSending = 0;
    int messageToBeSent = 0;

    public FakeMessage(String id, Peer origin, Peer destination, AbstractMessageType type,
        Map<String, Object> contents) {
        super(id, origin, destination, type, contents);
    }

    public FakeMessage(Message message) {
        super(message);
    }

    @Override
    protected void initReception() {
    	messageToBeReceived++;
    }
    
    @Override
    protected void afterReception() {
        afterReception++;
    }

    @Override
    protected void afterSending() {
        afterSending++;
    }

    @Override
    protected void initMessageToBeSent() {
        messageToBeSent++;
    }
}


class FakeMessageListener implements MessageListener {
    int isToBeSent = 0;
    int hasBeenSent = 0;
    int isToBeReceived = 0;
    int hasBeenReceived = 0;

    public void messageIsToBeSent(Message message) {
        isToBeSent++;
    }

    public void messageHasBeenSent(Message message) {
        hasBeenSent++;
    }

    public void messageHasBeenReceived(Message message) {
    	hasBeenReceived++;
    }

	public void messageIsToBeReceived(Message message) {
		isToBeReceived++;
	}
}
