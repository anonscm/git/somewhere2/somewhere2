/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.ReturnStatus;


public class CommunicationManagerTest {
	
	private CommunicationManager communicationManager;
	
	@Before
    public void Init() {
		App app = new FakeCommunicationApp();
		communicationManager = new CommunicationManager(app);
		communicationManager.setMessageReceiver(new FakeMessageReceiver(communicationManager));
		communicationManager.setMessageSender(new FakeMessageSender(communicationManager));
		app.getModules().add(communicationManager);
		
		FakeCommunicationModule fakeCommunicationModule = new FakeCommunicationModule(app);
		fakeCommunicationModule.getPeerGroup().setLocalPeer(new Peer("localhost"));
		communicationManager.setCommunicationModule(fakeCommunicationModule);
    }
	
	@Test
	public void doLoopbackSucefullyTest() {
		Command sendTextMessage = communicationManager.getApp().getUI().getCommand("sendTextMessage");
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("localhost");
		parameters.add("loop!");
		
		assertTrue(sendTextMessage.execute(parameters).isSuccessful());
		assertEquals(communicationManager.getSessionHandler().getMessageSessions().size(), 0);
		assertEquals(communicationManager.getSessionHandler().getCreatorSessions().size(), 0);
	}
	
	@Test
	public void pingLoopbackSucefullyTest() {
		Command ping = communicationManager.getApp().getUI().getCommand("ping");
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("localhost");
		
		assertTrue(ping.execute(parameters).isSuccessful());
		assertEquals(communicationManager.getSessionHandler().getMessageSessions().size(), 0);
		assertEquals(communicationManager.getSessionHandler().getCreatorSessions().size(), 0);
	}
	
	@Test
	public void listPeersTest() {
		communicationManager.getCommunicationModule().getPeerGroup().retrievePeer("neighbor1");
		communicationManager.getCommunicationModule().getPeerGroup().retrievePeer("neighbor2");
		communicationManager.getCommunicationModule().getPeerGroup().retrievePeer("neighbor3");
		
		Command listPeersCommunication = communicationManager.getApp().getUI().getCommand("listPeers");
		ReturnStatus res = listPeersCommunication.execute(null);
		
		String resultString = "neighbor2 : Connected\nneighbor3 : Connected\nneighbor1 : Connected\n";
		
		assertEquals(res.getTextualInfos(), resultString);
	}

}
