/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.heartbeat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageProcessor;
import fr.lri.iasi.somewhere.ui.UI;

/**
 * Responsible for processing a received pong message.
 * 
 * @author Andre Fonseca
 */
public class PongMessageProcessor extends AbstractMessageProcessor {
	static Logger logger = LoggerFactory.getLogger(PongMessageProcessor.class);
	
    public PongMessageProcessor(MessageSession session) {
        super(session);
    }

    /* --- Methods --- */
    /**
     * Process of pong message: whenever a pong message arrives and it is processed, it will print
     * an arrival message on the output stream.
     * 
     * @param message : the message to be processed
     */
    @Override
	public void process(Message message) {
		
		UI ui = session.getCommunicationManager().getApp().getUI();
		ui.print("\nPong message " +
	            message.getType().print(message));	
	}

}
