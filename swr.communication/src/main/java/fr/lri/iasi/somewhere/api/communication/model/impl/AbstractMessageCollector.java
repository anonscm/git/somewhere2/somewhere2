/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageAdapter;
import fr.lri.iasi.somewhere.api.communication.model.MessageTypeSingletonFactory;
import fr.lri.iasi.somewhere.ui.UI;

/**
 * Describes an abstract message collector. A message collector is a listener that is
 * responsible for collecting messages of an specific type for showing them to the user.
 * 
 * @author Andre Fonseca
 *
 */
public abstract class AbstractMessageCollector extends MessageAdapter {
	
	/* --- Properties --- */
	/** Module Manager to be used */
    protected final CommunicationManager communicationManager;
    
    /** Types of messages that are collected by this listener */
    protected final Set<AbstractMessageType> acceptedTypes = new HashSet<AbstractMessageType>();
    
    /** The main app user interface */
    protected final UI ui;
	
    public AbstractMessageCollector(CommunicationManager communicationManager) {
		if (communicationManager == null)
            throw new IllegalArgumentException("communicationManager");

        this.communicationManager = communicationManager;
        this.communicationManager.addMessageListener(this);
        this.ui = communicationManager.getApp().getUI();
	}
    
    /* --- Accessors --- */
    public Set<AbstractMessageType> getAcceptedTypes() {
    	return Collections.unmodifiableSet(this.acceptedTypes);
    }
    
    /* --- Mutators --- */
    protected void addAcceptedTypes(Class<? extends AbstractMessageType> messageTypeClass) {
    	AbstractMessageType type = MessageTypeSingletonFactory.create(messageTypeClass);
    	this.acceptedTypes.add(type);
    }
	
    /* --- Methods --- */
    /**
     * Finishes the reception of messages.
     */
	public void finishReceiving() {
		this.communicationManager.removeMessageListener(this);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public abstract void messageHasBeenSent(Message message);
	
}
