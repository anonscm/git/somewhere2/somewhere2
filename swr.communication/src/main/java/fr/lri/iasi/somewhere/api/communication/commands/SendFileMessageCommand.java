/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.commands;

import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Responsible for sending file to distant peer.
 * TODO: NOT ready to be used!
 * 
 * @author Andre Fonseca
 *
 */
public class SendFileMessageCommand extends SendTextMessageCommand {
	static Logger errorLogger = LoggerFactory.getLogger("errorLog");
	
    public SendFileMessageCommand(CommunicationManager moduleManager) {
        super(moduleManager);
    }

    /* --- Accessors --- */
    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "sendFileMessage";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return "Send the content of a file as a message to a peer";
    }

    /* --- Methods --- */
    /**
     * Send file message to distant peer.
     * @param parameters : list containing, first, the name of the destination peer and second, the path of file to be sent.
     * @return the return status
     */
    @Override
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;
        BufferedReader in = null;

        try {
            // Verify there are enough parameters
            if (parameters.size() < 2) {
                res = new GenericReturnStatus("Syntax Error! \n Usage :" +
                        "\t" + getName() + " DESTINATION FILE_PATH", false, null);

                return res;
            }

            // Get useful parameters
            String destination = parameters.get(0).toString();
            String path = parameters.get(1).toString();

            // Get the content of the file
            String thisLine = "";
            StringBuilder content = new StringBuilder();
            in = new BufferedReader(new FileReader(path));

            while ((thisLine = in.readLine()) != null)
                content.append(thisLine);

            // Send the message !
            this.getModuleManager().sendMessage(destination, content.toString());
            
            res = new GenericReturnStatus("Message Successfully sent", true,
                    null);
            
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error sending message ...",
                    false, null);
        } finally {
        	if (in != null) {
        		try {
					in.close();
				} catch (IOException e) {
					res = new GenericReturnStatus("There was an error sending message ...",
		                    false, null);
				}
        	}
        }

        return res;
    }
}
