/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.commands;

import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Responsible for sending text message to distant peer.
 * 
 * @author Andre Fonseca
 *
 */
public class SendTextMessageCommand implements Command {
	static Logger errorLogger = LoggerFactory.getLogger("errorLog");
	
    /* --- Properties --- */
	/** The communication manager module */
    private final CommunicationManager communicationManager;

    public SendTextMessageCommand(CommunicationManager moduleManager) {
        if (moduleManager == null) {
            throw new IllegalArgumentException("moduleManager");
        }

        this.communicationManager = moduleManager;
    }

    /* --- Accessors --- */
    public CommunicationManager getModuleManager() {
        return communicationManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "sendTextMessage";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return "Send a text message to a peer";
    }

    /* --- Methods --- */
    /**
     * Send text message to distant peer.
     * @param parameters : list containing, first, the name of the destination peer and second, the text to be sent.
     * @return the return status
     */
    @Override
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;

        try {
            // Verify there are enough parameters
            if (parameters.size() < 2) {
                res = new GenericReturnStatus("Syntax Error! \n Usage :" +
                        "\t" + getName() + " DESTINATION MESSAGE_CONTENT",
                        false, null);

                return res;
            }

            // Get useful parameters
            String destination = parameters.get(0).toString();
            String content = parameters.get(1).toString();

            // Send the message !
            this.communicationManager.sendMessage(destination, content);

            res = new GenericReturnStatus("Message Successfully sent", true,
                    null);
        } catch (Exception e) {
        	errorLogger.error("There was an error sending message", e);
        	
            res = new GenericReturnStatus("There was an error sending message ...",
                    false, null);
        }

        return res;
    }
    
}
