/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.commands;

import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import java.util.Collection;
import java.util.List;


/**
 * A command to print the list of peers
 * 
 * @author Andre Fonseca
 */
public class ListPeersCommand implements Command {
	
    /* --- Properties --- */
	/** The communication manager module */
    private final CommunicationManager communicationManager;

    public ListPeersCommand(CommunicationManager communicationManager) {
        if (communicationManager == null) {
            throw new IllegalArgumentException("communicationManager");
        }

        this.communicationManager = communicationManager;
    }

    /* --- Accessors --- */
    public CommunicationManager getCommunicationManager() {
        return communicationManager;
    }

    /**
     * @return the name of the command
     */
    @Override
    public String getName() {
        return "listPeers";
    }

    /**
     * @return the description of the command
     */
    @Override
    public String getDescription() {
        return "Print a list of peers.";
    }

    /* --- Methods --- */
    /**
     * Outputs a list of current neighbor peers.
     * @param parameters : empty list
     * @return the return status
     */
    @Override
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;

        try {
            Collection<Peer> peers = communicationManager.getPeers().values();
            StringBuilder builder = new StringBuilder();

            for (Peer p : peers) {
            	builder.append(p.toString());
            	builder.append("\n");
            }

            res = new GenericReturnStatus(builder.toString(), true, null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error retreiving peers infos...",
                    false, null);
        }

        return res;
    }
}
