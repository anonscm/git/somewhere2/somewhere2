/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;


/**
 * Interface that notifies events on distant peers.
 * 
 * @author Andre Fonseca
 */
public interface PeerListener {

    /**
     * Called when a peer is coming into the neighborhood
     * @param peer peer that is coming
     */
    void peerIsConnecting(Peer peer);

    /**
     * Called when a peer is leaving the neighborhood
     * @param peer peer that is leaving
     */
    void peerIsLeaving(Peer peer);
    
}
