/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication;

/**
 * Special type of communication exception that deals with already set to be received messages.
 * 
 * @author Andre Fonseca
 *
 */
public class AlreadySetToBeReceivedException extends CommunicationException {
	private static final long serialVersionUID = 2346890525617039393L;
	
	public AlreadySetToBeReceivedException(String string) {
		super(string);
	}

}
