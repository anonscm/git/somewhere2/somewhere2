/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model.impl;

import java.io.Serializable;

import fr.lri.iasi.somewhere.api.communication.model.MessageType;

/**
 * Describes an abstract message type.
 * 
 * @author Andre Fonseca
 *
 */
public abstract class AbstractMessageType implements MessageType, Serializable {
    private static final long serialVersionUID = 1839493L;

    /**
     * Message types with the same type must always be equals (see also: hashCode)
     */
    @Override
    public boolean equals(Object o) {
    	if (o == null) 
    		return false;
    	if ((o == this) || (o.getClass().equals(this.getClass())))
    		return true;
    	
    	return false;
    }
    
    /**
     * Message types with the same type must always be equals (see also: equals)
     */
    @Override
    public int hashCode() {
    	return 42;
    }
    
    @Override
    public String toString() {
    	return getName();
    }
}
