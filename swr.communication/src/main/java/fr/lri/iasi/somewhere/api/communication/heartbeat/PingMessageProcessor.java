/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.heartbeat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageBuilder;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.MessageTypeSingletonFactory;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageProcessor;
import fr.lri.iasi.somewhere.api.communication.model.types.PongMessageType;

/**
 * Responsible for processing a received ping message.
 * 
 * @author Andre Fonseca
 */
public class PingMessageProcessor extends AbstractMessageProcessor {
	static Logger logger = LoggerFactory.getLogger(PingMessageProcessor.class);
    
    public PingMessageProcessor(MessageSession session) {
        super(session);
    }
    
    /* --- Methods --- */
    /**
     * Process of ping message: whenever a ping message arrives and it is processed, it will
     * generate a pong message to be sent as a answer to the sender peer.
     * 
     * @param message : the message to be processed
     */
    @Override
    public void process(Message message) {
    	try {
    		MessageBuilder builder = new MessageBuilder(session.getCommunicationManager());
    		CommunicationManager.TEXT.addContent(builder, "pong");
    		
			session.sendMessage(builder,
					message.getOrigin().getName(),
					MessageTypeSingletonFactory.create(PongMessageType.class));
			
		} catch (CommunicationException e) {
			logger.error("There were problems responding the ping message: {}", e.getMessage());
		}
    }

}
