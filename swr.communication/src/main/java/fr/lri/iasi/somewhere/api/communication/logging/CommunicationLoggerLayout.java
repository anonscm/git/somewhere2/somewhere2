/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.logging;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.LayoutBase;

import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.Peer;

/**
 * Formats the events messages over the SL4J log.
 * 
 * @author Andre Fonseca
 *
 */
public class CommunicationLoggerLayout extends LayoutBase<ILoggingEvent> {

	@Override
	public String doLayout(ILoggingEvent event) {
		
		StringBuffer builder = new StringBuffer();
		
		builder.append("\n<" + event.getMarker() + " time=\"" + event.getTimeStamp() + "\">");
		
		Object object = event.getArgumentArray()[0];
		if (object != null) {
			if (object instanceof Peer) {
				Peer peer = (Peer) object;
				
				builder.append("\n<Peer>");
				builder.append("\n\t<name>" + peer.getName() + "</name>");
				builder.append("\n\t<isConnected>" + peer.isConnected() + "</isConnected>");
				builder.append("\n</Peer>");
				
			} else if (object instanceof Message) {
				Message message = (Message) object;
				MessageSession session = (MessageSession) event.getArgumentArray()[1];
				if (session != null) {
					builder.append("\n<Message>");
					builder.append("\n\t<id>" + message.getId() + "</id>");
					builder.append("\n\t<sessionId>" + session.getSessionId() + "</sessionId>");
					builder.append("\n\t<creatorSessionId>" + session.getCreatorSessionId() + "</creatorSessionId>");
					builder.append("\n\t<type>" + message.getType() + "</type>");
					builder.append("\n\t<origin>" + message.getOrigin().getName() + "</origin>");
					builder.append("\n\t<destination>" + message.getDestination().getName() + "</destination>");
					builder.append("\n\t<contents>" + message.getContents() + "</contents>");
					builder.append("\n</Message>");
				}
			}
		}
		
		builder.append("\n</" + event.getMarker() + ">");
		
		return builder.toString();
	}

}
