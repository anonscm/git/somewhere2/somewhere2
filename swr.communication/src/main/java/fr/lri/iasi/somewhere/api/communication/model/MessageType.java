/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;

/**
 * Describe a message type interface. A message type classifies the nature and behavior of a message.
 * 
 * @author Andre Fonseca
 *
 */
public interface MessageType {
	
	/**
	 * @return the human-readable name of message type
	 */
	String getName();
    
	/**
	 * @return true if the message labeled with the current type, behavior like a response (or return) to another message.
	 */
	boolean isReturningType();
    
	/**
	 * @return true if the message labeled with the current type, needs to open a new message session when sent to other peer.
	 */
	boolean needsNewSessionSend();
	
	/**
	 * @return true if the message labeled with the current type, needs to open a new message session when arriving to a peer.
	 */
	boolean needsNewSessionReceive();
    
	/**
	 * @return true if the message labeled with the current type, needs to maintain a message session after sent to other peer.
	 */
	boolean needsToMaintainSessionSend(MessageSession session);
	
	/**
	 * @return true if the message labeled with the current type, needs to maintain a message session after received.
	 */
	boolean needsToMaintainSessionReceive(MessageSession session);
    
	/**
	 * Creates (or retrieve) the corresponding processor for a given session.
	 * @param session
	 * @return a message processor corresponding to this message type.
	 */
	MessageProcessor createProcessor(MessageSession session);
	
	/**
	 * Print the current message on the string format
	 * @param message
	 * @return string version of this message
	 */
	String print(Message message);

}
