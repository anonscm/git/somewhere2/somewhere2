/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.ui.UI;

/**
 * Process simple text messages, printing it on the default output stream.
 * 
 * @author Andre Fonseca
 *
 */
public class SimpleMessageProcessor extends AbstractMessageProcessor {
	static Logger logger = LoggerFactory.getLogger(SimpleMessageProcessor.class);
    
    public SimpleMessageProcessor(MessageSession session) {
        super(session);
    }

    /**
     * {@inheritDoc}
     */
	@Override
	public void process(Message message) {
		
		UI ui = session.getCommunicationManager().getApp().getUI();
		ui.print("A new message has been received: " +
	            message.getType().print(message) + "\n");		
	}

}
