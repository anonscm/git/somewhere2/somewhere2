/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model.impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageProcessor;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;

/**
 * Describes an abstract message processor.
 * 
 * @author Andre Fonseca
 *
 */
public abstract class AbstractMessageProcessor implements MessageProcessor {
	
	/* --- Properties --- */
	/** Communication Manager to use */
    protected final MessageSession session;
    
    /** The concurrent ExecutorService that will handle requests to the pipe server */
    private final ExecutorService processorExecutorService;
    
    public AbstractMessageProcessor(MessageSession session) {
        if (session == null)
            throw new IllegalArgumentException("session");

        this.session = session;
        this.processorExecutorService = Executors.newFixedThreadPool(15);
    }
    
    /* --- Methods --- */
    /**
	 * {@inheritDoc}
	 */
    public final void execute(final Message message) {
    	if (!processorExecutorService.isShutdown()) {
			processorExecutorService.submit(new ProcessorExecutor() {
				public void exec() {
					process(message);
					
					// Increment the session's receive count of this message type.
					session.addReceivedMessagesCount(message.getType().getName());
				}
			});
    	}
	}
    
    /**
	 * {@inheritDoc}
	 */
    public final void stop() {
		processorExecutorService.shutdown();
	}
    
    /**
     * A class used as a runnable thing to execute initialization subroutines
     */
    protected abstract class ProcessorExecutor implements Runnable {
        public void run() {
            try {
                exec();
            } catch (Exception e) {
            	e.printStackTrace();
                throw new RuntimeException(e.getMessage(), e);
            }
        }

        abstract protected void exec();
    }

}
