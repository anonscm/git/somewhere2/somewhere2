/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication;

import fr.lri.iasi.somewhere.AbstractModule;
import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.api.communication.commands.AddPeerCommand;
import fr.lri.iasi.somewhere.api.communication.commands.ListPeersCommand;
import fr.lri.iasi.somewhere.api.communication.commands.PingCommand;
import fr.lri.iasi.somewhere.api.communication.commands.RemovePeerCommand;
import fr.lri.iasi.somewhere.api.communication.commands.SendFileMessageCommand;
import fr.lri.iasi.somewhere.api.communication.commands.SendTextMessageCommand;
import fr.lri.iasi.somewhere.api.communication.configuration.CommunicationConfiguration;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageBuilder;
import fr.lri.iasi.somewhere.api.communication.model.MessageContentKey;
import fr.lri.iasi.somewhere.api.communication.model.MessageListener;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.MessageSessionHandler;
import fr.lri.iasi.somewhere.api.communication.model.MessageTypeSingletonFactory;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.api.communication.model.PeerListener;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageSender;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageReceiver;
import fr.lri.iasi.somewhere.api.communication.model.types.SimpleMessageType;
import fr.lri.iasi.somewhere.ui.UI;

import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * The main class of the Communication module. It also acts as the main message and peer listener, delegating tasks 
 * to a collection of other listeners.
 * 
 * @author Andre Fonseca
 */
public class CommunicationManager extends AbstractModule
    implements PeerListener, MessageListener {
    /* --- Constants --- */
    /** The name of this module */
    protected static final String moduleName = "communication";

    /* --- Properties --- */
    /** The configuration for this module */
    protected final CommunicationConfiguration configuration;

    /** List of messages to be Sent */
    private final List<Message> messagesToBeSent = new ArrayList<Message>();

    /** List of received messages */
    private final List<Message> receivedMessages = new ArrayList<Message>();

    /** List of peers listeners */
    private final List<PeerListener> peerListeners = new CopyOnWriteArrayList<PeerListener>();

    /** List of messages listeners */
    private final List<MessageListener> messageListeners = new CopyOnWriteArrayList<MessageListener>();
    
    /** The main message sender */
    private AbstractMessageSender sender;
    
    /** The main message receiver */
    private AbstractMessageReceiver receiver;

    /** Current concrete communication module */
    private AbstractCommunicationModule<?, ? extends Peer> communicationModule;
    
    /** Control all active message sessions within this module */
    private final MessageSessionHandler sessionHandler;
    
    /** Create/Find text content type*/
    public static final MessageContentKey<String> TEXT = MessageContentKey.create("text");
    
    /** Create/Find ordering message content type*/
    public static final MessageContentKey<Integer> MSG_NUMBER = MessageContentKey.create("number");

    /** Create/Find ping content type*/
    public static final MessageContentKey<String> PING = MessageContentKey.create("ping");
    
    /** Create/Find pong content type*/
    public static final MessageContentKey<String> PONG = MessageContentKey.create("pong");
    
    /** Create/Find the "time to live" content type*/
    public static final MessageContentKey<Long> TTL = MessageContentKey.create("ttl");
    
    public CommunicationManager(final App app) {
        super(app);

        configuration = new CommunicationConfiguration(this);
        sessionHandler = new MessageSessionHandler(this);
        addBaseCommands();
    }

    /* --- Accessors --- */
    public CommunicationConfiguration getConfiguration() {
        return configuration;
    }

    public Map<String, Peer> getPeers() {
        return Collections.unmodifiableMap(communicationModule.getPeerGroup().getPeers());
    }

    public List<Message> getMessagesToBeSent() {
        return Collections.unmodifiableList(messagesToBeSent);
    }

    public List<Message> getReceivedMessages() {
        return Collections.unmodifiableList(receivedMessages);
    }

    public List<PeerListener> getPeerListeners() {
        return Collections.unmodifiableList(peerListeners);
    }

    public List<MessageListener> getMessageListeners() {
        return Collections.unmodifiableList(messageListeners);
    }

    public Peer getLocalPeer() {
    	return communicationModule.getPeerGroup().getLocalPeer();
    }
    
    public MessageSessionHandler getSessionHandler() {
    	return sessionHandler;
    }

    public AbstractCommunicationModule<?, ? extends Peer> getCommunicationModule() {
        return communicationModule;
    }

    /* --- Mutators --- */
    public void setMessageSender(AbstractMessageSender sender) {
    	this.sender = sender;
    }
    
    public void setMessageReceiver(AbstractMessageReceiver receiver) {
    	this.receiver = receiver;
    }
    
    /* --- Add/remove --- */
    public void addPeer(Peer peer) {
        peer.addPeerListener(this);
        peer.connect();
    }

    public void removePeer(Peer peer) {
        peer.removePeerListener(this);
        peer.disconnect();
    }

    protected void addMessageToBeSent(Message message)
        throws CommunicationException {
        messagesToBeSent.add(message);
        message.addMessageListener(this);
        message.messageIsToBeSent();
    }

    protected void removeMessageToBeSent(Message message)
    	throws CommunicationException {
        messagesToBeSent.remove(message);
        message.sent();
        message.removeAllMessageListeners();
    }

    protected void addReceivedMessage(Message message)
        throws CommunicationException {
        receivedMessages.add(message);
        message.addMessageListener(this);
        message.messageIsToBeReceived();
    }

    protected void removeReceivedMessage(Message message)
    	throws CommunicationException {
        receivedMessages.remove(message);
        message.received();
        message.removeAllMessageListeners();
    }

    public void setCommunicationModule(
        AbstractCommunicationModule<?, ? extends Peer> communicationModule) {
        this.communicationModule = communicationModule;
    }

    public void addPeerListener(PeerListener listener) {
        peerListeners.add(listener);
    }

    public void removePeerListener(PeerListener listener) {
        peerListeners.remove(listener);
    }

    public void removeAllPeerListeners() {
        peerListeners.clear();
    }

    public void addMessageListener(MessageListener listener) {
        messageListeners.add(listener);
    }

    public void removeMessageListener(MessageListener listener) {
        messageListeners.remove(listener);
    }

    public void removeAllMessageListeners() {
        messageListeners.clear();
    }

    /* --- Fire Methods --- */
    protected void firePeerIsConnecting(Peer peer) {
        for (PeerListener l : this.peerListeners) {
            l.peerIsConnecting(peer);
        }
    }

    protected void firePeerIsLeaving(Peer peer) {
        for (PeerListener l : this.peerListeners) {
            l.peerIsLeaving(peer);
        }
    }

    protected void fireMessageIsToBeSent(Message message)
    	throws CommunicationException {
    	
        for (MessageListener l : this.messageListeners) {
            l.messageIsToBeSent(message);
        }
        
        if (this.sender == null)
        	throw new CommunicationException("No sender was specified on the communciation manager.");
        
        this.sender.send(message);
    }

    protected void fireMessageHasBeenSent(Message message) {
        for (MessageListener l : this.messageListeners) {
            l.messageHasBeenSent(message);
        }
    }

    protected void fireMessageIsToBeReceived(Message message) 
    		throws CommunicationException {
    	
        for (MessageListener l : this.messageListeners) {
            l.messageIsToBeReceived(message);
        }
        
        if (this.receiver == null)
        	throw new CommunicationException("No receiver was specified on the communciation manager.");
        	
        this.receiver.receive(message);
    }
    
    protected void fireMessageHasBeenReceived(Message message) {
        for (MessageListener l : this.messageListeners) {
            l.messageHasBeenReceived(message);
        }
    }
    
    /* --- Other Methods --- */
    /**
     * {@inheritDoc}
     */
    public static CommunicationManager getModuleInstance(App app) {
        if (app == null) {
            throw new IllegalArgumentException("app");
        }

        Module m = app.getModule(moduleName);

        if (m == null) {
            return null;
        }

        try {
            CommunicationManager res = (CommunicationManager) m;

            return res;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Add commands to the UI
     */
    protected void addBaseCommands() {
        UI ui = app.getUI();
        ui.addCommand(new AddPeerCommand(this));
        ui.addCommand(new ListPeersCommand(this));
        ui.addCommand(new RemovePeerCommand(this));
        ui.addCommand(new SendTextMessageCommand(this));
        ui.addCommand(new SendFileMessageCommand(this));
        ui.addCommand(new PingCommand(this));
    }

    /**
     * {@inheritDoc}
     */
    public String getModuleName() {
        return moduleName;
    }

    /**
     * Send a given message to a known destination specified
     *  by a property in the message.
     * @see fr.lri.iasi.somewhere.api.communication.model.Message
     * @param message Message to be sent
     */
    public void sendMessage(Message message) 
    	throws CommunicationException {
        if (message == null) {
            throw new IllegalArgumentException("message");
        }

        // Add the message in the messagesToBeSent list
        addMessageToBeSent(message);
        
        // After message is sent...
        removeMessageToBeSent(message);
    }

    /**
     * Receive a given message
     * @param message Message received
     */
    public void receiveMessage(Message message)
        throws CommunicationException {
        if (message == null) {
            throw new IllegalArgumentException("message");
        }

        // Add the message in the messageReceived list
        addReceivedMessage(message);
        
        // After message is received...
        removeReceivedMessage(message);
    }
    
    /**
     * {@inheritDoc}
     */
    public void peerIsConnecting(Peer peer) {
        firePeerIsConnecting(peer);
    }

    /**
     * {@inheritDoc}
     */
    public void peerIsLeaving(Peer peer) {
        firePeerIsLeaving(peer);
    }

    /**
     * {@inheritDoc}
     */
    public void messageIsToBeSent(Message message)
    	throws CommunicationException {
        fireMessageIsToBeSent(message);
    }

    /**
     * {@inheritDoc}
     */
    public void messageHasBeenSent(Message message) {
        fireMessageHasBeenSent(message);
    }

    /**
     * {@inheritDoc}
     */
    public void messageHasBeenReceived(Message message) {
        fireMessageHasBeenReceived(message);
    }
    
    /**
     * {@inheritDoc}
     */
	public void messageIsToBeReceived(Message message) 
		throws CommunicationException {
		fireMessageIsToBeReceived(message);
	}

    /**
     * {@inheritDoc}
     */
    public void quit() {
        removeAllMessageListeners();
        removeAllPeerListeners();
    }

    /**
     * {@inheritDoc}
     */
    public void init() {
        // Setting log file
    	initLog("/META-INF/communication-logback.xml");
    }

    /**
     * {@inheritDoc}
     * 
     * NOTE: No XML configuration file specified for this module.
     */
    public void importConfigXML(Node config) {
        // ... 
    }

    /**
     * {@inheritDoc}
     * 
     * NOTE: No XML configuration file specified for this module.
     */
    public Node exportConfigXML() {
        return null;
    }
    
    /**
     * Sends a message given the destination and the content
     * @param destination The name of the peer to send the message to
     * @param content The content of the message
     * @throws CommunicationException 
     */
    public void sendMessage(String destination, String content)
    		throws CommunicationException {

        // Create the message !
        MessageBuilder builder = new MessageBuilder(this);
        CommunicationManager.TEXT.addContent(builder, content);
        
        // Send it!
        MessageSession session = this.getSessionHandler().addMessageSessionForNewMessage();
        session.sendMessage(builder, destination, MessageTypeSingletonFactory.create(SimpleMessageType.class));
    }
    
}
