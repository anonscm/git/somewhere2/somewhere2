/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.commands;

import java.util.List;

import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.heartbeat.Heartbeat;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

/**
 * Responsible for the ping action.
 * 
 * @author Andre Fonseca
 */
public class PingCommand implements Command {
	
	/* --- Properties --- */
	/** The communication manager module */
    private final CommunicationManager communicationManager;
    
    /** Indicates the number of times to ping */
    private final static int TIMES = 5;

    public PingCommand(CommunicationManager moduleManager) {
        if (moduleManager == null) {
            throw new IllegalArgumentException("moduleManager");
        }

        this.communicationManager = moduleManager;
    }

    /* --- Accessors --- */
    public CommunicationManager getModuleManager() {
        return communicationManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
	public String getName() {
		return "ping";
	}

	/**
     * {@inheritDoc}
     */
    @Override
	public String getDescription() {
		return "Send a ping message to a peer";
	}

	/* --- Methods --- */
	/**
     * Pings indicated peer for a given number of times in order to check if it is accessible.
     * @param parameters : list containing the name of the peer destination
     * @return the return status
     */
    @Override
	public ReturnStatus execute(List<Object> parameters) {
		ReturnStatus res = null;

        try {
            // Verify there are enough parameters
            if (parameters.size() != 1) {
                res = new GenericReturnStatus("Syntax Error! \n Usage :" +
                        "\t" + getName() + " DESTINATION",
                        false, null);

                return res;
            }

            // Get useful parameters
            String destination = parameters.get(0).toString();
         
            // Create Heartbeat object
            Heartbeat heartbeat = new Heartbeat(communicationManager, destination, TIMES);

            // Start pinging !
            heartbeat.ping();

            res = new GenericReturnStatus("\nPing Successfully sent", true,
                    null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error sending the ping message ...",
                    false, null);
        }

        return res;
	}

}
