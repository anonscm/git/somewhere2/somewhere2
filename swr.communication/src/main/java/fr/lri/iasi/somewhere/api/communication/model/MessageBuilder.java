/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;

import java.util.HashMap;
import java.util.Map;

import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.types.SimpleMessageType;


/**
 * A builder to build messages and send them
 * @author Andre Fonseca
 */
public class MessageBuilder {
	
    /* --- Properties --- */
    /** The manager of this module */
    private final CommunicationManager communicationManager;
    
    /** The message id */
    private String id;

    /** The destination */
    private Peer destination;

    /** The origin */
    private Peer origin;

    /** The message Type */
    private MessageType type;

    /** The content list */
    private Map<String, Object> contents;

    public MessageBuilder(CommunicationManager communicationManager) {
        if (communicationManager == null) {
            throw new IllegalArgumentException("communicationManager");
        }

        this.communicationManager = communicationManager;
        this.destination = new Peer(communicationManager.getLocalPeer());
        this.origin = new Peer(communicationManager.getLocalPeer());
        this.type = MessageTypeSingletonFactory.create(SimpleMessageType.class);
        this.contents = new HashMap<String, Object>();
    }

    public MessageBuilder(MessageBuilder messageBuilder) {
        if (messageBuilder == null) {
            throw new IllegalArgumentException("messageBuilder");
        }

        this.id = messageBuilder.id;
        this.communicationManager = messageBuilder.communicationManager;
        this.destination = new Peer(messageBuilder.destination);
        this.origin = new Peer(messageBuilder.origin);
        this.type = messageBuilder.type;
        this.contents = new HashMap<String, Object>(messageBuilder.contents);
    }

    /* --- Accessors --- */
    public CommunicationManager getCommunicationManager() {
        return communicationManager;
    }
    
    public String getId() {
        return id;
    }

    public Peer getDestination() {
        return destination;
    }

    public Peer getSource() {
        return origin;
    }

    public MessageType getType() {
        return type;
    }

    public Map<String, Object> getContents() {
        return contents;
    }

    /* --- Methods --- */
    @Override
    public boolean equals(Object o) {
        if (o instanceof MessageBuilder) {
            MessageBuilder b = (MessageBuilder) o;

            return ((b.id.equals(id)) &&
            (b.destination.equals(destination)) &&
            (b.origin.equals(origin)) && (b.type.equals(type)) &&
            (b.contents.equals(contents)));
        } else {
            return false;
        }
    }
    
    @Override
   	public int hashCode() {
   		final int prime = 31;
   		int result = 1;
   		result = prime * result
   				+ ((contents == null) ? 0 : contents.hashCode());
   		result = prime * result
   				+ ((destination == null) ? 0 : destination.hashCode());
   		result = prime * result + ((id == null) ? 0 : id.hashCode());
   		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
   		result = prime * result + ((type == null) ? 0 : type.hashCode());
   		return result;
   	}
    
    /**
     * Set the id field
     * @param destination the destination of the message
     * @return a new MessageBuilder
     */
    public MessageBuilder setId(String id) {
        if (id == null) {
            throw new IllegalArgumentException("id");
        }

        MessageBuilder res = new MessageBuilder(this);
        res.id = id;

        return res;
    }

    /**
     * Set the destination field
     * @param destination the destination of the message
     * @return a new MessageBuilder
     */
    public MessageBuilder setDestination(Peer destination) {
        if (destination == null) {
            throw new IllegalArgumentException("destination");
        }

        MessageBuilder res = new MessageBuilder(this);
        res.destination = new Peer(destination);

        return res;
    }

    /**
     * Set the destination field
     * @param destination the name of the destination of the message
     * @return a new MessageBuilder
     */
    public MessageBuilder setDestination(String name) {
        Peer destinationPeer = null;
        Peer peer = communicationManager.getPeers().get(name);

        if (peer != null) {
            destinationPeer = new Peer(peer);
        }


        if (destinationPeer == null) {
            if (communicationManager.getLocalPeer().getName().equals(name)) {
                destinationPeer = new Peer(communicationManager.getLocalPeer());
            } else {
                destinationPeer = new Peer(name);
            }
        }

        MessageBuilder res = setDestination(destinationPeer);

        return res;
    }

    /**
     * Set the origin field
     * @param origin the origin of the message
     * @return a new MessageBuilder
     */
    public MessageBuilder setOrigin(Peer origin) {
        if (origin == null) {
            throw new IllegalArgumentException("origin");
        }

        MessageBuilder res = new MessageBuilder(this);
        res.origin = new Peer(origin);

        return res;
    }

    /**
     * Set the type field
     * @param type the type of the message
     * @return a new MessageBuilder
     */
    public MessageBuilder setType(MessageType type) {
        if (origin == null) {
            throw new IllegalArgumentException("type");
        }

        MessageBuilder res = new MessageBuilder(this);
        res.type = type;

        return res;
    }

    /**
     * Add content
     * @param  content the content to add
     * @return a new MessageBuilder
     */
    void addContent(String key, Object content) {
        if (content == null) {
            throw new IllegalArgumentException("content");
        }

        contents.put(key, content);
    }
    
    /**
	 * Build the Message !
	 */
	public Message build() {
		Message res = new Message(
				id,
				origin,
				destination,
				type,
				contents);

		return res;
	}

}
