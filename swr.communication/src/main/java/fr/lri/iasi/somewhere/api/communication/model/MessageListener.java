/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;


/**
 * Interface that handle message events on local peer.
 * 
 * @author Andre Fonseca
 */
public interface MessageListener {
	
    /**
     * Called when a message is going to be sent
     * @param message message to send
     */
    void messageIsToBeSent(Message message) throws CommunicationException;

    /**
     * Called when a message has just been sent
     * @param message message to send
     */
    void messageHasBeenSent(Message message);
    
    /**
     * Called when a message is being received
     * @param message received message
     */
    void messageIsToBeReceived(Message message) throws CommunicationException;

    /**
     * Called when a message has just been received
     * @param message received message
     */
    void messageHasBeenReceived(Message message);
    
}
