/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a final answer to be sent to the user. A <i>FinalAnswer</i> differs from
 * a normal {@link Message} since it is not meant to be transfer on the network, only
 * to be presented for the user on the local peer.
 * 
 * <p>It also encapsulates a generic answer content along with another answer 
 * informations (response time, answer number...)</p>
 * 
 * @author Andre Fonseca
 *
 * @param <T> The type of the main content of the final answer
 */
public class FinalAnswer<T> implements Serializable {
	private static final long serialVersionUID = 3819122767384696339L;

	/* --- Properties --- */
	/** The main content of the current final answer */ 
	private T content;
	
	/** The final answer number */
	private int number;
	
	/** The response time */
	private String responseTime;
	
	/** Other possible annotations of the current final answer */
	private Map<String, FinalAnswerAnnotation<?>> annotations = new HashMap<String, FinalAnswerAnnotation<?>>();
	
	/* --- Accessors --- */
	public T getContent() {
		return content;
	}
	
	public int getNumber() {
		return number;
	}
	
	public String getResponseTime() {
		return this.responseTime;
	}
	
	public FinalAnswerAnnotation<?> getAnnotation(String name) {
		return annotations.get(name);
	}
	
	/* --- Mutators --- */
	public void setNumber(int number) {
		this.number = number;
	}
	
	public void setResponseTime(long responseTime) {
		DecimalFormat decimal = new DecimalFormat( "0.0000" );
		double timestamp = (System.nanoTime() - responseTime) / 1000000000.0;
		this.responseTime = decimal.format(timestamp);
	}
	
	public void setContent(T content) {
		this.content = content;
	}
	
	public void addAnnotation(FinalAnswerAnnotation<?> annotation) {
		this.annotations.put(annotation.getName(), annotation);
	}
	
	/*--- Methods --- */
	public String toString() {
		return content.toString();
	}

}
