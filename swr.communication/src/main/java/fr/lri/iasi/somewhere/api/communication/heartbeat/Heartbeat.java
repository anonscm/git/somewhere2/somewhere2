/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.heartbeat;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.MessageBuilder;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.MessageTypeSingletonFactory;
import fr.lri.iasi.somewhere.api.communication.model.types.PingMessageType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * At a specified interval, send a heartbeat message to known peers to verify if they are available
 * 
 * @author Andre Fonseca
 */
public class Heartbeat {
    static Logger logger = LoggerFactory.getLogger(Heartbeat.class);

    /** Communication Manager to use */
    private final CommunicationManager communicationManager;

    /** Interval between two heartbeats */
    private long hearbeatsInterval = 20 * 100L;

    /** If thread is to continue to live */
    private boolean threadShallContinue = true;
    
    /** The the peer to receive ping messages */
    private final String destination;
    
    /** How many times the heartbeat will run. If '0', it will run until stopped externally. */
    private int timesBeat = Integer.MAX_VALUE;

    public Heartbeat(CommunicationManager communicationManager, String destination, 
    		int times) {
        if (communicationManager == null)
            throw new IllegalArgumentException("communicationManager");
        if (destination == null)
            throw new IllegalArgumentException("peerName");
        if (times < 0)
            throw new IllegalArgumentException("times");

        this.communicationManager = communicationManager;
        this.destination = destination;
        this.timesBeat = times;
    }

    public Heartbeat(CommunicationManager communicationManager, String destination,
    	int times, long hearbeatsInterval) {
        this(communicationManager, destination, times);
        setHearbeatsInterval(hearbeatsInterval);
    }

    /* --- Accessors --- */
    public long getHearbeatsInterval() {
        return hearbeatsInterval;
    }

    public CommunicationManager getCommunicationManager() {
        return communicationManager;
    }

    /* --- Mutators --- */
    public void setHearbeatsInterval(long hearbeatsInterval) {
        if (hearbeatsInterval <= 0L) {
            throw new IllegalArgumentException("hearbeatsInterval");
        }

        this.hearbeatsInterval = hearbeatsInterval;
    }

    /* --- Methods --- */
    /**
     * Start heartbeating peers
     * @throws CommunicationException 
     * @throws InterruptedException 
     */
    public void ping() throws CommunicationException, InterruptedException {
    	
		int i = 0;
        while ((threadShallContinue) && (i < timesBeat)) {
            sendPingMessages();
            Thread.sleep(hearbeatsInterval);
            i++;
        }
    }


    /**
     * Stop heartbeating peers
     */
    public void stop() {
        threadShallContinue = false;
    }
    

    /**
     * Send ping messages to known peers
     */
    public void sendPingMessages()
    	throws CommunicationException {

        // Create the message !
        MessageBuilder builder = new MessageBuilder(communicationManager);
        CommunicationManager.PING.addContent(builder, "ping");

        MessageSession session = communicationManager.getSessionHandler().addMessageSessionForNewMessage();
        session.sendMessage(builder, destination, MessageTypeSingletonFactory.create(PingMessageType.class));
    }

}
