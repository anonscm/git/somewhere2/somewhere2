/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.commands;

import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import java.util.List;


/**
 * A command to remove a peer from the local peer's neighbor list.
 * 
 * @author Andre Fonseca
 */
public class RemovePeerCommand implements Command {
	
    /* --- Properties --- */
	/** The communication manager module */
    private final CommunicationManager communicationManager;

    public RemovePeerCommand(CommunicationManager communicationManager) {
        if (communicationManager == null) {
            throw new IllegalArgumentException("communicationManager");
        }

        this.communicationManager = communicationManager;
    }

    /* --- Accessors --- */
    public CommunicationManager getCommunicationManager() {
        return communicationManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "removePeer";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return "Remove a neighbor peer.";
    }

    /* --- Methods --- */
    /**
     * Disconnect local peer from destination peer, removing it from the local peer's neighbors list.
     * @param parameters The parameters of the command
     * @return the return status
     */
    @Override
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;

        try {
            // Verify there are enough parameters
            if (parameters.size() != 1) {
                res = new GenericReturnStatus("Syntax Error! \n Usage :" +
                        "\t" + getName() + " DESTINATION", false, null);

                return res;
            }

            // Get useful parameters
            String peerName = parameters.get(0).toString();
            Peer peer = communicationManager.getPeers().get(peerName);

            if (peer != null) {
                communicationManager.removePeer(peer);

                res = new GenericReturnStatus("Peer removed sucefully",
                        true, null);

                return res;
            }

            res = new GenericReturnStatus("The requested peer is not a neighbor of the local peer",
                    false, null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error removing the requested peer...",
                    false, null);
        }

        return res;
    }
}
