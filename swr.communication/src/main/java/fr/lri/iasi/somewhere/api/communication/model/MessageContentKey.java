/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Alternative implementation of the type-safe heterogeneous container pattern. Creates a type map that will
 * represent the content of a message. Each element of this content, will be inserted and retrieved from the
 * map respecting the type indicated on its registration.
 * <p>
 * Example:
 * <p>
 * <code>public static final MessageContentKey<String> TEXT = MessageContentKey.create("text");</code>
 * <p>
 * Will create, a content element named "text" that will be typed as a <i>String</i>. Every operation that
 * tries to read or write this content element, need to respect its type.
 * <p>
 * Example (Creating a message with the content "text"):
 * <p>
 * <code> String content = "hello world!";<br>
 * MessageBuilder builder = new MessageBuilder(communicationManager);<br>
   CommunicationManager.TEXT.addContent(builder, content);</code>
 * <p>
 * If the variable <code> content </code> is not a String, it will occur a compilation error.
 * <p>
 * Another Example (Reading a message content of index "text"):
 * <p>
 * <code> String result = CommunicationManager.TEXT.getContent(message);</code>
 * <p>
 * If the variable <code> result </code> is not a String, it will occur a compilation error.
 * 
 * @author Andre Fonseca
 *
 * @param <T> Type of the content key
 */
public final class MessageContentKey<T> {

	/* --- Properties --- */
	/** Name of piece of content */
	private final String name;
	
	/** Map containing all the registered content keys. The key is the name of the piece of content and the value is
	 * a content key object, indicating a type. */
	private static final Map<String, MessageContentKey<?>> keyIndex = new HashMap<String, MessageContentKey<?>>(); 
	
	private MessageContentKey(String name) {
		if (name == null)
			throw new IllegalArgumentException("name");
		
		this.name = name;
	}
	
	/* --- Accessor --- */
	public String getName() {
		return name;
	}
	
	@SuppressWarnings("unchecked")
	public T getContent(Message context) {
		return (T) context.getContent(name);
	}
	
	/* --- Mutators --- */
	public void addContent(MessageBuilder context, T value) {
		context.addContent(name, value);
	}
	
	public void addContent(Message context, T value) {
		context.addContent(name, value);
	}
	
	/* --- Methods --- */
	/**
	 * Register a content type on the map.
	 * 
	 * @param name of the content type
	 * @return the created message content key
	 */
	@SuppressWarnings("unchecked")
	public static <T> MessageContentKey<T> create(String name) {
		if (name == null)
			throw new IllegalArgumentException("name");

		MessageContentKey<T> key = (MessageContentKey<T>) keyIndex.get(name);
		if (key != null)
			return key;
		
		key = new MessageContentKey<T>(name);
		keyIndex.put(name, key);
		return key;
	}
}
