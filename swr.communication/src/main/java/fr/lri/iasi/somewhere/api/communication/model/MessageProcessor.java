/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;

/**
 * Describes a message processor interface. A message processor is related to the message type and it executes tasks
 * over these messages on their arrival on the local peer.
 * 
 * @author Andre Fonseca
 *
 */
public interface MessageProcessor {
    
	/**
	 * Process the arrived message.
	 * 
	 * @param message
	 */
	void process(Message message);
	
	/**
	 * Initializes and executes the message processor.
	 * 
	 * @param message
	 */
	void execute(Message message);
	
	/**
	 * Stop the message processor.
	 */
	void stop();
}
