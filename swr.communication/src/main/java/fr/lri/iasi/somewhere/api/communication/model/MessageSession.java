/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import com.google.common.collect.ConcurrentHashMultiset;
import com.google.common.collect.Multiset;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;

/**
 * Describes the main entity of the concurrency model. Responsible for treating a specific message propagation, using a collection
 * of processors to treat different message types.
 * 
 * @author Andre Fonseca
 *
 */
public class MessageSession {
	static Logger logger = LoggerFactory.getLogger(MessageSession.class);
	private static Logger fileLogger = LoggerFactory.getLogger("communicationEvents");
    
	/* -- Log Properties --- */
    private Marker sentEvent = MarkerFactory.getMarker("SENT");
    private Marker receivedEvent = MarkerFactory.getMarker("RECEIVED");
    
	/* -- Properties --- */
    /** The communication manager module */
    private final CommunicationManager communicationManager;
    
    /** The session id  */
    private String sessionId;
    
    /** The id of the session that triggered its creation */
    private String creatorSessionId;
    
    /** The name of the peer that triggered its creation */
    private final String creatorPeerName;

    /** The collection of processors used by this session */
    protected final ConcurrentHashMap<MessageType, MessageProcessor> processors = new ConcurrentHashMap<MessageType, MessageProcessor>();
    
    /** Counter of sent messages by its type  */
    protected final Multiset<String> sentMessagesCount = ConcurrentHashMultiset.create();
    
    /** Counter of received messages by its type  */
    protected final Multiset<String> receivedMessagesCount = ConcurrentHashMultiset.create();
    
    /** Map of sent messages for this session */
    private final List<Message> sentMessages = new CopyOnWriteArrayList<Message>();
    
    /** Map of received messages for this session */
    private final List<Message> receivedMessages = new CopyOnWriteArrayList<Message>();
    
    MessageSession (CommunicationManager communicationManager,
    		String creatorSessionId, String creatorPeer) {
    	if (communicationManager == null)
    		throw new IllegalArgumentException("communicationManager");
    	
    	this.communicationManager = communicationManager;
    	this.creatorSessionId = creatorSessionId;
    	this.creatorPeerName = creatorPeer;
    	this.sessionId = UUID.randomUUID().toString();
    }
    
    /**
     * Creates a message session for a user message.
     * 
     * @param communicationManager : the communication manager to be used.
     * @return the new message session
     */
    static MessageSession createMessageSessionForNewMessage(CommunicationManager communicationManager) {
    	
    	MessageSession session = new MessageSession(communicationManager, null, "User");
    	session.creatorSessionId = session.sessionId;
    	return session;
    }
    
    /**
     * Creates a message session for a user message.
     * 
     * @param communicationManager : the communication manager to be used.
     * @param origin: the peer that triggered the message session.
     * @return the new message session
     */
    static MessageSession createMessageSessionForNewMessage(CommunicationManager communicationManager, String origin) {
    	
    	MessageSession session = new MessageSession(communicationManager, null, origin);
    	session.creatorSessionId = session.sessionId;
    	return session;
    }

    /* -- Accessors --- */
    public CommunicationManager getCommunicationManager() {
        return communicationManager;
    }
    
	public String getCreatorSessionId() {
		return creatorSessionId;
	}

	public String getSessionId() {
		return sessionId;
	}
	
	public String getCreatorPeerName() {
		return creatorPeerName;
	}
	
	public Map<MessageType, MessageProcessor> getProcessors() {
		return Collections.unmodifiableMap(processors);
	}
	
	public List<Message> getSentMessages() {
		return Collections.unmodifiableList(sentMessages);
	}
	
	public List<Message> getReceivedMessages() {
		return Collections.unmodifiableList(receivedMessages);
	}
	
	public void addSentMessagesCount(String typeName) {
		sentMessagesCount.add(typeName);
	}
	
	public void addReceivedMessagesCount(String typeName) {
		receivedMessagesCount.add(typeName);
	}
	
	public int getSentMessagesCount(String typeName) {
		return sentMessagesCount.count(typeName);
	}
	
	public int getReceivedMessagesCount(String typeName) {
		return receivedMessagesCount.count(typeName);
	}
	
	/* --- Mutators --- */
	void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	void setCreatorId(String creatorId) {
		this.creatorSessionId = creatorId;
	}
	
	/* --- Add/remove --- */
	public MessageProcessor getProcessor(MessageType messageType) {
		MessageProcessor processor = null;
		if (!processors.containsKey(messageType))
			processor = processors.putIfAbsent(messageType, messageType.createProcessor(this));
		
		if (processor == null)
			processor = processors.get(messageType);
		
		return processor;
	}
	
	void removeProcessor(MessageType messageType) {
		processors.remove(messageType);
	}
	
	public boolean addSentMessages(Message message) {
		return sentMessages.add(message);
	}
	
	public boolean addReceivedMessages(Message message) {
		return receivedMessages.add(message);
	}

	/* --- Methods --- */
	/**
	 * Send message task.
	 * 
	 * @param builder : the message builder of the message to be sent.
	 * @param destination : the destination peer.
	 * @param type : the type of the message to be sent.
	 * @throws CommunicationException if there is a problem sending the message.
	 */
	public void sendMessage(MessageBuilder builder, String destination, MessageType type) 
		throws CommunicationException {
		
	    // Create the message !
	    Message message = (new MessageBuilder(builder))
			    				  .setId(this.sessionId)
			    				  .setDestination(new Peer(destination))
			                      .setType(type)
			                      .build();
	    
	    // Send the message
	    this.communicationManager.sendMessage(message);
	    
	    addSentMessages(message);
	    this.sentMessagesCount.add(message.getType().getName());
	    
	    // Log information
	    final Object[] logList = {message, this};
	    fileLogger.info(sentEvent, "{}", logList);
	}
	
	/**
	 * Receive message task.
	 * 
	 * @param message : the message to be sent.
	 * @throws CommunicationException if there is a problem receiving the message.
	 */
	public void receiveMessage(Message message) 
		throws CommunicationException {
		
		// Log information
		final Object[] logList = {message, this};
		fileLogger.info(receivedEvent, "{}", logList);
	
	    // Process the received message !
		final MessageType type = message.getType();
		final MessageProcessor processor = getProcessor(type);
		processor.execute(message);
		
		if (!addReceivedMessages(message))
	    	throw new CommunicationException();
	}
	
	/**
	 * Kills this message session
	 */
	void die() {
		for (Entry<MessageType, MessageProcessor> processorEntry : this.processors.entrySet()) {
			processorEntry.getValue().stop();
		}
	}
}
