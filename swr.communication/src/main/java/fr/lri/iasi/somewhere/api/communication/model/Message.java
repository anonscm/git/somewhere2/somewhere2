/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import fr.lri.iasi.somewhere.api.communication.AlreadyReceivedException;
import fr.lri.iasi.somewhere.api.communication.AlreadySentException;
import fr.lri.iasi.somewhere.api.communication.AlreadySetToBeReceivedException;
import fr.lri.iasi.somewhere.api.communication.AlreadySetToBeSentException;
import fr.lri.iasi.somewhere.api.communication.CommunicationException;

/**
 * Describes a generic message
 * 
 * @author Andre Fonseca
 */
public class Message implements Serializable {
	private static final long serialVersionUID = 945838945847L;

	/* --- Properties --- */
	/** The message id. */
	private String id;

	/** A temporary id to be used by the message in special situations (when messages with same id needs to be treated in different sessions) */
	private transient String tmpId;

	/** The origin peer */
	private final Peer origin;

	/** The destination peer */
	private final Peer destination;

	/** The message type */
	private final MessageType type;

	/** Map containing the message contents */
	private final Map<String, Object> contents = new HashMap<String, Object>();

	/** List of message listeners */
	private transient final List<MessageListener> messageListeners = new CopyOnWriteArrayList<MessageListener>();

	/** Current message status */
	private MessageStatus status = MessageStatus.INIT;

	protected Message(String id, Peer origin, Peer destination,
			MessageType type, Map<String, Object> contents) {
		if (origin == null) {
			throw new IllegalArgumentException("origin");
		}

		if (destination == null) {
			throw new IllegalArgumentException("destination");
		}

		if (type == null) {
			throw new IllegalArgumentException("type");
		}

		if (contents == null) {
			throw new IllegalArgumentException("contents");
		}

		this.id = id;
		this.origin = new Peer(origin);
		this.destination = new Peer(destination);
		this.type = type;
		this.contents.putAll(contents);
	}

	public Message(Message message) {
		this(message.id, message.origin, message.destination, message.type,
				message.contents);
	}

	/* --- Accessors --- */
	public Peer getOrigin() {
		return origin;
	}

	public Peer getDestination() {
		return destination;
	}

	public MessageType getType() {
		return type;
	}

	public Map<String, Object> getContents() {
		return Collections.unmodifiableMap(contents);
	}

	Object getContent(String key) {
		return contents.get(key);
	}

	public List<MessageListener> getMessageListeners() {
		return Collections.unmodifiableList(messageListeners);
	}

	public MessageStatus getStatus() {
		return status;
	}

	public String getId() {
		return id;
	}

	public String getTmpId() {
		return tmpId;
	}

	/* --- Mutators --- */
	public void setId(String id) {
		this.id = id;
	}

	public void setTmpId(String tmpId) {
		this.tmpId = tmpId;
	}

	/* --- Add/remove --- */
	public void addMessageListener(MessageListener listener) {
		messageListeners.add(listener);
	}

	public void removeMessageListener(MessageListener listener) {
		messageListeners.remove(listener);
	}

	public void removeAllMessageListeners() {
		messageListeners.clear();
	}
	
	public void addContent(String key, Object content) {
		if (content == null) {
            throw new IllegalArgumentException("content");
        }

        this.contents.put(key, content);
	}

	/* --- Fire Methods --- */
	protected void fireMessageIsToBeSent() throws CommunicationException {
		for (MessageListener l : this.messageListeners) {
			l.messageIsToBeSent(this);
		}
	}

	protected void fireMessageHasBeenSent() {
		for (MessageListener l : this.messageListeners) {
			l.messageHasBeenSent(this);
		}
	}

	protected void fireMessageIsToBeReceived() throws CommunicationException {
		for (MessageListener l : this.messageListeners) {
			l.messageIsToBeReceived(this);
		}
	}

	protected void fireMessageHasBeenReceived() {
		for (MessageListener l : this.messageListeners) {
			l.messageHasBeenReceived(this);
		}
	}

	/* --- Other Methods --- */
	@Override
	public boolean equals(Object o) {
		if (o instanceof Message) {
			Message m = (Message) o;

			return ((m.id == null ? id == null : m.id.equals(id))
					&& (m.tmpId == null ? tmpId == null : m.tmpId.equals(tmpId))
					&& (m.origin.equals(origin))
					&& (m.destination.equals(destination))
					&& (m.type.equals(type)) && (m.contents.equals(contents)));
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((contents == null) ? 0 : contents.hashCode());
		result = prime * result
				+ ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((tmpId == null) ? 0 : tmpId.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/**
	 * Called when this message has just been received and some initialization
	 * must be performed Note : This method is called by messageIsToBeReceived
	 */
	protected void initReception() {

	}

	/**
	 * Called when this message is set to be received
	 */
	public final void messageIsToBeReceived()
			throws AlreadySetToBeReceivedException, CommunicationException {
		// Verify that the message has a good status
		switch (status) {
		case INIT:

			// Good !
			break;

		case TOBESENT:

			// Also Good - loopback !
			break;

		case TOBERECEIVED:
			throw new AlreadySetToBeReceivedException("Message was already sent.");

		default:
			throw new CommunicationException();
		}

		// Do things !
		initReception();

		// Update status and stuffs
		status = MessageStatus.TOBERECEIVED;

		fireMessageIsToBeReceived();
	}

	/**
	 * Called when this message has been received Note : This method is called
	 * by received
	 */
	protected void afterReception() {

	}

	/**
	 * Called when this message have just been received
	 */
	public final void received() throws AlreadyReceivedException,
			CommunicationException {
		// Verify that the message has a good status
		switch (status) {
		case TOBERECEIVED:

			// Good !
			break;

		case RECEIVED:
			throw new AlreadyReceivedException("Message was already received.");

		default:
			throw new CommunicationException();
		}

		// Do things !
		afterReception();

		// Update status and stuffs
		status = MessageStatus.RECEIVED;
		fireMessageHasBeenReceived();
	}

	/**
	 * Called when this message is set to be sent and some initialization must
	 * be performed Note : This method is called by messageIsToBeSent
	 */
	protected void initMessageToBeSent() {

	}

	/**
	 * Called when this message is set to be sent
	 */
	public final void messageIsToBeSent() throws AlreadySetToBeSentException,
			CommunicationException {
		// Verify that the message has a good status
		switch (status) {
		case INIT:

			// Good !
			break;

		case TOBESENT:
			throw new AlreadySetToBeSentException("Message was already set to be sent.");

		default:
			throw new CommunicationException();
		}

		// Do things !
		initMessageToBeSent();

		// Update status and stuffs
		status = MessageStatus.TOBESENT;

		fireMessageIsToBeSent();
	}

	/**
	 * Called when this message have just been sent and some initialization must
	 * be performed Note : This method is called by sent
	 */
	protected void afterSending() {

	}

	/**
	 * Called when this message have just been sent
	 */
	public final void sent() throws AlreadySentException,
			CommunicationException {
		// Verify that the message has a good status
		switch (status) {
		case TOBESENT:

			// Good !
			break;

		case RECEIVED:

			// Also Good - loopback !
			break;

		case SENT:
			throw new AlreadySentException("Message was already sent.");

		default:
			throw new CommunicationException();
		}

		// Do things !
		afterSending();

		// Update status and stuffs
		status = MessageStatus.SENT;
		fireMessageHasBeenSent();
	}

	/**
	 * Manual serialization of the attributes
	 */
	private void writeObject(ObjectOutputStream out) throws IOException {

		out.writeUTF(id);
		out.writeObject(origin);
		out.writeObject(destination);
		out.writeObject(type);
		out.writeObject(contents);
		out.writeByte(status.ordinal());
	}

	/**
	 * Manual deserialization
	 */
	private void readObject(ObjectInputStream in) throws IOException,
			ClassNotFoundException, SecurityException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException {
		this.id = in.readUTF();

		Field fOrigin = Message.class.getDeclaredField("origin");
		fOrigin.setAccessible(true);
		fOrigin.set(this, in.readObject());

		Field fDestination = Message.class.getDeclaredField("destination");
		fDestination.setAccessible(true);
		fDestination.set(this, in.readObject());

		Field fType = Message.class.getDeclaredField("type");
		fType.setAccessible(true);
		fType.set(this, in.readObject());

		Field fContents = Message.class.getDeclaredField("contents");
		fContents.setAccessible(true);
		fContents.set(this, in.readObject());

		Field fListeners = Message.class.getDeclaredField("messageListeners");
		fListeners.setAccessible(true);
		fListeners.set(this, new CopyOnWriteArrayList<MessageListener>());

		this.status = MessageStatus.values()[in.readByte()];
	}

	/**
	 * Enumeration describing the different message statuses.
	 * 
	 * @author Andre Fonseca
	 * 
	 */
	public enum MessageStatus {
		INIT, 
		TOBERECEIVED, 
		TOBESENT, 
		SENT, 
		RECEIVED;
	}
}
