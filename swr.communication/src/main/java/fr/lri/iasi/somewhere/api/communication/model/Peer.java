/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A peer that can emit or receive messages
 * 
 * @author Andre Fonseca
 */
public class Peer implements Serializable {
    private static final long serialVersionUID = 645838945847L;
    private static Logger logger = LoggerFactory.getLogger(Peer.class);
    private static Logger fileLogger = LoggerFactory.getLogger("communicationEvents");
    
    /* --- Log Properties --- */
    private Marker connectedEvent = MarkerFactory.getMarker("CONNECTED");
    private Marker disconnectedEvent = MarkerFactory.getMarker("DISCONNECTED");

    /* --- Properties --- */
    /** The peer's name */
    protected final String name;

    /** Says if the peer is connected */
    protected final AtomicBoolean isConnected = new AtomicBoolean();

    /** List of this object's listeners */
    protected transient final List<PeerListener> peerListeners = new CopyOnWriteArrayList<PeerListener>();

    public Peer(String name, boolean isConnected) {
        if ((name == null) || name.equals("")) {
            throw new IllegalArgumentException("name");
        }

        this.name = name;
        this.isConnected.set(isConnected);
    }

    public Peer(String name) {
        this(name, false);
    }

    public Peer(Peer peer) {
        this(peer.name, peer.isConnected());
    }

    /* --- Accessors --- */
    public String getName() {
        return name;
    }

    public boolean isConnected() {
        return isConnected.get();
    }

    public List<PeerListener> getPeerListeners() {
        return Collections.unmodifiableList(peerListeners);
    }

    /* --- Add/remove --- */
    public void addPeerListener(PeerListener listener) {
        peerListeners.add(listener);
    }

    public void removePeerListener(PeerListener listener) {
        peerListeners.remove(listener);
    }

    /* --- Mutators --- */
    public void setConnected() {
        if (this.isConnected.compareAndSet(false, true)) {
        	firePeerIsConnecting();
        }
    }
    
    public void setNotConnected() {
    	if (this.isConnected.compareAndSet(true, false)) {
    		firePeerIsLeaving();
        }
    }

    /* --- Fire Methods --- */
    protected void firePeerIsConnecting() {
        for (PeerListener l : this.peerListeners)
            l.peerIsConnecting(this);
    }

    protected void firePeerIsLeaving() {
        for (PeerListener l : this.peerListeners)
            l.peerIsLeaving(this);
    }

    /* --- Other Methods --- */
    /**
     * Called when the peer is connecting, and some initialization must be performed
     * Note : This method is called by connect
     * @throws CommunicationException
     */
    protected void initConnection() throws CommunicationException {
        // ...
    }

    /**
     * Called when the peer is connecting
     */
    public final void connect() {
        if (isConnected.get()) {
            logger.warn("Tried to connect already connected peer " + getName());

            return;
        }

        try {
            initConnection();
            setConnected();
            fileLogger.info(connectedEvent, "{}", this);
        } catch (CommunicationException e) {
            isConnected.set(false);
            logger.warn("There was an error connecting peer " + getName() + " : " + 
            				e.getMessage());
        }
    }

    /**
     * Called when the peer is leaving, and some initialization must be performed
     * Note : This method is called by disconnect
     * @throws CommunicationException
     */
    protected void initDisconnection() throws CommunicationException {
        // ...
    }

    /**
     * Called when the peer is leaving
     */
    public void disconnect() {
        if (!isConnected.get()) {
            logger.warn("Tried to disconnect already disconnected peer " +
                getName());

            return;
        }

        try {
            initDisconnection();
            setNotConnected();
            fileLogger.info(disconnectedEvent, "{}", this);
        } catch (CommunicationException e) {
            isConnected.set(true);
            logger.warn("There was an error disconnecting peer " + getName());
        }
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof Peer) {
            Peer p = (Peer) o;

            return p.name.equals(this.name);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        String res = name;
        res += " : ";

        if (isConnected()) {
            res += "Connected";
        } else {
            res += "Disconnected";
        }

        return res;
    }
    
    /**
     * Manual serialization of the attributes
     */
    private void writeObject(ObjectOutputStream out) throws IOException {	
	    out.writeUTF(name);
	    out.writeObject(isConnected);
    }
    
    /**
     * Manual deserialization
     * @throws ClassNotFoundException 
     */
    private void readObject(ObjectInputStream in) throws IOException, SecurityException, 
    													NoSuchFieldException, IllegalArgumentException, 
    													IllegalAccessException, ClassNotFoundException {
    	Field fName = Peer.class.getDeclaredField("name");
    	fName.setAccessible(true);
    	fName.set(this, in.readUTF());
    	
    	Field fisConnected = Peer.class.getDeclaredField("isConnected");
    	fisConnected.setAccessible(true);
    	fisConnected.set(this, in.readObject());
    }
}
