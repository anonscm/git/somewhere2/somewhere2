/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;

/**
 * Abstract version message receiver. It is responsible for receiving messages from other peers.
 * To be extent on concrete communication modules.
 * 
 * @author Andre Fonseca
 *
 */
public abstract class AbstractMessageReceiver {
	protected static Logger fileLogger = LoggerFactory.getLogger("communicationEvents");
	
	/* -- Log Properties --- */
    protected Marker sessionClosed = MarkerFactory.getMarker("SESSION_CLOSED");
    
    /* -- Properties --- */
    /** The module manager */
    protected CommunicationManager communicationManager;
    
    public AbstractMessageReceiver(CommunicationManager communicationManager) {
    	if (communicationManager == null)
    		throw new IllegalArgumentException("communicationManager");
    	
    	this.communicationManager = communicationManager;
    }
	
    /* -- Methods --- */
	/**
	 * Receive message from origin
	 */
	public void receive(Message message) throws CommunicationException {
		
		if (message.getDestination().getName().equals("User"))
			return;
		
		MessageSession session = null;
		if (message.getType().isReturningType()) {
			/* If it don't need any new session because it is a returning message...
			 * (it always already exists a session waiting for it in this case...)
			 */
			session = this.communicationManager.getSessionHandler().getMessageSessions().get(message.getId());
		} else {
			// If it is not a returning message and don't need a new session anyway...
			session = this.communicationManager.getSessionHandler().getCreatorSessions().get(message.getId());
		}

		// If there is such a session, process the message, otherwise, throw an error.
		if (session != null) {
			session.receiveMessage(message);
		} else {			
			// Log information
		    final Object[] logList = {message, this};
		    fileLogger.info(sessionClosed, "{}", logList);
		}
	}
	
	/**
	 * Process the message receiving
	 * 
	 * @param message to be received
	 * @throws CommunicationException if there is a problem receiving the given message
	 */
	public abstract void processReceiving(Message message) throws CommunicationException;

}
