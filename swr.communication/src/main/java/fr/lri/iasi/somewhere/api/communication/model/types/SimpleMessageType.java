/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model.types;

import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageProcessor;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageType;
import fr.lri.iasi.somewhere.api.communication.model.impl.SimpleMessageProcessor;

/**
 * Simple text message type.
 * 
 * @author Andre Fonseca
 *
 */
public final class SimpleMessageType extends AbstractMessageType {
	private static final long serialVersionUID = 1839460L;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return "simpleMessage";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean needsNewSessionSend() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean needsNewSessionReceive() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isReturningType() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean needsToMaintainSessionSend(MessageSession session) {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean needsToMaintainSessionReceive(MessageSession session) {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MessageProcessor createProcessor(MessageSession session) {
		return new SimpleMessageProcessor(session);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String print(Message message) {
		String res = "";

		res += "\n Message infos :";
		res += ("\n\t Origin : " + message.getOrigin());
		res += ("\n\t Destination : " + message.getDestination());
		res += ("\n\t Type : " + message.getType());
		res += ("\n\t Contents : " + message.getContents());

		return res;
	}

}
