/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.configuration;

import fr.lri.iasi.somewhere.Module;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;


/**
 * Configuration of communication bundle
 * @author Andre Fonseca
 */
public class CommunicationConfiguration extends fr.lri.iasi.somewhere.Configuration {
    static Logger logger = LoggerFactory.getLogger(CommunicationConfiguration.class);
    private static final long serialVersionUID = 345838945863L;
    
    /** Name of the peer */
    protected String peerName;
    
    /** Name of the peer group, if any */
    protected String peerGroupName;

    public CommunicationConfiguration(Module module) {
        super(module);
    }
    
    /* --- Accessors --- */
    public String getPeerName() {
        return this.peerName;
    }
    
    public String getPeerGroupName() {
        return this.peerGroupName;
    }
    
    /* --- Mutators --- */
    public void setPeerName(final String peerName) {
        if ((peerName != null) && !peerName.equals("")) {
            this.peerName = peerName;
        }
    }
    
    public void setPeerGroupName(final String peerGroupName) {
        if ((peerGroupName != null) && !peerGroupName.equals("")) {
            this.peerGroupName = peerGroupName;
        }
    }

    /* --- Methods --- */
    /**
     * {@inheritDoc}
     */
    public void setAllToDefault() {
    	peerName = getComputerFullName();
    	peerGroupName = "somewherePeerGroup";
    }
    
    /**
     * Return the computer full name. <br>
     * @return the name or <b>null</b> if the name cannot be found
     */
    private String getComputerFullName() {
        String hostName = null;

        try {
            final InetAddress addr = InetAddress.getLocalHost();
            hostName = addr.getHostName();
        } catch (final Exception e) {
            // ...
        }

        return hostName;
    }
}
