/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model.impl;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.Message;

/**
 * Describes an abstract message sender. It is responsible for sending messages
 * to other peers. To be extent on concrete communication modules.
 * 
 * @author Andre Fonseca
 *
 */
public abstract class AbstractMessageSender {
    
    /* -- Properties --- */
    protected CommunicationManager communicationManager;
    
    public AbstractMessageSender(CommunicationManager communicationManager) {
    	if (communicationManager == null)
    		throw new IllegalArgumentException("communicationManager");
    	
    	this.communicationManager = communicationManager;
    }
	
	/**
	 * Send message to destination
	 */
	public void send(Message message) throws CommunicationException {
		if (message.getDestination().getName().equals("User") || 
				message.getDestination().getName().equals(message.getOrigin().getName())) {
			
			communicationManager.receiveMessage(new Message(message));
			return;
		}
			
		processSending(message);
	}
	
	/**
	 * Process the message sending
	 * 
	 * @param message to be sent
	 * @throws CommunicationException if there is a problem sending the given message
	 */
	public abstract void processSending(Message message) throws CommunicationException;

}
