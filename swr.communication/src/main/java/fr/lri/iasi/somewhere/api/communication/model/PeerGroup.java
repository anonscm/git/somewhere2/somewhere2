/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import fr.lri.iasi.somewhere.api.communication.AbstractCommunicationModule;

/**
 * Describes an abstract peer group. A peer group contains the reference of all distant peers that will be
 * part of a computation, including the local peer.
 * 
 * To be extended in a concrete communication module.
 * 
 * @author Andre Fonseca
 *
 * @param <Manager> The concrete communication module where to peergroup belongs
 * @param <Member> The concrete type of member belonging to the peergroup
 * @param <LocalMember> The concrete type of local peer
 */
public abstract class PeerGroup <Manager extends AbstractCommunicationModule<?, Member>, Member extends Peer, LocalMember extends Member> 
	implements PeerListener {

	/** The containing concrete communication module */
	protected Manager moduleManager;

	/** The peer group name */
	protected String name;

	/** The Local peer */
	protected LocalMember localPeer;

	/** The set of known peers */
	protected ConcurrentMap<String, Member> peers = new ConcurrentHashMap<String, Member>();

	public PeerGroup(Manager moduleManager) {
		if (moduleManager == null) {
			throw new IllegalArgumentException("moduleManager");
		}

		this.moduleManager = moduleManager;
	}

	/* --- Accessors --- */
	public Manager getModuleManager() {
		return moduleManager;
	}

	public String getName() {
		return this.name;
	}

	public LocalMember getLocalPeer() {
		return localPeer;
	}

	public ConcurrentMap<String, Member> getPeers() {
		return peers;
	}

	/* --- Mutators --- */
	public void setName(String name) {
		this.name = name;
	}

	public void setLocalPeer(LocalMember localPeer) {
		this.localPeer = localPeer;
	}

	public void addPeer(String name, Member peer) {
		this.peers.put(name, peer);
	}

	/* --- Methods --- */
	/**
	 * Initialize peer group.
	 */
	public abstract void init();

	/**
	 * Unititialize peer group, normally removing all neighbor peers.
	 */
	public abstract void uninit();

	/**
	 * Get a peer from its name If the peer is unknown, create it
	 * 
	 * @param name of the wanted peer
	 * @result the Peer object
	 */
	public Member retrievePeer(String name) {
		if ((name == null) || name.equals("")) {
			throw new IllegalArgumentException("name");
		}

		// Verify if its not a local peer
		if (localPeer != null && name.equals(localPeer.getName()))
			return localPeer;

		// Get a peer from the peers map, or put in if absent.
		Member peer = null;

		if (!peers.containsKey(name)) {
			peer = moduleManager.createPeer(name);
			Member oldPeer = peers.putIfAbsent(name, peer);
			if (oldPeer != null)
				peer = oldPeer;
			
			moduleManager.getCommunicationManager().addPeer(peer);
		} else {
			peer = peers.get(name);
		}

		return peer;
	}

}
