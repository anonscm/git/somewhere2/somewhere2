/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.commands;

import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import java.util.List;

/**
 * Responsible for adding a new neighbor peer.
 * 
 * @author Andre Fonseca
 */
public class AddPeerCommand implements Command {
	
    /* --- Properties --- */
	/** The communication manager module */
    private final CommunicationManager communicationManager;

    public AddPeerCommand(CommunicationManager communicationManager) {
        if (communicationManager == null) {
            throw new IllegalArgumentException("communicationManager");
        }

        this.communicationManager = communicationManager;
    }

    /* --- Accessors --- */
    public Module getModuleManager() {
        return communicationManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "addPeer";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return "Add a neighbor peer.";
    }

    /* --- Methods --- */
    /**
     * Add a new neighbor peer. If it is already a neighbor, the command will fail.
     * @param parameters : list containing the name of the new peer.
     * @return the return status
     */
    @Override
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;

        try {
            // Get useful parameters
            String peerName = parameters.get(0).toString();
            Peer peer = communicationManager.getPeers().get(peerName);

            if (peer != null) {
                res = new GenericReturnStatus("This peer is already a neighbor of the local peer.",
                        false, null);

                return res;
            }

            communicationManager.getCommunicationModule().getPeerGroup().retrievePeer(peerName);

            res = new GenericReturnStatus("Peer added sucefully", true, null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error adding the neighbor peer...",
                    false, null);
        }

        return res;
    }
}
