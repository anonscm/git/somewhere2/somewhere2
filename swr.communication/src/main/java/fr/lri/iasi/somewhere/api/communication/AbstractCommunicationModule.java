/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication;

import fr.lri.iasi.somewhere.AbstractModule;
import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.api.communication.model.PeerGroup;

/**
 * The abstract communication module to be extended by a concrete communication module.
 * 
 * @author Andre Fonseca
 *
 * @param <ConcretePeerGroup> The specific peer group used by the concrete communication module.
 * @param <Member> The specific peer type used by the concrete communication module.
 */
public abstract class AbstractCommunicationModule <ConcretePeerGroup extends PeerGroup<?,? extends Peer,? extends Peer>,
												   Member extends Peer> extends AbstractModule {
    /** The communication manager */
    protected final CommunicationManager communicationManager;
    
    /** The group of known peers */
    protected ConcretePeerGroup peerGroup;

    public AbstractCommunicationModule(final App app) {
        super(app);

        // Find a communicationManager
        this.communicationManager = CommunicationManager.getModuleInstance(app);
        
        setLoadingOrder(3);

        if (communicationManager == null) {
            throw new UnsupportedOperationException(
                "A communicationManager module must be loaded");
        }
    }
    
    /* --- Accessors --- */
    public CommunicationManager getCommunicationManager() {
        return communicationManager;
    }

    public ConcretePeerGroup getPeerGroup() {
        return peerGroup;
    }
    
    /* --- Methods --- */
    /**
     * Restart the communication module.
     */
    public abstract void restart();

    /**
     * Creates specific peer from its name.
     * 
     * @param peerName : the name of the peer to be created
     * @return the specific peer
     */
    public abstract Member createPeer(String peerName);

    /**
     * Creates specific message from a generic message.
     * 
     * @param msg : the generic message
     * @return the specific message used by the communication module.
     */
    public abstract Message createMessage(Message msg);
}
