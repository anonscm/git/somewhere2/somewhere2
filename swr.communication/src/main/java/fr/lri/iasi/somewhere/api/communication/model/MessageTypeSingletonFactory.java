/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageType;

/**
 * Singleton factory pattern for message types. Message types are unique objects registered stored on a static map.
 * 
 * @author Andre Fonseca
 *
 */
public final class MessageTypeSingletonFactory {
	
	/* --- Properties --- */
	/** Map containing registered MessageTypes indexed by its name */
	private static final Map<Class<? extends AbstractMessageType>, AbstractMessageType> singletonRegistry = new ConcurrentHashMap<Class<? extends AbstractMessageType>, AbstractMessageType>();
	
	private MessageTypeSingletonFactory() {
	}
	
	/* --- Methods --- */
	/**
	 * Register message type indicated by its class.
	 * 
	 * @param messageTypeClass : the class of the message type.
	 * @return the message type object
	 */
	public static AbstractMessageType create(Class<? extends AbstractMessageType> messageTypeClass) {
		if (singletonRegistry.containsKey(messageTypeClass))
			return singletonRegistry.get(messageTypeClass);
		
		AbstractMessageType messageType = null;
		try {
			messageType = messageTypeClass.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		singletonRegistry.put(messageTypeClass, messageType);
		return messageType;
	}

}
