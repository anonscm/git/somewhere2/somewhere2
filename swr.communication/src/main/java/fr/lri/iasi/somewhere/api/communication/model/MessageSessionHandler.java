/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.api.communication.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Listener responsible for creates and stores message sessions. Also, it
 * addresses messages to be treated by their respective message sessions.
 * 
 * @author Andre Fonseca
 * 
 */
public class MessageSessionHandler implements MessageListener {
	static Logger logger = LoggerFactory.getLogger(MessageSessionHandler.class);
	private static Logger fileLogger = LoggerFactory.getLogger("communicationEvents");
    
	/* -- Log Properties --- */
    private Marker tmpSessionCreation = MarkerFactory.getMarker("TMP_SESSION_CREATION");

	/* --- Properties --- */
	/** The communication manager module */
	private final CommunicationManager communicationManager;

	/** Map of message sessions by its id */
	private final ConcurrentHashMap<String, MessageSession> messageSessions = new ConcurrentHashMap<String, MessageSession>();

	/** Map of message sessions by the id of the session that triggered its creation. */
	private final ConcurrentHashMap<String, MessageSession> creatorSessions = new ConcurrentHashMap<String, MessageSession>();

	/**
	 * Map of message sessions temporary ids by the real message ids. Its is used 
	 * when the message id (creator session id) need to be changed to be treated 
	 * by different sessions.
	 */
	private final Map<String, String> tmpCreatorIds = new ConcurrentHashMap<String, String>();

	public MessageSessionHandler(CommunicationManager communicationManager) {
		if (communicationManager == null)
			throw new IllegalArgumentException("communicationManager");

		this.communicationManager = communicationManager;
		this.communicationManager.addMessageListener(this);
	}

	/* --- Accessors --- */
	public Map<String, MessageSession> getMessageSessions() {
		return Collections.unmodifiableMap(messageSessions);
	}

	public Map<String, MessageSession> getCreatorSessions() {
		return Collections.unmodifiableMap(creatorSessions);
	}

	CommunicationManager getCommunicationManager() {
		return communicationManager;
	}

	/* --- Mutators --- */
	/**
	 * Adds a message session for a given message.
	 * 
	 * @param message : arrival message
	 * @return the new message session
	 */
	MessageSession addMessageSession(Message message) {
		MessageSession session = null;
		MessageSession previousCreator = null;

		if (!creatorSessions.containsKey(message.getId())) {
			// If session do not exists, everything is ok. Creating a new session...
			session = new MessageSession(communicationManager, message.getId(), message.getOrigin().getName());
			
			// Checks, atomically, if there is another creator session with the same id. If yes, ignore the created session...
			previousCreator = creatorSessions.putIfAbsent(message.getId(), session);
			if (previousCreator == null) {
				messageSessions.put(session.getSessionId(), session);
				return session;
			}
		}
		
		/*
		 * If creator session for the message id already exists, it means that two or more messages 
		 * were sent from the same session to this peer. In this case it is necessary to create a 
		 * new session...
		 */
		session = MessageSession.createMessageSessionForNewMessage(communicationManager, message.getOrigin().getName());
		creatorSessions.put(session.getCreatorSessionId(), session);
		messageSessions.put(session.getSessionId(), session);
		tmpCreatorIds.put(session.getCreatorSessionId(), message.getId());
		
		/* log the tmp session creation */
		final Object[] logList = {session.getCreatorSessionId(), message.getId()};
		fileLogger.info(tmpSessionCreation, "<temp>{}</temp>\n<creatorSessionId>{}</creatorSessionId>", logList);

		/* ...and temporarily replace the message id by the id of this new session in order 
		 * for the message to be treated by it. The real message id will be restored afterwards.
		 */
		message.setTmpId(message.getId());
		message.setId(session.getCreatorSessionId());			

		return session;
	}

	/**
	 * Adds a message session for totally new message (i.e: user's message).
	 * 
	 * @return the new message session
	 */
	public MessageSession addMessageSessionForNewMessage() {
		MessageSession session = MessageSession.createMessageSessionForNewMessage(communicationManager);
		messageSessions.put(session.getSessionId(), session);
		creatorSessions.put(session.getCreatorSessionId(), session);
		return session;
	}

	/**
	 * Removes message session from session handler.
	 */
	void removeMessageSession(String sessionId) {
		MessageSession session = messageSessions.remove(sessionId);
		if (session != null) {
			creatorSessions.remove(session.getCreatorSessionId());
			tmpCreatorIds.remove(session.getCreatorSessionId());
			session.die();
		}
	}

	/**
	 * Removes message session from session handler using the id of
	 * its creator.
	 * 
	 * @param creatorId: the id of the target session creator.
	 */
	void removeMessageSessionByCreatorId(String creatorId) {
		MessageSession session = creatorSessions.remove(creatorId);
		if (session != null) {
			messageSessions.remove(session.getSessionId());
			tmpCreatorIds.remove(creatorId);
			session.die();
		}
	}

	/* --- Methods --- */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void messageIsToBeReceived(final Message message) throws CommunicationException {
		final MessageSession session;
		
		// If the message needs a new session when it arrives...
		if (message.getType().needsNewSessionReceive()) {
			if (message.getId() != null) {
				// If it is sent by another peer...
				session = addMessageSession(message);
			} else {
				// If it is sent by the user...
				session = addMessageSessionForNewMessage();
				message.setId(session.getSessionId());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void messageIsToBeSent(Message message) throws CommunicationException {

		if (message.getId() == null) {
			// If it is being sent by the user...
			MessageSession session = addMessageSessionForNewMessage();
			message.setId(session.getSessionId());
		} else if (message.getType().isReturningType()) {
			// If it is a returning message...
			MessageSession session = messageSessions.get(message.getId());

			if (session != null) {
				/* If the indicated session does not exist, checks if the
				 * message id was replaced temporally.
				 */
				String tmpId = tmpCreatorIds.get(session.getCreatorSessionId());

				if (tmpId != null) {
					// If yes, restore the original id
					String id = message.getId();

					message.setTmpId(id);
					message.setId(tmpId);
				} else {
					message.setId(session.getCreatorSessionId());
				}
			}
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void messageHasBeenSent(Message message) {

		if (message.getType().isReturningType()) {
			/* Checks if the message id was replaced temporally, uses the original id
			 * in order to address the creator session on the origin peer.
			 */
			String creatorId;
			if (message.getTmpId() != null)
				creatorId = message.getTmpId();
			else
				creatorId = message.getId();

			// If the message don't need to maintain the session alive after sent...
			MessageSession creatorSession = creatorSessions.get(creatorId);
			if (creatorSession != null && !message.getType().needsToMaintainSessionSend(creatorSession))
				removeMessageSessionByCreatorId(creatorId);
		} else {
			// Checks if the message id was replaced temporally and restores the original id.
			if (message.getTmpId() != null) {
				String tmpId = message.getId();
				String id = message.getTmpId();

				message.setId(id);
				message.setTmpId(tmpId);
			}

			// If the message don't need to maintain the session alive after sent...
			MessageSession session = messageSessions.get(message.getId());
			if (session != null && !message.getType().needsToMaintainSessionSend(session))
				removeMessageSession(message.getId());
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void messageHasBeenReceived(Message message) {

		// Verifies if the message needs to maintain session alive after its reception.
		if (message.getType().isReturningType()) {
			MessageSession creatorSession = messageSessions.get(message.getId());
			
			if (creatorSession != null && !message.getType().needsToMaintainSessionReceive(creatorSession))
				removeMessageSession(message.getId());
		} else {
			MessageSession session = creatorSessions.get(message.getId());
			if (session != null && !message.getType().needsToMaintainSessionReceive(session))
				removeMessageSessionByCreatorId(message.getId());
		}

	}

}
