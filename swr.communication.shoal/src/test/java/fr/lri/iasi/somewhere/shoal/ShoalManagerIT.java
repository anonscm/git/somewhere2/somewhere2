/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.shoal;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.ui.Command;


public class ShoalManagerIT {
	
	private static ShoalManager shoal_first;
	private static ShoalManager shoal_second;
	
	@BeforeClass
    public static void initializeFirst() throws InterruptedException {
		initializeSecond();
		
		App app_first = new FakeShoalApp();
		CommunicationManager communicationManager = new CommunicationManager(app_first);
		communicationManager.getConfiguration().setPeerName("first");
		communicationManager.getConfiguration().setPeerGroupName("unit_test");
		app_first.getModules().add(communicationManager);
		
		shoal_first = new ShoalManager(app_first);
		communicationManager.setCommunicationModule(shoal_first);
		app_first.getModules().add(shoal_first);

		shoal_first.init();
    }
	
    public static void initializeSecond() throws InterruptedException {
    	App app_second = new FakeShoalApp();
		CommunicationManager communicationManager = new CommunicationManager(app_second);
		communicationManager.getConfiguration().setPeerName("second");
		communicationManager.getConfiguration().setPeerGroupName("unit_test");
		app_second.getModules().add(communicationManager);
		
		shoal_second = new ShoalManager(app_second);
		communicationManager.setCommunicationModule(shoal_second);
		app_second.getModules().add(shoal_second);

		shoal_second.init();
    }
	
	@Test
	public void testAddPeer() {		
		// Adding peer
		Peer peer = shoal_first.getPeerGroup().retrievePeer("second");
		assertTrue(peer.isConnected());
	}
	
	@Test
	public void sendMessageCorrectly() {
		// Adding peer, if it is not already added
		Peer peer = shoal_first.getPeerGroup().retrievePeer("second");
		
		// Sending message
		Command sendTextMessage = shoal_first.getApp().getUI().getCommand("sendTextMessage");
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(peer.getName());
		parameters.add("Hey!");
		
		assertTrue(sendTextMessage.execute(parameters).isSuccessful());
	}
	
	//TODO Not working on the same VM
	/*@Test
	public void notifyNotAcessible() throws InterruptedException {
		Peer peer = shoal_first.getPeerGroup().retrievePeer("second");
		shoal_second.getPeerGroup().getLocalPeer().setAccessible(false);
		Thread.sleep(100);

		assertTrue(peer.isConnected());
		assertFalse(peer.isAccessible());
	}
	
	@Test
	public void notifyAcessible() throws InterruptedException {
		Peer peer = shoal_first.getPeerGroup().retrievePeer("second");
		shoal_second.getPeerGroup().getLocalPeer().setAccessible(true);
		Thread.sleep(100);

		assertTrue(peer.isConnected());
		assertTrue(peer.isAccessible());
	}*/
	
	@Test
	public void testRemovePeer() {		
		// Adding peer, if it is not already added
		Peer peer = shoal_first.getPeerGroup().retrievePeer("second");
		
		// Removing peer
		shoal_first.getCommunicationManager().removePeer(peer);	
		assertFalse(peer.isConnected());
	}
}
