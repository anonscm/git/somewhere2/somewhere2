/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.shoal;

import java.util.Properties;

import com.sun.enterprise.ee.cms.core.GMSException;
import com.sun.enterprise.ee.cms.core.GMSFactory;
import com.sun.enterprise.ee.cms.core.GroupHandle;
import com.sun.enterprise.ee.cms.core.GroupManagementService;
import com.sun.enterprise.ee.cms.core.ServiceProviderConfigurationKeys;

import fr.lri.iasi.somewhere.api.communication.configuration.CommunicationConfiguration;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.api.communication.model.PeerGroup;
import fr.lri.iasi.somewhere.shoal.configuration.ShoalConfiguration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents a peer group, containing:
 *        - a local peer
 *        - its neighbors
 */
public class ShoalPeerGroup extends PeerGroup<ShoalManager, ShoalPeer, ShoalLocalPeer> {
    static Logger logger = LoggerFactory.getLogger(ShoalPeerGroup.class);

    /* --- Properties --- */
    /** Shoal GMS (manager) */
    private GroupManagementService gms;

    /** The Message Reception Handler */
    protected final MessageReceiver messageReceiver;

    /** The Message Join Group Handler */
    protected final JoinGroupHandler joinGroupHandler;

    /** The Message Leaving Peer Handler */
    protected final LeavingPeerHandler leavingPeerHandler;

    public ShoalPeerGroup(final ShoalManager moduleManager) {
        super(moduleManager);

        // Register itself as a peer Listener
        this.moduleManager.getCommunicationManager().addPeerListener(this);

        // Create handlers for message sending and reception
        this.moduleManager.getCommunicationManager().setMessageSender(new MessageSender(this));
        this.messageReceiver = new MessageReceiver(this);
        this.moduleManager.getCommunicationManager().setMessageReceiver(this.messageReceiver);

        // Create handlers for group notifications
        this.joinGroupHandler = new JoinGroupHandler(this);
        this.leavingPeerHandler = new LeavingPeerHandler(this);
    }

    /* --- Accessors --- */
    public GroupManagementService getGMS() {
        return gms;
    }

    public ShoalManager getModuleManager() {
        return moduleManager;
    }

    /**
     * Uninitialize our stuffs about the peer group
     */
    public void uninit() {
    	this.localPeer.disconnect();
    }

    /**
     * Initialize the peerGroup
     */
    public void init() {
        logger.info("Starting Shoal framework...");
        
        CommunicationConfiguration communicationConfig = this.moduleManager.getCommunicationManager().getConfiguration();
        
        // Get the group name from the configuration
        this.name = communicationConfig.getPeerGroupName();
        
        // Initialize the Shoal GMS
        this.gms = initializeGMS(communicationConfig.getPeerName(), this.name);

        // Create the local peer
        this.localPeer = new ShoalLocalPeer(communicationConfig.getPeerName(), this, this.gms);
        
        // Registering group events
        this.messageReceiver.registerForGroupEvents(this.gms);
        this.joinGroupHandler.registerForGroupEvents(this.gms);
        this.leavingPeerHandler.registerForGroupEvents(this.gms);
        
        // Connect the local peer
        this.localPeer.connect();
        
        // Report the connection to other peers
        this.gms.reportJoinedAndReadyState();
    }

    /**
     * Initialize Shoal GMS
     */
    private GroupManagementService initializeGMS(final String serverName,
        final String groupName) {
        logger.info("Initializing Shoal for member:" + serverName + " group:" +
            groupName);
        
        // Getting configuration properties
        ShoalConfiguration configuration = this.moduleManager.getConfiguration();
        
        Properties props = new Properties();
        props.put(ServiceProviderConfigurationKeys.IS_BOOTSTRAPPING_NODE.toString(), String.valueOf(configuration.isBootStrapping()));
        props.put(ServiceProviderConfigurationKeys.LOOPBACK.toString(), String.valueOf(configuration.isLoopBack()));
        props.put(ServiceProviderConfigurationKeys.FAILURE_DETECTION_RETRIES.toString(), String.valueOf(configuration.getFailureDetectionRetries()));
        props.put(ServiceProviderConfigurationKeys.FAILURE_VERIFICATION_TIMEOUT.toString(), String.valueOf(configuration.getFailureVerificationTimeout()));
        props.put(ServiceProviderConfigurationKeys.FAILURE_DETECTION_TIMEOUT.toString(), String.valueOf(configuration.getFailureDetectionTimeout()));
        props.put(ServiceProviderConfigurationKeys.INCOMING_MESSAGE_THREAD_POOL_SIZE, String.valueOf(Integer.MAX_VALUE));
        
        if (!configuration.getInterfaceName().isEmpty())
        	props.put(ServiceProviderConfigurationKeys.BIND_INTERFACE_ADDRESS.toString(), configuration.getInterfaceName());
        
        if (!configuration.getSeeds().isEmpty())
        	props.put(ServiceProviderConfigurationKeys.VIRTUAL_MULTICAST_URI_LIST.toString(), String.valueOf(configuration.getSeeds()));
        
        if (configuration.getPreferIPv6()) 
        	System.setProperty("java.net.preferIPv6Addresses", "true");
        else
        	System.setProperty("java.net.preferIPv6Addresses", "false");
        
        // Initialize GMS and return it
        return (GroupManagementService) GMSFactory.startGMSModule(serverName,
            groupName, configuration.getPeerType(), props);
    }

    /**
     * {@inheritDoc}
     */
    public void peerIsConnecting(Peer peer) {
    }

    /**
     * {@inheritDoc}
     */
    public void peerIsLeaving(Peer peer) {
    }
}
