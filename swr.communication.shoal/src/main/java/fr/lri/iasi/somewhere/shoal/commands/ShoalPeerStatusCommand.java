/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.shoal.commands;

import com.sun.enterprise.ee.cms.core.GroupManagementService;

import fr.lri.iasi.somewhere.shoal.ShoalManager;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import java.io.Serializable;

import java.util.List;
import java.util.Map;


/**
 * A command to print infos on the Shoal peer
 */
public class ShoalPeerStatusCommand implements Command {
    /* --- Properties --- */
    private final ShoalManager moduleManager;

    public ShoalPeerStatusCommand(final ShoalManager moduleManager) {
        if (moduleManager == null) {
            throw new IllegalArgumentException("moduleManager");
        }

        this.moduleManager = moduleManager;
    }

    /* --- Accessors --- */
    public ShoalManager getModuleManager() {
        return moduleManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "shoalPeerStatus";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return "Print infos about the Shoal Peer.";
    }

    /**
     * {@inheritDoc}
     *  NOTE : note that the return status contains only text
     *  information
     */
    @Override
    public ReturnStatus execute(final List<Object> parameters) {
        ReturnStatus res = null;

        try {
        	StringBuilder str = new StringBuilder();

            str.append("Infos about local Shoal peer : \n");

            GroupManagementService gms = moduleManager.getPeerGroup().getGMS();
            Map<Serializable, Serializable> members = gms.getAllMemberDetails(
                    "memberToken");

            for (Serializable member : members.values())
            	str.append("\t" + member + "\n");

            res = new GenericReturnStatus(str.toString(), true, null);
        } catch (final Exception e) {
            res = new GenericReturnStatus("There was an error retreiving Shoal infos...",
                    false, null);
        }

        return res;
    }
}
