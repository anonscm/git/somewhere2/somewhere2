/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.shoal;

import com.sun.enterprise.ee.cms.core.CallBack;
import com.sun.enterprise.ee.cms.core.GroupManagementService;
import com.sun.enterprise.ee.cms.core.Signal;
import com.sun.enterprise.ee.cms.impl.client.PlannedShutdownActionFactoryImpl;

import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.Peer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Used to deal with leaving peer group messages reception
 * 
 * @author Andre Fonseca
 */
public class LeavingPeerHandler implements CallBack {
    static Logger logger = LoggerFactory.getLogger(LeavingPeerHandler.class);
    static Logger errorLogger = LoggerFactory.getLogger("errorLogger");

    /* --- Properties --- */

    /** The PeerGroup this peer is in */
    protected final ShoalPeerGroup peerGroup;

    /** The communication manager */
    protected final CommunicationManager communicationManager;

    public LeavingPeerHandler(final ShoalPeerGroup peerGroup) {
        if (peerGroup == null) {
            throw new IllegalArgumentException("peerGroup");
        }

        this.peerGroup = peerGroup;

        this.communicationManager = peerGroup.getModuleManager()
                                             .getCommunicationManager();
    }

    /* --- Accessors --- */
    public ShoalPeerGroup getPeerGroup() {
        return peerGroup;
    }

    /**
     * Called when something happens with Shoal
     * @param signal Describes what happened
     */
    @Override
    public void processNotification(Signal signal) {
        logger.debug("Received Notification of type : " +
            signal.getClass().getName());

        try {
            logger.debug("Failure Signal received : Source=" +
                signal.getMemberToken());

            Peer peer = peerGroup.getPeers().get(signal.getMemberToken());

            if ((peer != null) && !(peer.equals(peerGroup.getLocalPeer()))) {
            	if (peer.isConnected())
            		peer.disconnect();
            }

        } catch (final Exception e) {
        	errorLogger.error("Exception occured while dealing with Shoal signal : {}", e.getMessage());
        }
    }

    /**
     * Register with shoal events we watch (received by the <code>processNotification</code> methods)
     * @param gms the Gms to register the events to
     */
    void registerForGroupEvents(final GroupManagementService gms) {
        gms.addActionFactory(new PlannedShutdownActionFactoryImpl(this));
    }
}
