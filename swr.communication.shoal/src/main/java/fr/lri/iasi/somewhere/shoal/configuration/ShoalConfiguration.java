/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.shoal.configuration;

import fr.lri.iasi.somewhere.Module;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.enterprise.ee.cms.core.GroupManagementService;
import com.sun.enterprise.ee.cms.core.GroupManagementService.MemberType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Configuration of shoal bundle
 * 
 * @author Andre Fonseca
 */
public class ShoalConfiguration extends fr.lri.iasi.somewhere.Configuration {
	static Logger logger = LoggerFactory.getLogger(ShoalConfiguration.class);
	private static final long serialVersionUID = 745838945963L;

	/* --- Properties --- */
	/** Type of the Peer */
	protected MemberType peerType;

	/** If the localPeer will receive notifications of itself */
	protected boolean loopBack;

	/** If the peer will be a bootstrapping host for other members to use for discovery purposes */
	protected boolean bootStrapping;

	/** Number of concurrent threads for MessageSender */
	protected int messageSenderThreadsNr;

	/** List of Seed peers. Must be MULTICAST addresses (e.g: tcp://192.168.32.1:5200) */
	protected List<String> seeds;

	/** Interface name where the application will be binded in case of multiple NICs */
	protected String interfaceName;

	/** Modify the transport to ipv6, if available */
	protected boolean preferIPv6;
	
	/** Indicates the number of retries in order to set a peer as 'failed' */
	protected int failureDetectionRetries;	

	/** Indicates the timeout to detect peer as 'failed' */
	protected int failureDetectionTimeout;
	
	/** Indicates the timeout verify failed peers */
	protected int failureVerificationTimeout;

	public ShoalConfiguration(final Module module) {
		super(module);
	}

	/* --- Mutators --- */
	public void setMessageSenderThreadsNr(final int messageSenderThreadsNr) {
		this.messageSenderThreadsNr = messageSenderThreadsNr;
	}

	public void setPeerType(MemberType peerType) {
		this.peerType = peerType;
	}

	public void setLoopBack(boolean loopBack) {
		this.loopBack = loopBack;
	}

	public void setBootStrapping(boolean bootStrapping) {
		this.bootStrapping = bootStrapping;
	}

	public void setSeeds(List<String> seeds) {
		this.seeds = seeds;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	public void setPreferIPv6(boolean preferIPv6) {
		this.preferIPv6 = preferIPv6;
	}
	
	public void setFailureDetectionTimeout(int failureDetectionTimeout) {
		this.failureDetectionTimeout = failureDetectionTimeout;
	}
	
	public void setFailureDetectionRetries(int failureDetectionRetries) {
		this.failureDetectionRetries = failureDetectionRetries;
	}
	
	public void setFailureVerificationTimeout(int failureVerificationTimeout) {
		this.failureVerificationTimeout = failureVerificationTimeout;
	}

	/* --- Accessors --- */
	public int getMessageSenderThreadsNr() {
		return messageSenderThreadsNr;
	}

	public MemberType getPeerType() {
		return peerType;
	}

	public boolean isLoopBack() {
		return loopBack;
	}

	public boolean isBootStrapping() {
		return bootStrapping;
	}

	public String getSeeds() {
		StringBuilder builder = new StringBuilder();
		Iterator<String> it = seeds.iterator();
		while (it.hasNext()) {
			builder.append(it.next());
			if (it.hasNext())
				builder.append(", ");
		}
		return builder.toString();
	}

	public String getInterfaceName() {
		return interfaceName;
	}

	public boolean getPreferIPv6() {
		return preferIPv6;
	}
	
	public int getFailureVerificationTimeout() {
		return failureVerificationTimeout;
	}
	
	public int getFailureDetectionTimeout() {
		return failureDetectionTimeout;
	}

	public int getFailureDetectionRetries() {
		return failureDetectionRetries;
	}
	
	/* --- Methods --- */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setAllToDefault() {
		peerType = GroupManagementService.MemberType.CORE;
		loopBack = false;
		bootStrapping = false;
		messageSenderThreadsNr = 25;
		seeds = new ArrayList<String>();
		interfaceName = "";
		preferIPv6 = false;
		failureDetectionRetries = 5;
		failureDetectionTimeout = 500;
		failureVerificationTimeout = 500;
	}

}
