/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.shoal;

import com.sun.enterprise.ee.cms.core.GroupHandle;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageSender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;


/**
 * Used to deal with messages sending
 * 
 * @author Andre Fonseca
 */
public final class MessageSender extends AbstractMessageSender {
    static Logger logger = LoggerFactory.getLogger(MessageSender.class);

    /* --- Properties --- */

    /** The PeerGroup this peer is in */
    private final ShoalPeerGroup peerGroup;

    /** The concurrent ExecutorService that will execute the different threads/routines used */
    private final ExecutorService messageExecutorService;

    MessageSender(final ShoalPeerGroup peerGroup) {
    	super(peerGroup.getModuleManager()
                .getCommunicationManager());

        this.peerGroup = peerGroup;
        final int nbThreads = peerGroup.getModuleManager().getConfiguration().getMessageSenderThreadsNr();
        this.messageExecutorService = Executors.newFixedThreadPool(nbThreads);
    }

    /* --- Accessors --- */
    public ShoalPeerGroup getPeerGroup() {
        return peerGroup;
    }

    /**
     * Uninitialize the message sender.
     */
    public void uninit() {
        messageExecutorService.shutdownNow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processSending(final Message message) 
    	throws CommunicationException {
        ShoalPeer destination = peerGroup.retrievePeer(message.getDestination().getName());

        if (destination.isConnected()) {
        	Future<Boolean> future = messageExecutorService.submit(new SendMsg(destination, message));
        	
        	try {
				future.get();
			} catch (InterruptedException e) {
				Thread.interrupted();
			} catch (ExecutionException e) {
				throw new CommunicationException(e);
			}
        } else {
            throw new CommunicationException("Cannot connect to " + destination);
        }
    }

    /**
     * Class used to send a message
     * 
     * @author Andre Fonseca
     */
    private final class SendMsg implements Callable<Boolean> {
        /** The message to send */
        private final Message message;
        
        /** The destination peer */
        private final ShoalPeer peer;
        
        /** The Shoal group handle object */
        private final GroupHandle gh;

        private SendMsg(final ShoalPeer peer, final Message message) {
            if (message == null) {
                throw new IllegalArgumentException("message");
            }

            this.message = ShoalMessage.createShoalMessage(message);
            this.peer = peer;
            this.gh = peerGroup.getGMS().getGroupHandle();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Boolean call() throws CommunicationException{
            ByteArrayOutputStream bytes = null;
            ObjectOutputStream obInputStream = null;

            try {       	
                // Serialize the message
            	byte[] serializedObject = null;
            	bytes = new ByteArrayOutputStream();
            	
                obInputStream = new ObjectOutputStream(bytes);
                obInputStream.writeObject(message);
                obInputStream.flush();
                obInputStream.close();
                serializedObject = bytes.toByteArray();
                bytes.close();

                // Send message
                gh.sendMessage(peer.getName(), peerGroup.getName(), serializedObject);

            } catch (final Exception e) {
                throw new CommunicationException(e);
            } finally {
                try {
                    if (bytes != null) {
                    	bytes.close();
                    }

                    if (obInputStream != null) {
                    	obInputStream.close();
                    }
                } catch (final IOException e) {
                	throw new CommunicationException(e);
                }
            }
            
            return true;
        }
    }
}
