/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.shoal;

import com.sun.enterprise.ee.cms.core.MessageSignal;

import fr.lri.iasi.somewhere.api.communication.model.MessageType;
import fr.lri.iasi.somewhere.api.communication.model.Peer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import java.util.Map;


/**
 * Describes a Shoal message
 * 
 * @author Andre Fonseca
 */
public final class ShoalMessage extends fr.lri.iasi.somewhere.api.communication.model.Message {
    static Logger logger = LoggerFactory.getLogger(ShoalMessage.class);
    private static final long serialVersionUID = 745838945840L;

    /* --- Methods --- */
    private ShoalMessage(final String id, final Peer origin, final Peer destination,
        final MessageType type, final Map<String, Object> contents) {
        super(id, origin, destination, type, contents);
    }

    /**
     * Creates a Shoal message from another message
     * @param msg
     * @return a shoal message
     */
    public static ShoalMessage createShoalMessage(
        final fr.lri.iasi.somewhere.api.communication.model.Message msg) {
        return new ShoalMessage(msg.getId(), msg.getOrigin(), msg.getDestination(),
            msg.getType(), msg.getContents());
    }

    /**
     * Unmarshal and creates a Shoal message from a signal
     * @param peerGroup
     * @param msgSignal
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static ShoalMessage createShoalMessage(
        final ShoalPeerGroup peerGroup, final MessageSignal msgSignal)
        throws IOException, ClassNotFoundException {
        if (msgSignal == null) {
            throw new IllegalArgumentException("msgSignal");
        }

        // Deserialization of the msg
        ShoalMessage msg = null;
        ByteArrayInputStream foo = null;
        ObjectInputStream bar = null;

        try {
            // Serialize the message
            foo = new ByteArrayInputStream(msgSignal.getMessage());
            bar = new ObjectInputStream(foo);
            msg = (ShoalMessage) bar.readObject();

            // Close outputstreams
            bar.close();
            foo.close();
        } finally {
            try {
                if (foo != null) {
                    foo.close();
                }

                if (bar != null) {
                    bar.close();
                }
            } catch (final Exception e) {
                // ...
            }
        }

        return msg;
    }

}
