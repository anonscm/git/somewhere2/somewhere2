/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.shoal;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.api.communication.AbstractCommunicationModule;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.shoal.commands.ShoalPeerStatusCommand;
import fr.lri.iasi.somewhere.shoal.configuration.ShoalConfiguration;
import fr.lri.iasi.somewhere.shoal.configuration.ShoalXMLConfiguration;
import fr.lri.iasi.somewhere.ui.UI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.slf4j.bridge.SLF4JBridgeHandler;

import org.w3c.dom.Node;


/**
 * The main class of the Shoal module
 * @author Andre Fonseca
 */
public class ShoalManager extends AbstractCommunicationModule<ShoalPeerGroup, ShoalPeer> {
    static Logger logger = LoggerFactory.getLogger(ShoalManager.class);

    /* --- Constants --- */
    /** The name of this module */
    protected static final String moduleName = "shoal";

    /* --- Properties --- */

    /** The configuration for this module */
    protected final ShoalXMLConfiguration configuration;
    

    public ShoalManager(final App app) {
        super(app);

        // Base init stuffs
        initShoalLog();

        // Set this module as the concrete communication module
        communicationManager.setCommunicationModule(this);

        // Init conf
        configuration = new ShoalXMLConfiguration(this);

        // Create a new peergroup
        peerGroup = new ShoalPeerGroup(this);

        // Add Commands to the UI
        addBaseCommands();
    }

    /* --- Accessors --- */
    public ShoalConfiguration getConfiguration() {
        return configuration;
    }

    public ShoalPeerGroup getPeerGroup() {
        return peerGroup;
    }

    /**
     * Set Shoal Logging parameters
     */
    protected void initShoalLog() {
        SLF4JBridgeHandler.install();
        
        java.util.logging.Logger ltmp = java.util.logging.Logger.getLogger("ShoalLogger");

        ltmp.setLevel(java.util.logging.Level.OFF);
    }

    /**
     * @return the current Shoal module instance
     */
    public static ShoalManager getModuleInstance(final App app) {
        if (app == null) {
            throw new IllegalArgumentException("app");
        }

        final Module m = app.getModule(moduleName);

        if (m == null) {
            return null;
        }

        try {
            final ShoalManager res = (ShoalManager) m;

            return res;
        } catch (final Exception e) {
            return null;
        }
    }

    /**
     * Add shoal commands to the UI
     */
    protected void addBaseCommands() {
        final UI ui = app.getUI();
        ui.addCommand(new ShoalPeerStatusCommand(this));
    }

    /**
     * {@inheritDoc}
     */
    public String getModuleName() {
        return moduleName;
    }

    /**
     * {@inheritDoc}
     */
    public void quit() {
        // Uninit the peer group
        peerGroup.uninit();
    }

    /**
     * {@inheritDoc}
     */
    public void init() {
        try {    	
            // Create the peer group
            peerGroup.init();
        } catch (final Exception e) {
            quit();
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void importConfigXML(final Node config) {
        configuration.parseConfig(config);
    }

    /**
     * {@inheritDoc}
     */
    public Node exportConfigXML() {
        try {
            return configuration.export();
        } catch (final Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public ShoalPeer createPeer(String peerName) {
        return new ShoalPeer(peerName, this.getPeerGroup(),
            this.getPeerGroup().getGMS());
    }

    /**
     * {@inheritDoc}
     */
    public Message createMessage(Message msg) {
        return ShoalMessage.createShoalMessage(msg);
    }
    
    /**
     * {@inheritDoc}
     */
	public void restart() {
		
		if (peerGroup.getLocalPeer() != null) {
			// Stopping shoal
			peerGroup.uninit();
			
			// Restarting shoal
			try {
				peerGroup.init();
			} catch (Exception e) {
				throw new RuntimeException(e.getMessage(), e);
			}
		}
	}
}
