/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.shoal;

import com.sun.enterprise.ee.cms.core.GroupManagementService;
import com.sun.enterprise.ee.cms.spi.MemberStates;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.model.Peer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * A peer that can emit or receive messages - Shoal version
 * @author Andre Fonseca
 */
public class ShoalPeer extends Peer {
    static Logger logger = LoggerFactory.getLogger(ShoalPeer.class);
    private static final long serialVersionUID = 845838945847L;

    /* --- Properties --- */
    /** The PeerGroup this peer is in */
    protected transient final ShoalPeerGroup peerGroup;

    /** The Shoal GMS associated with this peer */
    protected transient final GroupManagementService gms;

    public ShoalPeer(final String name, final ShoalPeerGroup peerGroup,
        final GroupManagementService gms) {
        super(name);

        if (peerGroup == null) {
            throw new IllegalArgumentException("peerGroup");
        }

        if (gms == null) {
            throw new IllegalArgumentException("gms");
        }

        this.peerGroup = peerGroup;
        this.gms = gms;
    }

    /* --- Accessors --- */
    public ShoalPeerGroup getPeerGroup() {
        return peerGroup;
    }

    public GroupManagementService getGMS() {
        return gms;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initConnection() throws CommunicationException {
        MemberStates state = gms.getGroupHandle().getMemberState(name);
        
        if (state == MemberStates.PEERSTOPPING || state == MemberStates.UNKNOWN ||
        		state == MemberStates.STOPPED || state == MemberStates.CLUSTERSTOPPING)
            throw new CommunicationException("Peer " + name + " isn't in a valid state : " + state);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initDisconnection() throws CommunicationException {
        // ...
    }
}
