/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.shoal;

import com.sun.enterprise.ee.cms.core.GMSConstants;
import com.sun.enterprise.ee.cms.core.GroupManagementService;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The local peer - Shoal version
 * @author Andre Fonseca
 */
public class ShoalLocalPeer extends ShoalPeer {
    static Logger logger = LoggerFactory.getLogger(ShoalLocalPeer.class);
    private static final long serialVersionUID = 345838945847L;

    public ShoalLocalPeer(final String name, final ShoalPeerGroup peerGroup,
        final GroupManagementService gms) {
        super(name, peerGroup, gms);
        
        this.addPeerListener(peerGroup);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initConnection() throws CommunicationException {   
    	try {
    		joinGMSGroup();
        } catch (Exception e) {
            disconnect();
            throw new CommunicationException(e.getMessage());
        }   	
    }

    /**
     * Join a GMS Group
     */
    private void joinGMSGroup() throws CommunicationException {
        try {
            logger.info("Joining Group " + getPeerGroup().getName());
            gms.join();
            gms.reportJoinedAndReadyState();
            
            // Give some time to propagate the join message
            Thread.sleep(100);
        } catch (final Exception e) {
            logger.error("There was an error joining shoal group '" +
                getPeerGroup().getName() + "' : " + e);
            throw new CommunicationException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initDisconnection() {
        // Leave the group gracefully
        leaveGroupAndShutdown();
    }

    /**
     * Leave a GMS group, and shutdown this peer
     */
    private void leaveGroupAndShutdown() {
        logger.info("Shutting down Shoal local peer... ");
        gms.shutdown(GMSConstants.shutdownType.INSTANCE_SHUTDOWN);
    }

}
