/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.shoal;

import com.sun.enterprise.ee.cms.core.CallBack;
import com.sun.enterprise.ee.cms.core.GroupManagementService;
import com.sun.enterprise.ee.cms.core.MessageSignal;
import com.sun.enterprise.ee.cms.core.Signal;
import com.sun.enterprise.ee.cms.impl.client.MessageActionFactoryImpl;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageReceiver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Used to deal with messages reception from the outside
 * 
 * @author Andre Fonseca
 */
public class MessageReceiver extends AbstractMessageReceiver implements CallBack {
    static Logger logger = LoggerFactory.getLogger(MessageReceiver.class);
    static Logger errorLogger = LoggerFactory.getLogger("errorLogger");

    /* --- Properties --- */

    /** The PeerGroup this peer is in */
    protected final ShoalPeerGroup peerGroup;

    public MessageReceiver(final ShoalPeerGroup peerGroup) {
    	super(peerGroup.getModuleManager()
                .getCommunicationManager());

        this.peerGroup = peerGroup;
    }

    /* --- Accessors --- */
    public ShoalPeerGroup getPeerGroup() {
        return peerGroup;
    }

    /**
     * Called when something happens with Shoal
     * @param signal Describes what happened
     */
    @Override
    public void processNotification(final Signal signal) {
        logger.debug("Received Notification of type : " +
            signal.getClass().getName());

        try {

            if (signal instanceof MessageSignal) {
                final MessageSignal msgSignal = (MessageSignal) signal;

                logger.debug("Message Signal received : Source=" 
                		+ signal.getMemberToken());

                Message message = ShoalMessage.createShoalMessage(peerGroup, msgSignal);
                processReceiving(message);

            } else {
                logger.debug("Signal received : Source=" +
                    signal.getMemberToken());
            }
            
        } catch (final Exception e) {
        	errorLogger.error("Exception occured while dealing with Shoal signal : ", e);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
	public void processReceiving(Message message) throws CommunicationException {
    	communicationManager.receiveMessage(message);
	}

    /**
     * Register with shoal events we watch (received by the <code>processNotification</code> methods)
     * @param gms the Gms to register the events to
     */
    void registerForGroupEvents(final GroupManagementService gms) {
        final String componentName = peerGroup.getName();
        gms.addActionFactory(new MessageActionFactoryImpl(this), componentName);
    }

}
