/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.shoal.configuration;

import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.XPathReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.enterprise.ee.cms.core.GroupManagementService;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;


/**
 * Configuration, with ability to build configuration from an XML file
 * @author Andre Fonseca
 */
public class ShoalXMLConfiguration extends ShoalConfiguration {
    static Logger logger = LoggerFactory.getLogger(ShoalXMLConfiguration.class);
    private static final long serialVersionUID = 785838945858L;

    public ShoalXMLConfiguration(final Module module) {
        super(module);
    }

    /**
     * Parse XML configuration node to obtain corresponding
     * Configuration properties
     */
    public void parseConfig(final Node config) {
        try {
            // Verify if there is a valid config
            if (config == null) {
                return;
            }

            final XPathReader reader = new XPathReader(config);
            parseLocalPeer(reader);
            parseSeeds(reader);
            parseNetwork(reader);

        } catch (final Exception e) {
            logger.error("There were errors parsing XML configuration file : " +
                e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
    }
    
    /**
     * Parse XML configuration file, part 'localPeer'
     */
    private void parseLocalPeer(XPathReader reader) throws XPathExpressionException {
    	
        String peerTypeStr = reader.read("localPeer/peerType",
                XPathConstants.STRING).toString();

        if (peerTypeStr.equalsIgnoreCase("Core")) {
            peerType = GroupManagementService.MemberType.CORE;
        } else if (peerTypeStr.equalsIgnoreCase("Spectator")) {
            peerType = GroupManagementService.MemberType.SPECTATOR;
        } else if (peerTypeStr.equalsIgnoreCase("Watchdog")) {
            peerType = GroupManagementService.MemberType.WATCHDOG;
        } else {
            peerType = GroupManagementService.MemberType.CORE;
        }

        String bootStrappingStr = reader.read("localPeer/isBootStrapping", XPathConstants.STRING).toString();
        if (!bootStrappingStr.isEmpty())
        	bootStrapping = Boolean.parseBoolean(bootStrappingStr);

        String noValue = "NaN";
        String senderThreadsNr = reader.read("localPeer/messageSenderThreadsNr", XPathConstants.NUMBER).toString();
        if (!senderThreadsNr.equalsIgnoreCase(noValue))
        	messageSenderThreadsNr = Integer.parseInt(senderThreadsNr);
    }
    
    
    /**
     * Parse XML configuration file, part 'seeds'
     */
    private void parseSeeds(XPathReader reader) throws XPathExpressionException {
        NodeList rdvPeersNodes = (NodeList) reader.read("seeds",
                XPathConstants.NODESET);

        for (int index = 0; index < rdvPeersNodes.getLength(); index++) {
            Node node = rdvPeersNodes.item(index);
            seeds.add(node.getTextContent());
        }
    }
    
    /**
     * Parse XML configuration file, part 'network'
     */
    private void parseNetwork(XPathReader reader) throws XPathExpressionException {
    	String loopBackStr = reader.read("network/loopback", XPathConstants.STRING).toString();
    	if (!loopBackStr.isEmpty())
    		loopBack = Boolean.parseBoolean(loopBackStr);
    	
    	String interfaceNameStr = reader.read("network/bindToInterfaceAddress", XPathConstants.STRING).toString();
    	if (!interfaceNameStr.isEmpty())
    		interfaceName = interfaceNameStr;
    	
    	String preferIPv6Str = reader.read("network/preferIPv6", XPathConstants.STRING).toString();
    	if (!preferIPv6Str.isEmpty())
    		preferIPv6 = Boolean.parseBoolean(preferIPv6Str);
    	
    	String noValue = "NaN";
    	String failureDetectionRetriesStr = reader.read("network/failureDetectionRetries", XPathConstants.NUMBER).toString();
        if (!failureDetectionRetriesStr.equalsIgnoreCase(noValue))
        	failureDetectionRetries = Integer.parseInt(failureDetectionRetriesStr);
        
        String failureVerificationTimeoutStr = reader.read("network/failureVerificationTimeout", XPathConstants.NUMBER).toString();
        if (!failureVerificationTimeoutStr.equalsIgnoreCase(noValue))
        	failureVerificationTimeout = Integer.parseInt(failureVerificationTimeoutStr);
        
        String failureDetectionTimeoutStr = reader.read("network/failureDetectionTimeout", XPathConstants.NUMBER).toString();
        if (!failureDetectionTimeoutStr.equalsIgnoreCase(noValue))
        	failureDetectionTimeout = Integer.parseInt(failureDetectionTimeoutStr);
    }

    /**
     * Export the configuration into an XML node
     */
    public Node export() {
        final Document doc;

        try {
            final DocumentBuilder builder = DocumentBuilderFactory.newInstance()
                                                                  .newDocumentBuilder();
            doc = builder.newDocument();

            final Element element = doc.createElement(getModule().getModuleName());
            doc.appendChild(element);

			exportLocalPeer(doc, element);
			exportSeeds(doc, element);
			exportNetwork(doc, element);
			
        } catch (final Exception e) {
            logger.error(
                "There were errors exporting XML configuration file : " +
                e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }

        return doc;
    }
    
    
    /**
     * Export XML configuration file, part 'localPeer'
     */
    private void exportLocalPeer(Document doc, Element element)
        throws DOMException {
        // Create the base 'localPeer' element
        Element base = doc.createElement("localPeer");
        element.appendChild(base);

        // Create the child nodes
        Element peerTypeElement = doc.createElement("peerType");

        switch (peerType) {
        case WATCHDOG:
            peerTypeElement.setTextContent("Watchdog");

            break;

        case SPECTATOR:
            peerTypeElement.setTextContent("Spectator");

            break;

        case CORE:default:
            peerTypeElement.setTextContent("Core");
        }

        base.appendChild(peerTypeElement);

        Element isBootStrappingElement = doc.createElement("isBootStrapping");
        isBootStrappingElement.setTextContent(Boolean.toString(bootStrapping));
        base.appendChild(isBootStrappingElement);

        Element messageSenderThreadsNrElement = doc.createElement("messageSenderThreadsNr");
        messageSenderThreadsNrElement.setTextContent(Integer.valueOf(
        		messageSenderThreadsNr).toString());
        base.appendChild(messageSenderThreadsNrElement);
    }
    
    /**
     * Export XML configuration file, part 'seeds'
     */
    private void exportSeeds(Document doc, Element element)
        throws DOMException {
        // Create the base 'seeds' element
        Element base = doc.createElement("seeds");
        element.appendChild(base);

        for (String seed : seeds) {
            Element pElem = doc.createElement(seed);
            base.appendChild(pElem);
        }
    }

    /**
     * Export XML configuration file, part 'network'
     */
    private void exportNetwork(Document doc, Element element)
        throws DOMException {
        // Create the base 'network' element
        Element base = doc.createElement("network");
        element.appendChild(base);

        // Create the child nodes
        Element loopbackElement = doc.createElement("loopback");
        loopbackElement.setTextContent(Boolean.valueOf(loopBack).toString());
        base.appendChild(loopbackElement);
        
        Element interfaceNameElement = doc.createElement("bindToInterfaceAddress");
        interfaceNameElement.setTextContent(Boolean.valueOf(interfaceName).toString());
        base.appendChild(interfaceNameElement);
        
        Element preferIPv6Element = doc.createElement("preferIPv6");
        preferIPv6Element.setTextContent(Boolean.valueOf(preferIPv6).toString());
        base.appendChild(preferIPv6Element);
        
        Element failureDetectionRetriesElement = doc.createElement("failureDetectionRetries");
        failureDetectionRetriesElement.setTextContent(Integer.valueOf(
        		failureDetectionRetries).toString());
        base.appendChild(failureDetectionRetriesElement);
        
        Element failureVerificationTimeoutElement = doc.createElement("failureVerificationTimeout");
        failureVerificationTimeoutElement.setTextContent(Integer.valueOf(
        		failureVerificationTimeout).toString());
        base.appendChild(failureVerificationTimeoutElement);
        
        Element failureDetectionTimeoutElement = doc.createElement("failureDetectionTimeout");
        failureDetectionTimeoutElement.setTextContent(Integer.valueOf(
        		failureDetectionTimeout).toString());
        base.appendChild(failureDetectionTimeoutElement);
    }
}
