/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.shoal;

import fr.lri.iasi.somewhere.FakeAppAdapter;
import fr.lri.iasi.somewhere.ui.BaseUI;
import fr.lri.iasi.somewhere.ui.UI;

public class FakeShoalApp extends FakeAppAdapter {
	
	private UI ui = new BaseUI(this);

	@Override
	public UI getUI() {
		return ui;
	}
	
}