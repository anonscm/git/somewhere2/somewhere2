/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class is a base representation of what a configuration is
 * @author Andre Fonseca
 */
public abstract class Configuration implements Serializable {
	private static final long serialVersionUID = -4899042983945853435L;
	static Logger logger = LoggerFactory.getLogger(Configuration.class);

    /* --- Properties --- */
	/** The module to be configured */
    protected final Module module;
    
    public Configuration(Module module) {
        if (module == null) {
            throw new IllegalArgumentException("module");
        }

        this.module = module;
        setAllToDefault();
    }

    /* --- Accessors --- */
    public final Module getModule() {
        return module;
    }
    
    /* --- Methods --- */ 
    /**
     * Set all config properties to default
     */
    protected abstract void setAllToDefault();
    
}
