/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere;

import java.io.IOException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.namespace.QName;
import javax.xml.parsers.*;
import javax.xml.xpath.*;

/**
 * Responsible for executing XPath queries over a XML document.
 * 
 * @author Andre Fonseca
 *
 */
public class XPathReader {
	
	/* --- Properties --- */
	/** The root node of the configuration file */
    private Node xmlDocument;
    
    /** The xPath object */
    private XPath xPath;

    public XPathReader(String xmlFile) throws SAXException, IOException, ParserConfigurationException {
        initObjects(xmlFile);
    }

    public XPathReader(Node node) throws DOMException, ParserConfigurationException {
        initObjects(node);
    }

    /* --- Methods --- */
    /**
     * Builds XML document object from path string.
     * @param xmlFile the path string
     * @throws ParserConfigurationException 
     * @throws IOException 
     * @throws SAXException 
     */
    private void initObjects(String xmlFile) throws SAXException, IOException, ParserConfigurationException {
    	
    	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    	factory.setIgnoringComments(true);
    	factory.setIgnoringElementContentWhitespace(true);
    	
        xmlDocument = factory.newDocumentBuilder().parse(xmlFile);
        xPath = XPathFactory.newInstance().newXPath();
    }

    /**
     * Build XML document object from DOM node.
     * @param node the DOM object
     * @throws ParserConfigurationException 
     * @throws DOMException 
     */
    private void initObjects(Node node) throws DOMException, ParserConfigurationException {
    	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    	factory.setIgnoringComments(true);
    	factory.setIgnoringElementContentWhitespace(true);
    	
        xmlDocument = factory.newDocumentBuilder().newDocument().importNode(node, true);
        xPath = XPathFactory.newInstance().newXPath();
    }

    /**
     * Execute the XPath expression over the XML document.
     * 
     * @param the XPath expression
     * @param the desired returnType
     * @return the node(s) representing the result of the XPath expression over the document.
     * @throws XPathExpressionException 
     */
    public Object read(String expression, QName returnType) throws XPathExpressionException {
        XPathExpression xPathExpression = xPath.compile(expression);

        return xPathExpression.evaluate(xmlDocument, returnType);
    }
}
