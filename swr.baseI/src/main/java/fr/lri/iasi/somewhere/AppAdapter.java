/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere;

import java.util.HashSet;
import java.util.Set;

import fr.lri.iasi.somewhere.ui.UI;

/**
 * Dummy app adapter - (only for test purpose).
 * 
 * @author Andre Fonseca
 *
 */
public class AppAdapter implements App {
	
	private Set<Module> modules = new HashSet<Module>();

	@Override
	public void quitApp() {
	}

	@Override
	public void loadConfig(ConfigurationManager configurationManager) {
	}

	@Override
	public Set<Module> getModules() {	
		return modules;
	}

	@Override
	public Module getModule(String name) {
		for (Module module : modules) {
			if (module.getModuleName().equals(name))
				return module;
		}
		return null;
	}

	@Override
	public UI getUI() {
		return null;
	}

	@Override
	public boolean addModules(Set<String> modulesNames) {
		return false;
	}

	@Override
	public boolean removeModules(Set<String> modulesNames) {
		return false;
	}

}
