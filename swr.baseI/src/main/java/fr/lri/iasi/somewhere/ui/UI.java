/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.ui;

import java.util.Map;


/**
 * An interface that describes a User Interface
 * 
 * @author Andre Fonseca
 */
public interface UI {
    Map<String, Command> getCommands();

    /**
     * Get a known command
     * @param name Name of the wanted command
     * @return the wanted command or null
     */
    Command getCommand(String name);

    /**
     * Add a command object to UI
     * @param command
     */
    void addCommand(Command command);

    /**
     * Removes the indicated command object from UI
     * @param command
     */
    void removeCommand(Command command);

    /**
     * Removes the command object with the indicated name from UI
     * @param name
     */
    void removeCommandOfName(String name);
    
    /**
     * Prints selected information.
     * @param information
     */
    <T> void print(T information);
}
