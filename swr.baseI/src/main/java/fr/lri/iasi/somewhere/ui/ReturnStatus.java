/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.ui;


/**
 * A base interface that describes the return of a command
 */
public interface ReturnStatus {
    /**
     * @return a general textual info about the result
     *  of the command
     */
    String getTextualInfos();

    /**
     * @return a boolean that says if the general result of
     *  the command was a success
     */
    boolean isSuccessful();

    /**
     * @return a xml-based information about the result of a
     *  command
     */
    org.w3c.dom.Node getResult();
}
