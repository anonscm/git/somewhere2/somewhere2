/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;


/**
 * This is an abstract implementation of a module
 * 
 * @author Andre Fonseca
 */
public abstract class AbstractModule implements Module {
    static Logger logger = LoggerFactory.getLogger(AbstractModule.class);

    /* --- Properties --- */
    /** The app this module will run with */
    protected final App app;
    
    /** The default module loading order */
    private Integer loadingOrder = 1;

    public AbstractModule(App app) {
        if (app == null) {
            throw new IllegalArgumentException("app");
        }

        this.app = app;
    }

    /* --- Accessors --- */
    public App getApp() {
        return app;
    }

    /**
     * @return if two modules are the same
     */
    public boolean equals(Object o) {
        if (o instanceof Module) {
            Module m = (Module) o;

            return getModuleName().equals(m.getModuleName());
        } else {
            return false;
        }
    }

    /**
     * Two AbstractModule are identified by the hash of their name
     */
    public int hashCode() {
        return getModuleName().hashCode();
    }
    
    /**
     * {@inheritDoc}
     */
    public Integer getLoadingOrder() {
    	return loadingOrder;
    }
    
    /**
     * {@inheritDoc}
     */
    public void setLoadingOrder(int order) {
    	// The minimum value for a module loading order is "1"
    	if (order > 0)
    		this.loadingOrder = order;
    }
    
    /**
     * Init slf4j log context for the current module
     * 
     * @param resourcePath
     */
    public void initLog(String resourcePath) {
    	LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        
        try {
          JoranConfigurator configurator = new JoranConfigurator();
          configurator.setContext(lc);
          configurator.doConfigure(getClass().getResource(resourcePath));
        } catch (JoranException je) {
           je.printStackTrace();
        }
    }
   
}
