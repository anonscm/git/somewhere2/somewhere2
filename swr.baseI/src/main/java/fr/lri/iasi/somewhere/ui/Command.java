/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.ui;

import java.util.List;


/**
 * A base interface describing a command
 *  to be executed by a UI
 */
public interface Command {
    /**
     * @return the name of the command
     */
    String getName();

    /**
     * @return the description of the command
     */
    String getDescription();

    /**
     * Execute the command
     * @param parameters The parameters of the command
     * @return the return status
     */
    ReturnStatus execute(List<Object> parameters);
}
