/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere;

import fr.lri.iasi.somewhere.ui.UI;

import java.util.Set;


/**
 * An interface that describes an application
 * 
 * @author Andre Fonseca
 */
public interface App {
    /**
     * Called when the whole application must be quit
     */
    void quitApp();

    /**
     * Load configuration of all added modules
     */
    void loadConfig(ConfigurationManager configurationManager);

    /**
     * @return Modules List
     */
    Set<Module> getModules();

    /**
     * Get the module named <b>name</b> if it exists
     * return null otherwise
     * @param name name of the wanted module
     * @return the wanted module
     */
    Module getModule(String name);

    /**
     * @return the App's UI object
     */
    UI getUI();

    /**
     * Add and init somewhere modules
     * @param modulesNames names of the modules to load
     * @return success
     */
    boolean addModules(Set<String> modulesNames);

    /**
     * Uninit and Remove somewhere modules
     * @param modulesNames names of the modules to remove
     * @return success
     */
    boolean removeModules(Set<String> modulesNames);
}
