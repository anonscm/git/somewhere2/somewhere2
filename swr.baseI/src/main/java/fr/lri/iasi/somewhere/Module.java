/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere;

import org.w3c.dom.Node;


/**
 * An interface that describes a somewhere module
 * 
 * @author Andre Fonseca
 */
public interface Module {
    /**
     * Called when a module must be quitted
     */
    void quit();
    
    /**
     * @return the Name of the module
     */
    String getModuleName();
    
    /**
     * @return the loading order of the module
     */
    Integer getLoadingOrder();
    
    /**
     * Update the loading order of the module
     */
    void setLoadingOrder(int order);

    /**
     * Called when a module must be initialized
     */
    void init();

    /**
     * Called when a part of the config has changed
     */
    void importConfigXML(Node config);

    /**
     * @return a XML representation of the configuration
     */
    Node exportConfigXML();
}
