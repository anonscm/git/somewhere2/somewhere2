/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.ui;

/**
 * This interface is used to inform when the Command has finish
 * your execution.
 * @author Andre Fonseca
 */
public interface CommandListener {
	
	/** 
	 * Called when the command execution is completed.
     * Useful when the command need to receive answers in a synchronous manner.
	 */
    void executionCompleted();
    
    /** 
	 * @return true if the execution is completed
	 */
    boolean isExecutionCompleted();
    
    /** 
	 * Indicates the command start time.
	 */
    long getCommandStartTime();

}
