/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.ui;

import org.w3c.dom.Node;


/**
 * The return status of command 'quit'
 */
public class QuitReturnStatus extends GenericReturnStatus {
    public QuitReturnStatus(String textualInfos, boolean isSuccessful,
        Node result) {
        super(textualInfos, isSuccessful, result);
    }
}
