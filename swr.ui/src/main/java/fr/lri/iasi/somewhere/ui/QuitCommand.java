/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.ui;

import java.util.List;


/**
 * An abstract command to quit the program
 * 
 * @author Andre Fonseca
 */
public abstract class QuitCommand implements Command {
    /**
     * @return the name of the command
     */
    public final String getName() {
        return "quit";
    }

    /**
     * @return the description of the command
     */
    public String getDescription() {
        return "Quit the program.";
    }

    /**
     * A routine to send quit signals to other
     *  part of the program
     * @return true if success
     */
    protected abstract boolean quit();

    /**
     * Execute the command
     * @param parameters The parameters of the command
     * @return the return status
     */
    public ReturnStatus execute(List<Object> parameters) {
        // Quit things.
        boolean success = quit();

        String msg = "";

        if (success) {
            msg = "Successfully quitted...";
        } else {
            msg = "There were errors quitting.";
        }

        ReturnStatus res = new QuitReturnStatus(msg, success, null);

        return res;
    }
}
