/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.ui;

import org.w3c.dom.Node;


/**
 * A simple command return status
 * 
 * @author Andre Fonseca
 */
public class GenericReturnStatus implements ReturnStatus {
    /* --- Properties --- */
	/** Generated textual info about the result */
    private final String textualInfos;
    
    /** Indicates if the command was successful */
    private final boolean isSuccessful;
    
    /** XML based information about the result */
    private final Node result;

    /**
     * Note : result can be null
     */
    public GenericReturnStatus(String textualInfos, boolean isSuccessful,
        Node result) {
        if (textualInfos == null) {
            throw new IllegalArgumentException("textualInfos");
        }

        this.textualInfos = textualInfos;
        this.isSuccessful = isSuccessful;
        this.result = result;
    }

    /**
     * @return a general textual info about the result
     *  of the command
     */
    public final String getTextualInfos() {
        return textualInfos;
    }

    /**
     * @return a boolean that says if the general result of
     *  the command was a success
     */
    public final boolean isSuccessful() {
        return isSuccessful;
    }

    /**
     * @return a xml-based information about the result of a
     *  command
     */
    public final Node getResult() {
        return result;
    }
}
