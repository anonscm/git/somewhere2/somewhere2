/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.ui;

import fr.lri.iasi.somewhere.App;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * A base implementation of a UI front-end
 * 
 * @author Andre Fonseca
 */
public abstract class BaseUI implements UI {
    /* --- Properties --- */
    /** A map containing all the loaded commands indexed by its name. */
	private final Map<String, Command> commands = new HashMap<String, Command>();
    
	/** The main app module */
	protected final App app;

    public BaseUI(App app) {
        if (app == null) {
            throw new RuntimeException("app");
        }

        this.app = app;

        addBaseCommands();
    }

    /* --- Accessors --- */
    public Map<String, Command> getCommands() {
        return Collections.unmodifiableMap(commands);
    }

    /**
     * Get a known command
     * @param name Name of the wanted command
     * @return the wanted command or null
     */
    public Command getCommand(String name) {
        return getCommands().get(name);
    }

    /* --- Mutators --- */
    public void addCommand(Command command) {
        if ((command == null) || (command.getName() == null) ||
                command.getName().equals("")) {
            throw new IllegalArgumentException("command");
        }

        commands.put(command.getName(), command);
    }

    public void removeCommand(Command command) {
        if ((command == null) || (command.getName() == null) ||
                command.getName().equals("")) {
            throw new IllegalArgumentException("command");
        }

        commands.remove(command.getName());
    }

    public void removeCommandOfName(String name) {
        if ((name == null) || name.equals("")) {
            throw new IllegalArgumentException("name");
        }

        commands.remove(name);
    }

    /**
     * Add base commands to the list of commands
     */
    protected void addBaseCommands() {
        addCommand(new HelpCommand(this));
    }
}
