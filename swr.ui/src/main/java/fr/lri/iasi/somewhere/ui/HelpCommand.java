/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * A command to print infos about others commands
 * 
 * @author Andre Fonseca
 */
public class HelpCommand implements Command {
    static Logger logger = LoggerFactory.getLogger(HelpCommand.class);

    /* --- Properties --- */
    /** The current UI module */
    protected final UI ui;

    public HelpCommand(UI ui) {
        if (ui == null) {
            throw new IllegalArgumentException("ui");
        }

        this.ui = ui;
    }

    /* --- Accessors --- */
    public final UI getUI() {
        return ui;
    }

    /**
     * @return the name of the command
     */
    public String getName() {
        return "help";
    }

    /**
     * @return the description of the command
     */
    public String getDescription() {
        return "Print format help of the other commands.";
    }

    /**
     * @param name Name of the command
     * @return the description of the specified command
     */
    protected String describeCommand(String name) {
        if ((name == null) || name.equals("")) {
            throw new IllegalArgumentException("name");
        }

        Map<String, Command> cmds = ui.getCommands();
        Command cmd = cmds.get(name);

        if (cmd == null) {
            throw new IllegalArgumentException("name");
        }

        String res = "* " + name + " : " + cmd.getDescription();

        return res;
    }

    /**
     * Execute the command
     * @param parameters The parameters of the command
     * @return the return status
     *  NOTE : note that the return status contains only text
     *  information
     */
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;

        try {
            Map<String, Command> cmds = ui.getCommands();
            Set<String> wantedCmds = cmds.keySet();

            if ((parameters != null) && (parameters.size() > 0)) {
                // Only print infos about specified commands
                wantedCmds = new HashSet<String>();

                for (Object o : parameters) {
                    String s = o.toString();
                    wantedCmds.add(s);
                }
            }

            // Sort the wanted commands set
            List<String> wantedCmdsList = new ArrayList<String>(wantedCmds);
            Collections.sort(wantedCmdsList);

            // Print infos about commands on a string
            StringBuilder str = new StringBuilder();

            for (String s : wantedCmdsList) {
            	str.append("\n");
            	str.append(describeCommand(s));
            }

            res = new HelpReturnStatus(str.toString(), true, null);
        } catch (Exception e) {
            logger.error("Abnormal execution of " + getName() + " : " + e);
            res = new HelpReturnStatus("There was an error retreiving commands infos...",
                    false, null);
        }

        return res;
    }
}
