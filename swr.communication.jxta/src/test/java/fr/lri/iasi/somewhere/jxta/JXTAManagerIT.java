/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.ui.Command;


public class JXTAManagerIT {
	
	private static JXTAManager jxta_first;
	private static JXTAManager jxta_second;
	
	@BeforeClass
    public static void initializeFirst() {
		initializeSecond();
		
		App app_first = new FakeJXTAApp();
		CommunicationManager communicationManager = new CommunicationManager(app_first);
		communicationManager.getConfiguration().setPeerName("first");
		communicationManager.getConfiguration().setPeerGroupName("unit_test");
		app_first.getModules().add(communicationManager);
		
		jxta_first = new JXTAManager(app_first);
		communicationManager.setCommunicationModule(jxta_first);
		app_first.getModules().add(jxta_first);

		jxta_first.init();
    }
	
    public static void initializeSecond() {
    	App app_second = new FakeJXTAApp();
		CommunicationManager communicationManager = new CommunicationManager(app_second);
		communicationManager.getConfiguration().setPeerName("second");
		communicationManager.getConfiguration().setPeerGroupName("unit_test");
		app_second.getModules().add(communicationManager);
		
		jxta_second = new JXTAManager(app_second);
		communicationManager.setCommunicationModule(jxta_second);
		app_second.getModules().add(jxta_second);

		jxta_second.init();
    }
	
	@Test
	public void testAddPeer() {		
		// Adding peer
		Peer peer = jxta_first.getPeerGroup().retrievePeer("second");
		assertTrue(peer.isConnected());
	}
	
	@Test
	public void sendMessageCorrectly() {
		// Adding peer, if it is not already added
		Peer peer = jxta_first.getPeerGroup().retrievePeer("second");
		
		// Sending message
		Command sendTextMessage = jxta_first.getApp().getUI().getCommand("sendTextMessage");
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(peer.getName());
		parameters.add("Hey!");
		
		assertTrue(sendTextMessage.execute(parameters).isSuccessful());
	}
	
	@Test
	public void testRemovePeer() {		
		// Adding peer, if it is not already added
		Peer peer = jxta_first.getPeerGroup().retrievePeer("second");
		
		// Removing peer
		jxta_first.getCommunicationManager().removePeer(peer);	
		assertFalse(peer.isConnected());
	}
	
	@Test
	public void recoverPeerConnection() throws InterruptedException{
		jxta_second.quit();
		Thread.sleep(1000);
		
		// Peer "second" is not accessible yet, so it will be added but as "not connected"
		Peer peer = jxta_first.getPeerGroup().retrievePeer("second");
		assertFalse(peer.isConnected());
		
		// Peer "second" is initialized and it will notify the group that he has arrived. Peer "first" will add it after some milliseconds. 
		jxta_second.init();
		Thread.sleep(3000);
		assertTrue(peer.isConnected());
	}
	
	@AfterClass
    public static void cleanCache() {
		deleteDir(new File("jxtaCache"));
    }
	
	/**
	 * Used as helper to delete folders recursively
	 * @param dir
	 * @return
	 */
	private static boolean deleteDir(File dir) {
	    if (dir.isDirectory()) {
	        String[] children = dir.list();
	        for (int i=0; i<children.length; i++) {
	            boolean success = deleteDir(new File(dir, children[i]));
	            if (!success) {
	                return false;
	            }
	        }
	    }

	    // The directory is now empty so delete it
	    return dir.delete();
	}
}
