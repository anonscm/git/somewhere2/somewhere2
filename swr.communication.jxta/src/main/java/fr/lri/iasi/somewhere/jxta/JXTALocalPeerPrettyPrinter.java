/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import net.jxta.peer.PeerID;

import net.jxta.rendezvous.RendezVousService;


/**
 * A class to pretty print a <b>JXTALocalPeer</b> object
 */
public class JXTALocalPeerPrettyPrinter {
    /* --- Properties --- */
    protected final JXTALocalPeer localPeer;

    public JXTALocalPeerPrettyPrinter(JXTALocalPeer localPeer) {
        if (localPeer == null) {
            throw new IllegalArgumentException("localPeer");
        }

        this.localPeer = localPeer;
    }

    /* --- Accessors --- */
    public JXTALocalPeer getLocalPeer() {
        return localPeer;
    }

    /**
     * @return a string giving a pretty print of
     *  infos about the local peer
     */
    public String toString() {
        String res = "";

        // Says base info about the peer
        res += printBaseInfos();

        // Print stuff on the Advertisement of the local peer
        res += printPeerAdvertisementInfos();

        // Print stuffs about metrics, using PeerInfoService, if provided
        // by this implementation of JXTA
        res += printMetrics();

        return res;
    }

    /**
     * @return a string giving a pretty print of
     *  base infos
     */
    protected String printBaseInfos() {
        String res = "";
        net.jxta.peergroup.PeerGroup netPeerGroup = localPeer.getJXTAPeerGroup();

        res += ("\n\t Peer name : " + netPeerGroup.getPeerName());
        res += ("\n\t Peer ID : " + netPeerGroup.getPeerID());
        res += ("\n\t Peer Group name : " + netPeerGroup.getPeerGroupName());
        res += ("\n\t Peer Group ID : " + netPeerGroup.getPeerGroupID());
        res += ("\n\t Storage Path : " + netPeerGroup.getStoreHome());

        return res;
    }

    /**
     * @return a string giving a pretty print of
     *  stuffs on the Advertisement of the local peer
     */
    protected String printPeerAdvertisementInfos() {
        String res = "";
        net.jxta.peergroup.PeerGroup netPeerGroup = localPeer.getJXTAPeerGroup();

        res += "\n\n\t Local Peer Advertisement :";

        res += netPeerGroup.getPeerAdvertisement().toString();

        res += "\n";

        return res;
    }

    /**
     * @return a string giving a pretty print of
     *  metrics, using PeerInfoService, if provided by
     *  this implementation of JXTA
     */
    protected String printMetrics() {
        String res = "";
        net.jxta.peergroup.PeerGroup netPeerGroup = localPeer.getJXTAPeerGroup();

        res += "\n\n\t Local Monitoring :";

        boolean isLocalMonitoringAvailable = netPeerGroup.getPeerInfoService()
                                                         .isLocalMonitoringAvailable();
        res += ("\n\t\t Available : " + isLocalMonitoringAvailable);

        if (isLocalMonitoringAvailable) {
            // TODO
        }

        res += "\n";

        return res;
    }

    /**
     * @return a string giving a pretty print of
     *  infos about rendezvous status
     */
    protected String printRendezVousInfos() {
        String res = "";
        net.jxta.peergroup.PeerGroup netPeerGroup = localPeer.getJXTAPeerGroup();
        RendezVousService rvs = netPeerGroup.getRendezVousService();

        res += "\n\n\t Rendez Vous :";

        res += ("\n\t\t Is this a rendez-vous peer : " + rvs.isRendezVous());
        res += ("\n\t\t Is this peer connected to a RendezVous Peer : " +
        rvs.isConnectedToRendezVous());

        // Print connected peers
        res += "\n\t\t Connected Peers :";

        for (PeerID peerID : rvs.getLocalEdgeView()) {
            res += ("\n\t\t\t " + peerID);
        }

        res += ("\n\t\t RendezVous Status : " + rvs.getRendezVousStatus());

        return res;
    }
}
