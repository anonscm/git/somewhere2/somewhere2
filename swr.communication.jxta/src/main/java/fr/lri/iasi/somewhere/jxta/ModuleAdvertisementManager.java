/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import fr.lri.iasi.somewhere.api.communication.configuration.CommunicationConfiguration;
import fr.lri.iasi.somewhere.jxta.configuration.JXTAConfiguration;

import net.jxta.document.AdvertisementFactory;

import net.jxta.id.IDFactory;

import net.jxta.platform.ModuleClassID;

import net.jxta.protocol.ModuleClassAdvertisement;
import net.jxta.protocol.ModuleImplAdvertisement;
import net.jxta.protocol.ModuleSpecAdvertisement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creates module advertisements necessary for the JXTA communication.
 * 
 * NOTE: Each module advertisement handled by this class is described on the JXSE documentation.
 * 
 * @author Andre Fonseca
 *
 */
public class ModuleAdvertisementManager {
    static Logger logger = LoggerFactory.getLogger(ModuleAdvertisementManager.class);
    protected static final String MCA_NAME = "JXTAMOD:Somewhere2";
    protected static final String MCA_DESCRIPTION = "JXTA support for Somewhere P2P Inference system";

    /** Module Manager */
    protected final JXTAManager moduleManager;

    /** Module class id to be used */
    private ModuleClassID moduleClassID;

    /** Module class advertisement to be used */
    private ModuleClassAdvertisement mcadv;

    /** Module specification advertisement to be used */
    private ModuleImplAdvertisement miadv;

    /** Module implementation advertisement to be used */
    private ModuleSpecAdvertisement msadv;

    /* --- Methods --- */
    public ModuleAdvertisementManager(JXTAManager moduleManager) {
        if (moduleManager == null) {
            throw new IllegalArgumentException("moduleManager");
        }

        this.moduleManager = moduleManager;
        moduleClassID = IDFactory.newModuleClassID();
    }

    /* --- Accessors --- */
    public ModuleImplAdvertisement getModuleImplAdv() {
        return miadv;
    }

    /**
     * Creates module class advertisement.
     */
    void createModuleClassAdvertisement() {
        try {
            // Get DiscoveryService from netPeerGroup
            net.jxta.peergroup.PeerGroup netPeerGroup = moduleManager.getPeerGroup()
                                                                     .getLocalPeer()
                                                                     .getJXTAPeerGroup();

            // Create a Module class Advertisement (MCA)
            mcadv = (ModuleClassAdvertisement) AdvertisementFactory.newAdvertisement(ModuleClassAdvertisement.getAdvertisementType());
            mcadv.setName(MCA_NAME);
            mcadv.setDescription(MCA_DESCRIPTION);
            mcadv.setModuleClassID(moduleClassID);

            // Publish the MCA in the local cache and in the netPeerGroup
            netPeerGroup.getDiscoveryService().publish(mcadv);
            netPeerGroup.getDiscoveryService().remotePublish(mcadv);
        } catch (Exception e) {
            logger.error(
                "There was an error creating the Module Class advertisement... : " +
                e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Creates module impl advertisement.
     */
    void createModuleImplAvertisement() {
        try {
            // Get DiscoveryService from netPeerGroup
            net.jxta.peergroup.PeerGroup netPeerGroup = moduleManager.getPeerGroup()
                                                                     .getLocalPeer()
                                                                     .getJXTAPeerGroup();

            // Create a Module Impl Advertisement (MIA)
            miadv = netPeerGroup.getAllPurposePeerGroupImplAdvertisement();
            miadv.setModuleSpecID(IDFactory.newModuleSpecID(moduleClassID));

            // Publish the MIA in the local cache and in the netPeerGroup
            netPeerGroup.getDiscoveryService().publish(miadv);
            netPeerGroup.getDiscoveryService().remotePublish(null, miadv);
        } catch (Exception e) {
            logger.error(
                "There was an error creating the Module Implementation advertisement... : " +
                e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Creates module spec advertisement.
     */
    void createModuleSpecAdvertisement() {
        try {
            JXTAConfiguration config = moduleManager.getConfiguration();
            CommunicationConfiguration communicationConfig = moduleManager.getCommunicationManager().getConfiguration();

            // Create a Module Spec Advertisement (MSA)
            msadv = (ModuleSpecAdvertisement) AdvertisementFactory.newAdvertisement(ModuleSpecAdvertisement.getAdvertisementType());
            msadv.setName("service." + communicationConfig.getPeerName());
            msadv.setVersion("" + config.getVersion());
            msadv.setCreator(config.getCreator());
            msadv.setModuleSpecID(IDFactory.newModuleSpecID(moduleClassID));
            msadv.setSpecURI(config.getURL());
        } catch (Exception e) {
            logger.error(
                "There was an error creating the Module Specification advertisement... : " +
                e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Publishes the created module spec advertisement.
     */
    void publishModuleSpecAdvertisement() {
        try {
            JXTAConfiguration config = moduleManager.getConfiguration();

            // Get DiscoveryService from somewherePeerGroup
            net.jxta.peergroup.PeerGroup somewherePeerGroup = moduleManager.getPeerGroup()
                                                                           .getLocalPeer()
                                                                           .getSomewherePeerGroup();

            // Store the pipe advertisement.
            // This information will be retrieved by the client when it will connect to the service
            msadv.setPipeAdvertisement(moduleManager.getPeerGroup()
                                                    .getLocalPeer()
                                                    .getInputPipeAdvertisement());

            // Publish the MSA in the local cache and into the peergroup
            somewherePeerGroup.getDiscoveryService().publish(msadv);
            somewherePeerGroup.getDiscoveryService()
                              .remotePublish(msadv,config.getAdvExpirationTime());
        } catch (Exception e) {
            logger.error(
                "There was an error publishing the Module Specification advertisement... : " +
                e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
