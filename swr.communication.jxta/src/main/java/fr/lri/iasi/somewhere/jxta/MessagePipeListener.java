/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageReceiver;

import net.jxta.endpoint.Message;

import net.jxta.pipe.PipeMsgEvent;
import net.jxta.pipe.PipeMsgListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class is used to listen when a new JXTA message is
 * received, and forward it to the communication manager
 * @author Andre Fonseca
 */
public class MessagePipeListener extends AbstractMessageReceiver implements PipeMsgListener {
    static Logger logger = LoggerFactory.getLogger(JXTALocalPeer.class);

    /* --- Properties --- */
    /** Module Manager */
    protected final JXTAManager moduleManager;

    public MessagePipeListener(JXTAManager moduleManager) {
        super(moduleManager.getCommunicationManager());

        this.moduleManager = moduleManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void pipeMsgEvent(PipeMsgEvent event) {
        Message msg = event.getMessage();
        JXTAMessage jxtaMsg = null;

        try {
            jxtaMsg = JXTAMessage.createJXTAMessage(moduleManager.getPeerGroup(),
                    msg);

            processReceiving(jxtaMsg);
        } catch (Exception e) {
            logger.error("Received a new JXTA Message, but was unable to " +
                "add it the list of received messages => " + e);
        }
    }

    /**
     * {@inheritDoc}
     */
	@Override
	public void processReceiving(
			fr.lri.iasi.somewhere.api.communication.model.Message message)
			throws CommunicationException {
		
		this.communicationManager.receiveMessage(message);
	}
}
