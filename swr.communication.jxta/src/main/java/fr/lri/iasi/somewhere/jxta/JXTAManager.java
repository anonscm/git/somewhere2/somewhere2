/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.api.communication.AbstractCommunicationModule;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.jxta.commands.JXTAPeerStatusCommand;
import fr.lri.iasi.somewhere.jxta.configuration.JXTAConfiguration;
import fr.lri.iasi.somewhere.jxta.configuration.JXTAXMLConfiguration;
import fr.lri.iasi.somewhere.ui.UI;

import net.jxta.logging.Logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.slf4j.bridge.SLF4JBridgeHandler;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


/**
 * The main class of the JXTA module
 */
public class JXTAManager extends AbstractCommunicationModule<JXTAPeerGroup, JXTAPeer> {
    static Logger logger = LoggerFactory.getLogger(JXTAManager.class);

    /* --- Constants --- */
    /** The name of this module */
    protected static final String moduleName = "jxta";

    /* --- Properties --- */

    /** The configuration for this module */
    protected final JXTAXMLConfiguration configuration;


    public JXTAManager(App app) {
        super(app);
        
        // Set this module as the concrete communication module
        communicationManager.setCommunicationModule(this);

        // Init conf
        configuration = new JXTAXMLConfiguration(this);

        // Create a new peergroup
        peerGroup = new JXTAPeerGroup(this);

        // Add Commands to the UI
        addBaseCommands();
    }

    /* --- Accessors --- */
    public JXTAConfiguration getConfiguration() {
        return configuration;
    }

    public JXTAPeerGroup getPeerGroup() {
        return peerGroup;
    }

    public CommunicationManager getCommunicationManager() {
        return communicationManager;
    }

    /* --- Methods --- */
    /**
     * Set JXTA Logging parameters
     */
    protected void initJXTALog() {
        SLF4JBridgeHandler.install();

        java.util.logging.Logger jxtaLog = java.util.logging.Logger.getLogger(
                "net.jxta");
        
        jxtaLog.setLevel(java.util.logging.Level.SEVERE);
        System.setProperty(Logging.JXTA_LOGGING_PROPERTY, java.util.logging.Level.SEVERE.toString());
    }

    /**
     * {@inheritDoc}
     */
    public static JXTAManager getModuleInstance(App app) {
        if (app == null) {
            throw new IllegalArgumentException("app");
        }

        Module m = app.getModule(moduleName);

        if (m == null) {
            return null;
        }

        try {
            JXTAManager res = (JXTAManager) m;

            return res;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Add jxta commands to the UI
     */
    protected void addBaseCommands() {
        UI ui = app.getUI();
        ui.addCommand(new JXTAPeerStatusCommand(this));
    }

    /**
     * {@inheritDoc}
     */
    public String getModuleName() {
        return moduleName;
    }

    /**
     * {@inheritDoc}
     */
    public void quit() {
        // Uninit the peer group
        peerGroup.uninit();
    }

    /**
     * {@inheritDoc}
     */
    public void init() {
        try {
        	// Base init stuffs
            initJXTALog();
            
            // Create the peer group
            peerGroup.init();
        } catch (Exception e) {
            quit();
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void importConfigXML(Node config) {
        configuration.parseConfig(config);
    }

    /**
     * {@inheritDoc}
     */
    public Node exportConfigXML() {
        try {
            if (configuration == null) {
                DocumentBuilder builder = DocumentBuilderFactory.newInstance()
                                                                .newDocumentBuilder();
                Document doc = builder.newDocument();
                Element element = doc.createElement(getModuleName());
                doc.appendChild(element);

                return doc;
            } else {
                return configuration.export();
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public JXTAPeer createPeer(String peerName) {
        return new JXTAPeer(peerName, this.getPeerGroup());
    }

    /**
     * {@inheritDoc}
     */
    public Message createMessage(Message msg) {
        return JXTAMessage.createJXTAMessage(msg);
    }

    /**
     * {@inheritDoc}
     */
	public void restart() {
		
		if (peerGroup.getLocalPeer() != null) {
			// Stopping jxta
			peerGroup.uninit();
			
			// Stopping jxta
			try {
				peerGroup.init();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

}
