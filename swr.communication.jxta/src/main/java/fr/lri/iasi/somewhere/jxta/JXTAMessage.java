/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import fr.lri.iasi.somewhere.api.communication.model.MessageType;
import fr.lri.iasi.somewhere.api.communication.model.Peer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import java.util.Map;


/**
 * A message to be send - JXTA version
 * @author Andre Fonseca
 */
public class JXTAMessage extends fr.lri.iasi.somewhere.api.communication.model.Message {
    static Logger logger = LoggerFactory.getLogger(JXTAMessage.class);
    private static final long serialVersionUID = 745838945847L;

    /* --- Methods --- */
    private JXTAMessage(final String id, final Peer origin, final Peer destination, final MessageType type,
        final Map<String, Object> contents) {
        super(id, origin, destination, type, contents);
    }

    public static JXTAMessage createJXTAMessage(
        fr.lri.iasi.somewhere.api.communication.model.Message message) {
        return new JXTAMessage(message.getId(), message.getOrigin(), message.getDestination(),
            message.getType(), message.getContents());
    }

    public static JXTAMessage createJXTAMessage(final JXTAPeerGroup peerGroup,
        final net.jxta.endpoint.Message message)
        throws IOException, ClassNotFoundException {
        if (message == null) {
            throw new IllegalArgumentException("msgSignal");
        }

        // Deserialization of the msg
        JXTAMessage msg = null;
        ByteArrayInputStream bytes = null;
        ObjectInputStream obInputStream = null;

        try {
            byte[] serializedObject = message.getMessageElement("JXTAMessage")
                                             .getBytes(false);
            bytes = new ByteArrayInputStream(serializedObject);
            obInputStream = new ObjectInputStream(bytes);
            msg = (JXTAMessage) obInputStream.readObject();

            // Close outputstreams
            bytes.close();
            obInputStream.close();
        } finally {
            try {
                if (bytes != null) {
                    bytes.close();
                }

                if (obInputStream != null) {
                    obInputStream.close();
                }
            } catch (final Exception e) {
                // ...
            }
        }

        return msg;
    }
    
}
