/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta.commands;

import fr.lri.iasi.somewhere.jxta.JXTALocalPeerPrettyPrinter;
import fr.lri.iasi.somewhere.jxta.JXTAManager;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import java.util.List;


/**
 * A command to print infos on the JXTA peer
 */
public class JXTAPeerStatusCommand implements Command {
    /* --- Properties --- */
    protected final JXTAManager moduleManager;

    public JXTAPeerStatusCommand(JXTAManager moduleManager) {
        if (moduleManager == null) {
            throw new IllegalArgumentException("moduleManager");
        }

        this.moduleManager = moduleManager;
    }

    /* --- Accessors --- */
    public JXTAManager getJXTAManager() {
        return moduleManager;
    }

    /**
     * @return the name of the command
     */
    public String getName() {
        return "jxtaPeerStatus";
    }

    /**
     * @return the description of the command
     */
    public String getDescription() {
        return "Print infos about the JXTA Peer.";
    }

    /**
     * Execute the command
     * @param parameters The parameters of the command
     * @return the return status
     *  NOTE : note that the return status contains only text
     *  information
     */
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;

        try {
            String str = "";

            str += "Infos about local peer : \n";
            str += new JXTALocalPeerPrettyPrinter(moduleManager.getPeerGroup()
                                                               .getLocalPeer()).toString();

            res = new GenericReturnStatus(str, true, null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error retreiving advertisements infos...",
                    false, null);
        }

        return res;
    }
}
