/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.configuration.CommunicationConfiguration;
import fr.lri.iasi.somewhere.jxta.configuration.JXTAConfiguration;

import net.jxta.endpoint.MessageElement;
import net.jxta.endpoint.StringMessageElement;
import net.jxta.id.IDFactory;

import net.jxta.peergroup.PeerGroupID;

import net.jxta.pipe.PipeID;

import net.jxta.platform.NetworkConfigurator;
import net.jxta.platform.NetworkManager;

import net.jxta.protocol.PeerGroupAdvertisement;

import net.jxta.util.JxtaBiDiPipe;
import net.jxta.util.JxtaServerPipe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;

import java.net.URI;
import java.net.UnknownHostException;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * A peer that can emit or receive messages - JXTA version
 * It's the local peer !
 * @author Andre Fonseca
 */
public class JXTALocalPeer extends JXTAPeer {
    static Logger logger = LoggerFactory.getLogger(JXTALocalPeer.class);

    /* --- Constants --- */
    private static final long serialVersionUID = 945838945848L;

    /* --- Properties --- */
    protected net.jxta.peergroup.PeerGroup netPeerGroup;

    /** the PeerGroup of Somewhere */
    protected net.jxta.peergroup.PeerGroup somewherePeerGroup;

    /** JXTA Network Manager - an object to access base JXTA stuffs */
    protected NetworkManager networkManager;

    /** Module Manager */
    protected final JXTAManager moduleManager;

    /** Says if this local peer is correctly initialized */
    protected volatile boolean isInitialized = false;

    /** The concurrent ExecutorService that will execute the different threads/routines related to the peer's initialization */
    protected ExecutorService initializationExecutorService;

    /** The concurrent ExecutorService that will handle requests to the pipe server */
    protected ExecutorService requestHandlerExecutorService;

    /** PipeServer that listen and establish to remote connections*/
    protected JxtaServerPipe pipeServer;

    /* --- Methods --- */
    public JXTALocalPeer(String name, JXTAPeerGroup peergroup,
        JXTAManager moduleManager) {
        super(name, peergroup);

        if (moduleManager == null) {
            throw new IllegalArgumentException("moduleManager");
        }

        this.moduleManager = moduleManager;
        this.addPeerListener(peergroup);
        
        this.initializationExecutorService = Executors.newSingleThreadExecutor();
        this.requestHandlerExecutorService = Executors.newCachedThreadPool();
    }

    /* --- Accessors --- */
    public net.jxta.peergroup.PeerGroup getJXTAPeerGroup() {
        return netPeerGroup;
    }

    public net.jxta.peergroup.PeerGroup getSomewherePeerGroup() {
        return somewherePeerGroup;
    }

    public boolean isInitialized() {
        return isInitialized;
    }

    /**
     * Create a new thread which will initialize the local peer
     */
    @Override
    public void initConnection() throws CommunicationException {     
        try {
        	// Config base JXTA
        	configureJXTA();
        	
        	// Configure TCP parameters
        	configureNetwork();
        	
        	// Configure Rendez-vous and Relay Peers
        	configureRDVAndRelayPeers();
        	
        	// Init base JXTA
        	startJXTA();
        	
        	// Publish the application service
        	publishService();
        	
        	// Create server pipe
        	createServerPipe();
        } catch (IOException ioe) {
            disconnect();
        } catch (Exception e) {
            disconnect();
            throw new CommunicationException(e.getMessage());
        }
        
        initializationExecutorService.execute(new Runnable() {
                public void run() {
                    try {
                    	// Listen for remote connections
                        waitConnections();
                    } catch (Exception e) {
                        // Fails silently...
                    }
                }
            });

        isInitialized = true;
    }

    /**
     * Uninitialize the local peer
     */
    @Override
    public void initDisconnection() throws CommunicationException {
    	this.requestHandlerExecutorService.shutdownNow();
    	this.initializationExecutorService.shutdownNow();

        try {
        	this.pipeServer.close();
        } catch (IOException e) {
            throw new CommunicationException(e.getMessage());
        }

        stopJXTA();
    }

    /**
     * Stop JXTA
     * @throws CommunicationException 
     */
    protected void stopJXTA() throws CommunicationException {
        logger.info("Stopping JXTA support...");

        try {
            for (JXTAPeer peer : peerGroup.getPeers().values()) {
                if (peer != this) {
                    peer.disconnect();
                }
            }

            // Stop JXTA networkManager if its already created
            if (networkManager != null) {
                networkManager.stopNetwork();
            }
            
            // Delete cache folder
            JXTAConfiguration config = moduleManager.getConfiguration();
            File cacheFolder = new File(config.getCacheFolder());
            FileUtils.deleteDirectory(cacheFolder);

            isInitialized = false;
            logger.info("Successfully stopped JXTA.");
        } catch (Exception e) {
            logger.error("There was an error stopping JXTA support... : {}", e);
            throw new CommunicationException(e.getMessage());
        }
    }

    /**
     * Configure JXTA
     * @throws CommunicationException 
     */
    protected void configureJXTA() throws CommunicationException {
        logger.info("Configuring JXTA support...");

        try {
            JXTAConfiguration config = moduleManager.getConfiguration();
            CommunicationConfiguration communicationConfig = moduleManager.getCommunicationManager().getConfiguration();

            // Configure the local peer according to its type
            switch (config.getPeerType()) {
	            case NetworkConfigurator.EDGE_NODE:
	                networkManager = new NetworkManager(NetworkManager.ConfigMode.EDGE,
	                		communicationConfig.getPeerName(),
	                        new File(new File(config.getCacheFolder()),
	                        		communicationConfig.getPeerName()).toURI());
	
	                break;
	
	            case NetworkConfigurator.RDV_NODE:
	                networkManager = new NetworkManager(NetworkManager.ConfigMode.RENDEZVOUS,
	                		communicationConfig.getPeerName(),
	                        new File(new File(config.getCacheFolder()),
	                        		communicationConfig.getPeerName()).toURI());
	
	                break;
	
	            case NetworkConfigurator.RELAY_NODE:
	                networkManager = new NetworkManager(NetworkManager.ConfigMode.RELAY,
	                		communicationConfig.getPeerName(),
	                        new File(new File(config.getCacheFolder()),
	                        		communicationConfig.getPeerName()).toURI());
	
	                break;
	
	            default:
	                logger.error("Unknown Peer Type : " + config.getPeerType());
	                throw new UnsupportedOperationException();
            }

            logger.info("Successfully configuring JXTA.");
        } catch (Exception e) {
            logger.error("There was an error configuring JXTA support... : {}", e);
            throw new CommunicationException(e.getMessage());
        }
    }

    /**
     * Configure JXTA Networks parameters
     * @throws IOException 
     * @throws CommunicationException 
     */
    protected void configureNetwork() throws IOException, CommunicationException {
        logger.info("Configuring JXTA TCP Parameters...");

        try {
            JXTAConfiguration config = moduleManager.getConfiguration();

            NetworkConfigurator networkConfigurator = networkManager.getConfigurator();
            networkConfigurator.setTcpEnabled(config.isEnabled());
            networkConfigurator.setTcpPort(config.getPort());
            networkConfigurator.setTcpIncoming(config.isIncomingEnabled());
            networkConfigurator.setTcpOutgoing(config.isOutgoingEnabled());
            networkConfigurator.setUseMulticast(true);
        } catch (Exception e) {
            logger.error(
                "There was an error configuring JXTA TCP Parameters... : {}", e);
            throw new CommunicationException(e.getMessage());
        }
    }

    /**
     * Configure Rendez-vous and Relay Peers
     * @throws CommunicationException 
     */
    protected void configureRDVAndRelayPeers()
        throws IOException, UnknownHostException, CommunicationException {
        logger.info("Configuring JXTA known Rendez-vous and Relay Peers...");

        try {
            JXTAConfiguration config = moduleManager.getConfiguration();
            NetworkConfigurator networkConfigurator = networkManager.getConfigurator();

            // Clear everything
            networkConfigurator.clearRelaySeeds();
            networkConfigurator.clearRelaySeedingURIs();
            networkConfigurator.clearRendezvousSeeds();
            networkConfigurator.clearRendezvousSeedingURIs();

            // Add rendez-vous peers
            for (String rdvPeer : config.getRdvPeers()) {
                URI uri = URI.create(rdvPeer);
                networkConfigurator.addSeedRendezvous(uri);
            }

            // Add Relay peers
            for (String relayPeer : config.getRelayPeers()) {
                URI uri = URI.create(relayPeer);
                networkConfigurator.addSeedRelay(uri);
            }
        } catch (Exception e) {
            logger.error(
                "There was an error configuring JXTA known Rendez-vous " +
                "and Relay Peers... : {}", e);
            throw new CommunicationException(e.getMessage());
        }
    }

    /**
     * Start JXTA
     * @throws CommunicationException 
     */
    protected void startJXTA() throws CommunicationException {
        logger.info("Starting JXTA support...");

        JXTAConfiguration config = moduleManager.getConfiguration();

        try {
            // Start the JXTA network
            netPeerGroup = networkManager.startNetwork();

            if (config.getPeerType() == NetworkConfigurator.EDGE_NODE) {

                // peer must be able to turn into rendezvous since the it may be not available sometime
                netPeerGroup.getRendezVousService()
                            .setAutoStart(config.isAutoRdvEnable(), config.getRendezvousConnectionTimeout() * 1000);
            }

            logger.info("Successfully started JXTA.");
        } catch (Exception e) {
            logger.error("There was an error starting JXTA support... : {}", e);
            throw new CommunicationException(e.getMessage());
        }
    }

    /**
     * Publish the service of the application
     * @throws CommunicationException 
     */
    protected void publishService() throws CommunicationException {
        logger.info("Publishing JXTA service...");

        try {
            peerGroup.getModuleAdvertisementManager()
                     .createModuleClassAdvertisement();
            peerGroup.getModuleAdvertisementManager()
                     .createModuleImplAvertisement();
            peerGroup.getModuleAdvertisementManager()
                     .createModuleSpecAdvertisement();

            joinCreateSomewhereSubGroup();
            createServerPipeAdvertisement();

            peerGroup.getModuleAdvertisementManager()
                     .publishModuleSpecAdvertisement();
            
            logger.info("Successfully published JXTA service.");
        } catch (Exception e) {
            logger.error("There was an error publishing JXTA service... : {} ", e);
            throw new CommunicationException(e.getMessage());
        }
    }

    /**
     * Creates or join in a JXTA subgroup.
     * 
     * @throws CommunicationException 
     */
    private void joinCreateSomewhereSubGroup() throws CommunicationException {
    	
    	CommunicationConfiguration config = moduleManager.getCommunicationManager().getConfiguration();
    	
        try {
            PeerGroupAdvertisement peerGroupAdvertisement = peerGroup.getPeerGroupAdvertisementManager()
                                                                     .fecthGroupAdvertisement(config.getPeerGroupName());

            if (peerGroupAdvertisement == null) {
                // Creates the Somewhere subgroup and publish it
                PeerGroupID somewherePeerGrooupId = IDFactory.newPeerGroupID(config.getPeerGroupName().getBytes());
                
                somewherePeerGroup = netPeerGroup.newGroup(somewherePeerGrooupId,
                        peerGroup.getModuleAdvertisementManager()
                                 .getModuleImplAdv(), config.getPeerGroupName(),
                                 config.getPeerGroupName() + " Group", true);
            } else {
                // Join in a peerGroup using its advertisement
                somewherePeerGroup = netPeerGroup.newGroup(peerGroupAdvertisement);
                
                // Multicast the group to inform its participation
                net.jxta.endpoint.Message jxtaMessage = new net.jxta.endpoint.Message();
                MessageElement messageElement = new StringMessageElement("joinGroup", this.getName(), null);
                jxtaMessage.addMessageElement("somewhere", messageElement);
                
                somewherePeerGroup.getEndpointService().propagate(jxtaMessage, 
                		"jxtaPropagate", config.getPeerGroupName());
            }
            
            // Register listener for join group multicast
            somewherePeerGroup.getEndpointService().addIncomingMessageListener(new JXTAGroupNotificationListener(peerGroup), 
            		"jxtaPropagate", config.getPeerGroupName());
        } catch (Exception e) {
            logger.error(
                "There was an error joining int the group " + config.getPeerGroupName() + "... : {} ", e);
            throw new CommunicationException(e.getMessage());
        }
    }

    /**
     * Creates a pipe advertisement in order to receive outside messages.
     * 
     * @throws CommunicationException 
     */
    private void createServerPipeAdvertisement() throws CommunicationException {
        try {
            // Create Pipe Advertisement
            PipeID pipeID = IDFactory.newPipeID(somewherePeerGroup.getPeerGroupID(),
                    getName().getBytes());
            inputPipeAdvertisement = getPeerGroup().getPipeAdvertisementManager()
                                         .createInputPipeAdvertisement(pipeID);
        } catch (Exception e) {
            logger.error(
                "There was an error creating the pipe advertisement... : {}", e);
            throw new CommunicationException(e.getMessage());
        }
    }

    /**
     * Create the bidirectional server pipe that endpoint clients will use to connect to the service
     * @throws CommunicationException 
     */
    private void createServerPipe() throws CommunicationException {
        try {
            pipeServer = new JxtaServerPipe(somewherePeerGroup,
                    inputPipeAdvertisement);
            pipeServer.setPipeTimeout(0);
        } catch (IOException e) {
            logger.error("There was an error creating the server pipe... : {}" ,e);
            throw new CommunicationException(e.getMessage());
        }
    }

    /**
     * Handle outside connections.
     * 
     * @param pipeServer
     * @throws IOException
     */
    private void waitConnections() throws InterruptedException, IOException {
        while (!requestHandlerExecutorService.isShutdown()) {
            final JxtaBiDiPipe bidiPipe = pipeServer.accept();
            requestHandlerExecutorService.execute(new Runnable() {
                    public void run() {
                        bidiPipe.setMessageListener(peerGroup.getMessageReceiver());
                        bidiPipe.setPipeStateListener(new JXTAPipeStateListener(moduleManager));

                        String peerName = bidiPipe.getRemotePeerAdvertisement()
                                                  .getName();
                        JXTAPeer jxtaPeer = peerGroup.retrievePeer(peerName);
                        jxtaPeer.setPipe(bidiPipe);
                    }
                });
        }
    }

}
