/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import net.jxta.discovery.DiscoveryEvent;
import net.jxta.discovery.DiscoveryListener;

import net.jxta.document.Advertisement;

import net.jxta.protocol.DiscoveryResponseMsg;
import net.jxta.protocol.PipeAdvertisement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Enumeration;


/**
 * Responsible for the discovery of pipes advertisements.
 * @author Andre Fonseca
 *
 */
public class PipeDiscoveryListener implements DiscoveryListener {
    static Logger logger = LoggerFactory.getLogger(PipeDiscoveryListener.class);

    /* --- Properties --- */
    /** Related advertisementManager */
    private PipeAdvertisementManager advManager;

    /** The name of peer to search for */
    private final String name;

    /** The indicated pipe advertisement */
    private PipeAdvertisement advertisement;

    /** Indicates if the pipe advertisement was found */
    private boolean found = false;

    public PipeDiscoveryListener(PipeAdvertisementManager advManager,
        String name) {
        if (advManager == null) {
            throw new IllegalArgumentException("advManager");
        }

        if (name == null) {
            throw new IllegalArgumentException("name");
        }

        this.advManager = advManager;
        this.name = name;
    }

    /* --- Acessors --- */
    public boolean hasAdvertisementFound() {
        return found;
    }

    public PipeAdvertisement getAdvertisement() {
        return advertisement;
    }

    /* --- Methods --- */   
    /**
     * {@inheritDoc}
     */
    @Override
    public void discoveryEvent(DiscoveryEvent event) {
        DiscoveryResponseMsg discoveryResponseMsg = event.getResponse();

        Enumeration<Advertisement> enumAdvertisements = discoveryResponseMsg.getAdvertisements();
        advertisement = advManager.verifyPipeAdvertisements(enumAdvertisements,
                "inpipe." + name);
        found = true;
    }
}
