/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import net.jxta.util.JxtaBiDiPipe;
import net.jxta.util.PipeStateListener;

/**
 * Listener responsible for handling bidi-pipe events.
 * 
 * @author Andre Fonseca
 *
 */
public class JXTAPipeStateListener implements PipeStateListener {
    
	/* --- Properties --- */
    /** Module Manager */
    protected final JXTAManager moduleManager;

    public JXTAPipeStateListener(JXTAManager moduleManager) {
        if (moduleManager == null) {
            throw new IllegalArgumentException("moduleManager");
        }

        this.moduleManager = moduleManager;
    }

    /* --- Methods --- */
    /**
     * {@inheritDoc}
     */
    @Override
    public void stateEvent(Object source, int event) {
        switch (event) {
        	case PipeStateListener.PIPE_OPENED_EVENT:

            	break;
	        case PipeStateListener.PIPE_CLOSED_EVENT:
	            unbindConnection((JxtaBiDiPipe) source);
	
	            break;
	
	        case PipeStateListener.PIPE_FAILED_EVENT:
	            unbindConnection((JxtaBiDiPipe) source);
	
	            break;
        }
    }

    /**
     * Unbind pipe connection if pipe failed or was closed remotely.
     * @param pipe
     */
    private void unbindConnection(JxtaBiDiPipe pipe) {	
        String peerName = pipe.getRemotePeerAdvertisement().getName();
        JXTAPeer peer = moduleManager.getPeerGroup().retrievePeer(peerName);
        
        if (peer !=  moduleManager.getPeerGroup().getLocalPeer() && peer.isConnected()) {
        	peer.disconnect();
        }
    }
}
