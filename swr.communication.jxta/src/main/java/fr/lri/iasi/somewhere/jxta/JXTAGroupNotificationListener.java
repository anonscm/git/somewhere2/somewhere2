/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.somewhere.api.communication.model.Peer;
import net.jxta.endpoint.EndpointAddress;
import net.jxta.endpoint.EndpointListener;
import net.jxta.endpoint.Message;

public class JXTAGroupNotificationListener implements EndpointListener {
	static Logger logger = LoggerFactory.getLogger(JXTAGroupNotificationListener.class);

    /* --- Properties --- */

    /** The PeerGroup this peer is in */
    protected final JXTAPeerGroup peerGroup;

    public JXTAGroupNotificationListener(final JXTAPeerGroup peerGroup) {
        if (peerGroup == null) {
            throw new IllegalArgumentException("peerGroup");
        }

        this.peerGroup = peerGroup;
    }

    /* --- Accessors --- */
    public JXTAPeerGroup getPeerGroup() {
        return peerGroup;
    }

	@Override
	public void processIncomingMessage(Message message,
			EndpointAddress srcAddr, EndpointAddress dstAddr) {

		if (message.getMessageElement("joinGroup") != null) {
			String peerName = message.getMessageElement("joinGroup").toString();
			Peer peer = peerGroup.getPeers().get(peerName);
		
		    if ((peer != null) && !(peer.equals(peerGroup.getLocalPeer())))
		    	peer.connect();
		}
	}


}
