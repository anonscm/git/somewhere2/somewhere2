/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.model.Peer;

import net.jxta.protocol.PipeAdvertisement;

import net.jxta.util.JxtaBiDiPipe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


/**
 * A peer that can emit or receive messages - JXTA version
 * @author Andre Fonseca
 */
public class JXTAPeer extends Peer {
    static Logger logger = LoggerFactory.getLogger(JXTAPeer.class);
    private static final long serialVersionUID = 945838945847L;

    /* --- Properties --- */
    /** The PeerGroup this peer is in */
    protected final JXTAPeerGroup peerGroup;
    private JxtaBiDiPipe pipe;
    protected PipeAdvertisement inputPipeAdvertisement;

    /** The concurrent ExecutorService that will execute the different threads/routines related to the pipe connections to local/remote peers */
    private final ExecutorService pipeConnectionExecutorService;

    /* --- Methods --- */
    public JXTAPeer(String name, JXTAPeerGroup peerGroup) {
        super(name);

        if (peerGroup == null) {
            throw new IllegalArgumentException("peerGroup");
        }

        this.peerGroup = peerGroup;
        this.pipeConnectionExecutorService = Executors.newCachedThreadPool();
    }

    /* --- Accessors --- */
    public JXTAPeerGroup getPeerGroup() {
        return peerGroup;
    }

    public JxtaBiDiPipe getPipe() {
        return pipe;
    }

    public PipeAdvertisement getInputPipeAdvertisement() {
        return this.inputPipeAdvertisement;
    }

    /* --- Mutators --- */
    public void setPipeInputAdvertisement(PipeAdvertisement pipeAdvertisement) {
        this.inputPipeAdvertisement = pipeAdvertisement;
    }

    protected void setPipe(JxtaBiDiPipe pipe) {
        this.pipe = pipe;
    }

    /**
     * {@inheritDoc}
     */
    protected void initConnection() throws CommunicationException {
        bindToLocalPeer();
    }

    /**
     * {@inheritDoc}
     */
    protected void initDisconnection() throws CommunicationException {
        unbindToLocalPeer();
    }

    /**
     * Binds the local peer pipe and the current peer pipe.
     * 
     * @throws CommunicationException 
     */
    void bindToLocalPeer() throws CommunicationException {
        // Establish connection with neighbor
        Future<?> future = pipeConnectionExecutorService.submit(new Callable<Boolean>() {
                    public Boolean call() throws CommunicationException {
                        return createPipe();
                    }
                });

        // We need to block until all connection tasks are done. This is made by catching the "future" state of the task. 
        try {
            future.get();
        } catch (ExecutionException e) {
            throw new CommunicationException(e.getMessage());
        } catch (InterruptedException e) {
			Thread.interrupted();
		}
    }

    /**
     * Closes pipe between the local peer and the current peer.
     * 
     * @throws CommunicationException 
     */
    void unbindToLocalPeer() throws CommunicationException {
        // Close pipe connection with neighbor
        Future<?> future = pipeConnectionExecutorService.submit(new Runnable() {
                    public void run() {
						try {
							pipe.close();
						} catch (IOException e) {
						}
                    }
                });

        // We need to block until all connection tasks are done. This is made by catching the "future" state of the task. 
        try {
            future.get();
        } catch (ExecutionException e) {
            throw new CommunicationException(e.getMessage());
        } catch (InterruptedException e) {
        	Thread.interrupted();
		}
    }

    /**
     * Creates pipe for local peer.
     * 
     * @throws CommunicationException 
     */
    private boolean createPipe() throws CommunicationException {
        try {
            PipeAdvertisementManager pipeAdvertisementManager = peerGroup.getPipeAdvertisementManager();

            // Fetch advertisement from local cache or search it in the network.
            pipeAdvertisementManager.fetchPipeAdvertisement(this, "Name");

            int timeOut = peerGroup.getModuleManager().getConfiguration()
                                   .getPipeResolveTimeout() * 100;

            // Add listener to handle the asynchronous requests instead of waitforMessage function	
            MessagePipeListener msgListener = peerGroup.getMessageReceiver();

            // Create the birectionalPipe.
            net.jxta.peergroup.PeerGroup somewherePeerGroup = peerGroup.getLocalPeer()
                                                                       .getSomewherePeerGroup();

            if (pipe != null)
                pipe = null;

            pipe = new JxtaBiDiPipe(somewherePeerGroup, inputPipeAdvertisement,
                    timeOut, msgListener, true);

            pipe.setPipeStateListener(new JXTAPipeStateListener( peerGroup.getModuleManager()));
        } catch (Exception e) {
            throw new CommunicationException(e.getMessage());
        }
        
        return true;
    }
}
