/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import net.jxta.discovery.DiscoveryService;

import net.jxta.document.Advertisement;

import net.jxta.protocol.PeerGroupAdvertisement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Enumeration;


public class PeerGroupAdvertisementManager {
    static Logger logger = LoggerFactory.getLogger(PeerGroupAdvertisementManager.class);

    /* --- Properties --- */
    /** Module Manager */
    protected final JXTAManager moduleManager;

    /* --- Methods --- */
    public PeerGroupAdvertisementManager(JXTAManager moduleManager) {
        if (moduleManager == null) {
            throw new IllegalArgumentException("moduleManager");
        }

        this.moduleManager = moduleManager;
    }

    /* --- Accessors --- */
    public JXTAManager getModuleManager() {
        return moduleManager;
    }

    public PeerGroupAdvertisement fecthGroupAdvertisement(String groupName)
        throws AdvertisementNotFoundException {
        if (groupName == null) {
            throw new IllegalArgumentException("groupName");
        }

        DiscoveryService discoveryService = moduleManager.getPeerGroup()
                                                         .getLocalPeer()
                                                         .getJXTAPeerGroup()
                                                         .getDiscoveryService();
        PeerGroupAdvertisement peerGroupAdvertisement = null;

        try {
            Enumeration<Advertisement> localGroupAdvertEnum = discoveryService.getLocalAdvertisements(DiscoveryService.GROUP,
                    "Name", groupName);
            peerGroupAdvertisement = (PeerGroupAdvertisement) localGroupAdvertEnum.nextElement();

            return peerGroupAdvertisement;
        } catch (Exception ex) {
        }

        // If there are no local advertisements that correspond to the target PiperAdvertisement, fetch it remotely
        if (peerGroupAdvertisement == null) {
            final PeerGroupDiscoveryListener listener = new PeerGroupDiscoveryListener();
            discoveryService.getRemoteAdvertisements(null,
                DiscoveryService.GROUP, "Name", groupName, 1, listener);

            try {
                Thread.sleep(moduleManager.getConfiguration()
                                          .getFetchAdvertisementTimeout() * 1000);
            } catch (InterruptedException ex) {
            } finally {
                discoveryService.removeDiscoveryListener(listener);
            }

            if (listener.hasAdvertisementFound()) {
                return listener.getAdvertisement();
            }
        }

        return null;
    }
}
