/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import net.jxta.discovery.DiscoveryEvent;
import net.jxta.discovery.DiscoveryListener;

import net.jxta.document.Advertisement;

import net.jxta.protocol.DiscoveryResponseMsg;
import net.jxta.protocol.PeerGroupAdvertisement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Enumeration;

/**
 * Listener responsible for handling peer discovery events.
 * 
 * @author Andre Fonseca
 *
 */
public class PeerGroupDiscoveryListener implements DiscoveryListener {
    static Logger logger = LoggerFactory.getLogger(PeerGroupDiscoveryListener.class);

    /* --- Properties --- */
    /** The peer group advertisement */
    private PeerGroupAdvertisement advertisement;

    /** Indicates if a new peer was found */
    private boolean found = false;

    /* --- Acessors --- */
    public boolean hasAdvertisementFound() {
        return found;
    }

    public PeerGroupAdvertisement getAdvertisement() {
        return advertisement;
    }

    /* --- Methods --- */
    /**
     * {@inheritDoc}
     */
    @Override
    public void discoveryEvent(DiscoveryEvent event) {
        DiscoveryResponseMsg discoveryResponseMsg = event.getResponse();

        Enumeration<Advertisement> enumAdvertisements = discoveryResponseMsg.getAdvertisements();
        advertisement = (PeerGroupAdvertisement) enumAdvertisements.nextElement();
        found = true;
    }
}
