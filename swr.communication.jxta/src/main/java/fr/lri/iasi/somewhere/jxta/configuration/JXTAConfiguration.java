/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta.configuration;

import fr.lri.iasi.somewhere.Module;

import net.jxta.platform.NetworkConfigurator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


/**
 * Configuration of jxta bundle
 * @author Andre Fonseca
 */
public class JXTAConfiguration extends fr.lri.iasi.somewhere.Configuration
    implements Serializable {
    static Logger logger = LoggerFactory.getLogger(JXTAConfiguration.class);
    private static final long serialVersionUID = 745838945863L;

    /* --- Constants --- */
    /** Default cache folder (for property <b>cacheFolder</b>) */
    private final String DEFAULT_CACHE_FOLDER = "jxtaCache";

    /** Default JXTA Port (for property <b>port</b>) */
    private final int DEFAULT_PORT = 6000;

    /* --- Properties --- */
    /** Type of the Peer */
    protected int peerType;

    /** TODO */
    protected boolean autoRdvEnable;

    /** The path of the cache folder */
    protected String cacheFolder;

    /** TODO */
    protected String cacheReUse;

    /** Port used for this peer */
    protected int port;

    /** TODO */
    protected boolean enabled;

    /** TODO */
    protected boolean incomingEnabled;

    /** TODO */
    protected boolean outgoingEnabled;

    /** TODO */
    protected boolean cachedOutputPipe;

    /** TODO */
    protected int version;

    /** TODO */
    protected String creator;

    /** TODO */
    protected String url;

    /** TODO */
    protected int inpipePipeID;

    /** TODO */
    protected int moduleClassID;

    /** TODO */
    protected int advExpirationTime;

    /** TODO */
    protected int advLifeTime;

    /** TODO */
    protected int pipeResolveTimeout;

    /** TODO */
    protected int fetchAdvertisementTimeout;

    /** TODO */
    protected int numberOfRetriesForDiscovery;

    /** TODO */
    protected int rendezvousConnectionTimeout;

    /** TODO */
    protected int gapBetweenTwoDiscoveryRequests;

    /** The timeout used for a message */
    protected int timeoutForAMessage;

    /** Number of concurrent threads for MessageSender */
    protected int numberOfMessageSenderThreads;

    /** List of Rendez-vous peers */
    protected List<String> rdvPeers;

    /** List of Relay peers */
    protected List<String> relayPeers;

    public JXTAConfiguration(Module module) {
        super(module);
    }

    /* --- Mutators --- */
    public void setPeerType(int peerType) {
        this.peerType = peerType;
    }

    public void setAutoRdvEnable(boolean autoRdvEnable) {
        this.autoRdvEnable = autoRdvEnable;
    }

    public void setCacheFolder(String cacheFolder) {
        if ((cacheFolder != null) && !cacheFolder.equals("")) {
            this.cacheFolder = cacheFolder;
        }
    }

    public void setCacheReUse(String cacheReUse) {
        if ((cacheReUse != null) && !cacheReUse.equals("")) {
            this.cacheReUse = cacheReUse;
        }
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setIncomingEnabled(boolean incomingEnabled) {
        this.incomingEnabled = incomingEnabled;
    }

    public void setOutgoingEnabled(boolean OutgoingEnabled) {
        this.outgoingEnabled = OutgoingEnabled;
    }

    public void setCachedOutputPipe(boolean cachedOutputPipe) {
        this.cachedOutputPipe = cachedOutputPipe;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void setCreator(String creator) {
        if ((creator != null) && !creator.equals("")) {
            this.creator = creator;
        }
    }

    public void setURL(String URL) {
        if ((URL != null) && !URL.equals("")) {
            this.url = URL;
        }
    }

    public void setInpipePipeID(int inpipePipeID) {
        this.inpipePipeID = inpipePipeID;
    }

    public void setModuleClassID(int moduleClassID) {
        this.moduleClassID = moduleClassID;
    }

    public void setAdvExpirationTime(int advExpirationTime) {
        this.advExpirationTime = advExpirationTime;
    }

    public void setAdvLifeTime(int advLifeTime) {
        this.advLifeTime = advLifeTime;
    }

    public void setPipeResolveTimeout(int pipeResolveTimeout) {
        this.pipeResolveTimeout = pipeResolveTimeout;
    }

    public void setFetchAdvertisementTimeout(int fetchAdvertisementTimeout) {
        this.fetchAdvertisementTimeout = fetchAdvertisementTimeout;
    }

    public void setNumberOfRetriesForDiscovery(int numberOfRetriesForDiscovery) {
        this.numberOfRetriesForDiscovery = numberOfRetriesForDiscovery;
    }

    public void setGapBetweenTwoDiscoveryRequests(
        int gapBetweenTwoDiscoveryRequests) {
        this.gapBetweenTwoDiscoveryRequests = gapBetweenTwoDiscoveryRequests;
    }

    public void setTimeoutForAMessage(int timeoutForAMessage) {
        this.timeoutForAMessage = timeoutForAMessage;
    }

    public void setRdvPeers(List<String> rdvPeers) {
        this.rdvPeers = rdvPeers;
    }

    public void setRelayPeers(List<String> relayPeers) {
        this.relayPeers = relayPeers;
    }

    public void setRendezvousConnectionTimeout(int rendezvousConnectionTimeout) {
        this.rendezvousConnectionTimeout = rendezvousConnectionTimeout;
    }

    public void setNumberOfMessageSenderThreads(
        int numberOfMessageSenderThreads) {
        this.numberOfMessageSenderThreads = numberOfMessageSenderThreads;
    }

    /* --- Accessors --- */
    public int getPeerType() {
        return this.peerType;
    }

    public boolean isAutoRdvEnable() {
        return this.autoRdvEnable;
    }

    public String getCacheFolder() {
        return this.cacheFolder;
    }

    public String getCacheReUse() {
        return this.cacheReUse;
    }

    public int getPort() {
        return this.port;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public boolean isIncomingEnabled() {
        return this.incomingEnabled;
    }

    public boolean isOutgoingEnabled() {
        return this.outgoingEnabled;
    }

    public boolean isCachedOutputPipe() {
        return this.cachedOutputPipe;
    }

    public int getVersion() {
        return this.version;
    }

    public String getCreator() {
        return this.creator;
    }

    public String getURL() {
        return this.url;
    }

    public int getInpipePipeID() {
        return this.inpipePipeID;
    }

    public int getModuleClassID() {
        return this.moduleClassID;
    }

    public int getAdvExpirationTime() {
        return this.advExpirationTime;
    }

    public int getAdvLifeTime() {
        return this.advLifeTime;
    }

    public int getPipeResolveTimeout() {
        return this.pipeResolveTimeout;
    }

    public int getFetchAdvertisementTimeout() {
        return this.fetchAdvertisementTimeout;
    }

    public int getNumberOfRetriesForDiscovery() {
        return this.numberOfRetriesForDiscovery;
    }

    public int getGapBetweenTwoDiscoveryRequests() {
        return this.gapBetweenTwoDiscoveryRequests;
    }

    public int getTimeoutForAMessage() {
        return this.timeoutForAMessage;
    }

    public List<String> getRdvPeers() {
        return this.rdvPeers;
    }

    public List<String> getRelayPeers() {
        return this.relayPeers;
    }

    public int getRendezvousConnectionTimeout() {
        return rendezvousConnectionTimeout;
    }

    public int getNumberOfMessageSenderThreads() {
        return numberOfMessageSenderThreads;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAllToDefault() {
        peerType = NetworkConfigurator.EDGE_NODE;
        autoRdvEnable = true;
        cacheFolder = getWorkingDirectory() + getLocalDirectorySeparator() +
            DEFAULT_CACHE_FOLDER;
        cacheReUse = "";
        port = DEFAULT_PORT;
        enabled = true;
        incomingEnabled = true;
        outgoingEnabled = true;
        cachedOutputPipe = true;
        version = 42;
        creator = "Unknown";
        url = "Unknown";
        inpipePipeID = 0;
        moduleClassID = 0;
        advExpirationTime = 30;
        advLifeTime = 30;
        pipeResolveTimeout = 30;
        fetchAdvertisementTimeout = 1;
        numberOfRetriesForDiscovery = 3;
        gapBetweenTwoDiscoveryRequests = 10;
        rendezvousConnectionTimeout = 40;
        timeoutForAMessage = 30;
        numberOfMessageSenderThreads = 25;
        rdvPeers = new ArrayList<String>();
        relayPeers = new ArrayList<String>();
    }

    /**
     * Return the local working directory. <br>
     * @return the working dir
     */
    private static String getWorkingDirectory() {
        return System.getProperty("user.dir");
    }

    /**
     * @return the directory separator character for
     *  le local system. <br>
     */
    private static String getLocalDirectorySeparator() {
        return System.getProperty("file.separator");
    }
}
