/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import java.io.IOException;

import fr.lri.iasi.somewhere.api.communication.configuration.CommunicationConfiguration;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.api.communication.model.PeerGroup;

import net.jxta.endpoint.MessageElement;
import net.jxta.endpoint.StringMessageElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class represents a peer group, containing:
 *        - a local peer
 *        - its neighbors
 */
public class JXTAPeerGroup extends PeerGroup<JXTAManager, JXTAPeer, JXTALocalPeer> {
    static Logger logger = LoggerFactory.getLogger(JXTAPeerGroup.class);

    /* --- Properties --- */
    /** Manages pipe advertisements for this group */
    protected PipeAdvertisementManager pipeAdvertisementManager;

    /** Manages peer group advertisements for this group */
    protected PeerGroupAdvertisementManager peerGroupAdvertisementManager;

    /** Manages module advertisements for this group */
    protected ModuleAdvertisementManager moduleAdvertisementManager;
    
    /** The main message sender */
    protected MessageSender messageSender;
    
    /** The main message receiver */
    protected MessagePipeListener messageReceiver;

    public JXTAPeerGroup(JXTAManager moduleManager) {
    	super(moduleManager);

        pipeAdvertisementManager = new PipeAdvertisementManager(moduleManager);
        peerGroupAdvertisementManager = new PeerGroupAdvertisementManager(moduleManager);
        moduleAdvertisementManager = new ModuleAdvertisementManager(moduleManager);

        // Register itself as a peer Listener
        this.moduleManager.getCommunicationManager().addPeerListener(this);

        // The messageSender
        this.messageSender = new MessageSender(moduleManager);
        this.messageReceiver = new MessagePipeListener(moduleManager);
        this.moduleManager.getCommunicationManager().setMessageSender(this.messageSender);
        this.moduleManager.getCommunicationManager().setMessageReceiver(this.messageReceiver);
    }

    /* --- Accessors --- */
    public PipeAdvertisementManager getPipeAdvertisementManager() {
        return pipeAdvertisementManager;
    }

    public PeerGroupAdvertisementManager getPeerGroupAdvertisementManager() {
        return peerGroupAdvertisementManager;
    }

    public ModuleAdvertisementManager getModuleAdvertisementManager() {
        return moduleAdvertisementManager;
    }
    
    public MessageSender getMessageSender() {
        return messageSender;
    }

    public MessagePipeListener getMessageReceiver() {
        return messageReceiver;
    }

    /* --- Methods --- */
    /**
     * Uninitialize our stuffs about the peer group
     */
    public void uninit() {
        // Uninit the local peer
        localPeer.disconnect();
    }

    /**
     * Initialize the peerGroup
     */
    public void init() {
    	CommunicationConfiguration communicationConfig = moduleManager.getCommunicationManager().getConfiguration();
    	// Get the group name from the configuration
    	name = communicationConfig.getPeerGroupName();
    	
        // Init the local peer
        localPeer = new JXTALocalPeer(communicationConfig.getPeerName(), this, moduleManager);
        localPeer.connect();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void peerIsNoMoreAccessible(Peer peer) {
    	// Multicast the group to inform that is accessible
    	if (peer.equals(localPeer)) {
	        net.jxta.endpoint.Message jxtaMessage = new net.jxta.endpoint.Message();
	        MessageElement messageElement = new StringMessageElement("inaccessible", localPeer.getName(), null);
	        jxtaMessage.addMessageElement("somewhere", messageElement);
	        
	        try {
				localPeer.getSomewherePeerGroup().getEndpointService().propagate(jxtaMessage, "jxtaPropagate", this.name);
			} catch (IOException e) {
				logger.error("Problems sending not accessibile message...");
			}
    	}
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void peerIsAccessible(Peer peer) {
    	// Multicast the group to inform that is inaccessible
    	if (peer.equals(localPeer)) {
	        net.jxta.endpoint.Message jxtaMessage = new net.jxta.endpoint.Message();
	        MessageElement messageElement = new StringMessageElement("accessible", localPeer.getName(), null);
	        jxtaMessage.addMessageElement("somewhere", messageElement);
	        
	        try {
				localPeer.getSomewherePeerGroup().getEndpointService().propagate(jxtaMessage, "jxtaPropagate", this.name);
			} catch (IOException e) {
				logger.error("Problems sending accessibile message...");
			}
    	}
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void peerIsConnecting(Peer peer) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void peerIsLeaving(Peer peer) {
    }
}
