/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import net.jxta.discovery.DiscoveryService;

import net.jxta.document.Advertisement;

import java.util.Enumeration;


/**
 * A class to pretty print infos about Advertisements
 */
public class AdvertisementsPrettyPrinter {
    /* --- Properties --- */
    protected final JXTAPeerGroup peerGroup;

    public AdvertisementsPrettyPrinter(JXTAPeerGroup peerGroup) {
        if (peerGroup == null) {
            throw new IllegalArgumentException("peerGroup");
        }

        this.peerGroup = peerGroup;
    }

    /* --- Accessors --- */
    public JXTAPeerGroup getPeerGroup() {
        return peerGroup;
    }

    /**
     * @return a string giving a pretty print of
     *  infos about the local peer
     */
    public String toString() {
        String res = "";

        res += printDiscoveryInfos();

        return res;
    }

    /**
     * @return a string giving a pretty print of
     *  jxta discovery service
     */
    protected String printDiscoveryInfos() {
        String res = "";

        res += "\n Local Advertisements - PEER :";
        res += printDiscoveryInfos(DiscoveryService.PEER);
        res += "\n";

        res += "\n Local Advertisements - GROUP :";
        res += printDiscoveryInfos(DiscoveryService.GROUP);
        res += "\n";

        res += "\n Local Advertisements - ADV :";
        res += printDiscoveryInfos(DiscoveryService.ADV);
        res += "\n";

        return res;
    }

    /**
     * Help function for <b>printDiscoveryInfos()</b>
     * @param type Type of local advertisements to print infos of
     */
    protected String printDiscoveryInfos(int type) {
        String res = "";
        net.jxta.peergroup.PeerGroup somewherePeerGroup = peerGroup.getLocalPeer()
                                                                   .getSomewherePeerGroup();
        DiscoveryService ds = somewherePeerGroup.getDiscoveryService();

        try {
            for (Enumeration<Advertisement> e = ds.getLocalAdvertisements(
                        type, null, null); e.hasMoreElements();) {
                Advertisement i = e.nextElement();

                res += ("\n\t " + i.getID());

                res += ("\n\t\t Advertisement Type : " + i.getAdvType());
                res += "\n\t\t Index Fields : ";

                for (String s : i.getIndexFields()) {
                    res += ("\n\t\t\t " + s);
                }
            }
        } catch (Exception e) {
            res += "\n\t There was an error retreiving local advertisements ...";
        }

        return res;
    }
}
