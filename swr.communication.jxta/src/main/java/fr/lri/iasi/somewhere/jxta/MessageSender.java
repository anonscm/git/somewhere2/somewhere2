/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageSender;

import net.jxta.document.MimeMediaType;

import net.jxta.endpoint.ByteArrayMessageElement;
import net.jxta.endpoint.MessageElement;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Class to deals with JXTA message sending
 * @author Andre Fonseca
 */
public class MessageSender extends AbstractMessageSender {
    static Logger logger = LoggerFactory.getLogger(MessageSender.class);

    /* --- Properties --- */
    /** Module Manager */
    protected final JXTAManager moduleManager;

    /** The concurrent ExecutorService that will send messages to local/remote peers */
    private ExecutorService messageExecutorService;

    /* --- Methods --- */
    MessageSender(final JXTAManager moduleManager) {
        super(moduleManager.getCommunicationManager());

        this.moduleManager = moduleManager;

        // Create the message executor service
        int nbThreads = moduleManager.getConfiguration()
                                     .getNumberOfMessageSenderThreads();
        
        this.messageExecutorService = Executors.newFixedThreadPool(nbThreads);
    }

    /**
     *
     */
    public void uninit() {
        messageExecutorService.shutdownNow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processSending(final Message message)
    	throws CommunicationException {
        JXTAPeer destination = moduleManager.getPeerGroup()
                                            .retrievePeer(message.getDestination()
                                                            .getName());

        if ((destination.getPipe() == null) ||
                !destination.getPipe().isBound()) {
            destination.bindToLocalPeer();
            destination.setAccessible();
        }

        if (destination.isConnected() && destination.isAccessible()) {
        	Future<Boolean> future = messageExecutorService.submit(new SendMsg(destination, message));
            
            try {
				future.get();
			} catch (InterruptedException e) {
				Thread.interrupted();
			} catch (ExecutionException e) {
				throw new CommunicationException(e.getMessage());
			}
        } else {
            throw new CommunicationException("Cannot connect to " + destination);
        }
    }

    /**
     * Class used to send a message
     */
    private class SendMsg implements Callable<Boolean> {
        /** The message to send */
        private final Message message;
        private final JXTAPeer peer;

        private SendMsg(final JXTAPeer peer, final Message message) {
            if (message == null) {
                throw new IllegalArgumentException("message");
            }

            if (peer == null) {
                throw new IllegalArgumentException("peer");
            }

            this.message = JXTAMessage.createJXTAMessage(message);
            this.peer = peer;
        }

        /**
         * Sends a message
         * @return
         */
        @Override
        public Boolean call() throws CommunicationException{
            net.jxta.endpoint.Message jxtaMessage = null;
            ByteArrayOutputStream bytes = null;
            ObjectOutputStream obInputStream = null;

            try {
                jxtaMessage = new net.jxta.endpoint.Message();

                byte[] serializedObject = null;
                bytes = new ByteArrayOutputStream();

                obInputStream = new ObjectOutputStream(bytes);
                obInputStream.writeObject(message);
                obInputStream.flush();
                obInputStream.close();
                serializedObject = bytes.toByteArray();
                bytes.close();

                MessageElement messageElement = new ByteArrayMessageElement("JXTAMessage",
                        new MimeMediaType("application/octet-stream"),
                        serializedObject, null);
                jxtaMessage.addMessageElement(null, messageElement);

                peer.getPipe().sendMessage(jxtaMessage);

            } catch (Exception e) {
            	throw new CommunicationException(e.getMessage());
            } finally {
                try {
                    if (obInputStream != null) {
                        obInputStream.close();
                    }

                    if (bytes != null) {
                        bytes.close();
                    }
                } catch (final Exception e) {
                    return false;
                }
            }
            
            return true;
        }
    }
}
