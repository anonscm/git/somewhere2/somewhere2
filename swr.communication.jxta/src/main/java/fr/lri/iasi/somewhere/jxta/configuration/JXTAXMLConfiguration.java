/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta.configuration;

import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.XPathReader;

import net.jxta.platform.NetworkConfigurator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.Serializable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathConstants;


/**
 * Configuration, with ability to build configuration from an XML file
 * @author Andre Fonseca
 */
public class JXTAXMLConfiguration extends JXTAConfiguration
    implements Serializable {
    static Logger logger = LoggerFactory.getLogger(JXTAXMLConfiguration.class);
    private static final long serialVersionUID = 745838945858L;

    public JXTAXMLConfiguration(Module module) {
        super(module);
    }

    /**
     * Parse XML configuration node to obtain corresponding
     * Configuration properties
     */
    public void parseConfig(Node config) {
        try {
            // Verify if there is a valid config
            if (config == null) {
                return;
            }

            XPathReader reader = new XPathReader(config);
            parseLocalPeer(reader);
            parseSeeds(reader);
            parseNetwork(reader);
            parseService(reader);
            parseMessage(reader);
        } catch (Exception e) {
            logger.error("There were errors parsing XML configuration file : " +
                e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Parse XML configuration file, part 'localPeer'
     */
    private void parseLocalPeer(XPathReader reader) throws Exception {
    	
        String peerTypeStr = reader.read("localPeer/peerType",
                XPathConstants.STRING).toString();

        if (peerTypeStr.equalsIgnoreCase("Edge")) {
            peerType = NetworkConfigurator.EDGE_NODE;
        } else if (peerTypeStr.equalsIgnoreCase("Relay")) {
            peerType = NetworkConfigurator.RELAY_NODE;
        } else if (peerTypeStr.equalsIgnoreCase("Rdv")) {
            peerType = NetworkConfigurator.RDV_NODE;
        } else {
            peerType = NetworkConfigurator.EDGE_NODE;
        }

        String autoRdvEnableStr = reader.read("localPeer/autoRdvEnabled", XPathConstants.STRING).toString();
        if (!autoRdvEnableStr.isEmpty())
        	autoRdvEnable = Boolean.parseBoolean(autoRdvEnableStr);

        String cacheFolderStr = reader.read("localPeer/cacheFolder", XPathConstants.STRING).toString();
        if (!cacheFolderStr.isEmpty())
        	cacheFolder = cacheFolderStr;

        String cacheReUseStr = reader.read("localPeer/cacheReUse", XPathConstants.STRING).toString();
        if (!cacheReUseStr.isEmpty())
        	cacheReUse = cacheReUseStr;
    }

    /**
     * Parse XML configuration file, part 'seeds'
     */
    private void parseSeeds(XPathReader reader) throws Exception {
        /* -- rdvPeers -- */
        NodeList rdvPeersNodes = (NodeList) reader.read("seeds/rdvPeers",
                XPathConstants.NODESET);

        for (int index = 0; index < rdvPeersNodes.getLength(); index++) {
            Node node = rdvPeersNodes.item(index);
            rdvPeers.add(node.getTextContent());
        }

        /* -- relayPeers -- */
        NodeList relayPeersNodes = (NodeList) reader.read("seeds/relayPeers",
                XPathConstants.NODESET);

        for (int index = 0; index < relayPeersNodes.getLength(); index++) {
            Node node = relayPeersNodes.item(index);
            rdvPeers.add(node.getTextContent());
        }
    }

    /**
     * Parse XML configuration file, part 'network'
     */
    private void parseNetwork(XPathReader reader) throws Exception {
    	String portStr = reader.read("network/port", XPathConstants.NUMBER).toString();
    	if (!portStr.equalsIgnoreCase("NaN"))
    		port = Integer.parseInt(portStr);

        String enabledStr = reader.read("network/Enabled", XPathConstants.STRING).toString();
        if (!enabledStr.isEmpty())
        	enabled = Boolean.parseBoolean(enabledStr);

        String incomingEnabledStr = reader.read("network/IncomingEnabled", XPathConstants.STRING).toString();
        if (!incomingEnabledStr.isEmpty())
        	incomingEnabled = Boolean.parseBoolean(incomingEnabledStr);

        String outgoingEnabledStr = reader.read("network/OutgoingEnabled",XPathConstants.STRING).toString();
        if (!outgoingEnabledStr.isEmpty())
        	outgoingEnabled = Boolean.parseBoolean(outgoingEnabledStr);

        String cachedOutputPipeStr = reader.read("network/cachedOutputPipe", XPathConstants.STRING).toString();
        if (!cachedOutputPipeStr.isEmpty())
        	cachedOutputPipe = Boolean.parseBoolean(cachedOutputPipeStr);
    }

    /**
     * Parse XML configuration file, part 'service'
     */
    private void parseService(XPathReader reader) throws Exception {
    	String versionStr = reader.read("service/version", XPathConstants.NUMBER).toString();
    	if (!versionStr.equalsIgnoreCase("NaN"))
    		version = Integer.parseInt(versionStr);

    	String creatorStr = reader.read("service/creator", XPathConstants.STRING).toString();
    	if (!creatorStr.isEmpty())
    		creator = creatorStr;

    	String urlStr = reader.read("service/URL", XPathConstants.STRING).toString();
    	if (!urlStr.isEmpty())
    		url = urlStr;

    	String inpipePipeIDStr = reader.read("service/inpipePipeID", XPathConstants.NUMBER).toString();
    	if (!inpipePipeIDStr.equalsIgnoreCase("NaN"))
        	inpipePipeID = Integer.parseInt(inpipePipeIDStr);

    	String moduleClassIDStr = reader.read("service/moduleClassID", XPathConstants.NUMBER).toString();
    	if (!moduleClassIDStr.equalsIgnoreCase("NaN"))
    		moduleClassID = Integer.parseInt(moduleClassIDStr);

    	String advExpirationTimeStr = reader.read("service/advExpirationTime", XPathConstants.NUMBER).toString();
    	if (!advExpirationTimeStr.equalsIgnoreCase("NaN"))
    		advExpirationTime = Integer.parseInt(advExpirationTimeStr);

    	String advLifeTimeStr = reader.read("service/advLifeTime", XPathConstants.NUMBER).toString();
    	if (!advLifeTimeStr.equalsIgnoreCase("NaN"))
    		advLifeTime = Integer.parseInt(advLifeTimeStr);
    }

    /**
     * Parse XML configuration file, part 'message'
     */
    private void parseMessage(XPathReader reader) throws Exception {
    	String pipeResolveTimeoutStr = reader.read("message/pipeResolveTimeout", XPathConstants.NUMBER).toString();
    	if (!pipeResolveTimeoutStr.equalsIgnoreCase("NaN"))
    		pipeResolveTimeout = Integer.parseInt(pipeResolveTimeoutStr);

    	String fetchAdvertisementTimeoutStr = reader.read("message/fetchAdvertisementTimeout", XPathConstants.NUMBER).toString();
    	if (!fetchAdvertisementTimeoutStr.equalsIgnoreCase("NaN"))
    		fetchAdvertisementTimeout = Integer.parseInt(fetchAdvertisementTimeoutStr);

    	String numberOfRetriesForDiscoveryStr = reader.read("message/numberOfRetriesForDiscovery", XPathConstants.NUMBER).toString();
    	if (!numberOfRetriesForDiscoveryStr.equalsIgnoreCase("NaN"))
    		numberOfRetriesForDiscovery = Integer.parseInt(numberOfRetriesForDiscoveryStr);

    	String gapBetweenTwoDiscoveryRequestsStr = reader.read("message/gapBetweenTwoDiscoveryRequests", XPathConstants.NUMBER).toString();
    	if (!gapBetweenTwoDiscoveryRequestsStr.equalsIgnoreCase("NaN"))
    		gapBetweenTwoDiscoveryRequests = Integer.parseInt(gapBetweenTwoDiscoveryRequestsStr);

    	String timeoutForAMessageStr = reader.read("message/timeoutForAMessage", XPathConstants.NUMBER).toString();
    	if (!timeoutForAMessageStr.equalsIgnoreCase("NaN"))
    		timeoutForAMessage = Integer.parseInt(timeoutForAMessageStr);

    	String rendezvousConnectionTimeoutStr = reader.read("message/rendezvousConnectionTimeout", XPathConstants.NUMBER).toString();
    	if (!rendezvousConnectionTimeoutStr.isEmpty())
    		rendezvousConnectionTimeout = Integer.parseInt(rendezvousConnectionTimeoutStr);

    	String numberOfMessageSenderThreadsStr = reader.read("message/numberOfMessageSenderThreads", XPathConstants.NUMBER).toString();
    	if (!numberOfMessageSenderThreadsStr.isEmpty())
    		numberOfMessageSenderThreads = Integer.parseInt(numberOfMessageSenderThreadsStr);
    }

    /**
     * Export the configuration into an XML node
     */
    public Node export() {
        Document doc;

        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance()
                                                            .newDocumentBuilder();
            doc = builder.newDocument();

            Element element = doc.createElement(getModule().getModuleName());
            doc.appendChild(element);

            exportLocalPeer(doc, element);
            exportSeeds(doc, element);
            exportNetwork(doc, element);
            exportService(doc, element);
            exportMessage(doc, element);
        } catch (Exception e) {
            logger.error(
                "There were errors exporting XML configuration file : " +
                e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }

        return doc;
    }

    /**
     * Export XML configuration file, part 'localPeer'
     */
    private void exportLocalPeer(Document doc, Element element)
        throws Exception {
        // Create the base 'localPeer' element
        Element base = doc.createElement("localPeer");
        element.appendChild(base);

        // Create the child nodes
        Element peerTypeElement = doc.createElement("peerType");

        switch (peerType) {
        case NetworkConfigurator.RELAY_NODE:
            peerTypeElement.setTextContent("Relay");

            break;

        case NetworkConfigurator.RDV_NODE:
            peerTypeElement.setTextContent("Rdv");

            break;

        case NetworkConfigurator.EDGE_NODE:default:
            peerTypeElement.setTextContent("Edge");
        }

        base.appendChild(peerTypeElement);

        Element autoRdvEnabledElement = doc.createElement("autoRdvEnabled");
        autoRdvEnabledElement.setTextContent(Boolean.toString(autoRdvEnable));
        base.appendChild(autoRdvEnabledElement);

        Element cacheFolderElement = doc.createElement("cacheFolder");
        cacheFolderElement.setTextContent(cacheFolder);
        base.appendChild(cacheFolderElement);

        Element cacheReUseElement = doc.createElement("cacheReUse");
        cacheReUseElement.setTextContent(cacheReUse);
        base.appendChild(cacheReUseElement);
    }

    /**
     * Export XML configuration file, part 'seeds'
     */
    private void exportSeeds(Document doc, Element element)
        throws Exception {
        // Create the base 'seeds' element
        Element base = doc.createElement("seeds");
        element.appendChild(base);

        // Create the rdvPeers nodes
        Element rdvPeersElement = doc.createElement("rdvPeers");
        base.appendChild(rdvPeersElement);

        for (String p : rdvPeers) {
            Element pElem = doc.createElement(p);
            rdvPeersElement.appendChild(pElem);
        }

        // Create the relayPeers nodes
        Element relayPeersElement = doc.createElement("relayPeers");
        base.appendChild(relayPeersElement);

        for (String p : relayPeers) {
            Element pElem = doc.createElement(p);
            relayPeersElement.appendChild(pElem);
        }
    }

    /**
     * Export XML configuration file, part 'network'
     */
    private void exportNetwork(Document doc, Element element)
        throws Exception {
        // Create the base 'network' element
        Element base = doc.createElement("network");
        element.appendChild(base);

        // Create the child nodes
        Element portElement = doc.createElement("port");
        portElement.setTextContent(new Integer(port).toString());
        base.appendChild(portElement);

        Element enabledElement = doc.createElement("enabled");
        enabledElement.setTextContent(Boolean.toString(enabled));
        base.appendChild(enabledElement);

        Element incomingEnabledElement = doc.createElement("IncomingEnabled");
        incomingEnabledElement.setTextContent(Boolean.toString(incomingEnabled));
        base.appendChild(incomingEnabledElement);

        Element outgoingEnabledElement = doc.createElement("OutgoingEnabled");
        outgoingEnabledElement.setTextContent(Boolean.toString(outgoingEnabled));
        base.appendChild(outgoingEnabledElement);

        Element cachedOutputPipeElement = doc.createElement("cachedOutputPipe");
        cachedOutputPipeElement.setTextContent(Boolean.toString(
                cachedOutputPipe));
        base.appendChild(cachedOutputPipeElement);
    }

    /**
     * Export XML configuration file, part 'service'
     */
    private void exportService(Document doc, Element element)
        throws Exception {
        // Create the base 'service' element
        Element base = doc.createElement("service");
        element.appendChild(base);

        // Create the child nodes
        Element versionElement = doc.createElement("version");
        versionElement.setTextContent(new Integer(version).toString());
        base.appendChild(versionElement);

        Element creatorElement = doc.createElement("creator");
        creatorElement.setTextContent(creator);
        base.appendChild(creatorElement);

        Element URLElement = doc.createElement("URL");
        URLElement.setTextContent(url);
        base.appendChild(URLElement);

        Element inpipePipeIDElement = doc.createElement("inpipePipeID");
        inpipePipeIDElement.setTextContent(new Integer(inpipePipeID).toString());
        base.appendChild(inpipePipeIDElement);

        Element moduleClassIDElement = doc.createElement("moduleClassID");
        moduleClassIDElement.setTextContent(new Integer(moduleClassID).toString());
        base.appendChild(moduleClassIDElement);

        Element advExpirationTimeElement = doc.createElement(
                "advExpirationTime");
        advExpirationTimeElement.setTextContent(new Integer(advExpirationTime).toString());
        base.appendChild(advExpirationTimeElement);

        Element advLifeTimeElement = doc.createElement("advLifeTime");
        advLifeTimeElement.setTextContent(new Integer(advLifeTime).toString());
        base.appendChild(advLifeTimeElement);
    }

    /**
     * Export XML configuration file, part 'message'
     */
    private void exportMessage(Document doc, Element element)
        throws Exception {
        // Create the base 'message' element
        Element base = doc.createElement("message");
        element.appendChild(base);

        // Create the child nodes
        Element pipeResolveTimeoutElement = doc.createElement(
                "pipeResolveTimeout");
        pipeResolveTimeoutElement.setTextContent(new Integer(pipeResolveTimeout).toString());
        base.appendChild(pipeResolveTimeoutElement);

        Element fetchAdvertisementTimeoutElement = doc.createElement(
                "fetchAdvertisementTimeout");
        fetchAdvertisementTimeoutElement.setTextContent(new Integer(
                fetchAdvertisementTimeout).toString());
        base.appendChild(fetchAdvertisementTimeoutElement);

        Element numberOfRetriesForDiscoveryElement = doc.createElement(
                "numberOfRetriesForDiscovery");
        numberOfRetriesForDiscoveryElement.setTextContent(new Integer(
                numberOfRetriesForDiscovery).toString());
        base.appendChild(numberOfRetriesForDiscoveryElement);

        Element gapBetweenTwoDiscoveryRequestsElement = doc.createElement(
                "gapBetweenTwoDiscoveryRequests");
        gapBetweenTwoDiscoveryRequestsElement.setTextContent(new Integer(
                gapBetweenTwoDiscoveryRequests).toString());
        base.appendChild(gapBetweenTwoDiscoveryRequestsElement);

        Element timeoutForAMessageElement = doc.createElement(
                "timeoutForAMessage");
        timeoutForAMessageElement.setTextContent(new Integer(timeoutForAMessage).toString());
        base.appendChild(timeoutForAMessageElement);

        Element rendezvousConnectionTimeoutElement = doc.createElement(
                "rendezvousConnectionTimeout");
        rendezvousConnectionTimeoutElement.setTextContent(new Integer(
                rendezvousConnectionTimeout).toString());
        base.appendChild(rendezvousConnectionTimeoutElement);

        Element numberOfMessageSenderThreadsElement = doc.createElement(
                "numberOfMessageSenderThreads");
        numberOfMessageSenderThreadsElement.setTextContent(new Integer(
                numberOfMessageSenderThreads).toString());
        base.appendChild(numberOfMessageSenderThreadsElement);
    }
}
