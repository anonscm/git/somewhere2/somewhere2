/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.jxta;

import fr.lri.iasi.somewhere.api.communication.configuration.CommunicationConfiguration;

import net.jxta.discovery.DiscoveryService;

import net.jxta.document.Advertisement;
import net.jxta.document.AdvertisementFactory;

import net.jxta.pipe.PipeID;
import net.jxta.pipe.PipeService;

import net.jxta.protocol.ModuleSpecAdvertisement;
import net.jxta.protocol.PipeAdvertisement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import java.util.Enumeration;


/**
 * Responsible for create and fetch (locally or remotely) pipe advertisements.
 * 
 * @author Andre Fonseca
 *
 */
public class PipeAdvertisementManager {
    static Logger logger = LoggerFactory.getLogger(PipeAdvertisementManager.class);

    /* --- Properties --- */
    /** Module Manager */
    protected final JXTAManager moduleManager;

    public PipeAdvertisementManager(JXTAManager moduleManager) {
        if (moduleManager == null) {
            throw new IllegalArgumentException("moduleManager");
        }

        this.moduleManager = moduleManager;
    }

    /* --- Accessors --- */
    public JXTAManager getModuleManager() {
        return moduleManager;
    }

    /* --- Methods --- */
    /**
     * Creates a input pipe advertisement from the given pipe id.
     * @param pipeID
     * @return
     */
    public PipeAdvertisement createInputPipeAdvertisement(PipeID pipeID) {
    	CommunicationConfiguration communicationConfig = moduleManager.getCommunicationManager().getConfiguration();

        // Create Pipe advertisement and add it to access the service
        PipeAdvertisement pipeAdvertisement = (PipeAdvertisement) AdvertisementFactory.newAdvertisement(PipeAdvertisement.getAdvertisementType());
        pipeAdvertisement.setPipeID(pipeID);

        // The symbolic name of the pipe is built from
        // the PipeName name of Peer;  each Peer must therefore have a unique name.
        pipeAdvertisement.setName("inpipe." + communicationConfig.getPeerName());
        // Set the type of the pipe to be unicast
        pipeAdvertisement.setType(PipeService.UnicastType);

        return pipeAdvertisement;
    }

    /**
     * Fetch pipe advertisement
     * @param peerid
     * @param attribute
     * @param value
     * @throws AdvertisementNotFoundException
     */
    public void fetchPipeAdvertisement(JXTAPeer peer, String attribute)
        throws AdvertisementNotFoundException {
        if (peer == null) {
            throw new IllegalArgumentException("peer");
        }

        DiscoveryService discoveryService = moduleManager.getPeerGroup()
                                                         .getLocalPeer()
                                                         .getSomewherePeerGroup()
                                                         .getDiscoveryService();
        PipeAdvertisement pipeAdvertisement = null;

        // Fetch local advertisements
        try {
            Enumeration<Advertisement> enumAdv = discoveryService.getLocalAdvertisements(DiscoveryService.ADV,
                    attribute, "inpipe." + peer.getName());
            pipeAdvertisement = verifyPipeAdvertisements(enumAdv,
                    "inpipe." + peer.getName());
            peer.setPipeInputAdvertisement(pipeAdvertisement);
        } catch (IOException e) {
        	// Fails silently...
        }

        // If there are no local advertisements that correspond to the target PiperAdvertisement, fetch it remotely
        if (pipeAdvertisement == null) {
            final PipeDiscoveryListener listener = new PipeDiscoveryListener(this,
                    peer.getName());

            discoveryService.getRemoteAdvertisements(null,
                DiscoveryService.ADV, attribute, "service." + peer.getName(), 1, listener);

            try {
                Thread.sleep(moduleManager.getConfiguration()
                                          .getFetchAdvertisementTimeout() * 1000);
            } catch (InterruptedException ex) {
            	Thread.interrupted();
            } finally {
                discoveryService.removeDiscoveryListener(listener);
            }

            if (listener.hasAdvertisementFound()) {
                peer.setPipeInputAdvertisement(listener.getAdvertisement());
            } else {
                throw new AdvertisementNotFoundException();
            }
        }
    }

    /**
     * Retrieve the corresponding pipe advertisement from a
     * @param enumAdv
     * @param value
     * @return
     */
    PipeAdvertisement verifyPipeAdvertisements(
        Enumeration<Advertisement> enumAdv, String value) {
        while (enumAdv.hasMoreElements()) {
            Advertisement advertisement = enumAdv.nextElement();

            //process only Module Spec Advertisement type of advertisements
            if (advertisement.getAdvType().equals("jxta:MSA")) {
                ModuleSpecAdvertisement moduleSpecAdvertisement = (ModuleSpecAdvertisement) advertisement;
                PipeAdvertisement pipeAdvertisement = moduleSpecAdvertisement.getPipeAdvertisement();

                //if the name of the advertisement is same as that of given service name,
                //then the advertisement we are looking is right one
                if (pipeAdvertisement.getName().equalsIgnoreCase(value)) {
                    // we can find the pipe to connect to the service in the advertisement.
                    return pipeAdvertisement;
                }
            }
        }

        return null;
    }
}
