/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file
 * is part of Somewhere2 Copyright (C) 2012 - IASI - Philippe Chatalic, Andre
 * Fonseca This file must be used under the terms of the CeCILL. This source
 * file is licensed as described in the file LICENSE, which you should have
 * received as part of this distribution. The terms are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

import fr.lri.iasi.libs.formallogic.Restriction;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.impl.ClauseBuilder;
import fr.lri.iasi.libs.plogic.impl.ConjunctionOfClausesImpl;
import fr.lri.iasi.libs.plogic.restrictions.SubsetRestriction;
import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.FinalAnswer;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.MessageType;
import fr.lri.iasi.somewhere.api.communication.model.MessageTypeSingletonFactory;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageProcessor;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicAnswerMessageType;
import java.util.concurrent.ConcurrentMap;

/**
 * Processor responsible for the "Query" message type. Launches the consequence
 * finding strategy, return local answers (if any), propagates the queries to
 * other peers (if needed) and wait for finish messages for each query
 * propagated.
 *
 * @author Andre Fonseca
 */
public class PLogicQueryProcessor extends AbstractMessageProcessor {

    static Logger logger = LoggerFactory.getLogger(PLogicQueryProcessor.class);
    /* --- Properties --- */
    /**
     * Module Manager to be used
     */
    private final PLogicManager plogicManager;
    /**
     * The query that triggered the current message session
     */
    private Clause startingQuery;
    /**
     * The restrictions propagated along with the current query
     */
    private final Set<Restriction<Clause>> propagationRestrictions;
    /**
     * Finish messages counter. In order to finish, a query processor must
     * receive the same number of finishes than the number of its sent queries.
     */
    private CountDownLatch finishCountDown;
    /**
     * The starting time of the current propagation (considered only if it was
     * triggered by the user)
     */
    private long propagationStartTime;
    /**
     * The related answer processor
     */
    private PLogicAnswerProcessor answerProcessor;
    /**
     * Answers to wait counter
     */
    protected AtomicInteger answersToWait = new AtomicInteger();

    public PLogicQueryProcessor(MessageSession session) {
        super(session);

        App app = session.getCommunicationManager().getApp();
        this.plogicManager = PLogicManager.getModuleInstance(app);
        this.propagationRestrictions = Sets.newHashSet();
    }

    /* --- Accessors --- */
    Clause getStartingQuery() {
        return startingQuery;
    }

    Set<Restriction<Clause>> getPropagationRestrictions() {
        return propagationRestrictions;
    }

    CountDownLatch getFinishCountDown() {
        return finishCountDown;
    }

    AtomicInteger getAnswersToWait() {
        return answersToWait;
    }

    long getPropagationStartTime() {
        return propagationStartTime;
    }

    /* --- Methods --- */
    /**
     * {@inheritDoc}
     */
    @Override
    public void process(Message message) {
        // Initialize the propagation start time if the user sent the query
        if (this.session.getCreatorPeerName().equals("User")) {
            this.propagationStartTime = System.nanoTime();
        }

        // Creating useful variables
        final PropositionalInstanceWithMapping instance = this.plogicManager.getPropositionalInstance();
        final String localPeerName = this.plogicManager.getCommunicationManager().getLocalPeer().getName();
        final Collection<Restriction<Clause>> instanceRestrictions = instance.getProductionField().getRestrictions().values();
        final MessageType answerType = MessageTypeSingletonFactory.create(PLogicAnswerMessageType.class);
        this.answerProcessor = (PLogicAnswerProcessor) this.session.getProcessor(answerType);

        // Reading message
        final Map<String, Set<Clause>> history = PLogicManager.HISTORY.getContent(message);
        final Clause query = PLogicManager.QUERY.getContent(message);
        final Collection<Restriction<Clause>> propagationRestrictions = PLogicManager.PRODUCTION_FIELDS.getContent(message);
        final Collection<Restriction<Clause>> restrictions = Sets.newHashSet(instanceRestrictions);
        restrictions.addAll(propagationRestrictions);
        long ttl = CommunicationManager.TTL.getContent(message);
        long processStartTime = System.currentTimeMillis();

        // Saving initial propagation information (to be used by other processors)
        this.propagationRestrictions.addAll(propagationRestrictions);
        this.startingQuery = query;

        Set<Clause> peerHistory = history.get(localPeerName);
        if (peerHistory == null) {
            peerHistory = new LinkedHashSet<Clause>();
        }

        // Casting the query to local, if necessary
        final Clause localQuery = instance.localViewOf(query);

        logger.trace("Arrived query {} with history {}", localQuery, history);

        // Adding history to query only if it has any literal who belongs to the local theory language
        final ConjunctionOfClauses clausesToAdd = new ConjunctionOfClausesImpl();
        clausesToAdd.add(localQuery);

        final ConjunctionOfClauses clausesAlreadyAdded = new ConjunctionOfClausesImpl();
        for (Clause clause : peerHistory) {
            Clause localHistoryQuery = instance.localViewOf(clause);
            clausesAlreadyAdded.add(localHistoryQuery);
        }

        logger.trace("Queries to be added on theory: to add => {}, already added => {}", clausesToAdd, clausesAlreadyAdded);

        // Produce consequents removing the initial theory from the results
        ConjunctionOfClauses result = null;
        try {
            result = processConsequenceFinding(instance, clausesToAdd, clausesAlreadyAdded, ttl);

            logger.trace("Local consequence finding result: {}", result);

            if (result == null) {
                this.plogicManager.returnFinishAnswer(this.session, query);
                return;
            }
        } catch (TimeoutException e) {
            try {
                logger.trace("TimeOut during consequence finding result: {}", 
                                           " "+clausesToAdd+ " with previously added " + clausesAlreadyAdded);
                this.plogicManager.returnTimeoutAnswer(this.session);
            } catch (CommunicationException ce) {
                logger.error("There were problems sending the timeout message: {}", ce.getMessage());
                return;
            }
        } catch (CommunicationException e) {
            logger.error("There were problems sending the finish message: {}", e.getMessage());
            return;
        }

        // Add query to history
        final Clause distantQuery = instance.distantViewOf(localPeerName, query);
        peerHistory.add(distantQuery);
        history.put(localPeerName, peerHistory);

        // Verify the if there are new queries to be sent.
        final Multimap<String, Clause> newQueriesToBeSentByPeer = HashMultimap.create();
        ConcurrentMap<Literal, Collection<String>> instanceSharedLiteralIndex = instance.getSharedLiteralIndex();

        // Process clauses, excluding those excluded by production field, 
        // determining recursive queries that have to be asked and filtering terminal answers, 

        Iterator<Clause> it = result.iterator();
        while (it.hasNext()) {
            Clause clause = it.next();
            Clause pureLocalSubClause = instance.neitherDistantNorSharedLitteralSubClause(clause);

            boolean discardClause = false;
            boolean containsDistantLiterals = false;
            boolean containsSharedLiteral = false;

            // Discard clauses when the pure local part does not matches the production field
            for (Restriction<Clause> restriction : restrictions) {
                if (restriction.discard(pureLocalSubClause)) {
                    discardClause = true;
                    logger.trace("Non shared local part of {} is : {} ", clause, pureLocalSubClause);
                    logger.trace("Clause {} discarded because non shared local part does not match the production field: {}", clause, restriction);
                    break;
                }
            }
            if (!discardClause) {
                for (Literal literal : clause) {
                    // Distant literals are literals contained in the local mappings, that belongs to another theory
                    if (literal instanceof DistantLiteralImpl) {
                        containsDistantLiterals = true;
                        DistantLiteralImpl distantLiteral = (DistantLiteralImpl) literal;
                        ClauseBuilder builder = new ClauseBuilder();
                        builder.addLiteral(distantLiteral);
                        Clause unitaryClause = builder.create();

                        String peerName = distantLiteral.getPeerName();
                        newQueriesToBeSentByPeer.put(peerName, unitaryClause);

                        this.answerProcessor.getNonTerminalAnswers(unitaryClause).add(clause);
                        logger.trace("Clause {} has distant literals", clause);
                    } else {
                        // Shared literals are local literals contained in distant theories
                        Collection<String> peerNames = instanceSharedLiteralIndex.get(literal.opposite());
                        if (peerNames != null) {
                            containsSharedLiteral = true;
                            DistantLiteralImpl sharedLiteral = new DistantLiteralImpl(localPeerName, literal);
                            ClauseBuilder builder = new ClauseBuilder();
                            builder.addLiteral(sharedLiteral);
                            Clause unitaryClause = builder.create();

                            for (String peerName : peerNames) {
                                newQueriesToBeSentByPeer.put(peerName, unitaryClause);
                            }

                            this.answerProcessor.getNonTerminalAnswers(unitaryClause).add(clause);
                        }
                    }
                }
                if (!containsDistantLiterals && containsSharedLiteral) {
                    // Candidate for final Answer... but shared variable might not be in the production field
                    for (Restriction<Clause> restriction : restrictions) {
                        if (restriction.discard(clause)) {
                            logger.trace("Clause {} has shared literals, no distant, but does not match production field", clause, restriction);
                            discardClause = true;
                            break;
                        }
                    }
                }
            }
            if (discardClause || containsDistantLiterals) {
                it.remove();    // Remove non Terminal Answers
            } else {
                logger.trace("Clause {} is a Terminal Answer", clause);
            }

        }


        // Add terminal results to terminal clauses. NOTE: Don't need to synchronize it here... no concurrent access.
        if (!result.isEmpty()) {
            Set<Clause> terminalAnswers = this.answerProcessor.getTerminalAnswers();

            for (Clause clause : result) {
                terminalAnswers.add(clause);
                // logger.trace("Clause {} added to terminal clauses theory.", clause);
            }
        }

        // Send the produced answers to neighbor peer

        try {
            // Send the produced answers to user, if needed
            if (this.session.getCreatorPeerName().equals("User")) {
                this.produceFinalAnswers(result, this.answerProcessor);
            } else {
                this.plogicManager.returnProducedAnswers(this.session, message.getOrigin().getName(), query, history, result);
            }
        } catch (CommunicationException e) {
            logger.error("There were problems sending the answer message: {}", e.getMessage());
        }



        ttl = Math.max(0, ttl - (System.currentTimeMillis() - processStartTime));
        if (ttl > 0) {
            // Create countdownlatch in order to release the final finish message
            this.finishCountDown = new CountDownLatch(newQueriesToBeSentByPeer.values().size());
            logger.trace("Queries to be sent: {}", newQueriesToBeSentByPeer);

            // Propagate queries
            this.propagateQueries(newQueriesToBeSentByPeer, history, propagationRestrictions, ttl);

            // Wait all the finish messages to arrive, otherwise, send timeout
            try {
                boolean finished = this.finishCountDown.await(ttl, TimeUnit.MILLISECONDS);
                if (finished) {
                    this.plogicManager.returnFinishAnswer(this.session, query);
                } else {
                    this.plogicManager.returnTimeoutAnswer(this.session);
                }
            } catch (InterruptedException e) {
                Thread.interrupted();
            } catch (CommunicationException e) {
                logger.error("There were problems sending the finish message: {}", e.getMessage());
            }
        } else {
            try {
                this.plogicManager.returnTimeoutAnswer(this.session);
            } catch (CommunicationException e) {
                logger.error("There were problems sending the finish message: {}", e.getMessage());
            }
        }

    }

/**
 * Call the selected consequence finding strategy taking on account the ttl
 * parameter.
 *
 * @param instance is the current loaded instance.
 * @param clausesToAdd are the input clauses of the consequence finding
 * operation.
 * @param clausesAlreadyAdded are clauses already added processed on a previous
 * query
 * @param ttl is the current time-to-live
 * @return a collection of clauses resulted from the consequence finding
 * operation.
 * @throws CommunicationException if there is a timeout message and it has found
 * some problem to be sent.
 */
private ConjunctionOfClauses processConsequenceFinding(final PropositionalInstanceWithMapping instance,
            final ConjunctionOfClauses clausesToAdd, final ConjunctionOfClauses clausesAlreadyAdded, long ttl)
            throws TimeoutException {

        ConjunctionOfClauses result = null;
        ExecutorService executor = Executors.newSingleThreadExecutor();
        final Future<ConjunctionOfClauses> future = executor.submit(new Callable<ConjunctionOfClauses>() {
            public ConjunctionOfClauses call() {
                return instance.getTheory().produceConsequentsFrom(clausesToAdd, clausesAlreadyAdded, true);
            }
        });

        // Get the results or send a timeout message if the ttl expires
        try {
            result = future.get(ttl, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            Thread.interrupted();
        } catch (ExecutionException e) {
            logger.error("There were problems on the consequence finding process: {}", e.getMessage());
        }

        return result;
    }

    /**
     * Avoid loops on the query propagation.
     *
     * @param query is the query clause
     * @param the peer where this query is being directed
     * @param history of this query
     * @return boolean indicating if a loop was detected or not
     */
    private boolean avoidLoops(Clause query, String destination, Map<String, Set<Clause>> history) {
        Set<Clause> peerHistory = history.get(destination);
        // Remove peers that are already part of the history of this query
        if (peerHistory != null && peerHistory.contains(query)) {
            logger.trace("Loop Detected : Query {}, peer history {}", query, peerHistory);
            return true;
        }


        return false;
    }

    /**
     * Propagate queries to relevant neighbors.
     *
     * @param queries is the list of clauses to be sent
     * @param history of the starting query query
     * @param restrictions used on the current propagation
     * @return boolean indicating if new queries were sent or not
     */
    private void propagateQueries(Multimap<String, Clause> queriesByPeer,
            Map<String, Set<Clause>> history,
            Collection<Restriction<Clause>> propagationRestrictions,
            long ttl) {

        for (Entry<String, Clause> entry : queriesByPeer.entries()) {
            String destination = entry.getKey();
            Clause query = entry.getValue();

            try {
                if (avoidLoops(query, destination, history)) {
                    this.finishCountDown.countDown();
                    continue;
                }

                this.plogicManager.propagateQuery(session, destination, query, history, propagationRestrictions, ttl);
                logger.trace("Propagated query {} to peer {}", query, destination);
            } catch (CommunicationException e) {
                // If an error occurred sending the query, we don't need to wait it answers...
                logger.error("There was a problem propagating query " + query + " to peer " + destination + " : {}", e.getMessage());
                this.finishCountDown.countDown();
            }
        }
    }

    /**
     * Add final answer annotations on the clause.
     *
     * @param clause
     * @param subsumedClauses
     */
    private void produceFinalAnswers(Collection<Clause> result, PLogicAnswerProcessor answerProcessor) {
        for (Clause clause : result) {
            FinalAnswer<Clause> finalAnswer = new FinalAnswer<Clause>();
            finalAnswer.setContent(clause);
            finalAnswer.setNumber(answerProcessor.getAnswerCount().incrementAndGet());
            finalAnswer.setResponseTime(this.propagationStartTime);

            answerProcessor.getFinalAnswerMap().put(clause, answerProcessor.getAnswerCount().get());

            try {
                this.plogicManager.returnAnswersForUser(this.session, finalAnswer, this.startingQuery, this.answerProcessor.getTerminalAnswers().size());
                logger.trace("Sent final answers to user: {}", clause);
            } catch (CommunicationException e) {
                logger.error("There were problems sending the final answer to the user: {}", e.getMessage());
            }
        }
    }
}
