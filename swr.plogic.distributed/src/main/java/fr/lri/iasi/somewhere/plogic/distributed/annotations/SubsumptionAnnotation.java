/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.annotations;

import java.util.Set;

import fr.lri.iasi.somewhere.api.communication.model.FinalAnswerAnnotation;

/**
 * Describes a final answer annotation indicating subsumptions made by an answer over the answers already sent to the user.
 * 
 * @author Andre Fonseca
 *
 */
public class SubsumptionAnnotation implements FinalAnswerAnnotation<Set<Integer>> {
	private static final long serialVersionUID = -2420521196345857107L;
	
	/* --- Properties --- */
	/** The set of subsumed answers */
	private final Set<Integer> subsumedAnswers;
	
	public SubsumptionAnnotation(Set<Integer> subsumedAnswers) {
		if (subsumedAnswers == null) {
			throw new IllegalArgumentException("subsumedAnswers");
		}
		
		this.subsumedAnswers = subsumedAnswers;
	}
	
	public SubsumptionAnnotation(SubsumptionAnnotation annotation) {
		this(annotation.getContent());
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public String getName() {
		return "subsumption";
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public  Set<Integer> getContent() {
		return subsumedAnswers;
	}
	
	@Override
	public String toString() {
		return subsumedAnswers.toString();
	}

}
