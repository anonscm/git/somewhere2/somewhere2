/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.commands;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.somewhere.plogic.distributed.PLogicManager;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

/**
 * Command that checks if the last "loadPropositionalInstance" command has finished.
 * @author Andre Fonseca
 *
 */
public class CheckLoadedInstanceCommand implements Command {
	static Logger logger = LoggerFactory.getLogger(CheckLoadedInstanceCommand.class);
	
	/* --- Properties --- */
    /** Manager of the module */
    protected final PLogicManager manager;
    
    public CheckLoadedInstanceCommand(PLogicManager manager) {
        if (manager == null) {
            throw new IllegalArgumentException("manager");
        }

        this.manager = manager;
    }

    /* --- Accessors --- */
    /**
     * {@inheritDoc}
     */
	@Override
	public String getName() {
		return "checkLoadedInstance";
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public String getDescription() {
		return "Check if there is a instance fully loaded on Somewhere";
	}
	
	/* --- Methods --- */

	/**
     * {@inheritDoc}
     */
	@Override
	public ReturnStatus execute(List<Object> parameters) {
		ReturnStatus res = null;
		
        try {
			LoadPropositionalInstanceCommand command = (LoadPropositionalInstanceCommand) this.manager.getApp().getUI().getCommand("loadPropositionalInstance");

			res = new GenericReturnStatus(Boolean.toString(command.hasFinishedLoading()), true, null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error checking " +
                    "if there is fully loaded instance...", false, null);
        }

        return res;
	}

}
