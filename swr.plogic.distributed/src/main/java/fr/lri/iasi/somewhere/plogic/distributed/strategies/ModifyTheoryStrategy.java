/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.strategies;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.ui.CommandListener;

/**
 * Describes a theory modification strategy.
 * 
 * @author Andre Fonseca
 *
 */
public interface ModifyTheoryStrategy {
	
	/**
	 * Modify a theory by inserting/removing a clause.
	 * 
	 * @param clause : the clause to be inserted/removed.
	 * @param isMapping : indicates if the arg clause is a mapping.
	 * @param ttl : time to live of the modify operation
	 * @throws CommunicationException
	 */
	void modify(Clause clause, boolean isMapping, long ttl) throws CommunicationException;
	
	/**
	 * The listener that indicates the end of the modify operation.
	 * @param listener
	 */
	void setListener(CommandListener listener);
	
}
