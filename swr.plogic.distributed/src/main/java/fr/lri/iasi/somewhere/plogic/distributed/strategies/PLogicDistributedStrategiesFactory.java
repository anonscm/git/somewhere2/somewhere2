/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.strategies;

/**
 * Strategy factory. Responsible for the instantiation of the main strategies used
 * by the distributed processors.
 * 
 * @author Andre Fonseca
 *
 */
public final class PLogicDistributedStrategiesFactory {

	/* --- Properties --- */  
	/** The singleton object */
	private static PLogicDistributedStrategiesFactory instance = new PLogicDistributedStrategiesFactory();
	
	/** The answer combination strategy to be used by all answer processors */
	private AnswerCombinationStrategy answerCombinationStrategy;
	
	/** The modify theory (addition) strategy to be used */
	private ModifyTheoryStrategy addFormulaToInstanceStrategy;
	
	/** The modify theory (removal) strategy to be used */
	private ModifyTheoryStrategy removeFormulaFromInstanceStrategy;
	
	public static PLogicDistributedStrategiesFactory getInstance() {
		return instance;
	}
	
	/* --- Accessors --- */

	public AnswerCombinationStrategy getAnswerCombinationStrategy() {
		return answerCombinationStrategy;
	}
	
	public ModifyTheoryStrategy getAddFormulaToInstanceStrategy() {
		return addFormulaToInstanceStrategy;
	}
	
	public ModifyTheoryStrategy getRemoveFormulaFromInstanceStrategy() {
		return removeFormulaFromInstanceStrategy;
	}
	
	/* --- Mutators --- */
	public void setAddFormulaToInstanceStrategy(ModifyTheoryStrategy addFormulaToInstanceStrategy) {
		this.addFormulaToInstanceStrategy = addFormulaToInstanceStrategy;
	}
	
	public void setRemoveFormulaFromInstanceStrategy(ModifyTheoryStrategy removeFormulaFromInstanceStrategy) {
		this.removeFormulaFromInstanceStrategy = removeFormulaFromInstanceStrategy;
	}
	
	public void setAnswerCombinationStrategy(AnswerCombinationStrategy answerCombinationStrategy) {
		this.answerCombinationStrategy = answerCombinationStrategy;
	}

}
