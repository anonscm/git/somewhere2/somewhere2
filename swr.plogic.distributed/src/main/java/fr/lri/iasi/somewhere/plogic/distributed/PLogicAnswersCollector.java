/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file
 * is part of Somewhere2 Copyright (C) 2012 - IASI - Philippe Chatalic, Andre
 * Fonseca This file must be used under the terms of the CeCILL. This source
 * file is licensed as described in the file LICENSE, which you should have
 * received as part of this distribution. The terms are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageCollector;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicTimeoutMessageType;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicAnswerMessageType;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicFinishMessageType;
import fr.lri.iasi.somewhere.ui.CommandListener;

/**
 * Message listener used to output answers and its informations to the user
 * interface
 *
 * @author Andre Fonseca
 *
 */
public class PLogicAnswersCollector extends AbstractMessageCollector {

    static Logger logger = LoggerFactory.getLogger(PLogicAnswersCollector.class);
    /* --- Properties --- */
    /**
     * Command listener used to inform that all messages have been sent
     */
    private final CommandListener listener;
    /**
     * Answer messages counter
     */
    private AtomicInteger answersReceived = new AtomicInteger();
    /**
     * Final answer size
     */
    private AtomicInteger finalAnswerSize = new AtomicInteger();

    public PLogicAnswersCollector(PLogicManager plogicManager,
            CommandListener listener) {
        super(plogicManager.getCommunicationManager());

        // Preparing the message receiver    
        addAcceptedTypes(PLogicAnswerMessageType.class);
        this.listener = listener;
    }

    /* --- Accessors --- */
    public int getReceivedAnswersNum() {
        return answersReceived.get();
    }

    public int getFinalAnswerSize() {
        return finalAnswerSize.get();
    }

    /* --- Methods --- */
    /**
     * {@inheritDoc}
     */
    @Override
    public void messageHasBeenSent(Message message) {

        if (!message.getDestination().getName().equals("User")) {
            return;
        }

        if (acceptedTypes.contains(message.getType())) {
            ui.print(message.getType().print(message));

            finalAnswerSize.set(PLogicManager.FINAL_ANSWER_SIZE.getContent(message));
            answersReceived.incrementAndGet();
        } else if (message.getType() instanceof PLogicTimeoutMessageType) {
            ui.print(message.getType().print(message));
            listener.executionCompleted();
        } else if (message.getType() instanceof PLogicFinishMessageType) {
            listener.executionCompleted();
        }
    }
}
