/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.types;

import java.util.Map.Entry;

import fr.lri.iasi.libs.formallogic.Annotation;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.somewhere.api.communication.model.FinalAnswer;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.FinalAnswerAnnotation;
import fr.lri.iasi.somewhere.api.communication.model.MessageProcessor;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageType;
import fr.lri.iasi.somewhere.plogic.distributed.PLogicManager;
import fr.lri.iasi.somewhere.plogic.distributed.PLogicAnswerProcessor;


/**
 * The answer message type of the plogic.distributed module.
 * 
 * @author Andre Fonseca
 *
 */
public class PLogicAnswerMessageType extends AbstractMessageType {
    private static final long serialVersionUID = 22246994948969864L;  

    /**
     * {@inheritDoc}
     */
    @Override
	public String getName() {
		return "answer";
	}

    /**
     * {@inheritDoc}
     */
    @Override
	public boolean isReturningType() {
		return true;
	}

    /**
     * {@inheritDoc}
     */
    @Override
	public MessageProcessor createProcessor(MessageSession session) {
		return new PLogicAnswerProcessor(session);
	}

    /**
     * {@inheritDoc}
     */
    @Override
	public boolean needsNewSessionSend() {
		return false;
	}
	
    /**
     * {@inheritDoc}
     */
    @Override
	public boolean needsNewSessionReceive() {
		return false;
	}

    /**
     * {@inheritDoc}
     */
    @Override
	public boolean needsToMaintainSessionSend(MessageSession session) {
		return true;
	}
	
    /**
     * {@inheritDoc}
     */
    @Override
	public boolean needsToMaintainSessionReceive(MessageSession session) {
		return true;
	}

    /**
     * {@inheritDoc}
     */
    @Override
	public String print(Message message) {
		StringBuilder res = new StringBuilder();
		
		FinalAnswer<Clause> finalAnswer = PLogicManager.FINAL_ANSWERS.getContent(message);
		Clause clause = finalAnswer.getContent();
			
		res.append("# ");
		res.append(finalAnswer.getResponseTime());
		res.append("s\t");
		res.append(finalAnswer.getNumber());
		res.append("\t");
			
		res.append(clause.toString());
		
		for (Entry<String, Annotation<?, ?>> entry : clause.getAnnotations().entrySet()) {		
			res.append(" ");
			res.append(entry.getKey());
			res.append("->");
			res.append(entry.getValue());
		}
		
		FinalAnswerAnnotation<?> subsumptionAnnotation = finalAnswer.getAnnotation("subsumption");
		if (subsumptionAnnotation != null) {		
			res.append("\t\t(SUBSUMES) -> ");
			res.append(subsumptionAnnotation.getContent());
		}
		
		res.append("\n");

        return res.toString();
	}
}
