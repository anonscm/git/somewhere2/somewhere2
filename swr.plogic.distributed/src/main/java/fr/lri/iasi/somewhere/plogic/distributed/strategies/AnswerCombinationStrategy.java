/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.strategies;

import java.util.Collection;
import java.util.Map;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.plogic.distributed.PropositionalInstanceWithMapping;

/**
 * Describes an answer combination strategy.
 * 
 * @author Andre Fonseca
 *
 */
public interface AnswerCombinationStrategy {
	
	/**
	 * Performs the combination of an answer based on the query,
	 * the non terminal mapping where the answer must be placed and
	 * the already received answers by this peer.
	 * 
	 * @param instance : The instance that contains the non-terminal clauses.
	 * @param query : The sent query
	 * @param answer : The current answer
	 * @param nonTerminal : The mapping where the answer needs to be placed.
	 * @param answersByQuery : The answers already received for this query
	 * @param message : the original message for collecting optional parameters
	 * @return a collection of clauses representing the combination performed between the arg elements.
	 */
	Collection<Clause> combine(PropositionalInstanceWithMapping instance,
			Clause query,
			Clause answer, 
			Clause nonTerminal, 
			Map<Clause, Collection<Clause>> answersByQuery,
			Message message);

}
