/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;

import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageProcessor;

/**
 * Processor responsible for the "AddSharedLiteral" message type. After
 * this message is received by the local peer, update the sharedLiteralIndex.
 * 
 * @author Andre Fonseca
 * 
 */
public class PLogicAddSharedLiteralProcessor extends AbstractMessageProcessor {
	static Logger logger = LoggerFactory.getLogger(PLogicAddSharedLiteralProcessor.class);
	
	/* --- Properties --- */
	/** Module Manager to be used */
    private final PLogicManager plogicManager;
	
    public PLogicAddSharedLiteralProcessor(MessageSession session) {
        super(session);
        
        App app = session.getCommunicationManager().getApp();
        this.plogicManager = PLogicManager.getModuleInstance(app);
    }

    /* --- Methods --- */
    /**
     * {@inheritDoc}
     */
	@Override
	public void process(Message message) {
		
		// Adding peer to neighbors list, if it is not there already.
		String peerName = message.getOrigin().getName();
		plogicManager.getCommunicationManager().getCommunicationModule().getPeerGroup().retrievePeer(peerName);
		
		Set<Literal> sharedLiterals = PLogicManager.SHARED_LITERALS.getContent(message);
		ConcurrentMap<Literal, Collection<String>> sharedLiteralIndex = plogicManager.getPropositionalInstance().getSharedLiteralIndex();
		
		for (Literal literal : sharedLiterals) {
			Literal localLiteral = ((DistantLiteralImpl) literal).toLocal();
			Collection<String> peers = null;
			Collection<String> oldPeers = null;
			
			do {
				oldPeers = sharedLiteralIndex.get(localLiteral);
				peers = Sets.newLinkedHashSet();
				
				if (oldPeers == null) {
					peers.add(peerName);
					sharedLiteralIndex.putIfAbsent(localLiteral, peers);
					break;
				} else {
					peers.add(peerName);
					peers.addAll(oldPeers);
				}
			} while (!sharedLiteralIndex.replace(localLiteral, oldPeers, peers));
		}
	}

}
