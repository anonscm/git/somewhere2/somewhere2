/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.commands;

import java.util.concurrent.atomic.AtomicBoolean;

import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.CommandListener;

/**
 * Describes a blocking UI command (blocks the UI while it does receive the necessary
 * messages from the neighbor peers).
 * 
 * @author Andre Fonseca
 *
 */
public abstract class AbstractBlockingUICommand implements Command {
	
	/*--- Properties ---*/
	/** Indicates if the execution of this command is completed */
    protected AtomicBoolean executionCompleted = new AtomicBoolean();
    
    /** Indicates the timestamp when the command was called */
    protected long initialTime = 0;
    
    /** The command listener object */
    protected final CommandListener commandListener;
    
    protected AbstractBlockingUICommand() {
    	this.commandListener = new BlockingUIListener();
    	this.executionCompleted.set(false);
    }
	
    /*--- Methods ---*/
	/**
     * Blocks the UI until the execution is completed
     * @param initTime
     * @return the command execution time
     * @throws InterruptedException
     */
    public long waitExecutionToComplete(long initTime) 
		throws InterruptedException {
		
		while (!this.executionCompleted.get())
    		Thread.sleep(10);
		
		return (System.nanoTime() - initTime);
    }
    
    /**
     * The command listener used to indicate the end of the command execution.
     * 
     * @author Andre Fonseca
     */
    protected class BlockingUIListener implements CommandListener {

		@Override
		public void executionCompleted() {
			executionCompleted.compareAndSet(false, true);
		}
		
		@Override
		public long getCommandStartTime() {
			return initialTime;
		}

		@Override
		public boolean isExecutionCompleted() {
			return executionCompleted.get();
		}
		
	}

}
