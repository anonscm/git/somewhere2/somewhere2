/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.configuration;

import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.XPathReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;


/**
 * Configuration, with ability to build configuration from an XML file
 * @author Andre Fonseca
 */
public class PLogicXMLConfiguration extends PLogicConfiguration {
    static Logger logger = LoggerFactory.getLogger(PLogicXMLConfiguration.class);
    private static final long serialVersionUID = 785838945858L;

    public PLogicXMLConfiguration(final Module module) {
        super(module);
    }

    /**
     * Parse XML configuration node to obtain corresponding
     * Configuration properties
     */
    public void parseConfig(final Node config) {
        try {
            // Verify if there is a valid config
            if (config == null) {
                return;
            }

            final XPathReader reader = new XPathReader(config);
            parseConsequenceFinding(reader);
            parseTheoryInitialization(reader);

        } catch (final Exception e) {
            logger.error("There were errors parsing XML configuration file : " +
                e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
    }
    
    /**
     * Parse XML configuration file, part 'consequenceFinding'
     */
    private void parseConsequenceFinding(XPathReader reader) throws XPathExpressionException {
    	
    	String strategyStr = reader.read("consequenceFinding/strategy", XPathConstants.STRING).toString();
        if (!strategyStr.isEmpty())
        	consequenceFindingStrategy = strategyStr;
    	
        String datatypeStr = reader.read("consequenceFinding/datatype", XPathConstants.STRING).toString();
        if (!datatypeStr.isEmpty())
        	datatype = datatypeStr;
    }
    
    /**
     * Parse XML configuration file, part 'theoryInitialization'
     */
    private void parseTheoryInitialization(XPathReader reader) throws XPathExpressionException {
    	
    	String strategyStr = reader.read("theoryInitialization/strategy", XPathConstants.STRING).toString();
        if (!strategyStr.isEmpty())
        	theoryInitializationStrategy = strategyStr;
    }

    /**
     * Export the configuration into an XML node
     */
    public Node export() {
        final Document doc;

        try {
            final DocumentBuilder builder = DocumentBuilderFactory.newInstance()
                                                                  .newDocumentBuilder();
            doc = builder.newDocument();

            final Element element = doc.createElement(getModule().getModuleName());
            doc.appendChild(element);

			exportConsequenceFinding(doc, element);
			
        } catch (final Exception e) {
            logger.error(
                "There were errors exporting XML configuration file : " +
                e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }

        return doc;
    }
    
    
    /**
     * Export XML configuration file, part 'localPeer'
     */
    private void exportConsequenceFinding(Document doc, Element element)
        throws DOMException {
        // Create the base 'localPeer' element
        Element base = doc.createElement("consequenceFinding");
        element.appendChild(base);

        // Create the child nodes
        Element strategyElement = doc.createElement("strategy");
        strategyElement.setTextContent(consequenceFindingStrategy);
        base.appendChild(strategyElement);

        Element datatypeElement = doc.createElement("datatype");
        datatypeElement.setTextContent(datatype);
        base.appendChild(datatypeElement);
    }
    
}
