/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.commands;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Iterables;

import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.plogic.distributed.PLogicManager;
import fr.lri.iasi.somewhere.plogic.distributed.PropositionalInstanceWithMapping;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

/**
 * Show all shared literals of the local theory, on distant theories.
 * @author Andre Fonseca
 *
 */
public class ShowSharedLiteralsCommand implements Command {
	static Logger logger = LoggerFactory.getLogger(ShowSharedLiteralsCommand.class);
	
	/* --- Properties --- */
    /** Manager of the module */
    protected final PLogicManager manager;
    
    public ShowSharedLiteralsCommand(PLogicManager manager) {
    	if (manager == null) {
            throw new IllegalArgumentException("manager");
        }
    	
    	this.manager = manager;
    }
    
    /* --- Accessors --- */
    public PLogicManager getManager() {
        return manager;
    }

    /**
     * {@inheritDoc}
     */
	@Override
	public String getName() {
		return "showSharedLiterals";
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public String getDescription() {
		return "Show all shared literals contained by this instance (by distant peer).";
	}

	/* --- Methods --- */
	
	/**
     * {@inheritDoc}
     */
	@Override
	public ReturnStatus execute(List<Object> parameters) {
		ReturnStatus res = null;

        try {
            // Verify there are enough parameters
            if (parameters.size() > 1) {
                res = new GenericReturnStatus("Syntax Error! \n Usage :" +
                        "\t" + getName() + " or \t" + getName() + " PEER",
                        false, null);

                return res;
            }
            
            // If there is a loaded instance
            PropositionalInstanceWithMapping instance = manager.getPropositionalInstance();
            if (instance == null) {
            	res = new GenericReturnStatus("Instance not loaded! Load an instance check shared literals.", false, null);

                return res;
            }
            
            String peerStr = Iterables.getFirst(parameters, "").toString().trim();
            StringBuilder result = new StringBuilder();
            result.append("Local shared literals by neighbor:\n");
            
            if (!peerStr.isEmpty()) {
            	Set<Literal> distantLiterals = instance.getSharedLiterals(peerStr);
            	if (!distantLiterals.isEmpty()) {
            		result.append(peerStr + " => ");
            		result.append(distantLiterals);
            	}
            } else {
            	Peer localPeer = manager.getCommunicationManager().getLocalPeer();
            	Map<String,Peer> neighbors = manager.getCommunicationManager().getPeers();
            	
            	for (Entry<String, Peer> entry : neighbors.entrySet()) {
            		String peerName = entry.getKey();
            		if (peerName.equals(localPeer.getName()))
            			continue;
            		
            		Set<Literal> distantLiterals = instance.getSharedLiterals(peerName);
            		if (!distantLiterals.isEmpty()) {
                		result.append(peerName + " => ");
                		result.append(distantLiterals);
                		result.append("\n");
                	}
            	}
            }
            
            res = new GenericReturnStatus(result.toString(), true, null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error analyzing shared literals of the current instance !",
                    false, null);
        }

        return res;
	}

}
