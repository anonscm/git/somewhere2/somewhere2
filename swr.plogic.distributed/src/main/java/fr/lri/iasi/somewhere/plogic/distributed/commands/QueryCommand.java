/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.commands;

import fr.lri.iasi.libs.formallogic.Restriction;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.restrictions.LenghtRestriction;
import fr.lri.iasi.somewhere.plogic.distributed.MappingBuilder;
import fr.lri.iasi.somewhere.plogic.distributed.PLogicAnswersCollector;
import fr.lri.iasi.somewhere.plogic.distributed.PLogicManager;
import fr.lri.iasi.somewhere.plogic.distributed.parser.SWRInstanceParser.SWRLexem;
import fr.lri.iasi.somewhere.ui.CommandListener;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;

import java.text.DecimalFormat;
import java.util.List;


/**
 * A command to do a propositional query on the network
 * @author Andre Fonseca
 */
public class QueryCommand extends AbstractBlockingUICommand {
    static Logger logger = LoggerFactory.getLogger(QueryCommand.class);

    /* --- Properties --- */
    /** Manager of the module */
    protected final PLogicManager manager;

    public QueryCommand(PLogicManager manager) {
    	super();
    	
        if (manager == null) {
            throw new IllegalArgumentException("manager");
        }

        this.manager = manager;
    }

    /* --- Accessors --- */
    public PLogicManager getManager() {
        return manager;
    }
    
    public CommandListener getCommandListener() {
        return commandListener;
    }

    /**
     * @return the name of the command
     */
    public String getName() {
        return "query";
    }

    /**
     * @return the description of the command
     */
    public String getDescription() {
        return "Do one or several propositional query(ies) on the network.";
    }

    /* --- Methods --- */
    /**
     * Execute the command
     * @param parameters The parameters of the command
     * @return the return status
     *  NOTE : note that the return status contains only text
     *  information
     */
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;
        String queryStr = null;
        Joiner joiner = Joiner.on(" ").skipNulls();
        String paramStr = joiner.join(parameters);

        try {
        	// Clause param
        	int actualParamIndex = paramStr.indexOf("-");
        	if (actualParamIndex != -1)
        		queryStr = paramStr.substring(0, actualParamIndex);
        	else
        		queryStr = paramStr;
        	
        	int actualParametersSize = 0;
        	if (!queryStr.isEmpty())
        		actualParametersSize++;
        	
        	// TTL param
        	long ttl = 60000;
        	int ttlIndex = parameters.indexOf("-ttl");
        	if (ttlIndex != -1) {
        		Object ttlObject = Iterables.get(parameters, ttlIndex + 1, null);
    		
    			if (ttlObject != null) {
    				ttl = Integer.parseInt(ttlObject.toString()) * 1000;
    				actualParametersSize++;
    			}
        	}
        	
        	// BL parameter
        	Restriction<Clause> restriction = null;
        	int blIndex = parameters.indexOf("-bl");
        	if (blIndex != -1) {
        		Object blObject = Iterables.get(parameters, blIndex + 1, null);
    		
        		if (blObject != null) {
            		int bl = Integer.parseInt(blObject.toString());
            		if (bl < 0)
            			throw new IllegalArgumentException("Lenght restriction must be positive!");
            		
            		restriction = new LenghtRestriction(bl);
            	}
        	}
        	
            // Verify there are enough parameters
            if (actualParametersSize < 1 || actualParametersSize > 3) {
                res = new GenericReturnStatus("Syntax Error! \n Usage :" +
                        "\t" + getName() + " CLAUSE [-ttl SECONDS] [-bl LENGHT]",
                        false, null);

                return res;
            }
            
            // If there is a loaded instance
            if (manager.getPropositionalInstance() == null) {
            	res = new GenericReturnStatus("Instance not loaded! Load an instance before launch the query.", false, null);

                return res;
            }
            
            PLogicAnswersCollector collector = new PLogicAnswersCollector(manager, commandListener);
            String localPeerName = manager.getCommunicationManager().getLocalPeer().getName();
            
            // Building arguments
            String[] literals = queryStr.split("\\s+v\\s+");
            MappingBuilder builder = new MappingBuilder();
            for (String litStr : literals) {
        		litStr = litStr.trim();
        		
        		SWRLexem lexem = new SWRLexem(litStr);
                
        		boolean sign = lexem.getSign();
                String peerName = lexem.getPeerName();
                String varName = lexem.getVarName();
                
                if (sign) {
                	if (peerName == null || peerName.isEmpty() || peerName.equals(localPeerName)) {
                		builder.addLiteral(varName);
                	} else {
                		builder.addLiteral(peerName, varName);
                	}
                } else {
                	if (peerName == null || peerName.isEmpty() || peerName.equals(localPeerName)) {
                		builder.addNegativeLiteral(varName);
                	} else {
                		builder.addNegativeLiteral(peerName, varName);
                	}
                }
			}

            // Run the queries !
            this.executionCompleted.set(false);
        	this.initialTime = System.nanoTime();
            
            this.manager.receiveQueryFromUser(builder.create(), ttl, restriction);
            
            // Calculate the execution time and finish the query command
            double execTime = waitExecutionToComplete(this.initialTime) / 1000000000.0;
            collector.finishReceiving();

        	DecimalFormat decimal = new DecimalFormat( "0.0000" );
            res = new GenericReturnStatus("Process of the query " + queryStr + " complete!" +
            		"\nTotal number of received answers : " + collector.getReceivedAnswersNum() +
            		"\nSize of final answer : " + collector.getFinalAnswerSize() +
            		"\nAll results obtained in " + decimal.format(execTime) + " seconds.", true, null);
                
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error querying : " + e.getMessage(),
                    false, null);
        }

        return res;
    }
}
