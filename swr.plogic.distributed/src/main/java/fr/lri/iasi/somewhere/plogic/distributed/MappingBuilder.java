/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.impl.ClauseBuilder;

/**
 * Implementation of the Builder pattern to generate immutable clauses. The literals
 * on the generated clause may have reference to a distant peer, classifying the
 * generated clause as a mapping.
 * 
 * @author Andre Fonseca
 *
 */
public class MappingBuilder extends ClauseBuilder {
	
	/**
	 * Creates and add a distant literal to the clause to be generated.
	 * The literal will be positive its name will be based on the symbol parameter.
	 * 
	 * @param url the identifier resource of the distant peer
	 * @param symbol the name of the literal
	 */
	public void addLiteral(String url, String symbol) {
		if (verifySpecialElements(symbol))
			return;
		
		Literal lit = new DistantLiteralImpl(url, symbol, true);
		this.literals.add(lit);
	}

	/**
	 * Creates and add a distant literal to the clause to be generated.
	 * The literal will be negative its name will be based on the symbol parameter.
	 * 
	 * @param url the identifier resource of the distant peer
	 * @param symbol the name of the literal
	 */
	public void addNegativeLiteral(String url, String symbol) {
		if (verifySpecialElements(symbol))
			return;
		
		Literal lit = new DistantLiteralImpl(url, symbol, false);
		this.literals.add(lit);
	}

}
