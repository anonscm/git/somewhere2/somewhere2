/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.MessageType;
import fr.lri.iasi.somewhere.api.communication.model.MessageTypeSingletonFactory;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageProcessor;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicQueryMessageType;

/**
 * Processor responsible for process the "Timeout" message type.
 * 
 * @author Andre Fonseca
 * 
 */
public class PLogicTimeoutProcessor extends AbstractMessageProcessor {
	static Logger logger = LoggerFactory.getLogger(PLogicTimeoutProcessor.class);

	/* --- Properties --- */
	/** The related query processor (in the same message session). */
	protected final PLogicQueryProcessor queryProcessor;
	
	protected final PLogicManager plogicManager;

	public PLogicTimeoutProcessor(MessageSession session) {
		super(session);
		
		App app = session.getCommunicationManager().getApp();
        this.plogicManager = PLogicManager.getModuleInstance(app);

		MessageType queryType = MessageTypeSingletonFactory.create(PLogicQueryMessageType.class);
		this.queryProcessor = (PLogicQueryProcessor) session.getProcessor(queryType);
	}

	/* --- Methods --- */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void process(Message message) {
		
		// Count down the finish message on the related query processor.
		try {
			this.plogicManager.returnTimeoutAnswer(this.session);
		} catch (CommunicationException ce) {
			logger.error("There were problems sending the timeout message: {}", ce.getMessage());
			return;
		}
	}

}
