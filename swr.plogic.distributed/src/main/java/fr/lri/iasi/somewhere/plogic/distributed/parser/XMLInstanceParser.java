/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.parser;

import fr.lri.iasi.libs.formallogic.Restriction;
import fr.lri.iasi.libs.formallogic.utils.instance.AbstractInstanceParser;
import fr.lri.iasi.libs.formallogic.utils.instance.ProductionField;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryImpl;
import fr.lri.iasi.libs.plogic.restrictions.SubsetRestriction;
import fr.lri.iasi.somewhere.XPathReader;
import fr.lri.iasi.somewhere.plogic.distributed.DistantLiteralImpl;
import fr.lri.iasi.somewhere.plogic.distributed.MappingBuilder;
import fr.lri.iasi.somewhere.plogic.distributed.PropositionalInstanceWithMapping;
import fr.lri.iasi.somewhere.plogic.distributed.PropositionalProductionField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;


/**
 * Parse the content of a XML peer profile into a propositional instance
 * @author Andre Fonseca
 */
public class XMLInstanceParser extends AbstractInstanceParser {
    static Logger logger = LoggerFactory.getLogger(XMLInstanceParser.class);

    /* --- Constants --- */
    /** The name of the schema file */
    protected final static String SCHEMA_FILE = "plogic_instance.xsd";

    /* --- Properties --- */
    /** The DOM document of the input file */
    protected Document document;

    /**
     * @param path Path of the input XML file
     */
    public XMLInstanceParser(String path) throws FileNotFoundException {
        super(path);
    }

    /**
     * Validate the xml source
     * @return if is valid
     */
    protected boolean isSourceValid(Document document) {
        if (document == null) {
            throw new IllegalArgumentException("document");
        }

        // Lookup a factory for the W3C XML Schema language
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        // Compile the schema.
        URL schemaURL = getClass().getClassLoader().getResource("META-INF/" + SCHEMA_FILE);
        Schema schema = null;

        try {
            schema = factory.newSchema(schemaURL);
        } catch (SAXException e) {
            logger.error("There was a problem parsing schema file '" +
                SCHEMA_FILE + "'..");

            return false;
        }

        // Get a validator from the schema.
        Validator validator = schema.newValidator();

        // Check the document
        try {
            validator.validate(new DOMSource(document));

            return true;
        } catch (Exception ex) {
            logger.error("XML source file is not valid...");

            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    public PropositionalInstanceWithMapping parse() {
        DocumentBuilder parser;
		try {
			parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = parser.parse(new File(path));
			
			if (!isSourceValid(document)) {
	            throw new SAXException();
	        }

	        return parse(document);
		} catch (Exception e) {
			logger.error(
	                "There were errors parsing XML configuration file : " +
	                e.getMessage());
	        throw new RuntimeException(e.getMessage(), e);
		}
    }

    /**
     * Parse XML configuration node to obtain corresponding
     * peer profile
     * @param document the Document to parse
     * @return the PropositionalInstance obtained
     */
    private PropositionalInstanceWithMapping parse(Document document) {
        try {
            XPathReader reader = new XPathReader(document.getFirstChild());
            String uri = parseURI(reader);
            PropositionalClausalTheory theory = parseTheory(reader, uri, "/peer/theory");
            PropositionalClausalTheory mapping = parseTheory(reader, uri, "/peer/mapping");
            ProductionField<Clause> production = parseProduction(reader);

            return new PropositionalInstanceWithMapping(uri, theory,
                production, mapping);
        } catch (Exception e) {
            logger.error(
                "There were errors parsing XML configuration file : " +
                e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * Parse XML configuration file, part 'URI'
     * @return the URI
     * @throws IOException
     * @throws XPathExpressionException 
     */
    private String parseURI(XPathReader reader) throws IOException, XPathExpressionException {
        return (String) reader.read("/peer/uri", XPathConstants.STRING);
    }

    /**
     * Parse XML configuration file, part 'theory', or 'mapping'
     * @return the parsed theory
     * @throws IOException
     * @throws XPathExpressionException 
     */
    private PropositionalClausalTheory parseTheory(XPathReader reader, String uri, String nodePath)
        throws IOException, XPathExpressionException {
        PropositionalClausalTheory result = new PropositionalClausalTheoryImpl();

        NodeList theoryNodes = (NodeList) reader.read(nodePath,
                XPathConstants.NODESET);

        int i = 0;
        for (int index = 0; index < theoryNodes.getLength(); index++) {
            Node nodeClause = theoryNodes.item(index);
            NodeList litNodes = nodeClause.getChildNodes();
            MappingBuilder builder = new MappingBuilder();

            boolean isMapping = false;
            for (int index2 = 0; index2 < litNodes.getLength(); index2++) {
                Node nodeLit = litNodes.item(index);
                Node nodeLitChild = nodeLit.getFirstChild();
                String var = null;

                if (nodeLitChild.getNodeName().equals("not")) {
                    Node nl = nodeLitChild.getFirstChild();
                    NamedNodeMap attributes = nl.getAttributes();
                    var = nl.getTextContent();
                    
                    if (attributes != null) {
                    	Node url = attributes.getNamedItem("url");
	                    String urlStr = url.getTextContent();
	                    builder.addNegativeLiteral(urlStr, var);
	                    isMapping = true;
                    } else {
                    	builder.addNegativeLiteral(var);
                    }
                    
                } else {
                	NamedNodeMap attributes = nodeLitChild.getAttributes();
                	if (attributes != null) {
                		Node url = attributes.getNamedItem("url");
	                    String urlStr = url.getTextContent();
	                    builder.addLiteral(urlStr);
	                    isMapping = true;
                	} else {
                		var = nodeLitChild.getTextContent();
                		builder.addLiteral(var);
                	}
                }
            }
            
            if (isMapping)
            	builder.setLabel(uri + ":m" + i);
            else
            	builder.setLabel(uri + ":" + i);
            
            i++;

            result.add(builder.create());
        }

        return result;
    }

    /**
     * Parse XML configuration file, part 'production'
     * @return the parsed production
     * @throws IOException 
     * @throws XPathExpressionException
     */
    private ProductionField<Clause> parseProduction(XPathReader reader)
        throws IOException, XPathExpressionException {
    	ProductionField<Clause> result = new PropositionalProductionField();

        NodeList subsetNodes = (NodeList) reader.read("/peer/production",
                XPathConstants.NODESET);
        
        if (subsetNodes.getLength() > 0) {   	
        	SubsetRestriction restriction = new SubsetRestriction();
        	result.addRestriction(restriction);
        	
        	for (int index = 0; index < subsetNodes.getLength(); index++) {
                Node n = subsetNodes.item(index);
                String var = n.getTextContent();
                restriction.addToSubset(var);
        	}
        }

        return result;
    }


    /**
     * {@inheritDoc}
     */
    public void export(String path) {
        Document doc = null;

        try {
            // Create a document
            DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
            doc = docBuilder.newDocument();

            // Create the root element and add it to the document
            Element root = doc.createElement("peer");
            doc.appendChild(root);

            // Create all the elements
            exportHeader(doc, root);
            exportTheory(doc, root);
            exportMapping(doc, root);
            exportProduction(doc, root);

            // Prepare the output file 
            Source source = new DOMSource(doc);
            File file = new File(path);
            Result result = new StreamResult(file);

            // Write the DOM document to the file 
            Transformer xformer = TransformerFactory.newInstance()
                                                    .newTransformer();
            xformer.setOutputProperty(OutputKeys.INDENT, "yes");
            xformer.transform(source, result);
        } catch (Exception e) {
            logger.error("There was a problem exporting " +
                "propositional instance to file '" + path + "' !");
        }
    }

    /**
     * Export to instance to XML file, headers part
     * @param doc The XML Document
     * @param root the root of the document
     */
    private void exportHeader(Document doc, Element root)
        throws IOException {
        Element uri = doc.createElement("uri");
        uri.setTextContent(instance.getName());
        root.appendChild(uri);
    }

    /**
     * Export to instance to XML file, part 'theory'
     * @param doc The XML Document
     * @param root the root of the document
     */
    private void exportTheory(Document doc, Element root)
        throws IOException {
        Element theoryElement = doc.createElement("theory");

        PropositionalInstanceWithMapping propositionalInstance = (PropositionalInstanceWithMapping)instance;
        
        for (Clause c : propositionalInstance.getLocalTheory()) {
            Element clauseElement = doc.createElement("clause");

            for (Literal l : c) {
                Element literalElement = doc.createElement("literal");
                literalElement.setTextContent(l.getLabel());

                if (l.getSign()) {
                    clauseElement.appendChild(literalElement);
                } else {
                    Element notElement = doc.createElement("not");
                    notElement.appendChild(literalElement);
                    clauseElement.appendChild(notElement);
                }
            }

            theoryElement.appendChild(clauseElement);
        }

        root.appendChild(theoryElement);
    }

    /**
     * Export to instance to XML file, part 'mapping'
     * @param doc The XML Document
     * @param root the root of the document
     */
    private void exportMapping(Document doc, Element root)
        throws IOException {
        Element mappingElement = doc.createElement("mapping");
        
        PropositionalInstanceWithMapping propositionalInstance = (PropositionalInstanceWithMapping)instance;

        for (Clause c : propositionalInstance.getMappings()) {
            Element clauseElement = doc.createElement("clause");

            for (Literal l : c) {
                Element literalElement = doc.createElement("literal");
                literalElement.setTextContent(l.getLabel());

                if (l instanceof DistantLiteralImpl) {
                    DistantLiteralImpl dl = (DistantLiteralImpl) l;
                    literalElement.setAttribute("url", dl.getPeerName());
                }

                if (l.getSign()) {
                    clauseElement.appendChild(literalElement);
                } else {
                    Element notElement = doc.createElement("not");
                    notElement.appendChild(literalElement);
                    clauseElement.appendChild(notElement);
                }
            }

            mappingElement.appendChild(clauseElement);
        }

        root.appendChild(mappingElement);
    }

    /**
     * Export to instance to XML file, part 'production'
     * @param doc The XML Document
     * @param root the root of the document
     */
    private void exportProduction(Document doc, Element root)
        throws IOException {
        Element productionElement = doc.createElement("production");
        
        PropositionalInstanceWithMapping propositionalInstance = (PropositionalInstanceWithMapping)instance;
        ProductionField<Clause> productionSet = propositionalInstance.getProductionField();

        for (Restriction<?> restriction : productionSet.getRestrictions().values()) {
        	
        	if (restriction.getName().equals("subset")) {
        		SubsetRestriction subsetRestriction = (SubsetRestriction) restriction;
        		Set<String> set = subsetRestriction.getSubSet();
        		
        		for (String literalName : set) {
        			Element literalElement = doc.createElement("literal");
    	            literalElement.setTextContent(literalName);
    	            productionElement.appendChild(literalElement);
				}
        	}
        }

        root.appendChild(productionElement);
    }
    
}
