/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import fr.lri.iasi.libs.formallogic.utils.instance.Instance;
import fr.lri.iasi.libs.formallogic.utils.instance.ProductionField;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryImpl;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Describes a propositional instance
 * @author Andre Fonseca
 */
public class PropositionalInstance implements Instance {
    static Logger logger = LoggerFactory.getLogger(PropositionalInstance.class);

    /* --- Properties --- */
    /** The instance name */
    protected final String name;
    
    /** Represents the original version of the local theory (not initialized) */
    protected volatile PropositionalClausalTheory originalTheory;
    
    /** Represents the current local theory */
    protected volatile PropositionalClausalTheory theory;
    
    /** The production set of this instance */
    protected ProductionField<Clause> productionField;
    
    /** The lock used to block multiple modify operations */
    private final ReentrantLock lock = new ReentrantLock();

    public PropositionalInstance(String name, Collection<Clause> localTheory,
    	ProductionField<Clause> productionField) {
        if (name == null) {
            throw new IllegalArgumentException("name");
        }

        if (localTheory == null) {
            throw new IllegalArgumentException("localTheory");
        }

        if (productionField == null) {
            throw new IllegalArgumentException("productionField");
        }
        
        this.originalTheory = new PropositionalClausalTheoryImpl(localTheory);
        this.theory = new PropositionalClausalTheoryImpl(localTheory);
        this.productionField = productionField.copy();
        this.name = name;
    }

    public PropositionalInstance(String name,
        PropositionalInstance propositionalInstance) {
    	
    	this(name, propositionalInstance.getTheory(), propositionalInstance.getProductionField());
    }
    
    public PropositionalInstance(PropositionalInstance propositionalInstance) {
    	this(propositionalInstance.getName(), propositionalInstance); 
    }

    /* --- Accessors --- */
    /**
     * @return Unmodifiable set of clauses of the original theory (not initialized)
     */
    public Set<Clause> getOriginalTheory() {
    	return Collections.unmodifiableSet(this.originalTheory);
    }
    
    /**
     * @return the whole theory contained on this instance.
     */
    public PropositionalClausalTheory getTheory() {
    	return this.theory;
    }
    
    /**
     * @return a view of the local theory (do not contain clauses with distant literals)
     * contained in this instance.
     * 
     * NOTE: the result theory view do not support the 'remove' method.
     */
    public Iterable<Clause> getLocalTheory() {
    	Iterable<Clause> localTheoryClauses = Iterables.filter(
    			this.theory, new LocalClausePredicate());
    	
    	return localTheoryClauses;
    }

    public ProductionField<Clause> getProductionField() {
        return this.productionField;
    }

    public String getName() {
        return name;
    }

    /* --- Methods --- */
    /**
     * {@inheritDoc}
     */
    @Override
    public PropositionalInstance copy() {
    	return new PropositionalInstance(this);
    }
    
    /**
     * Adds a clause locally.
     * NOTE: a copy of the local theory will be created during the adding process
     * in order to avoid its locking. 
     * 
     * @param clause : the clause to be added.
     * @return true if the clause was added correctly. false otherwise.
     */
    public boolean addClause(Clause clause) {
    	boolean result = false;
    	final ReentrantLock lockModify = this.lock;
    	lockModify.lock();
    	
    	try{
    		result = this.originalTheory.add(clause);
    		if (result) {
    			Collection<Clause> newTheory = this.originalTheory.initialize();
			
    			this.theory = new PropositionalClausalTheoryImpl(newTheory);
    		}
    	} finally {
    		lockModify.unlock();
    	}
    	
    	return result;
    }
    
    /**
     * Removes a clause locally.
     * NOTE: a copy of the local theory will be created during the removing process
     * in order to avoid its locking. 
     * 
     * @param clause : the clause to be removed.
     * @return true if the clause was removed correctly. false otherwise.
     */
    public boolean removeClause(Clause clause) {
    	boolean result = false;
    	final ReentrantLock lockModify = this.lock;
    	lockModify.lock();
    	
    	try{
    		result = this.originalTheory.remove(clause);
    		if (result) {
    			Collection<Clause> newTheory = this.originalTheory.initialize();
    		
    			this.theory = new PropositionalClausalTheoryImpl(newTheory);
    		}
    	} finally {
    		lockModify.unlock();
    	}
    	
    	return result;
    }
    
    /**
     * Initializes the current theory
     */
    public void initializeTheory() {
    	this.theory = new PropositionalClausalTheoryImpl(this.theory.initialize());
    }
    
    @Override
    public boolean equals(Object o) {
    	if (o == this)
    		return true;
    	
        if (o instanceof PropositionalInstance) {
            PropositionalInstance p = (PropositionalInstance) o;

            return ((this.name.equals(p.name)) &&
            (this.originalTheory.equals(p.originalTheory)) &&
            (this.theory.equals(p.theory)) &&
            (this.productionField.equals(p.productionField)));
        }

        return false;
    }

    @Override
   	public int hashCode() {
   		final int prime = 31;
   		int result = 1;
   		result = prime * result + theory.hashCode();
   		result = prime * result + originalTheory.hashCode();
   		result = prime * result + productionField.hashCode();
   		result = prime * result + name.hashCode();
   		return result;
   	}

    @Override
    public String toString() {
        String res = "<instance>\n";
        res += "<originalTheory>\n";
        res += ("" + originalTheory + "\n");
        res += "</originalTheory>\n";
        res += "<initializedTheory>\n";
        res += ("" + theory + "\n");
        res += "</initializedTheory>\n";
        res += "<productionField>\n";
        res += ("" + productionField + "\n");
        res += "</productionField>\n";
        res += "</instance>\n";

        return res;
    }
    
    /**
     * Predicate responsible for selecting 'local-only' clauses.
     * 
     * @author Andre Fonseca
     *
     */
    protected static class LocalClausePredicate implements Predicate<Clause> {
		
		public boolean apply(Clause clause) {
			
			for (Literal lit : clause) {
				if (lit instanceof DistantLiteralImpl)
					return false;
			}
			
			return true;
		}
    }
    
}
