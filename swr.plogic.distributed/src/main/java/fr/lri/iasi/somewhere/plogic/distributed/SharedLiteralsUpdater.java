/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.model.MessageBuilder;
import fr.lri.iasi.somewhere.api.communication.model.MessageTypeSingletonFactory;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.api.communication.model.PeerAdapter;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicAddSharedLiteralType;

/**
 * Peer listener responsible for updating the shared literal index of a distant peer.
 * 
 * @author Andre Fonseca
 *
 */
public class SharedLiteralsUpdater extends PeerAdapter {
	static Logger logger = LoggerFactory.getLogger(SharedLiteralsUpdater.class);
	
	/* --- Properties --- */
    /** Manager of the module */
    protected final PLogicManager manager;
    
    public SharedLiteralsUpdater(PLogicManager manager) {
    	if (manager == null) {
            throw new IllegalArgumentException("manager");
        }
    	
    	this.manager = manager;
    	
    	// Register itself as a peer Listener
        this.manager.getCommunicationManager().addPeerListener(this);
    }
    
    /* --- Accessors --- */
    public PLogicManager getManager() {
        return manager;
    }
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void peerIsConnecting(Peer distantPeer) {
		
		Map<String, Set<Literal>> sharedLiteralsByPeer = manager.getPropositionalInstance().getDistantSharedLiteralsByPeer();		
		Set<Literal> sharedLiterals = sharedLiteralsByPeer.get(distantPeer.getName());
		if (sharedLiterals == null || sharedLiterals.isEmpty())
			return;
		
		// Reference the local peer
        Peer localPeer = new Peer(manager.getCommunicationManager().getLocalPeer().getName());
        
        // Create the message !
        MessageBuilder builder = new MessageBuilder(manager.getCommunicationManager())
        			.setOrigin(localPeer)
        			.setDestination(distantPeer)
        			.setType(MessageTypeSingletonFactory.create(PLogicAddSharedLiteralType.class));
        
        PLogicManager.SHARED_LITERALS.addContent(builder, sharedLiterals);
        
        try {
			manager.getCommunicationManager().sendMessage(builder.build());
		} catch (CommunicationException e) {
                    logger.warn(localPeer.getName() + " could not declare shared Litterals with "+ distantPeer.getName());
		}
	}
        
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void peerIsLeaving(Peer distantPeer) {
		
		Map<String, Set<Literal>> sharedLiteralsByPeer = manager.getPropositionalInstance().getDistantSharedLiteralsByPeer();
		sharedLiteralsByPeer.remove(distantPeer.getName());
		
	}

}
