/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.configuration;

import fr.lri.iasi.somewhere.Module;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Configuration of plogic.distributed bundle
 * @author Andre Fonseca
 */
public class PLogicConfiguration extends fr.lri.iasi.somewhere.Configuration {
    static Logger logger = LoggerFactory.getLogger(PLogicConfiguration.class);
    private static final long serialVersionUID = 745838945963L;

    /* --- Properties --- */  
    /** Consequence finding strategy to be used */
    protected String consequenceFindingStrategy;
    
    /** Theory initialization strategy to be used */
    protected String theoryInitializationStrategy;
    
    /** DataType where the propositional clauses will be encoded */
    protected String datatype;

	public PLogicConfiguration(final Module module) {
        super(module);
    }

    /* --- Mutators --- */
    public void setConsequenceFindingStrategy(final String consequenceFindingStrategy) {
        this.consequenceFindingStrategy = consequenceFindingStrategy;
    }
    
    public void setTheoryInitializationStrategy(final String theoryInitializationStrategy) {
        this.theoryInitializationStrategy = theoryInitializationStrategy;
    }
    
    public void setDataType(final String datatype) {
		this.datatype = datatype;
	}

    /* --- Accessors --- */
    public String getConsequenceFindingStrategy() {
        return consequenceFindingStrategy;
    }
    
    public String getTheoryInitializationStrategy() {
        return theoryInitializationStrategy;
    }
    
    public String getDataType() {
		return datatype;
	}

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAllToDefault() {
    	consequenceFindingStrategy = "ipia";
    	theoryInitializationStrategy = "naiveSaturation";
    	datatype = "hashset";
    }
    
}
