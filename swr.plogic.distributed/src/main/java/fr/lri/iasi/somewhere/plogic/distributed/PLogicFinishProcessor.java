/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca This file must be used under the terms of the
 * CeCILL. This source file is licensed as described in the file LICENSE, which you should have received as
 * part of this distribution. The terms are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.MessageType;
import fr.lri.iasi.somewhere.api.communication.model.MessageTypeSingletonFactory;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageProcessor;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicQueryMessageType;

/**
 * Processor responsible for process the "Finish" message type. It waits for all concerned answers to arrive
 * and counts down the number of Finish messages needed to end the calculus.
 *
 * @author Andre Fonseca
 *
 */
public class PLogicFinishProcessor extends AbstractMessageProcessor {

    static Logger logger = LoggerFactory.getLogger(PLogicFinishProcessor.class);
    /* --- Properties --- */
    /**
     * The related query processor (in the same message session).
     */
    protected final PLogicQueryProcessor queryProcessor;

    public PLogicFinishProcessor(MessageSession session) {
        super(session);

        MessageType queryType = MessageTypeSingletonFactory.create(PLogicQueryMessageType.class);
        this.queryProcessor = (PLogicQueryProcessor) session.getProcessor(queryType);
    }

    /* --- Methods --- */
    /**
     * {@inheritDoc}
     */
    @Override
    public void process(Message message) {
		
		final Clause query = PLogicManager.QUERY.getContent(message);
		final String origin = message.getOrigin().getName();
		final int messageNumber = PLogicManager.ANSWER_NUMBER.getContent(message);
		this.queryProcessor.getAnswersToWait().addAndGet(messageNumber - 1);
		logger.trace("Processing finish for {} from {}", query, origin);
		
		// Waiting all messages to arrive...
		while (session.getReceivedMessagesCount("answer") != this.queryProcessor.getAnswersToWait().get()) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
		// After all answer messages are arrived, count down the finish message on the related query processor.
		this.queryProcessor.getFinishCountDown().countDown();
		logger.trace("Received all answers for {} from {}", query, origin);
	}
            }
