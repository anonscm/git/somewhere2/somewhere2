/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.parser;

import fr.lri.iasi.libs.formallogic.Restriction;
import fr.lri.iasi.libs.formallogic.utils.instance.AbstractInstanceParser;
import fr.lri.iasi.libs.formallogic.utils.instance.ProductionField;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.impl.ClauseBuilder;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryImpl;
import fr.lri.iasi.libs.plogic.restrictions.SubsetRestriction;
import fr.lri.iasi.somewhere.plogic.distributed.MappingBuilder;
import fr.lri.iasi.somewhere.plogic.distributed.PropositionalInstanceWithMapping;
import fr.lri.iasi.somewhere.plogic.distributed.PropositionalProductionField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Parse the content of a SWR peer profile into a propositional instance
 * @author Andre Fonseca
 */
public class SWRInstanceParser extends AbstractInstanceParser {
    static Logger logger = LoggerFactory.getLogger(SWRInstanceParser.class);
    
    /** The local part of the parsed instance */
    private final PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
    
    /** The 'mappings' part of the parsed instance */
    private final PropositionalClausalTheory mappings = new PropositionalClausalTheoryImpl();
    
    /** The 'production' part of the parsed instance */
    private final ProductionField<Clause> production = new PropositionalProductionField();
    
    /**
     * @param path Path of the input SWR file
     */
    public SWRInstanceParser(String path) throws FileNotFoundException {
        super(path);
    }


    /* --- Methods --- */
    /**
     * {@inheritDoc}
     */
    public PropositionalInstanceWithMapping parse() {
        PropositionalInstanceWithMapping res = null;

        try {
            // Open the file
            BufferedReader in = new BufferedReader(new FileReader(path));

            // Parse everything
            res = parse(in);

            return res;
        } catch (Exception e) {
            logger.error(
                "There were errors parsing SWR configuration file : " +
                e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Parse the entire SWR file to obtain corresponding
     * peer profile
     * @param scanner Scanner to access to the file
     * @return the PropositionalInstance obtained
     */
    private PropositionalInstanceWithMapping parse(BufferedReader in)
        throws IOException {
    	
        String peerName = parseURI(in);   
        String str = null;
        boolean hasProduction = false;

        int i = 0;
        while ((str = in.readLine()) != null) {
        	str = str.trim();
            if (str.equals("Mappings")) {
            	hasProduction = parseMapping(in, peerName);
            	if (!hasProduction)
            		break;
            }
            
            if (str.equals("Production") || hasProduction) {
            	parseProduction(in, peerName);
            	break;
            }

            ClauseBuilder builder = new ClauseBuilder();

            for (String token : str.split("\\s+")) {
                SWRLexem lexem = new SWRLexem(token);

                if (!lexem.getPeerName().equals(peerName)) {
                    throw new IllegalArgumentException(token);
                }

                if (!lexem.getSign())
                	builder.addNegativeLiteral(lexem.getVarName());
                else
                	builder.addLiteral(lexem.getVarName());
            }

            builder.setLabel(peerName + ":" + i);
            i++;
            
            theory.add(builder.create());
        }

        return new PropositionalInstanceWithMapping(peerName, theory,
            production, mappings);
    }

    /**
     * Parse SWR configuration file, part 'URI'
     */
    private String parseURI(BufferedReader in) throws IOException {
        String str = in.readLine();

        if (str == null) {
            throw new IllegalArgumentException("the swr file is incorrectly formated!");
        }

        Pattern p = Pattern.compile("[\\w\\d]+");
        Matcher m = p.matcher(str);

        if (!m.matches()) {
            throw new IllegalArgumentException(str);
        }

        return m.group();
    }

    /**
     * Parse SWR configuration file, part 'Mapping'
     */
    private boolean parseMapping(BufferedReader in, String peerName)
        throws IOException {

        String str = null;
        int i = 0;
        while ((str = in.readLine()) != null) {
        	str = str.trim();
            if (str.equals("Production"))
                return true;

            MappingBuilder builder = new MappingBuilder();

            for (String token : str.split("\\s+")) {
                SWRLexem lexem = new SWRLexem(token);
                
                if (!lexem.getPeerName().equals(peerName)) {
                	if (!lexem.getSign())
	                	builder.addNegativeLiteral(lexem.getPeerName(), lexem.getVarName());
	                else
	                	builder.addLiteral(lexem.getPeerName(), lexem.getVarName());
                } else {
	                if (!lexem.getSign())
	                	builder.addNegativeLiteral(lexem.getVarName());
	                else
	                	builder.addLiteral(lexem.getVarName());
                }
            }

            builder.setLabel(peerName + ":m" + i);
            i++;
            
            this.mappings.add(builder.create());
        }
        
        return false;
    }

    /**
     * Parse SWR configuration file, part 'Production'
     */
    private void parseProduction(BufferedReader in, String peerName)
        throws IOException {
        SubsetRestriction restriction = new SubsetRestriction();

        Scanner scanner = new Scanner(in);

        while (scanner.hasNext()) {
            String token = scanner.next();
            SWRLexem lexem = new SWRLexem(token);

            if (!lexem.getSign()) {
                throw new IllegalArgumentException(token);
            }

            if (!lexem.getPeerName().equals(peerName)) {
                throw new IllegalArgumentException(token);
            }

            restriction.addToSubset(lexem.getVarName());
        }
        
        if (!restriction.getSubSet().isEmpty())
        	this.production.addRestriction(restriction);
    }


    /**
     * {@inheritDoc}
     */
    public void export(String path) {
        BufferedWriter out = null;

        try {
            FileWriter fstream = new FileWriter(instance.getName() + ".swr");
            out = new BufferedWriter(fstream);

            exportHeader(out);
            exportTheory(out);
            exportMapping(out);
            exportProduction(out);
        } catch (Exception e) {
            logger.error("There was a problem exporting " +
                "propositionnal instance to file '" + path + "' !");
        } finally {
            try {
                out.close();
            } catch (Exception e) {
                // Nothing ...
            }
        }
    }

    /**
     * Export to instance to SWR file, headers part
     * @param out BufferedWriter used to write stuffs
     */
    private void exportHeader(BufferedWriter out) throws IOException {
        out.write(this.instance.getName() + "\n");
    }

    /**
     * Export to instance to SWR file, part 'theory'
     * @param out BufferedWriter used to write stuffs
     */
    private void exportTheory(BufferedWriter out) 
    	throws IOException {
    	
    	PropositionalInstanceWithMapping propositionalInstance = (PropositionalInstanceWithMapping) this.instance;
    	
        for (Clause c : propositionalInstance.getLocalTheory()) {
            for (Literal l : c) {
                SWRLexem lexem = new SWRLexem(instance.getName(),
                        l.getLabel(), l.getSign());
                out.write(lexem.toSimplifiedString() + " ");
            }

            out.write("\n");
        }
    }

    /**
     * Export to instance to SWR file, part 'mapping'
     * @param out BufferedWriter used to write stuffs
     */
    private void exportMapping(BufferedWriter out)
        throws IOException {
    	
    	PropositionalInstanceWithMapping propositionalInstance = (PropositionalInstanceWithMapping) this.instance;
    	
        for (Clause c : propositionalInstance.getMappings()) {
            for (Literal l : c) {
                SWRLexem lexem = new SWRLexem(instance.getName(),
                        l.getLabel(), l.getSign());
                out.write(lexem.toSimplifiedString() + " ");
            }

            out.write("\n");
        }
    }

    /**
     * Export to instance to SWR file, part 'production'
     * @param out BufferedWriter used to write stuffs
     */
    private void exportProduction(BufferedWriter out)
        throws IOException {
    	
    	PropositionalInstanceWithMapping propositionalInstance = (PropositionalInstanceWithMapping) this.instance;
    	
        out.write("Production\n");

        for (Restriction<?> restriction : propositionalInstance.getProductionField().getRestrictions().values()) {
        	
        	if (restriction.getName().equals("subset")) {
        		SubsetRestriction subsetRestriction = (SubsetRestriction) restriction;
        		Set<String> set = subsetRestriction.getSubSet();
        		
        		for (String literalName : set) {
        			SWRLexem lexem = new SWRLexem(this.instance.getName(), literalName);
            		out.write(lexem.toSimplifiedString() + "\n");
				}
        	}
        }
    }

    /**
     * This class represents the lexem of a literal in a .swr file
     */
    public static class SWRLexem {
        /* --- Properties --- */
        protected final String peerName;
        protected final String varName;
        protected final boolean sign;

        public SWRLexem(String token) {
            Pattern p = Pattern.compile("(!?)([\\w\\d]+:)?([\\w\\d]+)");
            Matcher m = p.matcher(token);

            if (!m.matches())
                throw new IllegalArgumentException(token);

            if (m.group(1).isEmpty() || m.group(1) == null)
            	this.sign = true;
            else
            	this.sign = false;

            if (m.group(2) != null)
            	this.peerName = m.group(2).substring(0, m.group(2).length() - 1);
            else
            	this.peerName = null;
            
            this.varName = m.group(3);
        }

        public SWRLexem(String peerName, String varName, boolean sign) {
            if ((peerName == null) || peerName.isEmpty()) {
                throw new IllegalArgumentException("peerName");
            }

            if ((varName == null) || varName.isEmpty()) {
                throw new IllegalArgumentException("varName");
            }

            this.peerName = peerName;
            this.varName = varName;
            this.sign = sign;
        }

        public SWRLexem(String peerName, String varName) {
            this(peerName, varName, true);
        }

        /* --- Accessors --- */
        public String getPeerName() {
            return peerName;
        }

        public String getVarName() {
            return varName;
        }

        public boolean getSign() {
            return sign;
        }

        public String toString() {
            StringBuilder res = new StringBuilder();

            if (!sign) 
            	res.append("!");
            
            res.append(peerName);
            res.append(":");
            res.append(varName);

            return res.toString();
        }

        /**
         * Return a string representation,
         * but with only the sign, the peerName,
         * and the varName
         * @return string representation
         */
        public String toSimplifiedString() {
        	StringBuilder res = new StringBuilder();

            if (!sign) 
            	res.append("!");

            res.append(peerName);
            res.append(":");
            res.append(varName);

            return res.toString();
        }
    }
}
