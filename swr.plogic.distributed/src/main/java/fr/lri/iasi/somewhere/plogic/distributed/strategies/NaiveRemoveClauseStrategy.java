/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.strategies;

import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.plogic.distributed.PLogicManager;

/**
 * Describes a naive clause removal strategy that only informs to the neighbor peers
 * about the theory modification.
 * 
 * @author Andre Fonseca
 *
 */
public class NaiveRemoveClauseStrategy extends LocallyRemoveClauseStrategy {
	static Logger logger = LoggerFactory.getLogger(NaiveRemoveClauseStrategy.class);

	public NaiveRemoveClauseStrategy(PLogicManager manager) {
		super(manager);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void modify(Clause clause, boolean isMapping, long ttl) throws CommunicationException {
		super.modify(clause, isMapping, ttl);
		
		logger.info("Informing neighbors about possible inconsistency...");
		
		CommunicationManager communicationManager = this.plogicManager.getCommunicationManager();
		String localPeerName = communicationManager.getLocalPeer().getName();
		String messageText = "The mapping " + clause + " was removed from the peer " + localPeerName + 
				".\n Inconsistences may be introduced on the distributed instance.\n";
		
		Map<String, Peer> peerMaps = communicationManager.getPeers();
		for (Entry<String, Peer> entry : peerMaps.entrySet()) {
			communicationManager.sendMessage(entry.getKey(), messageText);
		}
	}

}
