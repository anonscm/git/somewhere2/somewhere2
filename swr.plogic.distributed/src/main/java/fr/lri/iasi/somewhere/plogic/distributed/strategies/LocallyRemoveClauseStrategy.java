/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.strategies;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.plogic.distributed.DistantLiteralImpl;
import fr.lri.iasi.somewhere.plogic.distributed.PLogicManager;
import fr.lri.iasi.somewhere.plogic.distributed.PropositionalInstanceWithMapping;
import fr.lri.iasi.somewhere.ui.CommandListener;

/**
 * Describes the removing clause locally strategy.
 * 
 * @author Andre Fonseca
 *
 */
public class LocallyRemoveClauseStrategy implements ModifyTheoryStrategy {
	static Logger logger = LoggerFactory.getLogger(LocallyRemoveClauseStrategy.class);
	
	/* --- Properties --- */
	/** Manager of the module */
    protected final PLogicManager plogicManager;
    
    /** The command listener object */
    protected CommandListener commandListener;
	
	public LocallyRemoveClauseStrategy(PLogicManager manager) {
		if (manager == null) {
            throw new IllegalArgumentException("manager");
        }	
		
		this.plogicManager = manager;
	}
	
	/* --- Mutators --- */
	@Override
	public void setListener(CommandListener listener) {
		this.commandListener = listener;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void modify(Clause clause, boolean isMapping, long ttl) throws CommunicationException {
		logger.info("Adding clause locally...");
		
		PropositionalInstanceWithMapping instance = plogicManager.getPropositionalInstance();
		
		// Removing the clause locally
    	boolean result = instance.removeClause(clause);
    	
    	if (result && isMapping) {
    		Set<Literal> notSharedLiterals = Sets.newLinkedHashSet();
    		
    		for (Literal literal : clause) {
				if (literal instanceof DistantLiteralImpl && !instance.getTheory().isInVocabulary(literal))
					notSharedLiterals.add(literal);
			}
    		
    		plogicManager.removeSharedLiterals(notSharedLiterals);
    	}
    	
    	// Inform the end of the clause addition to the listener command
    	this.commandListener.executionCompleted();
		
	}

}
