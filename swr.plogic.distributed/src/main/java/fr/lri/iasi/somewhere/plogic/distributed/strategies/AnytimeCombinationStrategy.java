/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.strategies;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.impl.ClauseBuilder;
import fr.lri.iasi.libs.plogic.impl.ConjunctionOfClausesImpl;
import fr.lri.iasi.libs.plogic.restrictions.SubsetRestriction;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.plogic.distributed.DistantLiteralImpl;

import fr.lri.iasi.somewhere.plogic.distributed.PropositionalInstanceWithMapping;

/**
 * Describes the "any time" combination strategy.
 * 
 * @author Andre Fonseca
 *
 */
public class AnytimeCombinationStrategy implements AnswerCombinationStrategy {
	static Logger logger = LoggerFactory.getLogger(AnytimeCombinationStrategy.class);
	
    /* --- Methods --- */
    /**
     * {@inheritDoc}
     */
    @Override
	public Collection<Clause> combine(PropositionalInstanceWithMapping instance,
			Clause query, 
			Clause answer, 
			Clause nonTerminal, 
			Map<Clause, Collection<Clause>> answersByQuery,
			Message message) {
		
		List<Collection<Clause>> cnfsToCombine = obtainCnfsToCombine(instance, query, answer, nonTerminal, answersByQuery);
		
		if (cnfsToCombine.isEmpty())
			return Collections.emptySet();
		
		// Combine the selected clauses
		Collection<Clause> combinations = this.combineSelectedClauses(cnfsToCombine);
		
		return combinations;
	}
	
	/**
	 * From the query literal, the obtained answer and a non-terminal answer containing the query, 
	 * obtain all the possible cnfs to be used in the combination step.
	 * 
	 * @param instance : the current loaded propositional instance
	 * @param query : the query clause
	 * @param answer : the answer obtained by the query
	 * @param concernedMapping : the non-terminal answer that contains the query literal
	 * @param answersByQuery: 
	 * @return the list of cnfs to combine
	 */
	private List<Collection<Clause>> obtainCnfsToCombine(PropositionalInstanceWithMapping instance,
			Clause query, Clause answer, 
			Clause concernedMapping, Map<Clause, Collection<Clause>> answersByQuery) {
		
		final List<Collection<Clause>> cnfsToCombine = Lists.newLinkedList();
		final SubsetRestriction subsetRestriction = (SubsetRestriction) instance.getProductionField().getRestrictions().get("subset");
		
		for (Literal lit : concernedMapping) {
			Collection<Clause> clausesToCombine = Lists.newLinkedList();
			
			if (!(lit instanceof DistantLiteralImpl) && !(instance.getSharedLiteralIndex().containsKey(lit.opposite()))) {
				if (subsetRestriction != null && subsetRestriction.discard(lit))
					continue;
				
				final ClauseBuilder builder = new ClauseBuilder();
				
				if (lit.getSign())
					builder.addLiteral(lit.getLabel());
				else
					builder.addNegativeLiteral(lit.getLabel());
				
				clausesToCombine.add(builder.create());
			} else if (instance.localViewOf(query).contains(lit)) {
				clausesToCombine.add(answer.copy());
			} else {
				final ClauseBuilder builder = new ClauseBuilder();
				builder.addLiteral(lit);
				
				Collection<Clause> answers = answersByQuery.get(builder.create());
				if (answers != null)
					clausesToCombine = Lists.newLinkedList(answers);
				
				if (clausesToCombine.isEmpty()) {
					cnfsToCombine.clear();
					break;
				}
			}
			
			if (!clausesToCombine.isEmpty())
				cnfsToCombine.add(clausesToCombine);
		}	
		
		return cnfsToCombine;	
	}
	
	/**
	 * Do the combination of the conjunction of clauses.
	 * 
	 * @param clauseConjunctionList is the list of conjunction of clauses that need to be combined
	 * @return the combined conjunction
	 */
	private List<Clause> combineSelectedClauses(List<Collection<Clause>> clauseConjunctionList) {
		
		// If there is only one rewritingSet in the list, return it
		if (clauseConjunctionList.size() == 1)
			return Lists.newLinkedList(clauseConjunctionList.get(0));
		
		// Otherwise, combine each rewritingSet with its predecessor in the list.
		ConjunctionOfClauses combinedClauseConjunction = null;
		for (Collection<Clause> clauses : clauseConjunctionList) {
			
			// The first rewritingSet will have no predecessor, so it will be not combined until the iteration of the second one.
			ConjunctionOfClauses clauseConjunction = new ConjunctionOfClausesImpl();
			for (Clause clause : clauses) {
				clauseConjunction.add(clause.copy());
			} 
			
			if (combinedClauseConjunction != null)
				clauseConjunction = combinedClauseConjunction.distribution(clauseConjunction);
			
			combinedClauseConjunction = clauseConjunction;
		}
		
		return Lists.newLinkedList(combinedClauseConjunction);
	}

}
