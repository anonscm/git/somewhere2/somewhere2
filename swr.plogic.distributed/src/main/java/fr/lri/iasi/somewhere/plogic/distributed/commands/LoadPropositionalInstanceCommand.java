/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.commands;

import fr.lri.iasi.libs.formallogic.utils.instance.ParsersFacadeListener;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.plogic.distributed.PLogicManager;
import fr.lri.iasi.somewhere.plogic.distributed.PropositionalInstanceWithMapping;
import fr.lri.iasi.somewhere.plogic.distributed.parser.PropositionalInstanceParsersFacade;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * A command to load a new propositional instance
 * @author Andre Fonseca
 */
public class LoadPropositionalInstanceCommand implements Command {
	static Logger logger = LoggerFactory.getLogger(LoadPropositionalInstanceCommand.class);
	
    /* --- Properties --- */
    /** Manager of the module */
    protected final PLogicManager manager;
    
    /** Indicates if the propositional instance has finished loading */
    private boolean finishLoading = false;

    public LoadPropositionalInstanceCommand(PLogicManager manager) {
        if (manager == null) {
            throw new IllegalArgumentException("manager");
        }

        this.manager = manager;
    }

    /* --- Accessors --- */
    public PLogicManager getManager() {
        return manager;
    }

    /**
     * @return the name of the command
     */
    public String getName() {
        return "loadPropositionalInstance";
    }
    
    /**
     * @return if the current command has finished loading the instance
     */
    public boolean hasFinishedLoading() {
        return finishLoading;
    }

    /**
     * @return the description of the command
     */
    public String getDescription() {
        return "Load a new PropositionalInstance in place of the old one " +
        "(if it already existed).";
    }

    /* --- Methods --- */
    /**
     * Execute the command
     * @param parameters The parameters of the command
     * @return the return status
     *  NOTE : note that the return status contains only text
     *  information
     */
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;

        try {
            // Verify there are enough parameters
            if (parameters.size() < 1) {
                res = new GenericReturnStatus("Syntax Error! \n Usage :" +
                        "\t" + getName() + " FILE_PATH", false, null);

                return res;
            }

            // Get useful parameters
            String path = parameters.get(0).toString();

            // Load propositional instance !
            this.finishLoading = false;
            PropositionalInstanceParsersFacade.parse(path, new Listener());

            res = new GenericReturnStatus("Successfully launched propositional instance analysis...",
                    true, null);
        } catch (Exception e) {
        	this.finishLoading = false;
            res = new GenericReturnStatus("There was an error loading " +
                    "new propositional instance: " + e.getMessage(), false, null);
        }

        return res;
    }
    
    /**
     * This class is used to know when the file loading is finished, and
     *  to update the <b>PropositionalInstance</b> of <b>PLogicManager</b>
     */
    protected class Listener implements ParsersFacadeListener<PropositionalInstanceWithMapping> {
        /**
         * {@inheritDoc}
         */
        public void loadingCompleted(PropositionalInstanceWithMapping propositionalInstance) {
            // Initializing the instance
        	propositionalInstance.initializeTheory();       	

        	// Restarting communication layer with new instance name
        	manager.setPropositionalInstance(propositionalInstance);
            manager.getCommunicationManager().getConfiguration().setPeerName(propositionalInstance.getName());
            manager.getCommunicationManager().getCommunicationModule().restart();
            
            // Reference the local peer
            Peer localPeer = new Peer(manager.getCommunicationManager().getLocalPeer().getName());
            
            // Inform neighbor peers about shared literals
        	Map<String, Set<Literal>> distantLiteralsByPeer = propositionalInstance.getDistantSharedLiteralsByPeer();
        	for (Entry<String, Set<Literal>> entry : distantLiteralsByPeer.entrySet()) {	
        		String peerName = entry.getKey();
        		if (peerName.equals(localPeer.getName()))
        			continue;
        		
        		// Retrieve neighbor
        		manager.getCommunicationManager().getCommunicationModule().getPeerGroup().retrievePeer(peerName);
        	}
            
            finishLoading = true;
        }
        
    }
}
