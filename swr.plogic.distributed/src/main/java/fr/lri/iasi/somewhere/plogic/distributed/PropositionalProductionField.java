/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import fr.lri.iasi.libs.formallogic.Restriction;
import fr.lri.iasi.libs.formallogic.utils.instance.ProductionField;
import fr.lri.iasi.libs.plogic.Clause;

/**
 * Represents a production set of a given instance.
 * 
 * @author Andre Fonseca
 */
public class PropositionalProductionField implements ProductionField<Clause> {
	private static final long serialVersionUID = -8492006615839700100L;
	
	/* --- Properties --- */
	/** Restrictions contained by this production set */
	private final Map<String, Restriction<Clause>> restrictions;
	
	public PropositionalProductionField() {
		this.restrictions = new HashMap<String, Restriction<Clause>>();
	}
	
	public PropositionalProductionField(PropositionalProductionField productionSet) {
		if (productionSet == null) {
			throw new IllegalArgumentException();
		}
		
		this.restrictions = new HashMap<String, Restriction<Clause>>(productionSet.getRestrictions());
	}
	
	/* --- Accessors --- */
	public Map<String, Restriction<Clause>> getRestrictions() {
		return Collections.unmodifiableMap(restrictions);
	}
	
	/* --- Mutators --- */
	public void addRestriction(Restriction<Clause> restriction) {
		this.restrictions.put(restriction.getName(), restriction);
	}
	
	public void removeRestriction(String name) {
		this.restrictions.remove(name);
	}
	
	/* --- Methods --- */
	/**
	 * @return a copy of the current production set
	 */
	public PropositionalProductionField copy() {
		return new PropositionalProductionField(this);
	}
	
	@Override
	public boolean equals(Object o) {
        if (o instanceof PropositionalProductionField) {
			PropositionalProductionField p = (PropositionalProductionField) o;

			return (this.restrictions.equals(p.restrictions));
        } else {
            return false;
        }
    }

	@Override
    public int hashCode() {
        return (41 * (41 + restrictions.hashCode()));
    }

	@Override
    public String toString() {
    	StringBuilder res = new StringBuilder();
    	res.append("<production>");
        
        for (Restriction<?> restriction : restrictions.values()) {
        	res.append("");
        	res.append(restriction);
        }
        
        res.append("</production>");

        return res.toString();
    }

}
