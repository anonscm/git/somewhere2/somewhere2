/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.commands;

import fr.lri.iasi.libs.formallogic.utils.instance.Instance;
import fr.lri.iasi.somewhere.plogic.distributed.PLogicManager;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import java.util.List;


/**
 * A command to show the propositional instance
 * @author Andre Fonseca
 */
public class ShowPropositionalInstanceCommand implements Command {
    /* --- Properties --- */
    /** Manager of the module */
    protected final PLogicManager manager;

    public ShowPropositionalInstanceCommand(PLogicManager manager) {
        if (manager == null) {
            throw new IllegalArgumentException("manager");
        }

        this.manager = manager;
    }

    /* --- Accessors --- */
    public PLogicManager getManager() {
        return manager;
    }

    /**
     * @return the name of the command
     */
    public String getName() {
        return "showPropositionalInstance";
    }

    /**
     * @return the description of the command
     */
    public String getDescription() {
        return "Show the propositional instance";
    }

    /**
     * Execute the command
     * @param parameters The parameters of the command
     * @return the return status
     *  NOTE : note that the return status contains only text
     *  information
     */
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;

        try {
        	Instance pi = manager.getPropositionalInstance();

            String str = pi.toString();

            res = new GenericReturnStatus(str, true, null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error showing the " +
                    "propositional instance ...", false, null);
        }

        return res;
    }
}
