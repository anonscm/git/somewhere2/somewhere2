/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.parser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.libs.formallogic.utils.instance.AbstractInstanceParser;
import fr.lri.iasi.libs.formallogic.utils.instance.Instance;
import fr.lri.iasi.libs.formallogic.utils.instance.ProductionField;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.impl.ClauseBuilder;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryImpl;
import fr.lri.iasi.somewhere.plogic.distributed.PropositionalInstanceWithMapping;
import fr.lri.iasi.somewhere.plogic.distributed.PropositionalProductionField;

/**
 * Parse the content of a DIMACS file into a local propositional instance
 * @author Andre Fonseca
 */
public class DIMACSInstanceParser extends AbstractInstanceParser{
	static Logger logger = LoggerFactory.getLogger(DIMACSInstanceParser.class);

	/**
     * @param path Path of the input DIMACS file
     */
	public DIMACSInstanceParser(String path) throws FileNotFoundException {
		super(path);
	}

	/* --- Methods --- */
	/**
     * {@inheritDoc}
     */
	@Override
	public Instance parse() {
		PropositionalInstanceWithMapping res = null;

        try {
            // Open the file
            BufferedReader in = new BufferedReader(new FileReader(path));

            // Parse everything
            res = parse(in);

            return res;
        } catch (Exception e) {
            logger.error(
                "There were errors parsing DIMACS configuration file : {} ", e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
	}
	
	/**
     * Parse the entire DIMACS file to obtain corresponding to the local theory
     * @param in BufferedReader to access to the file
     * @return the PropositionalInstance obtained
     */
    private PropositionalInstanceWithMapping parse(BufferedReader in)
        throws IOException {
    	
        String peerName = "local";
        PropositionalClausalTheory theory = parseTheory(in);
        PropositionalClausalTheory mapping = new PropositionalClausalTheoryImpl();
        ProductionField<Clause> production = new PropositionalProductionField();

        return new PropositionalInstanceWithMapping(peerName, theory,
            production, mapping);
    }
    
    /**
     * Parse DIMACS file
     */
    private PropositionalClausalTheory parseTheory(BufferedReader in)
        throws IOException {
        PropositionalClausalTheory result = new PropositionalClausalTheoryImpl();

        String str;

        int i = 0;
        while ((str = in.readLine()) != null) {
        	if (str.startsWith("c") || str.startsWith("p cnf"))
                continue;

            ClauseBuilder builder = new ClauseBuilder();

            for (String token : str.split("\\s+")) {
            	if (token.equals("0"))
            		break;
            	
            	if (token.startsWith("-"))
                	builder.addNegativeLiteral(token.substring(1));
                else
                	builder.addLiteral(token);
            }
            
            builder.setLabel("localPeer:" + i);
            i++;

            result.add(builder.create());
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
	@Override
	public void export(String path) {
		BufferedWriter out = null;

        try {
            FileWriter fstream = new FileWriter(this.instance.getName() + ".cnf");
            out = new BufferedWriter(fstream);

            exportHeader(out);
            exportTheory(out);
        } catch (Exception e) {
            logger.error("There was a problem exporting " +
                "propositionnal instance to file '" + path + "' !");
        } finally {
            try {
                out.close();
            } catch (Exception e) {
                // Nothing ...
            }
        }
	}
	
	/**
     * Export to instance to DIMACS file, headers part
     * @param out BufferedWriter used to write stuffs
     */
    private void exportHeader(BufferedWriter out) throws IOException {
    	PropositionalInstanceWithMapping instance = (PropositionalInstanceWithMapping) this.instance;
    	
    	Set<Literal> variables = new HashSet<Literal>();
    	for (Clause clause : instance.getTheory()) {
    		for (Literal literal : clause) {
    			variables.add(literal);
			}
		}
    	
    	int clauseNumber = instance.getTheory().size();
    	int variablesNumber = variables.size();
    	
        out.write("p cnf " + variablesNumber + " " + clauseNumber + "\n");
    }
    
    /**
     * Export to instance to DIMACS file, part 'theory'
     * @param out BufferedWriter used to write stuffs
     */
    private void exportTheory(BufferedWriter out) 
    	throws IOException {
    	
    	PropositionalInstanceWithMapping propositionalInstance = (PropositionalInstanceWithMapping) this.instance;
    	
        for (Clause c : propositionalInstance.getLocalTheory()) {
            for (Literal l : c) {
            	if (l.getSign())
            		out.write(l.getLabel() + " ");
            	else
            		out.write("-" + l.getLabel() + " ");
            }

            out.write("0\n");
        }
    }

}
