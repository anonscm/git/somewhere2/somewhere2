/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import fr.lri.iasi.libs.formallogic.Restriction;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses.ResultPair;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryImpl;
import fr.lri.iasi.libs.plogic.restrictions.LenghtRestriction;
import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.model.FinalAnswer;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.MessageType;
import fr.lri.iasi.somewhere.api.communication.model.MessageTypeSingletonFactory;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageProcessor;


import fr.lri.iasi.somewhere.plogic.distributed.annotations.SubsumptionAnnotation;
import fr.lri.iasi.somewhere.plogic.distributed.strategies.AnswerCombinationStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.strategies.PLogicDistributedStrategiesFactory;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicQueryMessageType;

/**
 * Processor responsible for the "Answer" message type. Launches
 * the combination strategy for received messages corresponding to the
 * query that created the related {@link QueryProcessor.}
 * 
 * @author Andre Fonseca
 *
 */
public class PLogicAnswerProcessor extends AbstractMessageProcessor {
	static Logger logger = LoggerFactory.getLogger(PLogicAnswerProcessor.class);
	
	/* --- Properties --- */
	/** Module Manager to be used */
    private final PLogicManager plogicManager;
    
    /** Set which contains the local answers which contains at least one external literal. */
    private final ConcurrentHashMap<Clause, Set<Clause>> nonTerminalAnswersMap;

	/** All the terminal answers for a query in this session */
    private final PropositionalClausalTheory terminalAnswers;
	
	/** Final answers counter (answers that are sent to the user) */
    private final AtomicInteger answerCount;
	
	/** Final answers map indicating its answer number */
    private final Map<Clause, Integer> finalAnswerMap;
	
	/** Map that stores a list of answer clauses by the query clause */
    private final ConcurrentHashMap<Clause, Collection<Clause>> answersByQuery;
	
	/** The combination strategy to be used. */
    private AnswerCombinationStrategy combinationStrategy;
    
    /** The related query processor */
    private PLogicQueryProcessor queryProcessor;
    
    public PLogicAnswerProcessor(MessageSession session) {
        super(session);
        
        App app = session.getCommunicationManager().getApp();
        this.plogicManager = PLogicManager.getModuleInstance(app);
        this.terminalAnswers = new PropositionalClausalTheoryImpl();
		this.nonTerminalAnswersMap = new ConcurrentHashMap<Clause, Set<Clause>>();
		this.finalAnswerMap = new HashMap<Clause, Integer>();
		this.answersByQuery = new ConcurrentHashMap<Clause, Collection<Clause>>();
		this.answerCount = new AtomicInteger();
		this.combinationStrategy = PLogicDistributedStrategiesFactory.getInstance().getAnswerCombinationStrategy();
    }
    
    /* --- Accessors --- */
    PropositionalClausalTheory getTerminalAnswers() {
    	return terminalAnswers;
    }
    
    Map<Clause, Integer> getFinalAnswerMap() {
    	return finalAnswerMap;
    }
    
    AtomicInteger getAnswerCount() {
    	return answerCount;
    }
    
    Set<Clause> getNonTerminalAnswers(Clause clause) {
    	Set<Clause> clauses = this.nonTerminalAnswersMap.get(clause);
    	if (clauses == null) {
    		clauses = Collections.newSetFromMap(new ConcurrentHashMap<Clause, Boolean>());
    		this.nonTerminalAnswersMap.putIfAbsent(clause, clauses);
    	}
    		
    	return clauses;
    }

    /* --- Methods --- */
	/**
     * {@inheritDoc}
     */
	@Override
	public void process(Message message) {
		
		// Get useful variables
		final MessageType queryType = MessageTypeSingletonFactory.create(PLogicQueryMessageType.class);
		this.queryProcessor = (PLogicQueryProcessor) this.session.getProcessor(queryType);
		final PropositionalInstanceWithMapping instance = this.plogicManager.getPropositionalInstance();
		final Clause startingQuery = this.queryProcessor.getStartingQuery();
		final Set<Restriction<Clause>> restrictions = this.queryProcessor.getPropagationRestrictions();
		final LenghtRestriction lenghtRestriction = (LenghtRestriction) instance.getProductionField().getRestrictions().get("lenght");
		if (lenghtRestriction != null)
			restrictions.add(lenghtRestriction);

		// Collecting answer content
		final Collection<Clause> answers = PLogicManager.ANSWERS.getContent(message);
		final Map<String, Set<Clause>> history = PLogicManager.HISTORY.getContent(message);
		final Clause query = PLogicManager.QUERY.getContent(message);
		
		// Updating the "arrived answers by query" map
		Collection<Clause> arrivedAnswers = this.answersByQuery.get(query);
		if (arrivedAnswers == null) {
			arrivedAnswers = Lists.newLinkedList();
			Clause localQueryView = instance.localViewOf(query);
			this.answersByQuery.put(localQueryView, arrivedAnswers);
		}
		
		final Collection<Clause> updatedAnswers = Sets.newHashSet();
		for (Clause answer : answers) {
			Clause updatedAnswer = instance.distantViewOf(message.getOrigin().getName(), answer);
			updatedAnswer = instance.localViewOf(updatedAnswer);
			arrivedAnswers.add(updatedAnswer);
			updatedAnswers.add(updatedAnswer);
		}
		
		logger.trace("Arrived answers {} for query {}", updatedAnswers, query);
		
		// If the answer is unsat, it needs to be combined with every non terminal clause. Otherwise, only with the concerned ones.
		Set<Clause> nonTerminalAnswers = this.nonTerminalAnswersMap.get(query);
		if (nonTerminalAnswers == null || nonTerminalAnswers.isEmpty())
			return;
		
		// Do the combination of received answers!
		for (Clause nonTerminal : nonTerminalAnswers) {
			
			for (Clause answer : updatedAnswers) {
				
				// Combine the selected clauses using the selected strategy
				Collection<Clause> combinations = this.combinationStrategy.combine(instance, 
						query, answer, nonTerminal, this.answersByQuery, message);
				
				logger.trace("Obtained combinations: {}", combinations);
				
				List<FinalAnswer<Clause>> finalAnswers = Lists.newArrayList();
				Iterator<Clause> it = combinations.iterator();
				combinationsLoop:
				while (it.hasNext()) {
					Clause combination = it.next();
		        	
		        	// Restrict results by the propagation restrictions
		        	for (Restriction<Clause> restriction : restrictions) {
						if (restriction.discard(combination)) {
							logger.trace("Combination {} discarded by: {}", combination, restriction);
							it.remove();
			        		continue combinationsLoop;
						}
					}
					
		        	// Verify subsumptions
		        	Collection<Clause> subsumedClauses = null;
					synchronized (this.terminalAnswers) {
						ResultPair result = this.terminalAnswers.addSubsuming(combination);
						// If the first information of the result is false, the combination was subsumed.
						if (!result.getFirst()) {
							logger.trace("Combination {} subsumed by clause contained on terminal clauses theory.", combination);
							it.remove();
							continue;
						}
						
						// The second information of the result are the subsumed clauses.
						subsumedClauses = result.getSecond();
						logger.trace("Combination {} subsumes the following clauses : {}", combination, subsumedClauses);
					}
					
					if (this.session.getCreatorPeerName().equals("User")) {
						FinalAnswer<Clause> finalAnswer = produceFinalAnswer(combination, subsumedClauses, queryProcessor.getPropagationStartTime());
						finalAnswers.add(finalAnswer);
						logger.trace("Combination {} sent to the user.", combination);
					}
				}

				// Return combined answers to neighbor peer or user
				try {
					if (this.session.getCreatorPeerName().equals("User")) {
						for (FinalAnswer<Clause> finalAnswer : finalAnswers) {
							this.plogicManager.returnAnswersForUser(this.session, finalAnswer, startingQuery, terminalAnswers.size());
						}
					} else if (!combinations.isEmpty()) {
						this.plogicManager.returnProducedAnswers(this.session, this.session.getCreatorPeerName(), startingQuery, history, combinations);
						logger.trace("Combinations {} sent to {}.", combinations, this.session.getCreatorPeerName());
					}
				} catch (CommunicationException e) {
					logger.error("There were problems sending the answer message: {}", e.getMessage());
				}
			}
		}
	}

	/**
	 * Add final answer annotations on the clause.
	 * @param clause
	 * @param subsumedClauses
	 * @param replacedClauses
	 * @param propagationStartTime
	 */
	private FinalAnswer<Clause> produceFinalAnswer(Clause clause, Collection<Clause> subsumedClauses, long propagationStartTime) {
		FinalAnswer<Clause> finalAnswer = new FinalAnswer<Clause>();
		finalAnswer.setContent(clause);
		finalAnswer.setNumber(this.answerCount.incrementAndGet());
		finalAnswer.setResponseTime(propagationStartTime);
		
		// Adding subsumption annotation
		if (subsumedClauses != null) {
			Set<Integer> subsumptionSet = Sets.newTreeSet();
			for (Clause subsumedClause : subsumedClauses) {
				Integer answernumber = this.finalAnswerMap.get(subsumedClause);
				
				if (answernumber != null)
					subsumptionSet.add(answernumber);
			}
			
			if (!subsumptionSet.isEmpty())
				finalAnswer.addAnnotation(new SubsumptionAnnotation(subsumptionSet));
		}
		
		this.finalAnswerMap.put(clause, this.answerCount.get());
		return finalAnswer;
	}
}
