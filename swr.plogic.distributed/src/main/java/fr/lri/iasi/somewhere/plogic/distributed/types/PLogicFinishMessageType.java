/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.types;

import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageProcessor;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageType;
import fr.lri.iasi.somewhere.plogic.distributed.PLogicFinishProcessor;


/**
 * The finish message type of the plogic.distributed module.
 * 
 * @author Andre Fonseca
 *
 */
public final class PLogicFinishMessageType extends AbstractMessageType {
	private static final long serialVersionUID = -8368335910537013905L;
	
	/**
     * {@inheritDoc}
     */
	@Override
	public String getName() {
		return "finish";
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public boolean isReturningType() {
		return true;
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public MessageProcessor createProcessor(MessageSession session) {
		return new PLogicFinishProcessor(session);
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public boolean needsNewSessionSend() {
		return false;
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public boolean needsNewSessionReceive() {
		return false;
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public boolean needsToMaintainSessionSend(MessageSession session) {
		return false;
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public boolean needsToMaintainSessionReceive(MessageSession session) {
		return true;
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public String print(Message message) {
		String res = "";

        res += "==============================================================================";
        res += ("\n\t Finish message");
        res += ("\n\t Origin : " + message.getOrigin().getName());
        res += ("\n\t id : " + message.getId());

        return res;
	}
}
