/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.libs.formallogic.Annotation;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.impl.LiteralImpl;
import fr.lri.iasi.somewhere.api.communication.model.Peer;


/**
 * Describes a propositional literal stored on a distant peer
 * @author Andre Fonseca
 */
public class DistantLiteralImpl extends LiteralImpl {
    static Logger logger = LoggerFactory.getLogger(DistantLiteralImpl.class);
    private static final long serialVersionUID = 745838945445L;

    /* --- Properties --- */
    protected final Peer peer;

    public DistantLiteralImpl(String peerName, String id, boolean sign) {
        super(id, sign);

        if (peerName == null) {
            throw new IllegalArgumentException("peerName");
        }

        this.peer = new Peer(peerName);
    }
    
    public DistantLiteralImpl(String peerName, String id, boolean sign, Map<String, Annotation<?,?>> annotations) {
    	this(peerName, id, sign);
    	this.annotations.putAll(annotations);
    }
    
    public DistantLiteralImpl(Peer peer, String id, boolean sign) {
        this(peer.getName(), id, sign);
    }
    
    public DistantLiteralImpl(Peer peer, String id, boolean sign, Map<String, Annotation<?,?>> annotations) {
        this(peer.getName(), id, sign, annotations);
    }

    public DistantLiteralImpl(String peerName, String id) {
        super(id);

        if (peerName == null) {
            throw new IllegalArgumentException("peerName");
        }

        this.peer = new Peer(peerName);
    }
    
    public DistantLiteralImpl(Peer peer, String id) {
        this(peer.getName(), id);
    }
    
    public DistantLiteralImpl(String peerName, Literal literal) {
        super(literal.getLabel(), literal.getSign());

        if (peerName == null) {
            throw new IllegalArgumentException("peerName");
        }

        this.peer = new Peer(peerName);
        this.annotations.putAll(literal.getAnnotations());
    }
    
    public DistantLiteralImpl(Peer peer, Literal literal) {
    	this(peer.getName(), literal);
    }

    /* --- Accessors --- */
    public Peer getPeer() {
        return peer;
    }

    public String getPeerName() {
        return peer.getName();
    }

    /* --- Methods --- */
    /**
	 * {@inheritDoc}
	 */
	@Override
	public boolean canBeDiscarded() {
		return false;
	}
	
    /**
     * {@inheritDoc}
     */
    @Override
    public Literal copy() {
        return new DistantLiteralImpl(peer, this);
    }
    
    /**
     * {@inheritDoc}
     */
	@Override
	public Literal opposite() {
		return new DistantLiteralImpl(peer, label, !sign, annotations);
	}
	
	/**
	 * Returns a copy of the distant literal as it belongs to the local theory.
	 */
	public Literal toLocal() {
		return new LiteralImpl(label, sign, annotations);
	}
	
	@Override
    public boolean equals(Object o) {
        if (o instanceof DistantLiteralImpl) {
            DistantLiteralImpl l = (DistantLiteralImpl) o;

            return ((this.label.equals(l.label)) && 
            		(this.sign == l.sign) && 
            		(this.peer.getName().equals(l.peer.getName())));
        } else {
            return false;
        }
    }
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((peer == null) ? 0 : peer.hashCode());
		return result;
	}
    
    @Override
    public String toString() {
        String signStr = "";

        if (!sign) {
            signStr = "!";
        }

        return signStr + peer.getName() + ":" + label;
    }
    
    @Override
    public String toXml() {
        String signStr = "";

        if (!sign) {
            signStr = "</not>";
        }

        return "<literal url=\""+ peer.getName() + "\">" + signStr + label + "</literal>";
    }
}
