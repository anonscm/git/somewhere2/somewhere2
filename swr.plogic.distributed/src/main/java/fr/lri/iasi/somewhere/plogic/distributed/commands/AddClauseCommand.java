/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.commands;

import java.text.DecimalFormat;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.somewhere.plogic.distributed.MappingBuilder;
import fr.lri.iasi.somewhere.plogic.distributed.PLogicManager;
import fr.lri.iasi.somewhere.plogic.distributed.PropositionalInstanceWithMapping;
import fr.lri.iasi.somewhere.plogic.distributed.parser.SWRInstanceParser.SWRLexem;
import fr.lri.iasi.somewhere.plogic.distributed.strategies.ModifyTheoryStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.strategies.PLogicDistributedStrategiesFactory;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

/**
 * A blocking ui command to add a propositional clause to the local theory
 * @author Andre Fonseca
 *
 */
public class AddClauseCommand extends AbstractBlockingUICommand {
	static Logger logger = LoggerFactory.getLogger(AddClauseCommand.class);
	
	/* --- Properties --- */
    /** Manager of the module */
    protected final PLogicManager manager;
    
    /** The modify theory strategy */
    protected ModifyTheoryStrategy strategy;
    
    public AddClauseCommand(PLogicManager manager) {
    	super();
    	
    	if (manager == null) {
            throw new IllegalArgumentException("manager");
        }
    	
    	this.manager = manager;
    	this.strategy = PLogicDistributedStrategiesFactory.getInstance().getAddFormulaToInstanceStrategy();
    	this.strategy.setListener(commandListener);
    }
    
    /* --- Accessors --- */
    public PLogicManager getManager() {
        return manager;
    }

    /**
     * {@inheritDoc}
     */
	@Override
	public String getName() {
		return "addClause";
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public String getDescription() {
		return "Add clause (or mapping) to the current instance";
	}

	/* --- Methods --- */
	/**
	 * Add command hook
	 */
	public void setModifyTheoryStrategy(ModifyTheoryStrategy strategy) {
		this.strategy = strategy;
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public ReturnStatus execute(List<Object> parameters) {
		ReturnStatus res = null;
		String clauseStr = null;
		Joiner joiner = Joiner.on(" ").skipNulls();
        String paramStr = joiner.join(parameters);

        try {
        	// Clause param
        	int actualParamIndex = paramStr.indexOf("-");
        	if (actualParamIndex != -1)
        		clauseStr = paramStr.substring(0, actualParamIndex);
        	else
        		clauseStr = paramStr;
        	
        	int actualParametersSize = 0;
        	if (!clauseStr.isEmpty())
        		actualParametersSize++;
        	
        	// TTL param
        	long ttl = 60000;
        	int ttlIndex = parameters.indexOf("-ttl");
        	if (ttlIndex != -1) {
        		Object ttlObject = Iterables.get(parameters, ttlIndex + 1, null);
    		
    			if (ttlObject != null) {
    				ttl = Integer.parseInt(ttlObject.toString()) * 1000;
    				actualParametersSize++;
    			}
        	}
        	
            // Verify there are enough parameters
            if (actualParametersSize < 1 || actualParametersSize > 2) {
                res = new GenericReturnStatus("Syntax Error! \n Usage :" +
                        "\t" + getName() + " CLAUSE [-ttl SECONDS]",
                        false, null);

                return res;
            }
            
            // If there is a loaded instance
            PropositionalInstanceWithMapping instance = manager.getPropositionalInstance();
            if (instance == null) {
            	res = new GenericReturnStatus("Instance not loaded! Load an instance before adding a clause.", false, null);

                return res;
            }
            
            // Mapping builder will be used since the clause to be added can have distant literals
        	MappingBuilder builder = new MappingBuilder();
        	
        	// Verify local peer name
        	String localPeerName = manager.getCommunicationManager().getLocalPeer().getName();
        	Iterable<Clause> mappings = instance.getMappings();
        	int size = Iterables.size(mappings);
            	
        	// Parsing clause string
        	boolean isMapping = false;
        	String[] literals = clauseStr.split("\\s+v\\s+");
        	for (String litStr : literals) {
        		SWRLexem lexem = new SWRLexem(litStr);
                
        		boolean sign = lexem.getSign();
                String peerName = lexem.getPeerName();
                String varName = lexem.getVarName();
                
                if (sign) {
                	if (peerName == null || peerName.isEmpty() || peerName.equals(localPeerName)) {
                		builder.addLiteral(varName);
                	} else {
                		builder.addLiteral(peerName, varName);
                		isMapping = true;
                	}
                } else {
                	if (peerName == null || peerName.isEmpty() || peerName.equals(localPeerName)) {
                		builder.addNegativeLiteral(varName);
                	} else {
                		builder.addNegativeLiteral(peerName, varName);
                		isMapping = true;
                	}
                }
			}
        	
        	// Set the label of this clause if it is a mapping
        	if (isMapping)
        		builder.setLabel(localPeerName + ":m" + size);
        	
        	this.executionCompleted.set(false);
        	this.initialTime = System.nanoTime();
        	
        	// Building clause
        	Clause toBeAdded = builder.create();

        	// Execute hooks
        	this.strategy.setListener(this.commandListener);
        	this.strategy.modify(toBeAdded, isMapping, ttl);
        	
        	// Calculate the execution time and finish the query command
            double execTime = waitExecutionToComplete(this.initialTime) / 1000000000.0;
            
            DecimalFormat decimal = new DecimalFormat( "0.0000" );
        	res = new GenericReturnStatus("Process of the clause addition " + toBeAdded + " finished!" +
        			"\nAll results obtained in " + decimal.format(execTime) + " seconds.", true, null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error adding clause " + clauseStr + " : " + e.getMessage(),
                    false, null);
        }

        return res;
	}

}
