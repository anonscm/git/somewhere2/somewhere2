/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca This file must be used under the terms of the
 * CeCILL. This source file is licensed as described in the file LICENSE, which you should have received as
 * part of this distribution. The terms are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import fr.lri.iasi.libs.formallogic.Restriction;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.strategies.impl.StrategiesConfigurator;
import fr.lri.iasi.somewhere.AbstractModule;
import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.FinalAnswer;
import fr.lri.iasi.somewhere.api.communication.model.MessageBuilder;
import fr.lri.iasi.somewhere.api.communication.model.MessageContentKey;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.MessageTypeSingletonFactory;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.plogic.distributed.commands.AddClauseCommand;
import fr.lri.iasi.somewhere.plogic.distributed.commands.CheckLoadedInstanceCommand;
import fr.lri.iasi.somewhere.plogic.distributed.commands.LoadPropositionalInstanceCommand;
import fr.lri.iasi.somewhere.plogic.distributed.commands.QueryCommand;
import fr.lri.iasi.somewhere.plogic.distributed.commands.RemoveClauseCommand;
import fr.lri.iasi.somewhere.plogic.distributed.commands.ShowPropositionalInstanceCommand;
import fr.lri.iasi.somewhere.plogic.distributed.commands.ShowSharedLiteralsCommand;
import fr.lri.iasi.somewhere.plogic.distributed.configuration.PLogicXMLConfiguration;
import fr.lri.iasi.somewhere.plogic.distributed.strategies.AnytimeCombinationStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.strategies.LocallyAddClauseStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.strategies.LocallyRemoveClauseStrategy;
import fr.lri.iasi.somewhere.plogic.distributed.strategies.PLogicDistributedStrategiesFactory;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicTimeoutMessageType;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicAddSharedLiteralType;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicAnswerMessageType;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicFinishMessageType;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicQueryMessageType;
import fr.lri.iasi.somewhere.plogic.distributed.types.PLogicRemoveSharedLiteralType;
import fr.lri.iasi.somewhere.ui.UI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.google.common.collect.Iterables;

/**
 * The main class of the PLogic Distributed module
 *
 * @author Andre Fonseca
 */
public class PLogicManager extends AbstractModule {

    static Logger logger = LoggerFactory.getLogger(PLogicManager.class);

    /* --- Constants --- */
    /**
     * The name of this module
     */
    protected static final String moduleName = "plogic.dist";

    /* --- Properties --- */
    /**
     * The Propositional instance
     */
    protected PropositionalInstanceWithMapping propositionalInstance;
    /**
     * The communication manager
     */
    private final CommunicationManager communicationManager;
    /**
     * The configuration for this module
     */
    protected final PLogicXMLConfiguration configuration;
    /**
     * The listener responsible for updating the shared literals index when a peer enters or leaves the
     * network
     */
    protected final SharedLiteralsUpdater sharedLiteralsUpdater;
    /**
     * Create/Find final answer size
     */
    public static final MessageContentKey<Integer> FINAL_ANSWER_SIZE = MessageContentKey.create("finalAnswerSize");
    /**
     * Create/Find final answers content type
     */
    public static final MessageContentKey<FinalAnswer<Clause>> FINAL_ANSWERS = MessageContentKey.create("finalAnswers");
    /**
     * Create/Find answers content type
     */
    public static final MessageContentKey<Collection<Clause>> ANSWERS = MessageContentKey.create("answers");
    /**
     * Create/Find history content type
     */
    public static final MessageContentKey<Map<String, Set<Clause>>> HISTORY = MessageContentKey.create("history");
    /**
     * Create/Find query content type
     */
    public static final MessageContentKey<Clause> QUERY = MessageContentKey.create("query");
    /**
     * Create/Find answer number content type
     */
    public static final MessageContentKey<Integer> ANSWER_NUMBER = MessageContentKey.create("answerNumber");
    /**
     * Create/Find shared literal content type
     */
    public static final MessageContentKey<Set<Literal>> SHARED_LITERALS = MessageContentKey.create("sharedLiteral");
    /**
     * Create/Find production field content type
     */
    public static final MessageContentKey<Collection<Restriction<Clause>>> PRODUCTION_FIELDS = MessageContentKey.create("productionFields");

    public PLogicManager(App app) {
        super(app);

        this.communicationManager = CommunicationManager.getModuleInstance(app);

        if (this.communicationManager == null) {
            throw new UnsupportedOperationException(
                    "A communicationManager module must be loaded");
        }

        // Set configuration object
        this.configuration = new PLogicXMLConfiguration(this);

        // Set the shared literals updater
        this.sharedLiteralsUpdater = new SharedLiteralsUpdater(this);

        // Add strategies to be used.
        addSelectedStrategies();

        // Add Commands to the UI
        addBaseCommands();
    }

    /* --- Accessors --- */
    public CommunicationManager getCommunicationManager() {
        return communicationManager;
    }

    public PropositionalInstanceWithMapping getPropositionalInstance() {
        return propositionalInstance;
    }

    /* --- Mutators --- */
    public void setPropositionalInstance(
            PropositionalInstanceWithMapping propositionalInstance) {
        if (propositionalInstance == null) {
            throw new IllegalArgumentException("propositionalInstance");
        }

        this.propositionalInstance = propositionalInstance;
    }

    /* --- Methods --- */
    /**
     * Return the current PLogic manager instance
     */
    public static PLogicManager getModuleInstance(App app) {
        if (app == null) {
            throw new IllegalArgumentException("app");
        }

        Module m = app.getModule(moduleName);

        if (m == null) {
            return null;
        }

        try {
            PLogicManager res = (PLogicManager) m;

            return res;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Add strategies to be used on the distributed calculus
     */
    private void addSelectedStrategies() {
        PLogicDistributedStrategiesFactory.getInstance().setAddFormulaToInstanceStrategy(new LocallyAddClauseStrategy(this));
        PLogicDistributedStrategiesFactory.getInstance().setRemoveFormulaFromInstanceStrategy(new LocallyRemoveClauseStrategy(this));
        PLogicDistributedStrategiesFactory.getInstance().setAnswerCombinationStrategy(new AnytimeCombinationStrategy());
    }

    /**
     * Add plogic commands to the UI
     */
    protected void addBaseCommands() {
        UI ui = app.getUI();
        ui.addCommand(new LoadPropositionalInstanceCommand(this));
        ui.addCommand(new QueryCommand(this));
        ui.addCommand(new ShowPropositionalInstanceCommand(this));
        ui.addCommand(new CheckLoadedInstanceCommand(this));
        ui.addCommand(new AddClauseCommand(this));
        ui.addCommand(new RemoveClauseCommand(this));
        ui.addCommand(new ShowSharedLiteralsCommand(this));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getModuleName() {
        return moduleName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void quit() {
        // ...
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void init() {
        // ...
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void importConfigXML(Node config) {
        configuration.parseConfig(config);

        String datatype = configuration.getDataType();
        if (!datatype.equalsIgnoreCase(StrategiesConfigurator.getInstance().getDataType())) {
            StrategiesConfigurator.getInstance().setDataType(configuration.getDataType());

            if (propositionalInstance == null) {
                return;
            }

            logger.info("Realoding propositional instance...");

            // Reload propositional instance
            this.propositionalInstance = new PropositionalInstanceWithMapping(this.propositionalInstance);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Node exportConfigXML() {
        try {
            if (configuration == null) {
                DocumentBuilder builder = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                Document doc = builder.newDocument();
                Element element = doc.createElement(getModuleName());
                doc.appendChild(element);

                return doc;
            } else {
                return configuration.export();
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Controls the receiving of a query message from the user
     *
     * @param query : the user query
     * @throws CommunicationException
     */
    public void receiveQueryFromUser(Clause query, long ttl)
            throws CommunicationException {

        receiveQueryFromUser(query, ttl, null);
    }

    /**
     * Controls the receiving of a query message from the user
     *
     * @param query : the user query
     * @param restiction : a restriction to be propagated along with the query message
     * @throws CommunicationException
     */
    public void receiveQueryFromUser(Clause query, long ttl, Restriction<Clause> restriction)
            throws CommunicationException {

        Peer userPeer = new Peer("User");
        Peer localPeer = new Peer(this.communicationManager.getLocalPeer().getName());

        Set<Restriction<Clause>> restrictions = new HashSet<Restriction<Clause>>();
        if (restriction != null) {
            restrictions.add(restriction);
        }

        // Create the message !
        MessageBuilder builder = new MessageBuilder(this.communicationManager)
                .setOrigin(userPeer)
                .setDestination(localPeer)
                .setType(MessageTypeSingletonFactory.create(PLogicQueryMessageType.class));

        PLogicManager.HISTORY.addContent(builder, new ConcurrentHashMap<String, Set<Clause>>());
        PLogicManager.QUERY.addContent(builder, query);
        PLogicManager.PRODUCTION_FIELDS.addContent(builder, restrictions);
        CommunicationManager.TTL.addContent(builder, ttl);

        this.communicationManager.receiveMessage(builder.build());
    }

    /**
     * Return the answers, produced locally, to the user or to a distant peer.
     *
     * @param session : the session responsible for sending the answer
     * @param answers : the finalAnswers
     * @param query : the starting query
     * @param finalAnswerSize : size of the final answer clause collection
     * @throws CommunicationException
     */
    void returnAnswersForUser(MessageSession session, FinalAnswer<Clause> finalAnswers, Clause query, int finalAnswerSize)
            throws CommunicationException {

        MessageBuilder builder = new MessageBuilder(this.communicationManager);

        PLogicManager.FINAL_ANSWER_SIZE.addContent(builder, finalAnswerSize);
        PLogicManager.FINAL_ANSWERS.addContent(builder, finalAnswers);
        PLogicManager.QUERY.addContent(builder, query);

        session.sendMessage(builder,
                "User",
                MessageTypeSingletonFactory.create(PLogicAnswerMessageType.class));
    }

    /**
     * Propagate query to distant peer
     *
     * @param session : the session responsible for sending the message.
     * @param destinationName : the name of the destination peer
     * @param query: the query to be sent
     * @param history: the history of the current propagation
     * @param restrictions: restrictions used on the current propagation
     * @param ttl: the remaining ttl of the message.
     * @throws CommunicationException
     */
    void propagateQuery(MessageSession session,
            String destinationName,
            Clause query,
            Map<String, Set<Clause>> history,
            Collection<Restriction<Clause>> restrictions,
            long ttl)
            throws CommunicationException {

        MessageBuilder builder = new MessageBuilder(this.communicationManager);
        PLogicManager.HISTORY.addContent(builder, history);
        PLogicManager.QUERY.addContent(builder, query);
        PLogicManager.PRODUCTION_FIELDS.addContent(builder, restrictions);
        CommunicationManager.TTL.addContent(builder, ttl);

        session.sendMessage(builder,
                destinationName,
                MessageTypeSingletonFactory.create(PLogicQueryMessageType.class));
    }

    /**
     * Sending an 'add shared literal' message to neighbor peer
     *
     * @param sharedLiterals : the literals that are being declared as shared.
     * @throws CommunicationException
     */
    public void informSharedLiterals(Set<Literal> sharedLiterals) throws CommunicationException {
        // Reference the local peer
        Peer localPeer = new Peer(this.communicationManager.getLocalPeer().getName());
        DistantLiteralImpl distantLiteral = (DistantLiteralImpl) Iterables.getFirst(sharedLiterals, null);
        Peer distantPeer = distantLiteral.getPeer();

        // Create the message !
        MessageBuilder builder = new MessageBuilder(this.communicationManager)
                .setOrigin(localPeer)
                .setDestination(distantPeer)
                .setType(MessageTypeSingletonFactory.create(PLogicAddSharedLiteralType.class));

        PLogicManager.SHARED_LITERALS.addContent(builder, sharedLiterals);

        this.communicationManager.sendMessage(builder.build());
    }

    /**
     * Sending a 'remove shared literal' message to neighbor peer
     *
     * @param notSharedLiterals : literals that are not shared anymore
     * @throws CommunicationException
     */
    public void removeSharedLiterals(Set<Literal> notSharedLiterals) throws CommunicationException {
        // Reference the local peer
        Peer localPeer = new Peer(this.communicationManager.getLocalPeer().getName());
        DistantLiteralImpl distantLiteral = (DistantLiteralImpl) Iterables.getFirst(notSharedLiterals, null);
        Peer distantPeer = distantLiteral.getPeer();

        // Create the message !
        MessageBuilder builder = new MessageBuilder(this.communicationManager)
                .setOrigin(localPeer)
                .setDestination(distantPeer)
                .setType(MessageTypeSingletonFactory.create(PLogicRemoveSharedLiteralType.class));

        PLogicManager.SHARED_LITERALS.addContent(builder, notSharedLiterals);

        this.communicationManager.sendMessage(builder.build());
    }

    /**
     * Return the answers, produced locally, to the user or to a distant peer.
     *
     * @param session : the session responsible for sending the answer
     * @param originName : the peer that sent the query where the answers must be sent
     * @param query : the query sent by the origin peer.
     * @param history : the history of queries sent during this propagation.
     * @param answers : the answer clauses.
     * @throws CommunicationException
     */
    void returnProducedAnswers(MessageSession session, String originName, Clause query, Map<String, Set<Clause>> history, Collection<Clause> answers)
            throws CommunicationException {

        MessageBuilder builder = new MessageBuilder(this.communicationManager);

        PLogicManager.HISTORY.addContent(builder, history);
        PLogicManager.ANSWERS.addContent(builder, answers);
        PLogicManager.QUERY.addContent(builder, query);
        PLogicManager.ANSWER_NUMBER.addContent(builder, session.getSentMessagesCount("answer") + 1);

        session.sendMessage(builder,
                originName,
                MessageTypeSingletonFactory.create(PLogicAnswerMessageType.class));
    }

    /**
     * Send finish message to the peer that had created this session
     *
     * @throws CommunicationException
     */
    void returnFinishAnswer(MessageSession session, Clause query) throws CommunicationException {
        MessageBuilder builder = new MessageBuilder(this.communicationManager);

        PLogicManager.QUERY.addContent(builder, query);
        PLogicManager.ANSWER_NUMBER.addContent(builder, session.getSentMessagesCount("finish")
                + session.getSentMessagesCount("answer") + 1);

        session.sendMessage(builder,
                session.getCreatorPeerName(),
                MessageTypeSingletonFactory.create(PLogicFinishMessageType.class));

        logger.trace("Sent finish message for {} to {}", query, session.getCreatorPeerName());
    }

    /**
     * Send timeout message to the peer that had created this session
     *
     * @throws CommunicationException
     */
    void returnTimeoutAnswer(MessageSession session) throws CommunicationException {

        MessageBuilder builder = new MessageBuilder(this.communicationManager);

        PLogicManager.ANSWER_NUMBER.addContent(builder, session.getSentMessagesCount("finish")
                + session.getSentMessagesCount("answer") + 1);

        session.sendMessage(builder,
                session.getCreatorPeerName(),
                MessageTypeSingletonFactory.create(PLogicTimeoutMessageType.class));

        logger.trace("Sent timeout message to {}", session.getCreatorPeerName());
    }
}
