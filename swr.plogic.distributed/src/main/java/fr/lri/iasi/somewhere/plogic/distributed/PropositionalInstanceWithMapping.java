/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import fr.lri.iasi.libs.formallogic.utils.instance.ProductionField;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.impl.ClauseBuilder;


/**
 * Describes a propositional instance with support for external mappings
 * @author Andre Fonseca
 */
public class PropositionalInstanceWithMapping extends PropositionalInstance {
    static Logger logger = LoggerFactory.getLogger(PropositionalInstanceWithMapping.class);

    /* --- Properties --- */
    /** Map of shared literals. Indicates local literals used by other instances. */
    protected ConcurrentMap<Literal, Collection<String>> sharedLiteralIndex = Maps.newConcurrentMap();
  
    
    public PropositionalInstanceWithMapping(String name, Collection<Clause> localTheory,
    	ProductionField<Clause> productionField, PropositionalClausalTheory mappings) {
        super(name, localTheory, productionField);

        if (mappings == null) {
            throw new IllegalArgumentException("mappings");
        }

        this.theory.addAll(mappings.copy());
        this.originalTheory.addAll(mappings.copy());
    }
    
    public PropositionalInstanceWithMapping(String name, Collection<Clause> theory,
    		ProductionField<Clause> productionField) {
    	
        super(name, theory, productionField);
    }

    public PropositionalInstanceWithMapping(String name,
        PropositionalInstanceWithMapping propositionalInstance) {
    	
        super(name, propositionalInstance.getTheory(), propositionalInstance.getProductionField());
        this.sharedLiteralIndex.putAll(propositionalInstance.getSharedLiteralIndex());
    }
    
    public PropositionalInstanceWithMapping(PropositionalInstanceWithMapping propositionalInstance) {
        this(propositionalInstance.getName(), propositionalInstance);
    }

    /* --- Accessors --- */
    public ConcurrentMap<Literal, Collection<String>> getSharedLiteralIndex() {
    	return this.sharedLiteralIndex;
    }
    
    /**
     * @return a view of the mappings (clauses with distant literals)
     * contained in this instance.
     * 
     * NOTE: the result theory view do not support the 'remove' method.
     */
    public Iterable<Clause> getMappings() {
    	Iterable<Clause> mappingClauses = Iterables.filter(
    			this.theory, new MappingPredicate());
    	
    	return mappingClauses;
    }
    
    /* --- Methods --- */
    /**
     * {@inheritDoc}
     */
    @Override
    public PropositionalInstanceWithMapping copy() {
    	return new PropositionalInstanceWithMapping(this);
    }
    
    /**
     * @return all literals who belongs to the local theory by are contained in mappings on distant peers.
     * These literals will be returned organized by peer (were the mapping is located).
     */
    public Map<String, Set<Literal>> getSharedLiterals() {
    	Map<String, Set<Literal>> sharedLiteralsByPeer = Maps.newLinkedHashMap();
    	
    	for (Entry<Literal, Collection<String>> entry : this.sharedLiteralIndex.entrySet()) {
    		Literal literal = entry.getKey();
			Collection<String> peerNames = entry.getValue();
			
			for (String peerName : peerNames) {
				Set<Literal> sharedLiterals = sharedLiteralsByPeer.get(peerName);
				if (sharedLiterals == null) {
					sharedLiterals = Sets.newLinkedHashSet();
					sharedLiteralsByPeer.put(peerName, sharedLiterals);
				}
				
				sharedLiterals.add(literal);
			}
		}
    	
    	return sharedLiteralsByPeer;
    }
    
    /**
     * @param peerName
     * @return all literals who belongs to the local theory by are contained in mappings on the indicated peer.
     */
    public Set<Literal> getSharedLiterals(String peerName) {
    	Set<Literal> sharedLiterals = Sets.newLinkedHashSet();

    	for (Entry<Literal, Collection<String>> entry : this.sharedLiteralIndex.entrySet()) {
    		Literal literal = entry.getKey();
			Collection<String> peerNames = entry.getValue();
			
			if (peerNames.contains(peerName)) {
				sharedLiterals.add(literal);
			}
		}
    	
    	return sharedLiterals;
    }
    
    /**
     * @return all literals who belongs to distant theories by are contained in mappings on the local theory.
     */
    public Set<Literal> getDistantSharedLiterals() {
    	Set<Literal> sharedLiterals = Sets.newLinkedHashSet();

    	for (Clause mapping : this.getMappings()) {
    		for (Literal literal : mapping) {
				if (literal instanceof DistantLiteralImpl) {
					sharedLiterals.add(literal);
				}
			}
    	}
    	
    	return sharedLiterals;
    }
    
    /**
     * @return all literals who belongs to distant theories by are contained in mappings on the local theory.
     * These literals will be returned organized by peer (were the literal belongs).
     */
    public Map<String, Set<Literal>> getDistantSharedLiteralsByPeer() {
    	Map<String, Set<Literal>> sharedLiteralsByPeer = Maps.newLinkedHashMap();
    	
    	Iterable<Clause> mappings = this.getMappings();
     	for (Clause clause : mappings) {
 			for (Literal literal : clause) {
 				if (literal instanceof DistantLiteralImpl) {
 					DistantLiteralImpl distantLiteral = (DistantLiteralImpl) literal;
 					
 					Set<Literal> disntantLiterals = sharedLiteralsByPeer.get(distantLiteral.getPeerName());
 					if (disntantLiterals == null) {
 						disntantLiterals = Sets.newLinkedHashSet();
 						sharedLiteralsByPeer.put(distantLiteral.getPeerName(), disntantLiterals);
 					}
 					
 					disntantLiterals.add(distantLiteral);
 				}
 			}
     	}
    	
    	return sharedLiteralsByPeer;
    }
    
    /**
     * @param clause
     * @return returns a local copy of the indicated clause. (All distant literals are seeing as local literals)
     */
    public Clause localViewOf(Clause clause) {
    	ClauseBuilder builder = new ClauseBuilder();
   	
    	for (Literal literal : clause) {
	    	if (literal instanceof DistantLiteralImpl) {
				DistantLiteralImpl distantLiteral = (DistantLiteralImpl) literal;
				if (distantLiteral.getPeerName().equals(this.name))
					builder.addLiteral(distantLiteral.toLocal());
				else
					builder.addLiteral(distantLiteral);
	    	} else {
				builder.addLiteral(literal);
    		}
    	}
    	
    	builder.addAllAnnotations(clause.getAnnotations().values());
    	
    	return builder.create();
    }
    
    /**
     * @param peerName
     * @param clause
     * @return returns a distant copy of the indicated clause. (All locals literals are seeing as distant literals belonging to the indicated peer)
     */
    public Clause distantViewOf(String peerName, Clause clause) {
    	MappingBuilder builder = new MappingBuilder();
    	
    	for (Literal literal : clause) {
    		if (!(literal instanceof DistantLiteralImpl)) {
    			if (literal.getSign())
    				builder.addLiteral(peerName, literal.getLabel());
    			else
    				builder.addNegativeLiteral(peerName, literal.getLabel());
        	} else {
    			builder.addLiteral(literal);
        	}
		}
    	
    	builder.addAllAnnotations(clause.getAnnotations().values());
    	
    	return builder.create();
    }
    
    
    /**
     * Predicate responsible for selecting 'mapping' clauses.
     * 
     * @author Andre Fonseca
     *
     */
    protected static class MappingPredicate implements Predicate<Clause> {
		
		public boolean apply(Clause clause) {
			
			for (Literal lit : clause) {
				if (lit instanceof DistantLiteralImpl)
					return true;
			}
			
			return false;
		}
    }
    
    /**
     * return a (sub)clause with only local AND non shared literals of clause 
     * 
     * @author Philippe Chatalic
     * 
     * @param clause
     * @return the clause made of literals of clause that are neither shared nor distant
     */
    public Clause neitherDistantNorSharedLitteralSubClause(Clause clause) {
    	ClauseBuilder builder = new ClauseBuilder();
   	
    	for (Literal literal : clause) {
	    	if (!(literal instanceof DistantLiteralImpl) 
                      && 
                    !this.sharedLiteralIndex.containsKey(literal.opposite())){
                    builder.addLiteral(literal);
                 }
       
        }
    	builder.addAllAnnotations(clause.getAnnotations().values());
    	
    	return builder.create();
    }



   /**
     * tests whether a clause contain at least one distant literal
     * 
     * @author Philippe Chatalic
     * 
     * @param clause
     */
    public boolean containDistantLiterals(Clause clause) {
        
			for (Literal lit : clause) {
				if (lit instanceof DistantLiteralImpl)
					return true;
			}			
			return false;
		}

    }