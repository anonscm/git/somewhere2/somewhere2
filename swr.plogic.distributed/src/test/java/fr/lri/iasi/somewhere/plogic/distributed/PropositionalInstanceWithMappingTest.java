/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

package fr.lri.iasi.somewhere.plogic.distributed;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Rule;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import fr.lri.iasi.libs.formallogic.utils.instance.InstanceParser;
import fr.lri.iasi.libs.formallogic.utils.instance.Instance;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.impl.ClauseBuilder;
import fr.lri.iasi.libs.plogic.impl.LiteralImpl;

import fr.lri.iasi.somewhere.plogic.distributed.parser.SWRInstanceParser;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;


/**
 *
 * @author pc
 */
public class PropositionalInstanceWithMappingTest {
    
	PropositionalInstanceWithMapping instance;
        
        
        Clause clauseWithOnlyOneSharedLiteral;
        Clause clauseWithOnlyOneDistantLiteral;
        Clause clauseWithOneSharedAndOneDistantLiteral ;
        Clause clauseWithOneDistantLiteral;
                
	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();
        
	
	@Before
	public void init() throws IOException {
        String instanceStr = "P_1\n";
		File instanceFile = testFolder.newFile("test3.swr");
		
		FileWriter fw = new FileWriter(instanceFile);
		fw.write(instanceStr);
		fw.close();
		
		InstanceParser p = new SWRInstanceParser(instanceFile.getPath());
		instance = (PropositionalInstanceWithMapping) p.parse();
        }

	
	@Test
	public void testNonShareLiteralSubClause() throws IOException {
		String instanceStr = "P_1\n";
		File instanceFile = testFolder.newFile("test3.swr");
		
		FileWriter fw = new FileWriter(instanceFile);
		fw.write(instanceStr);
		fw.close();
		
		InstanceParser p = new SWRInstanceParser(instanceFile.getPath());
		PropositionalInstanceWithMapping instance = (PropositionalInstanceWithMapping) p.parse();
		
                // Add the mapping clause :    !l  v  P_21:l v P_22:l
		MappingBuilder builder = new MappingBuilder();
		builder.addNegativeLiteral("1");
		builder.addLiteral("P_21", "1");
		builder.addLiteral("P_22", "1");
		Clause toBeAdded1 = builder.create();
		
		instance.addClause(toBeAdded1);
		
		assertEquals("[]", instance.getLocalTheory().toString());
		assertEquals("[(!1 v P_21:1 v P_22:1)]", instance.getMappings().toString());
                
                // Add the clause :    l 
		
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("1");
		Clause toBeAdded2 = builder1.create();;
		
		instance.addClause(toBeAdded2);
                
                // Saturation should be performed
     
                assertEquals("[(1)]", instance.getLocalTheory().toString());
		assertEquals("[(P_21:1 v P_22:1)]", instance.getMappings().toString());
                
                
                 // Add local literals shared with other peers

                Map<Literal, Collection<String>> sharedLiterals = instance.getSharedLiteralIndex();
                
                System.out.print("shared literals = "+sharedLiterals+"\n");
                
                Literal sharedLiteral = new LiteralImpl("shl", true);
                Collection<String> peersSharingLiteralShl = new HashSet<String>();
                peersSharingLiteralShl.add("P_21");
                peersSharingLiteralShl.add("P_100");
                sharedLiterals.put(sharedLiteral, peersSharingLiteralShl);
                
                System.out.print("literals = "+sharedLiteral+" is in sharedLiterals : " + sharedLiterals.containsKey(sharedLiteral));
                
                Literal sharedLiteral2 = new LiteralImpl("shl2", true);
                Collection<String> peersSharingLiteralShl2 = new HashSet<String>();
                peersSharingLiteralShl2.add("P_11");
                sharedLiterals.put(sharedLiteral2, peersSharingLiteralShl2);
                
                System.out.print("shared literals = "+sharedLiterals+"\n\n");
               
                //
                ClauseBuilder cb2 = new ClauseBuilder();
                cb2.addLiteral("a");
                cb2.addNegativeLiteral("b");
                Clause pureLocalClause = cb2.create();
                System.out.println("pureLocalClause = " + pureLocalClause);
               
                //
                ClauseBuilder cb3 = new ClauseBuilder();
                cb3.addLiteral("a");
                cb3.addNegativeLiteral("shl");
                Clause clauseWithOneSharedLiteral = cb3.create();
                System.out.println("clauseWithOneSharedLiteral = " + clauseWithOneSharedLiteral);
               
                //
                MappingBuilder cb4 = new MappingBuilder();
                cb4.addLiteral("a");
                cb4.addNegativeLiteral("P_21","l");
                Clause clauseWithOneDistantLiteral = cb4.create();
                 System.out.println("clauseWithOneDistantLiteral = " + clauseWithOneDistantLiteral);
                
                //
                MappingBuilder cb5 = new MappingBuilder();
                cb5.addLiteral("a");
                cb5.addNegativeLiteral("sh12");
                cb5.addNegativeLiteral("P_21","l");
                Clause clauseWithOneSharedAndOneDistantLiteral = cb5.create();
                System.out.println("clauseWithOneSharedAndOneDistantLiteral = " + clauseWithOneSharedAndOneDistantLiteral);
                
                //
                ClauseBuilder cb6 = new ClauseBuilder();
                cb6.addNegativeLiteral("shl2");
                Clause clauseWithOnlyOneSharedLiteral = cb6.create();
                System.out.println("clauseWithOnlyOneSharedLiteral = " + clauseWithOnlyOneSharedLiteral);
                
                //
                MappingBuilder cb7 = new MappingBuilder();
                cb7.addLiteral("P_21","l");
                Clause clauseWithOnlyOneDistantLiteral = cb7.create();
                System.out.println("clauseWithOnlyOneDistantLiteral = " + clauseWithOnlyOneDistantLiteral);
                //
                ClauseBuilder cba = new ClauseBuilder();
                cba.addLiteral("a");
                Clause clausea = cba.create();
                System.out.println("clausea = " + clausea);
                
                
                
                ClauseBuilder cb8 = new ClauseBuilder();
                Clause emptyClause = cb8.create();
                System.out.println("emptyClause = " + emptyClause);
                
                
                
                System.out.println("pureLocalClause\n");
                Clause pl1 = instance.neitherDistantNorSharedLitteralSubClause(pureLocalClause);
                System.out.println("pl1 = " + pl1);
                assertEquals(pl1,pureLocalClause);
                
                System.out.println("clauseWithOneSharedLiteral\n");
                Clause pl2 = instance.neitherDistantNorSharedLitteralSubClause(clauseWithOneSharedLiteral);
                System.out.println("pl2 = " + pl2);
                assertEquals(pl2,clausea);
                
                System.out.println("clauseWithOneDistantLiteral\n");
                Clause pl3 = instance.neitherDistantNorSharedLitteralSubClause(clauseWithOneDistantLiteral);
                System.out.println("pl3 = " + pl3);
                assertEquals(pl3,clausea);
                
                System.out.println("clauseWithOneSharedAndOneDistantLiteral\n");
                Clause pl4 = instance.neitherDistantNorSharedLitteralSubClause(clauseWithOneSharedAndOneDistantLiteral);
                System.out.println("pl4 = " + pl4);
                assertEquals(pl3,clausea);
                System.out.println("clauseWithOnlyOneSharedLiteral\n");
                System.out.println("\n");
                Clause pl5 = instance.neitherDistantNorSharedLitteralSubClause(clauseWithOnlyOneSharedLiteral);
                System.out.println("pl5 = " + pl5);
                assertEquals(pl5,emptyClause);
                
                System.out.println("clauseWithOnlyOneDistantLiteral\n");
                System.out.println("\n");
                Clause pl6 = instance.neitherDistantNorSharedLitteralSubClause(clauseWithOnlyOneDistantLiteral);
                System.out.println("pl6 = " + pl6);
                assertEquals(pl6,emptyClause);
                
                
          
                
		
	}


}
