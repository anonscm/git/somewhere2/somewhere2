/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.libs.formallogic.utils.instance.InstanceParser;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryImpl;
import fr.lri.iasi.libs.plogic.strategies.impl.StrategiesConfigurator;
import fr.lri.iasi.somewhere.plogic.distributed.PropositionalInstance;
import fr.lri.iasi.somewhere.plogic.distributed.parser.DIMACSInstanceParser;

@RunWith(Parameterized.class)
public class LocalConsequencesIT {
	private static Logger logger = LoggerFactory.getLogger(LocalConsequencesIT.class);
	
	@Rule 
	public TestName name = new TestName();
	
	/* --- PARAMETERS SETTINGS --- */ 
	public LocalConsequencesIT(String dataType) {
		StrategiesConfigurator.getInstance().setDataType(dataType);
	}
	
	@Parameterized.Parameters
	public static List<Object[]> parameters() {
		return Arrays.asList(new Object[][] {
			      {"hashSet"},
			      {"trie"}
			    });
	}
	
	public ConjunctionOfClauses testHelper(PropositionalClausalTheory toBeSatured) {
		PropositionalClausalTheory initialTheory = new PropositionalClausalTheoryImpl(toBeSatured);
		return initialTheory.initialize();
	}
	
	/* --- Hooks --- */
	@Before
	public void logTestName() {
		logger.info(name.getMethodName());
	}
	
	/* --- Tests --- */
	@Test
	public void test2tree_3() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/2tree-3.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(13, result.size());
	}
	
	@Test
	public void test2tree_5() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/2tree-5.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(36, result.size());
	}
	
	@Test
	public void test2tree_7() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/2tree-7.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(75, result.size());
	}
	
	@Test
	public void test2tree_9() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/2tree-9.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(150, result.size());
	}
	
	@Test
	public void test2tree_11() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/2tree-11.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(337, result.size());
	}
	
	@Test
	public void test2tree_13() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/2tree-13.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(668, result.size());
	}
	
	@Test
	public void test2tree_15() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/2tree-15.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(1623, result.size());
	}
	
	@Test
	public void testAdder_1() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/adder-1.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(11, result.size());
	}
	
	@Test
	public void testAdder_2() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/adder-2.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(73, result.size());
	}
	
	@Test
	public void testAdder_3() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/adder-3.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(303, result.size());
	}
	
	@Test
	public void testAim_50_1_6_no_1() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-1_6-no-1.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(1, result.size());
	}
	
	@Test
	public void testAim_50_1_6_no_2() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-1_6-no-2.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(1, result.size());
	}
	
	@Test
	public void testAim_50_1_6_no_3() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-1_6-no-3.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(1, result.size());
	}
	
	@Test
	public void testAim_50_1_6_no_4() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-1_6-no-4.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(1, result.size());
	}
	
	@Test
	public void testAim_50_1_6_yes1_1() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-1_6-yes1-1.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(50, result.size());
	}
	
	@Test
	public void testAim_50_1_6_yes1_2() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-1_6-yes1-2.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(50, result.size());
	}
	
	@Test
	public void testAim_50_1_6_yes1_3() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-1_6-yes1-3.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(50, result.size());
	}
	
	@Test
	public void testAim_50_1_6_yes1_4() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-1_6-yes1-4.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(50, result.size());
	}
	
	@Test
	public void testAim_50_2_0_no_1() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-2_0-no-1.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(1, result.size());
	}
	
	@Test
	public void testAim_50_2_0_no_2() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-2_0-no-2.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(1, result.size());
	}
	
	@Test
	public void testAim_50_2_0_no_3() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-2_0-no-3.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(1, result.size());
	}
	
	@Test
	public void testAim_50_2_0_no_4() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-2_0-no-4.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(1, result.size());
	}
	
	@Test
	public void testAim_50_2_0_yes1_1() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-2_0-yes1-1.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(50, result.size());
	}
	
	@Test
	public void testAim_50_2_0_yes1_2() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-2_0-yes1-2.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(50, result.size());
	}
	
	@Test
	public void testAim_50_2_0_yes1_3() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-2_0-yes1-3.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(50, result.size());
	}
	
	@Test
	public void testAim_50_2_0_yes1_4() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/aim-50-2_0-yes1-4.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(50, result.size());
	}
	
	@Test
	public void testChandra_21() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/chandra21.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(2685, result.size());
	}
	
	@Test
	public void testCycle1_3() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/cycle1-3.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(24, result.size());
	}
	
	@Test
	public void testCycle1_4() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/cycle1-4.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(43, result.size());
	}
	
	@Test
	public void testCycle1_5() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/cycle1-5.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(78, result.size());
	}
	
	@Test
	public void testCycle1_6() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/cycle1-6.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(145, result.size());
	}
	
	@Test
	public void testDeputes() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/deputes.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(109, result.size());
	}
	
	@Test
	public void testDette() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/dette.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(76, result.size());
	}
	
	@Test
	public void testEx1() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/ex1.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(26, result.size());
	}
	
	@Test
	public void testEx2() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/ex2.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(72, result.size());
	}
	
	@Test
	public void testEx4() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/ex4.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(80, result.size());
	}
	
	@Test
	public void testTwo_Pipes() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/two-pipes.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(638, result.size());
	}
	
	/*@Test
	public void testFour_Pipes() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/four-pipes.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(6208, result.size());
	}*/
	
	@Test
	public void testLogiciens() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/logiciens.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(100, result.size());
	}
	
	@Test
	public void testMine_2_2() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/mine-2-2.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(233, result.size());
	}
	
	@Test
	public void testMult_2_2() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/mult-2-2.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(285, result.size());
	}
	
	@Test
	public void testMult_3_2() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/mult-3-2.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(2290, result.size());
	}
	
	@Test
	public void testMult_inf_2_2() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/mult-inf-2-2.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(256, result.size());
	}
	
	@Test
	public void testMult_inf_3_2() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/mult-inf-3-2.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(298, result.size());
	}
	
	@Test
	public void testMult_inf_4_2() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/mult-inf-4-2.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(304, result.size());
	}
	
	@Test
	public void testMult_sup_2_2() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/mult-sup-2-2.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(233, result.size());
	}
	
	@Test
	public void testMult_sup_3_2() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/mult-sup-3-2.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(2195, result.size());
	}
	
	@Test
	public void testProf() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/prof.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(21, result.size());
	}
	
	@Test
	public void testType3_1() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/type3-1.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(3, result.size());
	}
	
	@Test
	public void testType3_2() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/type3-2.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(6, result.size());
	}
	
	@Test
	public void testType3_3() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/type3-3.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(11, result.size());
	}
	
	@Test
	public void testType5_1() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/type5-1.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(7, result.size());
	}
	
	@Test
	public void testType6_1() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/type6-1.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(7, result.size());
	}
	
	@Test
	public void testType7_1() throws FileNotFoundException {
		// Building instance
		URL path = getClass().getClassLoader().getResource("cnfs/type7-1.cnf");
		InstanceParser parser = new DIMACSInstanceParser(path.toString().replace("file:", ""));
		PropositionalInstance instance = (PropositionalInstance) parser.parse();
		ConjunctionOfClauses result = testHelper(instance.getTheory());
		
		// Comparing result
		assertEquals(7, result.size());
	}

}
