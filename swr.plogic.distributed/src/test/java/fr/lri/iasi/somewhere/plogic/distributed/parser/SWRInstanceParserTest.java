/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed.parser;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import fr.lri.iasi.libs.formallogic.utils.instance.Instance;
import fr.lri.iasi.libs.formallogic.utils.instance.InstanceParser;
import fr.lri.iasi.libs.formallogic.utils.instance.ProductionField;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.impl.ClauseBuilder;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryImpl;
import fr.lri.iasi.libs.plogic.restrictions.SubsetRestriction;

import fr.lri.iasi.somewhere.plogic.distributed.MappingBuilder;
import fr.lri.iasi.somewhere.plogic.distributed.PropositionalInstanceWithMapping;
import fr.lri.iasi.somewhere.plogic.distributed.PropositionalProductionField;

public class SWRInstanceParserTest {
	
	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();
	
	@Test
	public void parseTest1() throws IOException {
		String instanceStr = "P_1\nP_1:1\n";
		File instanceFile = testFolder.newFile("test1.swr");
		
		FileWriter fw = new FileWriter(instanceFile);
		fw.write(instanceStr);
		fw.close();
		
		InstanceParser p = new SWRInstanceParser(instanceFile.getPath());
		Instance instance = p.parse();
		
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		PropositionalClausalTheory mappings = new PropositionalClausalTheoryImpl();
		ProductionField<Clause> production = new PropositionalProductionField();
		
		ClauseBuilder builder = new ClauseBuilder();
		builder.addLiteral("1");
		theory.add(builder.create());
		
		Instance compareInstance = new PropositionalInstanceWithMapping("P_1", theory, production, mappings);
		
		assertEquals(compareInstance, instance);
	}
	
	@Test
	public void parseTest2() throws IOException {
		String instanceStr = "P_1\nP_1:1\nMappings\n!P_1:1 P_21:1 P_22:1\n";
		File instanceFile = testFolder.newFile("test2.swr");
		
		FileWriter fw = new FileWriter(instanceFile);
		fw.write(instanceStr);
		fw.close();
		
		InstanceParser p = new SWRInstanceParser(instanceFile.getPath());
		Instance instance = p.parse();
		
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		PropositionalClausalTheory mappings = new PropositionalClausalTheoryImpl();
		ProductionField<Clause> production = new PropositionalProductionField();
		
		ClauseBuilder builder = new ClauseBuilder();
		builder.addLiteral("1");
		theory.add(builder.create());
		
		MappingBuilder mBuilder = new MappingBuilder();
		mBuilder.addNegativeLiteral("1");
		mBuilder.addLiteral("P_21","1");
		mBuilder.addLiteral("P_22","1");
		mappings.add(mBuilder.create());
		
		Instance compareInstance = new PropositionalInstanceWithMapping("P_1", theory, production, mappings);
		
		assertEquals(compareInstance, instance);
	}
	
	@Test
	public void parseTest3() throws IOException {
		String instanceStr = "P_1\nP_1:1\nMappings\n!P_1:1 P_21:1 P_22:1\nProduction\nP_1:1";
		File instanceFile = testFolder.newFile("test3.swr");
		
		FileWriter fw = new FileWriter(instanceFile);
		fw.write(instanceStr);
		fw.close();
		
		InstanceParser p = new SWRInstanceParser(instanceFile.getPath());
		Instance instance = p.parse();
		
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		PropositionalClausalTheory mappings = new PropositionalClausalTheoryImpl();
		ProductionField<Clause> production = new PropositionalProductionField();
		
		ClauseBuilder builder = new ClauseBuilder();
		builder.addLiteral("1");
		theory.add(builder.create());
		
		MappingBuilder mBuilder = new MappingBuilder();
		mBuilder.addNegativeLiteral("1");
		mBuilder.addLiteral("P_21","1");
		mBuilder.addLiteral("P_22","1");
		mappings.add(mBuilder.create());
		
		SubsetRestriction restriction = new SubsetRestriction();
		restriction.addToSubset("1");
		production.addRestriction(restriction);
		
		Instance compareInstance = new PropositionalInstanceWithMapping("P_1", theory, production, mappings);
		
		assertEquals(compareInstance, instance);
	}

}
