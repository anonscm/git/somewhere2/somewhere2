/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import fr.lri.iasi.libs.formallogic.utils.instance.InstanceParser;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.impl.ClauseBuilder;
import fr.lri.iasi.somewhere.plogic.distributed.parser.SWRInstanceParser;

public class PropositionalInstanceTest {
	
	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();
	
	@Test
	public void testGetLocalTheory() throws IOException {
		String instanceStr = "P_1\nP_1:1";
		File instanceFile = testFolder.newFile("test1.swr");
		
		FileWriter fw = new FileWriter(instanceFile);
		fw.write(instanceStr);
		fw.close();
		
		InstanceParser p = new SWRInstanceParser(instanceFile.getPath());
		PropositionalInstance instance = (PropositionalInstance) p.parse();
		
		assertEquals("[(1)]", instance.getLocalTheory().toString());
	}
	
	@Test
	public void testGetMappings() throws IOException {
		String instanceStr = "P_1\nMappings\n!P_1:1 P_21:1 P_22:1\n";
		File instanceFile = testFolder.newFile("test2.swr");
		
		FileWriter fw = new FileWriter(instanceFile);
		fw.write(instanceStr);
		fw.close();
		
		InstanceParser p = new SWRInstanceParser(instanceFile.getPath());
		PropositionalInstanceWithMapping instance = (PropositionalInstanceWithMapping) p.parse();
		
		assertEquals("[(!1 v P_21:1 v P_22:1)]", instance.getMappings().toString());
	}
	
	@Test
	public void testModifyTheoryRemove() throws IOException {
		String instanceStr = "P_1\nP_1:1\nMappings\n!P_1:1 P_21:1 P_22:1\n";
		File instanceFile = testFolder.newFile("test3.swr");
		
		FileWriter fw = new FileWriter(instanceFile);
		fw.write(instanceStr);
		fw.close();
		
		InstanceParser p = new SWRInstanceParser(instanceFile.getPath());
		PropositionalInstanceWithMapping instance = (PropositionalInstanceWithMapping) p.parse();
		
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("1");
		Clause toBeRemoved1 = builder1.create();
		
		instance.removeClause(toBeRemoved1);
		
		assertEquals("[]", instance.getLocalTheory().toString());
		assertEquals("[(!1 v P_21:1 v P_22:1)]", instance.getMappings().toString());
		
		MappingBuilder builder2 = new MappingBuilder();
		builder2.addNegativeLiteral("1");
		builder2.addLiteral("P_21", "1");
		builder2.addLiteral("P_22", "1");
		Clause toBeRemoved2 = builder2.create();
		
		instance.removeClause(toBeRemoved2);
		
		assertEquals("[]", instance.getLocalTheory().toString());
		assertEquals("[]", instance.getMappings().toString());
	}
	
	@Test
	public void testModifyTheoryRAdd() throws IOException {
		String instanceStr = "P_1\n";
		File instanceFile = testFolder.newFile("test3.swr");
		
		FileWriter fw = new FileWriter(instanceFile);
		fw.write(instanceStr);
		fw.close();
		
		InstanceParser p = new SWRInstanceParser(instanceFile.getPath());
		PropositionalInstanceWithMapping instance = (PropositionalInstanceWithMapping) p.parse();
		
		MappingBuilder builder = new MappingBuilder();
		builder.addNegativeLiteral("1");
		builder.addLiteral("P_21", "1");
		builder.addLiteral("P_22", "1");
		Clause toBeAdded1 = builder.create();
		
		instance.addClause(toBeAdded1);
		
		assertEquals("[]", instance.getLocalTheory().toString());
		assertEquals("[(!1 v P_21:1 v P_22:1)]", instance.getMappings().toString());
		
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("1");
		Clause toBeAdded2 = builder1.create();;
		
		instance.addClause(toBeAdded2);
		
		assertEquals("[(1)]", instance.getLocalTheory().toString());
		assertEquals("[(P_21:1 v P_22:1)]", instance.getMappings().toString());
	}


}
