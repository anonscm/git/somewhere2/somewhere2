/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.plogic.distributed;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.impl.LiteralImpl;
import fr.lri.iasi.somewhere.api.communication.model.Peer;

public class DistantLiteralTest {
	
	@Test
	public void testEquals1() {
		Literal lit1 = new DistantLiteralImpl("first", "1", true);
		Literal lit2 = new DistantLiteralImpl("first", "1", true);

		assertEquals(lit2, lit1);
	}
	
	@Test
	public void testEquals2() {
		Literal lit1 = new DistantLiteralImpl("first", "1", true);
		Literal lit2 = new DistantLiteralImpl("second", "1", false);

		assertThat(lit1, is(not(lit2)));
	}
	
	@Test
	public void testEquals3() {
		Peer first = new Peer("first");
		Peer second = new Peer("first");
		
		Literal lit1 = new DistantLiteralImpl(first, "1", true);
		Literal lit2 = new DistantLiteralImpl(second, "1", true);

		assertEquals(lit2, lit1);
	}
	
	@Test
	public void testEquals4() {
		Peer first = new Peer("first");
		
		Literal lit1 = new LiteralImpl("1", true);
		Literal lit2 = new DistantLiteralImpl(first, "1", true);

		assertThat(lit1, is(not(lit2)));
		assertThat(lit2, is(not(lit1)));
	}

}
