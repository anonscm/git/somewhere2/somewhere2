/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.benchmark;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageCollector;
import fr.lri.iasi.somewhere.benchmark.configuration.BenchmarkConfiguration;
import fr.lri.iasi.somewhere.benchmark.types.BenchmarkAnswerMessageType;
import fr.lri.iasi.somewhere.benchmark.types.BenchmarkFinishMessageType;
import fr.lri.iasi.somewhere.ui.CommandListener;

/**
 * Message listener used to output answers and its informations to the user interface 
 * 
 * @author Andre Fonseca
 *
 */
public class BenchmarkAnswersCollector extends AbstractMessageCollector {
	static Logger logger = LoggerFactory.getLogger(BenchmarkAnswersCollector.class);

	/* --- Properties --- */
	/** Command listener used to inform that all messages have been sent */
	private final CommandListener listener;
	
	/** Answer messages counter */
	private AtomicInteger answersReceived = new AtomicInteger();
	
	/** Finish messages counter */
	private AtomicInteger finishesReceived = new AtomicInteger();
	
	/** The current benchmark module configuration */
	private final BenchmarkConfiguration configuration;

	public BenchmarkAnswersCollector(BenchmarkManager benchmarkManager,
			CommandListener listener) {
		super(benchmarkManager.getCommunicationManager());
		
		// Preparing the message receiver    
        addAcceptedTypes(BenchmarkAnswerMessageType.class);
        addAcceptedTypes(BenchmarkFinishMessageType.class);
        this.listener = listener;
        this.configuration = benchmarkManager.getBenchmarkConfiguration();
	}
	
	/* --- Accessors --- */
	public int getAnswerReceived() {
		return answersReceived.get();
	}
	
	public int getFinishesReceived() {
		return finishesReceived.get();
	}
	
	/* --- Methods --- */
	/**
     * {@inheritDoc}
     */
	@Override
	public void messageHasBeenSent(Message message) {
		
		if (!message.getDestination().getName().equals("User"))
			return;
		
		if (acceptedTypes.contains(message.getType())) {
			if (configuration.getShowOutput())
				ui.print(message.getType().print(message));
			
			if (message.getType() instanceof BenchmarkAnswerMessageType) {
				answersReceived.incrementAndGet();
			} else if (message.getType() instanceof BenchmarkFinishMessageType) {
				listener.executionCompleted();
			}
		}
    }
	

}
