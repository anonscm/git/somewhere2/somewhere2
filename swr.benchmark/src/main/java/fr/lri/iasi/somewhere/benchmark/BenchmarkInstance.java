/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.benchmark;

import java.io.Serializable;
import java.util.Collections;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.libs.formallogic.utils.instance.Instance;

public class BenchmarkInstance implements Instance, Serializable {
	private static final long serialVersionUID = -5376437984871864675L;
	static Logger logger = LoggerFactory.getLogger(BenchmarkInstance.class);
	
	/* --- Properties --- */
	/** Name of the local peer */
	private final String peerName;
	
	/** Name of the local peer */
	private final String instanceName;
	
    /** List of neighbors */
    protected final Set<String> neighbors;
    
    /** Local consequences factor */
    protected double localConsequencesFactor = 0;
    
    /** Propagate queries factor */
    protected double propagateQueriesFactor = 0;
    
    /** Propagate queries factor */
    protected double combinationFactor = 0;
    
    public BenchmarkInstance(final String peerName, final String instanceName,
    		final double localConsequencesFactor, final double propagateQueriesFactor, final double combinationFactor,
    		final Set<String> neighbors) {
    	if (peerName == null)
            throw new IllegalArgumentException("peerName");
    	if (instanceName == null)
            throw new IllegalArgumentException("instanceName");
    	if (neighbors == null)
            throw new IllegalArgumentException("neighbors");

    	this.peerName = peerName;
    	this.instanceName = instanceName;
        this.localConsequencesFactor = localConsequencesFactor;
        this.propagateQueriesFactor = propagateQueriesFactor;
        this.combinationFactor = combinationFactor;
        this.neighbors = neighbors;
    }
    
    public BenchmarkInstance(final BenchmarkInstance instance) {
    	if (instance == null)
            throw new IllegalArgumentException("instance");

    	this.peerName = instance.getPeerName();
    	this.instanceName = instance.getName();
        this.localConsequencesFactor = instance.getLocalConsequencesFactor();
        this.propagateQueriesFactor = instance.getPropagateQueriesFactor();
        this.combinationFactor = instance.getCombinationFactor();
        this.neighbors = instance.getNeighbors();
    }
    
    /* --- Accessors --- */
    public String getPeerName() {
    	return peerName;
    }
    
    public String getName() {
		return instanceName;
	}
    
    public Set<String> getNeighbors() {
        return Collections.unmodifiableSet(neighbors);
    }
    
    public double getLocalConsequencesFactor() {
        return localConsequencesFactor;
    }
    
    public double getPropagateQueriesFactor() {
        return propagateQueriesFactor;
    }
    
    public double getCombinationFactor() {
        return combinationFactor;
    }
    
    /* --- Methods --- */
    @Override
	public Instance copy() {
		return new BenchmarkInstance(this);
	}
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof BenchmarkInstance) {
        	BenchmarkInstance b = (BenchmarkInstance) o;

            return ((this.peerName.equals(b.peerName)) &&
            (this.instanceName.equals(b.instanceName)) &&
            (this.neighbors.equals(b.neighbors)));
        }

        return false;
    }
    
    @Override
    public int hashCode() {
        return (41 * ((41 * (41 + instanceName.hashCode())) +
        		neighbors.hashCode())) + peerName.hashCode();
    }

}
