/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.benchmark.configuration;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.XPathReader;

/**
 * XML configuration of benchmark bundle
 * 
 * @author Andre Fonseca
 *
 */
public class BenchmarkXMLConfiguration extends BenchmarkConfiguration {
	static Logger logger = LoggerFactory.getLogger(BenchmarkXMLConfiguration.class);
	private static final long serialVersionUID = 6925304530674261356L;

	public BenchmarkXMLConfiguration(Module module) {
		super(module);
	}
	
	/**
     * Parse XML configuration node to obtain corresponding
     * Configuration properties
     */
    public void parseConfig(final Node config) {
        try {
            // Verify if there is a valid config
            if (config == null) {
                return;
            }

            final XPathReader reader = new XPathReader(config);
            parseOutputOptions(reader);

        } catch (final Exception e) {
            logger.error("There were errors parsing XML configuration file : " +
                e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
    }
    
    /**
     * Parse XML configuration file, part 'output'
     */
    private void parseOutputOptions(XPathReader reader) throws Exception {
    	String showOutputStr = reader.read("output/showOutput", XPathConstants.STRING).toString();
    	if (!showOutputStr.isEmpty())
    		showOutput = Boolean.parseBoolean(showOutputStr);
    }
    
    /**
     * Export the configuration into an XML node
     */
    public Node export() {
        final Document doc;

        try {
            final DocumentBuilder builder = DocumentBuilderFactory.newInstance()
                                                                  .newDocumentBuilder();
            doc = builder.newDocument();

            final Element element = doc.createElement(getModule().getModuleName());
            doc.appendChild(element);

			exportOutputOptions(doc, element);
			
        } catch (final Exception e) {
            logger.error(
                "There were errors exporting XML configuration file : " +
                e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }

        return doc;
    }
    
    /**
     * Export XML configuration file, part 'output'
     */
    private void exportOutputOptions(Document doc, Element element)
        throws Exception {
        // Create the base 'network' element
        Element base = doc.createElement("output");
        element.appendChild(base);

        // Create the child nodes
        Element showOutputElement = doc.createElement("showOutput");
        showOutputElement.setTextContent(Boolean.valueOf(showOutput).toString());
        base.appendChild(showOutputElement);
    }
}
