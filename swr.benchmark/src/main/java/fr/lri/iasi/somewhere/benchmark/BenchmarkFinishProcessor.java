/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.benchmark;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.MessageType;
import fr.lri.iasi.somewhere.api.communication.model.MessageTypeSingletonFactory;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageProcessor;
import fr.lri.iasi.somewhere.benchmark.types.BenchmarkQueryMessageType;

/**
 * Processor responsible for the "Finish" message type.
 * 
 * @author Andr?? Fonseca
 *
 */
public class BenchmarkFinishProcessor extends AbstractMessageProcessor {
	static Logger logger = LoggerFactory.getLogger(BenchmarkFinishProcessor.class);
	
	/* --- Properties --- */
	/** Answers to wait counter */
	protected AtomicInteger answersToWait = new AtomicInteger();
	
	/** The related query processor (in the same message session). */
	protected BenchmarkQueryProcessor queryProcessor;
	
    public BenchmarkFinishProcessor(MessageSession session) {
        super(session);
        
        MessageType queryType = MessageTypeSingletonFactory.create(BenchmarkQueryMessageType.class);
        this.queryProcessor = (BenchmarkQueryProcessor) session.getProcessor(queryType);
    }

    /* --- Methods --- */
    /**
     * {@inheritDoc}
     */
	@Override
	public void process(Message message) {
		
		int messageNumber = BenchmarkManager.ANSWER_NUMBER.getContent(message);
		this.answersToWait.addAndGet(messageNumber - 1);
		
		// Waiting all messages to arrive...
		while (session.getReceivedMessagesCount("answer") != answersToWait.get()) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// After all answer messages are arrived, count down the finish message counter on the related query processor.
		this.queryProcessor.getFinishCountDown().countDown();
	}

}
