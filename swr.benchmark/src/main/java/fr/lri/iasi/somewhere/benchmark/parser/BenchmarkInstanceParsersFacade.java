/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.benchmark.parser;

import java.io.File;

import fr.lri.iasi.libs.formallogic.utils.instance.Instance;
import fr.lri.iasi.libs.formallogic.utils.instance.ParsersFacadeListener;


/**
 * This class is used as a facade to others parsing classes
 * @author Andr?? Fonseca
 */
public class BenchmarkInstanceParsersFacade {
	
    /**
     * Call the proper parser
     */
    private static Instance callParser(String path) {
        File file = new File(path);

        if (!file.isFile()) {
            throw new IllegalArgumentException("path");
        }

        String filename = file.getName();

        if (filename.lastIndexOf(".") == -1) {
            throw new IllegalArgumentException("path");
        }

        // Do things depending on the file extension
        Instance res = null;

        try {
            XMLInstanceParser p = new XMLInstanceParser(path);
            res = p.parse();
        } catch (Exception e) {
            res = null;
        }

        return res;
    }

    /**
     * Begin parsing a file
     * @param path path of file to open
     * @param listener Listener to call when the parsing is completed
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static void parse(String path,
        ParsersFacadeListener listener) {
    	
    	Instance instance = callParser(path);
    	
    	listener.loadingCompleted(instance);
    }
}
