/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.benchmark.configuration;

import fr.lri.iasi.somewhere.Module;

/**
 * Configuration of benchmark bundle
 * @author Andr?? Fonseca
 */
public class BenchmarkConfiguration extends fr.lri.iasi.somewhere.Configuration {
	private static final long serialVersionUID = 1948614455108155941L;
	
	/* --- Properties --- */
	/** Shows output can slow the benchmark process. Turn it on/off using this option */
	protected boolean showOutput;

	public BenchmarkConfiguration(Module module) {
		super(module);
	}
	
	/* --- Mutators --- */
	public void setShowOutput(boolean shows) {
		this.showOutput = shows;
	}
	
	/* --- Accessors --- */
	public boolean getShowOutput() {
		return showOutput;
	}
	
	/**
     * {@inheritDoc}
     */
    @Override
    public void setAllToDefault() {
    	this.showOutput = true;
    }

}
