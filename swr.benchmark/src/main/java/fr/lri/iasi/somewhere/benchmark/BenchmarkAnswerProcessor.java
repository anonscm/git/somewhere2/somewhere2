/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.benchmark;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageBuilder;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.MessageTypeSingletonFactory;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageProcessor;
import fr.lri.iasi.somewhere.benchmark.types.BenchmarkAnswerMessageType;

/**
 * Processor responsible for the "Answer" message type.
 * 
 * @author Andre Fonseca
 *
 */
public class BenchmarkAnswerProcessor extends AbstractMessageProcessor {
	static Logger logger = LoggerFactory.getLogger(BenchmarkAnswerProcessor.class);
	
	/* --- Properties --- */
	/** Module Manager to be used */
    private final BenchmarkManager benchmarkManager;

	public BenchmarkAnswerProcessor(MessageSession session) {
        super(session);
        
        App app = session.getCommunicationManager().getApp();
        this.benchmarkManager = BenchmarkManager.getModuleInstance(app);
    }

	/* --- Methods --- */
	/**
     * {@inheritDoc}
     */
	@Override
	public void process(Message message) {
		
		final Set<String> answers = BenchmarkManager.ANSWERS.getContent(message);
		final Set<String> history = BenchmarkManager.HISTORY.getContent(message);
		final Set<String> combinations = new HashSet<String>();
		final Set<String> receivedAnswers = new HashSet<String>();
		final Set<String> combiningPeers = new HashSet<String>();
		
		for (Message receivedMessage : session.getReceivedMessages()) {
			combiningPeers.add(receivedMessage.getOrigin().getName());
			
			Set<String> received = BenchmarkManager.ANSWERS.getContent(receivedMessage);
			if (received != null && !answers.equals(received))
				receivedAnswers.addAll(received);
			
			// Do not analyze messages that has arrived after the current message
			if (message.equals(receivedMessage))
				break;
		}
		
		// Do the combination of received answers!
		final double combinationFactor = benchmarkManager.getBenchmarkInstance().getCombinationFactor();
		for (String answer : answers) {
			Random wheel = new Random();

			for (String receivedAnswer : receivedAnswers) {
				final double canCombineFactor = wheel.nextDouble();
				
				if (canCombineFactor <= combinationFactor) {
					List<String> answerStrs = Arrays.asList(answer.split("_"));
					StringBuilder combinedAnswer = new StringBuilder();
					
					for (String answerStr : answerStrs) {
						if (!receivedAnswer.contains(answerStr) && !answerStr.isEmpty()) {
							combinedAnswer.append(answerStr);
							combinedAnswer.append("_");
						}
					}
					
					if (combinedAnswer != null) {
						String newAnswer = combinedAnswer.toString() + receivedAnswer;
						if (!hasEquivalents(combinations, newAnswer))
							combinations.add(newAnswer);
					}
				}
			}
		}
		
		combiningPeers.addAll(history);
		
		if (!combinations.isEmpty())
			returnAnswers(combinations, combiningPeers);
		else
			returnAnswers(answers, combiningPeers);
	}
	
	/**
	 * Verify equivalents strings on the combination in order to avoid re-working them.
	 * @param answers
	 * @param newAnswer
	 * @return
	 */
	private boolean hasEquivalents(Set<String> answers, String newAnswer) {
		for (String answer : answers) {
			List<String> answerStrs = Arrays.asList(answer.split("_"));
			List<String> answerToCompareStrs = Arrays.asList(newAnswer.split("_"));
			
			if (answerStrs.size() == answerToCompareStrs.size() && answerStrs.containsAll(answerToCompareStrs))
				return true;
		}
		
		return false;
	}
	
	/**
	 * Return the combined answers to the creator session.
	 * @param answers
	 * @param combiningPeers
	 */
	private void returnAnswers(Set<String> answers, Set<String> combiningPeers) {
		try {
			MessageBuilder builder = new MessageBuilder(session.getCommunicationManager());
			BenchmarkManager.ANSWERS.addContent(builder, answers);

			BenchmarkManager.HISTORY.addContent(builder, combiningPeers);
			BenchmarkManager.ANSWER_NUMBER.addContent(builder, session.getSentMessagesCount("answer") + 1);
			
			session.sendMessage(builder,
					session.getCreatorPeerName(),
					MessageTypeSingletonFactory.create(BenchmarkAnswerMessageType.class));
		} catch (CommunicationException e) {
			logger.error("There were problems sending the answer message: {}", e.getMessage());
		}
	}
	
}
