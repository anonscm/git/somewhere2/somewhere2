/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.benchmark;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.api.communication.CommunicationException;
import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageBuilder;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.MessageTypeSingletonFactory;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageProcessor;
import fr.lri.iasi.somewhere.benchmark.types.BenchmarkAnswerMessageType;
import fr.lri.iasi.somewhere.benchmark.types.BenchmarkFinishMessageType;
import fr.lri.iasi.somewhere.benchmark.types.BenchmarkQueryMessageType;

/**
 * Processor responsible for the "Query" message type.
 * 
 * @author Andre Fonseca
 */
public class BenchmarkQueryProcessor extends AbstractMessageProcessor {
	static Logger logger = LoggerFactory.getLogger(BenchmarkQueryProcessor.class);
	
	/* --- Properties --- */
	/** Module Manager to be used */
    private final BenchmarkManager benchmarkManager;
    
    /** Finish messages counter. In order to finish, a query processor must receive the same number of finishes than
     * the number of its sent queries. */
    private CountDownLatch finishCountDown;

	public BenchmarkQueryProcessor(MessageSession session) {
        super(session);

        App app = session.getCommunicationManager().getApp();
        this.benchmarkManager = BenchmarkManager.getModuleInstance(app);
    }
	
	/* --- Accessors --- */
	CountDownLatch getFinishCountDown() {
		return finishCountDown;
	}

	/* --- Methods --- */
    /**
     * {@inheritDoc}
     */
	@Override
	public void process(Message message) {
		
		final String localPeerName = session.getCommunicationManager().getLocalPeer().getName();
		final Set<String> history = BenchmarkManager.HISTORY.getContent(message);
		final String query = BenchmarkManager.QUERY.getContent(message);
    	history.add(localPeerName);
    	
		Set<String> neighbors = benchmarkManager.getBenchmarkInstance().getNeighbors();

		returnLocalAnswers(localPeerName, message.getOrigin().getName(), history);
		
		propagateQueries(query, neighbors, history);
		
		returnFinishAnswer();
	}
	
	/**
	 * Avoid loops on the query propagation
	 * @param <T>
	 * @param neighbors of the local peer
	 * @param history of this query
	 * 
	 */
	private boolean avoidLoops(String destination, Set<String> history) {
		
		// Remove peers that are already part of the history of this query
		if (history.contains(destination))
				return true;
		
		return false;
	}
	
	/**
	 * Propagate queries to neighbors
	 * @param <T>
	 * @param query content
	 * @param neighbors of the local peer
	 * @param history of this query
	 * @return boolean indicating if new queries were sent or not
	 */
	private void propagateQueries(String query, Set<String> neighbors, Set<String> history) {

		Random wheel = new Random();
		final double propagateQueryFactor = benchmarkManager.getBenchmarkInstance().getPropagateQueriesFactor();
		final double queryFactor = wheel.nextDouble();
		
		if (queryFactor <= propagateQueryFactor) {
			final int numNeighborsToQuery = wheel.nextInt(neighbors.size() + 1);
			MessageBuilder builder = new MessageBuilder(session.getCommunicationManager());
			BenchmarkManager.HISTORY.addContent(builder, history);
			BenchmarkManager.QUERY.addContent(builder, query);
				
			String[] neighborsToQuery = shuffleAndTruncate(neighbors.toArray(new String[0]), numNeighborsToQuery);
			
			this.finishCountDown = new CountDownLatch(numNeighborsToQuery);
			
			for (String destination : neighborsToQuery) {
				try {
					if (avoidLoops(destination, history)) {
						this.finishCountDown.countDown();
						continue;
					}
					
					session.sendMessage(builder,
							destination,
							MessageTypeSingletonFactory.create(BenchmarkQueryMessageType.class));
	
				} catch (CommunicationException e) {
					logger.error("There were problems sending the query message: {}", e);
				}
			}
			
			try {
				this.finishCountDown.await(60, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Shuffle a set of items in an array. Useful for select random neighbors.
	 * @param <T>
	 * @param items the array of elements
	 * @param m the number of elements in the subset
	 * @return shuffled subset
	 */
	private <T> T[] shuffleAndTruncate(T[] items, int num) {
		Random wheel = new Random();
		
		for (int i = 0; i < items.length; i++) {
			int pos = i + wheel.nextInt(items.length - i);
			T tmp = items[pos];
			items[pos] = items[i];
			items[i] = tmp;
		}
		
		return Arrays.copyOf(items, num);
	}


	/**
	 * Return local answers to the peer that has sent the query
	 * @param <T>
	 * @param localPeerName
	 * @param origin of the query peer
	 * @param history of this query
	 */
	private void returnLocalAnswers(String localPeerName, String origin, Set<String> history) {
		
		Random wheel = new Random();
		final double localConsequencesFactor = benchmarkManager.getBenchmarkInstance().getLocalConsequencesFactor();
		final double answersFactor = wheel.nextDouble();
		
		Set<String> localAnswers = new HashSet<String>();
		if (answersFactor <= localConsequencesFactor) {
			String content = localPeerName;
			localAnswers.add(content);
		}
	
		if (!localAnswers.isEmpty()) {
			MessageBuilder builder = new MessageBuilder(session.getCommunicationManager());
			BenchmarkManager.HISTORY.addContent(builder, history);
			BenchmarkManager.ANSWERS.addContent(builder, localAnswers);
			BenchmarkManager.ANSWER_NUMBER.addContent(builder, 1);
			
			try {
				session.sendMessage(builder,
						origin,
						MessageTypeSingletonFactory.create(BenchmarkAnswerMessageType.class));
			} catch (CommunicationException e) {
				logger.error("There were problems sending the query message: {}", e.getMessage());
			}
		}
	}
	
	/**
	 * Send finish message to the peer that had created this session
	 */
	private void returnFinishAnswer() {
		try {
			MessageBuilder builder = new MessageBuilder(session.getCommunicationManager());
			
			BenchmarkManager.ANSWER_NUMBER.addContent(builder, session.getSentMessagesCount("finish") + 
					session.getSentMessagesCount("answer") + 1);
			
			session.sendMessage(builder,
					session.getCreatorPeerName(), 
					MessageTypeSingletonFactory.create(BenchmarkFinishMessageType.class));
		} catch (CommunicationException e) {
			logger.error("There were problems sending the finish message: {}", e.getMessage());
		}
	}

}
