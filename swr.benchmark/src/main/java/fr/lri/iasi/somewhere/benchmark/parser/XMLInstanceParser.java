/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.benchmark.parser;

import fr.lri.iasi.libs.formallogic.utils.instance.AbstractInstanceParser;
import fr.lri.iasi.libs.formallogic.utils.instance.Instance;
import fr.lri.iasi.somewhere.XPathReader;
import fr.lri.iasi.somewhere.benchmark.BenchmarkInstance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPathConstants;


/**
 * Parse the content of a XML peer profile into a propositional instance
 * @author Andre Fonseca
 */
public class XMLInstanceParser extends AbstractInstanceParser {
    static Logger logger = LoggerFactory.getLogger(XMLInstanceParser.class);

    /* --- Constants --- */
    /** The name of the schema file */
    protected final static String SCHEMA_FILE = "benchmark_instance.xsd";

    /* --- Properties --- */
    /** The DOM document of the input file */
    protected Document document;

    /* --- Accessors --- */

    /**
     * @param path Path of the input XML file
     */
    public XMLInstanceParser(String path) throws FileNotFoundException {
        super(path);
    }

    /**
     * Validate the xml source
     * @return if is valid
     */
    protected boolean isSourceValid(Document document) {
        if (document == null) {
            throw new IllegalArgumentException("document");
        }

        // Lookup a factory for the W3C XML Schema language
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        // Compile the schema.
        URL schemaURL = getClass().getClassLoader().getResource("META-INF/" + SCHEMA_FILE);
        Schema schema = null;

        try {
            schema = factory.newSchema(schemaURL);
        } catch (SAXException e) {
            logger.error("There was a problem parsing schema file '" +
                SCHEMA_FILE + "'..");

            return false;
        }

        // Get a validator from the schema.
        Validator validator = schema.newValidator();

        // Check the document
        try {
            validator.validate(new DOMSource(document));

            return true;
        } catch (Exception ex) {
            logger.error("XML source file is not valid...");

            return false;
        }
    }

    /**
     * Parse a XML peer profile
     * @param document the Document to parse
     * @return the instance obtained
     */
    public Instance parse() {
        try {
        	DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = parser.parse(new File(path));
			
			if (!isSourceValid(document)) {
				throw new SAXException();
			}
        	
            XPathReader reader = new XPathReader(document.getFirstChild());
            String peerId = parsePeerID(reader);
            String instanceId = parseInstanceID(reader);
            double localFactor = parseLocalFactor(reader);
            double propagateFactor = propagateQueriesFactor(reader);
            double combinationFactor = parseCombinationFactor(reader);
            Set<String> neighbors = parseNeighbors(reader);

            return new BenchmarkInstance(peerId, instanceId, localFactor, propagateFactor, combinationFactor, neighbors);
        } catch (Exception e) {
            logger.error(
                "There were errors parsing XML configuration file : ", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Parse XML configuration file, part 'URI'
     * @return the URI
     */
    private String parsePeerID(XPathReader reader) throws Exception {
        return reader.read("uri", XPathConstants.STRING).toString();
    }
    
    /**
     * Parse XML configuration file, part 'Instance'
     * @return the Instance string
     */
    private String parseInstanceID(XPathReader reader) throws Exception {
        return reader.read("instance", XPathConstants.STRING).toString();
    }
    
    /**
     * Parse XML configuration file, part 'localConsequencesFactor'
     * @return the localConsequencesFactor double
     */
    private double parseLocalFactor(XPathReader reader) throws Exception {
        return (Double.parseDouble(reader.read("localConsequencesFactor", XPathConstants.NUMBER).toString()));
    }
    
    /**
     * Parse XML configuration file, part 'propagateQueriesFactor'
     * @return the localConsequencesFactor double
     */
    private double propagateQueriesFactor(XPathReader reader) throws Exception {
        return (Double.parseDouble(reader.read("propagateQueriesFactor", XPathConstants.NUMBER).toString()));
    }
    
    /**
     * Parse XML configuration file, part 'propagateQueriesFactor'
     * @return the localConsequencesFactor double
     */
    private double parseCombinationFactor(XPathReader reader) throws Exception {
        return (Double.parseDouble(reader.read("combinationFactor", XPathConstants.NUMBER).toString()));
    }
    
    /**
     * Parse XML configuration file, part 'neighbors'
     */
    private Set<String> parseNeighbors(XPathReader reader) throws Exception {
        Set<String> neighbors = new HashSet<String>();
        NodeList neihborsNodes = (NodeList) reader.read("neighbors/neighbor",
                XPathConstants.NODESET);

        for (int index = 0; index < neihborsNodes.getLength(); index++) {
            Node node = neihborsNodes.item(index);
            neighbors.add(node.getTextContent());
        }
        
        return neighbors;
    }


    /**
     * Export XML configuration node to obtain corresponding
     * peer profile
     * @param document the Document to parse
     * @return the PropositionalInstance obtained
     */
    public void export(String path) {
        Document doc = null;

        try {
            // Create a document
            DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
            doc = docBuilder.newDocument();

            // Create the root element and add it to the document
            Element root = doc.createElement("peer");
            doc.appendChild(root);

            // Create all the elements
            exportHeader(doc, root);
            //exportTheory(doc, root); TODO
            //exportMapping(doc, root);
            //exportProduction(doc, root);

            // Prepare the output file 
            Source source = new DOMSource(doc);
            File file = new File(path);
            Result result = new StreamResult(file);

            // Write the DOM document to the file 
            Transformer xformer = TransformerFactory.newInstance()
                                                    .newTransformer();
            xformer.setOutputProperty(OutputKeys.INDENT, "yes");
            xformer.transform(source, result);
        } catch (Exception e) {
            logger.error("There was a problem exporting " +
                "propositional instance to file '" + path + "' !", e);
        }
    }

    /**
     * Export to instance to XML file, headers part
     * @param doc The XML Document
     * @param root the root of the document
     */
    private void exportHeader(Document doc, Element root)
        throws Exception {
        Element uri = doc.createElement("uri");
        uri.setTextContent(instance.getName());
        root.appendChild(uri);
    }
	
}
