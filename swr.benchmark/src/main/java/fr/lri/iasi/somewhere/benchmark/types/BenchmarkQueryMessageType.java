/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.benchmark.types;

import fr.lri.iasi.somewhere.api.communication.model.Message;
import fr.lri.iasi.somewhere.api.communication.model.MessageProcessor;
import fr.lri.iasi.somewhere.api.communication.model.MessageSession;
import fr.lri.iasi.somewhere.api.communication.model.impl.AbstractMessageType;
import fr.lri.iasi.somewhere.benchmark.BenchmarkManager;
import fr.lri.iasi.somewhere.benchmark.BenchmarkQueryProcessor;

/**
 * The query message type of the benchmark module.
 * 
 * @author Andr?? Fonseca
 *
 */
public class BenchmarkQueryMessageType extends AbstractMessageType {
    private static final long serialVersionUID = 8671455623477769379L;
    
    /**
     * {@inheritDoc}
     */
    @Override
	public String getName() {
		return "query";
	}

    /**
     * {@inheritDoc}
     */
    @Override
	public boolean isReturningType() {
		return false;
	}

    /**
     * {@inheritDoc}
     */
    @Override
	public MessageProcessor createProcessor(MessageSession session) {
		return new BenchmarkQueryProcessor(session);
	}

    /**
     * {@inheritDoc}
     */
    @Override
	public boolean needsNewSessionSend() {
		return true;
	}
	
    /**
     * {@inheritDoc}
     */
    @Override
	public boolean needsNewSessionReceive() {
		return true;
	}

    /**
     * {@inheritDoc}
     */
    @Override
	public boolean needsToMaintainSessionSend(MessageSession session) {
		return true;
	}
	
    /**
     * {@inheritDoc}
     */
    @Override
	public boolean needsToMaintainSessionReceive(MessageSession session) {
		return true;
	}

    /**
     * {@inheritDoc}
     */
    @Override
	public String print(Message message) {
		String res = "";

        res += "==============================================================================";
        res += ("\n\t Query message");
        res += ("\n\t Origin : " + message.getOrigin().getName());
        res += ("\n\t Query : " + BenchmarkManager.QUERY.getContent(message) + "\n");

        return res;
	}

}
