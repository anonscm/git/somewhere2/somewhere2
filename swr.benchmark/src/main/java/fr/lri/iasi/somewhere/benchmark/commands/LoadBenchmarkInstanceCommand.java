/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.benchmark.commands;

import java.util.List;

import fr.lri.iasi.libs.formallogic.utils.instance.ParsersFacadeListener;
import fr.lri.iasi.somewhere.benchmark.BenchmarkInstance;
import fr.lri.iasi.somewhere.benchmark.BenchmarkManager;
import fr.lri.iasi.somewhere.benchmark.parser.BenchmarkInstanceParsersFacade;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

/**
 * Command responsible for load benchmark instance.
 * 
 * @author Andr?? Fonseca
 */
public class LoadBenchmarkInstanceCommand implements Command {
	/* --- Properties --- */
    /** Manager of the module */
    protected final BenchmarkManager manager;

    public LoadBenchmarkInstanceCommand(BenchmarkManager manager) {
        if (manager == null) {
            throw new IllegalArgumentException("manager");
        }

        this.manager = manager;
    }
    
	/* --- Accessors --- */
    public BenchmarkManager getManager() {
        return manager;
    }
    
	/**
     * @return the name of the command
     */
	public String getName() {
		return "loadBenchmarkInstance";
	}

	/**
     * @return the description of the command
     */
	public String getDescription() {
		return "Load a new Benchmark instance in place of the old one " +
        "(if it already existed).";
	}

	/**
     * Execute the command
     * @param parameters The parameters of the command
     * @return the return status
     *  NOTE : note that the return status contains only text
     *  information
     */
	public ReturnStatus execute(List<Object> parameters) {
		ReturnStatus res = null;

        try {
            // Verify there are enough parameters
            if (parameters.size() < 1) {
                res = new GenericReturnStatus("Syntax Error! \n Usage :" +
                        "\t" + getName() + " FILE_PATH", false, null);

                return res;
            }

            // Get useful parameters
            String path = parameters.get(0).toString();

            // Load propositional instance !
            BenchmarkInstanceParsersFacade.parse(path, new Listener());

            res = new GenericReturnStatus("Successfully launched benchmark instance analysis...",
                    true, null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error loading " +
                    "new propositional instance: " + e, false, null);
        }

        return res;
	}
	
	/**
     * This class is used to know when the file loading is finished, and
     *  to update the <b>BenchmarkInstance</b> of <b>BenchmarkManager</b>
     */
    protected class Listener implements ParsersFacadeListener<BenchmarkInstance> {
        /**
         * {@inheritDoc}
         */
        public void loadingCompleted(BenchmarkInstance instance) {
            manager.setBenchmarkInstance(instance);
            
            manager.getCommunicationManager().getConfiguration().setPeerName(instance.getPeerName());
            manager.getCommunicationManager().getConfiguration().setPeerGroupName(instance.getName());
            manager.getCommunicationManager().getCommunicationModule().restart();
            
            for (String neighbor : instance.getNeighbors()) {
            	manager.getCommunicationManager().getCommunicationModule().getPeerGroup().retrievePeer(neighbor);
			}
        }
    }
}
