/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.benchmark.commands;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lri.iasi.somewhere.api.communication.model.MessageBuilder;
import fr.lri.iasi.somewhere.api.communication.model.MessageTypeSingletonFactory;
import fr.lri.iasi.somewhere.api.communication.model.Peer;
import fr.lri.iasi.somewhere.benchmark.BenchmarkManager;
import fr.lri.iasi.somewhere.benchmark.BenchmarkAnswersCollector;
import fr.lri.iasi.somewhere.benchmark.types.BenchmarkQueryMessageType;
import fr.lri.iasi.somewhere.ui.CommandListener;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

/**
 * Describes the start benchmark command.
 * 
 * @author Andr?? Fonseca
 */
public class BenchmarkCommand implements Command {
	static Logger logger = LoggerFactory.getLogger(BenchmarkCommand.class);
	
	/* --- Properties --- */
	/** Module Manager to be used */
    private final BenchmarkManager benchmarkManager;
    
    /** Indicates if the execution of this command is completed */
    private AtomicBoolean executionCompleted = new AtomicBoolean();
    
    public BenchmarkCommand(BenchmarkManager benchmarkManager) {
        if (benchmarkManager == null)
            throw new IllegalArgumentException("benchmarkManager");

        this.benchmarkManager = benchmarkManager;
    }

    /* --- Accessors --- */
	@Override
	public String getName() {
		return "startBenchmark";
	}

	@Override
	public String getDescription() {
		return "Start benchmark process on the current network";
	}

	/* --- Methods --- */
	/**
     * Execute the command
     * @param parameters The parameters of the command
     * @return the return status
     *  NOTE : note that the return status contains only text
     *  information
     */
	public ReturnStatus execute(List<Object> parameters) {
		ReturnStatus res = null;

        try {
            // Verify if the parameters are good (no parameters in this case...)
            if (parameters != null && parameters.size() > 1) {
                res = new GenericReturnStatus("Syntax Error! \n Usage :" +
                        "\t" + getName() + " [PAYLOAD_KBYTES]", false, null);

                return res;
            }
            
            // If there is a loaded instance
            if (benchmarkManager.getBenchmarkInstance() == null) {
            	res = new GenericReturnStatus("Instance not loaded! Load an instance before start the benchmark.", false, null);

                return res;
            }
            
            this.executionCompleted.set(false);
            
            // Get useful parameters
            Integer payload = 1024;
            if (!parameters.isEmpty() && parameters.get(0) != null)
            	payload *= Integer.valueOf(parameters.get(0).toString());
            
            // Create the answer receiver
            BenchmarkAnswersCollector receiver = 
            		new BenchmarkAnswersCollector(benchmarkManager,
            									  new BenchmarkListener());
            
            // Start the benchmark !
            long initTime = System.nanoTime();
            sendMessages(payload);
            long answersTime = waitAnswers(initTime);
            
            receiver.finishReceiving();
            
            // Build results
            int messageSessions = benchmarkManager.getCommunicationManager().getSessionHandler().getMessageSessions().size();
            int creatorSessions = benchmarkManager.getCommunicationManager().getSessionHandler().getCreatorSessions().size();
            
            String result = "==============================BENCHMARK=RESULTS================================" +
            		"\n\t Were collected " + String.valueOf(receiver.getAnswerReceived()) + " normal answers." +
            		"\n\t Were collected " + String.valueOf(receiver.getFinishesReceived()) + " finish answers." +
            		"\n\t Benchmark took " + (answersTime/1000000) + " milliseconds to receive all answers." +
            		"\n\t Size of remaining sessions (expected => 0): messageSessions => " + messageSessions + " creatorSessions => " + creatorSessions + "." +
            				"\n==============================================================================";

            res = new GenericReturnStatus(
            		result, 
            		true,
                    null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error starting the benchmark ...",
                    false, null);
        }

        return res;
	}
	
	/**
     * Sends a message given the destination and the content
     * @param destination The name of the peer to send the message to
     * @param content The content of the message
     */
    protected void sendMessages(int payload)
        throws Exception {
    	if (payload < 0 || payload > 65536)
    		throw new IllegalArgumentException("payload");

    	// Build query content
    	StringBuilder queryBuilder = new StringBuilder();
    	for (int i = 0; i < (payload/2); i++)
    		queryBuilder.append('X');
    	       
        // Reference the local peer
    	Peer userPeer = new Peer("User");
        Peer localPeer = new Peer(benchmarkManager.getCommunicationManager().getLocalPeer().getName());
        
        // Create the message !
        MessageBuilder builder = new MessageBuilder(benchmarkManager.getCommunicationManager())
        			.setOrigin(userPeer)
        			.setDestination(localPeer)
        			.setType(MessageTypeSingletonFactory.create(BenchmarkQueryMessageType.class));

        // Set the message content
        BenchmarkManager.QUERY.addContent(builder, queryBuilder.toString());
        BenchmarkManager.HISTORY.addContent(builder, new LinkedHashSet<String>());
        
        benchmarkManager.getCommunicationManager().receiveMessage(builder.build());
    }
    

    /**
     * Blocks the UI until the execution is completed
     * @param initTime
     * @return the command execution time
     * @throws InterruptedException
     */
	public long waitAnswers(long initTime) 
		throws InterruptedException {
		
		while (!executionCompleted.get())
    		Thread.sleep(10);
		
		return (System.nanoTime() - initTime);
    }
	
	/**
     * The command listener used to indicate the end of the command execution.
     * 
     * @author Andre Fonseca
     */
	private class BenchmarkListener implements CommandListener {

		@Override
		public void executionCompleted() {
			executionCompleted.compareAndSet(false, true);
		}

		@Override
		public long getCommandStartTime() {
			return 0;
		}

		@Override
		public boolean isExecutionCompleted() {
			return executionCompleted.get();
		}
		
	}
}
