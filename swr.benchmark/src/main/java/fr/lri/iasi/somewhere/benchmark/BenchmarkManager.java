/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.benchmark;

import java.util.Set;

import fr.lri.iasi.somewhere.AbstractModule;
import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.api.communication.CommunicationManager;
import fr.lri.iasi.somewhere.api.communication.model.MessageContentKey;
import fr.lri.iasi.somewhere.benchmark.commands.BenchmarkCommand;
import fr.lri.iasi.somewhere.benchmark.commands.LoadBenchmarkInstanceCommand;
import fr.lri.iasi.somewhere.benchmark.configuration.BenchmarkXMLConfiguration;
import fr.lri.iasi.somewhere.ui.UI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.Node;


/**
 * The main class of the Benchmark module
 * @author Andr?? Fonseca
 */
public class BenchmarkManager extends AbstractModule {
    static Logger logger = LoggerFactory.getLogger(BenchmarkManager.class);

    /* --- Constants --- */
    /** The name of this module */
    protected static final String moduleName = "benchmark";

    /* --- Properties --- */
    /** The configuration for this module */
    protected final BenchmarkXMLConfiguration configuration;
    
    /** The communication manager */
    private final CommunicationManager communicationManager;
    
    /** The benchmark instance */
    private BenchmarkInstance benchmarkInstance;
    
    /** Create/Find answers content type*/
    public static final MessageContentKey<Set<String>> ANSWERS = MessageContentKey.create("answers");

    /** Create/Find history content type*/
    public static final MessageContentKey<Set<String>> HISTORY = MessageContentKey.create("history");
    
    /** Create/Find query content type*/
    public static final MessageContentKey<String> QUERY = MessageContentKey.create("query");

    /** Create/Find answer number content type*/
    public static final MessageContentKey<Integer> ANSWER_NUMBER = MessageContentKey.create("answerNumber");
    
    public BenchmarkManager(App app) {
        super(app);

        communicationManager = CommunicationManager.getModuleInstance(app);

        if (communicationManager == null) {
            throw new UnsupportedOperationException(
                "A communicationManager module must be loaded");
        }
        
        // Init conf
        configuration = new BenchmarkXMLConfiguration(this);

        // Add Commands to the UI
        addBaseCommands();
    }

    /* --- Accessors --- */
    public CommunicationManager getCommunicationManager() {
        return communicationManager;
    }

    public String getModuleName() {
        return moduleName;
    }
    
    public BenchmarkInstance getBenchmarkInstance() {
    	return benchmarkInstance;
    }
    
    public BenchmarkXMLConfiguration getBenchmarkConfiguration() {
    	return configuration;
    }
    
    /* --- Mutators --- */
    public void setBenchmarkInstance(BenchmarkInstance benchmarkInstance) {
    	this.benchmarkInstance = benchmarkInstance;
    }

    /* --- Methods --- */
    public void init() {
    	// Nothing by the moment...
    }

    public void quit() {
        // Nothing by the moment...
    }

    /**
     * Add benchmark commands to the UI
     */
    protected void addBaseCommands() {
        UI ui = app.getUI();
        ui.addCommand(new LoadBenchmarkInstanceCommand(this));
        ui.addCommand(new BenchmarkCommand(this));
    }

    /**
     * {@inheritDoc}
     */
    public void importConfigXML(Node config) {
    	configuration.parseConfig(config);
    }
    
    /**
     * {@inheritDoc}
     */
    public static BenchmarkManager getModuleInstance(App app) {
        if (app == null) {
            throw new IllegalArgumentException("app");
        }

        Module m = app.getModule(moduleName);

        if (m == null) {
            return null;
        }

        try {
        	BenchmarkManager res = (BenchmarkManager) m;

            return res;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    public Node exportConfigXML() {
    	try {
            return configuration.export();
        } catch (final Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
