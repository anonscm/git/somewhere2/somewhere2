/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.baseApp;

import org.w3c.dom.Node;

import fr.lri.iasi.somewhere.AbstractModule;
import fr.lri.iasi.somewhere.App;

/**
 * Fake module : FOR TESTS ONLY!
 * 
 * @author Andre Fonseca
 *
 */
public class FakeModule extends AbstractModule {
	
	/* --- Constants --- */
    /** The name of this module */
    protected String moduleName;

	public FakeModule(App app) {
		super(app);
	}

	@Override
	public void quit() {
		// ...
	}

	@Override
	public String getModuleName() {
		return moduleName;
	}
	
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	@Override
	public void init() {
		// ...
	}

	@Override
	public void importConfigXML(Node config) {
		// ...
	}

	@Override
	public Node exportConfigXML() {
		return null;
	}

}
