/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.baseApp;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;

import org.custommonkey.xmlunit.Diff;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import fr.lri.iasi.somewhere.SomewhereException;
import fr.lri.iasi.somewhere.baseApp.commands.ModulesListCommand;
import fr.lri.iasi.somewhere.baseApp.commands.ShowConfigurationCommand;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class AppTest {
    private AbstractApp app;

    @Before
    public void Init() {
        app = new FakeApp();
        app.modules.removeAll();
        
        FakeModule module1 = new FakeModule(app);
    	module1.setModuleName("module1");
    	
    	FakeModule module2 = new FakeModule(app);
    	module2.setModuleName("module2");
    	
    	try {
			app.modules.add(module1);
			app.modules.add(module2);
	    	app.modules.add(app);
		} catch (SomewhereException e) {
			// ...
		}
    }

    @Test
    public void modulesAreAddedCorrectly() {
        assertEquals(app.getModules().size(), 3);
    }

    @Test
    public void commandsAreAddedOnCreation() {
        assertFalse(app.getUI().getCommands().size() == 0);
    }

    @Test
    public void mustReturnAValidModuleName() {
        assertFalse(app.getModuleName().equals(""));
    }
    
    @Test
    public void modulesListCommandRunsCorrectly() {
    	
    	ModulesListCommand command = new ModulesListCommand(app);
    	ReturnStatus res = command.execute(null);
    	
    	String resultString = "module2\nmodule1\nbase";
        assertEquals(res.getTextualInfos(), resultString);
    }
    
    @Test(expected=SomewhereException.class)
    public void cannotAddModulesWithSameName() throws SomewhereException {
    	FakeModule module1 = new FakeModule(app);
    	module1.setModuleName("module1");
    	
		app.modules.add(module1);
    }
    
    @Test
    public void correctlyExportXMLConfig() throws SAXException, IOException {
    	Node config1 = createConfigHelper(2, 3);
    	app.importConfigXML(config1);
    	
    	Node export = app.exportConfigXML().getFirstChild();
    	Diff myDiff = new Diff(new DOMSource(config1), new DOMSource(export));
    	assertTrue(myDiff.similar());
    }
    
    @Test
    public void showConfigurationRunsCorrectly() {
    	Node config1 = createConfigHelper(2, 3);
    	app.importConfigXML(config1);
    	
    	ShowConfigurationCommand command = new ShowConfigurationCommand(app);
    	ReturnStatus res = command.execute(null);
    	
    	String resultString = "\n# Configuration of module module1 :\n<?xml version=\"1.0\" encoding=\"UTF-8\"" +
    						" standalone=\"no\"?>\n\n\n# Configuration of module module2 :\n<?xml version=\"1.0\"" +
    						" encoding=\"UTF-8\" standalone=\"no\"?>\n\n\n# Configuration of module base :\n<?xml" +
    						" version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n<base>\n    <logFile/>\n</base>\n";
    	String textInfo = res.getTextualInfos().replaceAll("\\s+", "");
    	
        assertEquals(textInfo, resultString.replaceAll("\\s+", ""));
    }
    
    @Test
    public void quitGracefully() {
    	Node config1 = createConfigHelper(2, 3);
    	app.importConfigXML(config1);
    	
    	app.modules.initAll();
    	
    	assertTrue(app.modules.removeAll());
        
    }
    
    private Node createConfigHelper(Integer order1, Integer order2) {
    	
    	Element root = null;
    	
		try {
			// Create new doc for the config subtree
			DocumentBuilder builder;
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.newDocument();
	    	root = doc.createElement("base");
	    	doc.appendChild(root);
	    	
	    	// 'LogFile' segment
	    	Element logFile = doc.createElement("logFile");
	    	root.appendChild(logFile);
	    	
		} catch (ParserConfigurationException e) {
			
		}
    	
    	return root;
    }
    
}
