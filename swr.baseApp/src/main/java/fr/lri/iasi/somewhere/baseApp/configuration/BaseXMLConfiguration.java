/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.baseApp.configuration;

import fr.lri.iasi.somewhere.Configuration;
import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.XPathReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;


/**
 * Specifies the base modules configuration (module loading order and base log file).
 * 
 * @author Andre Fonseca
 */
public class BaseXMLConfiguration extends Configuration {
	private static final long serialVersionUID = -1082118552539305828L;
	static Logger logger = LoggerFactory.getLogger(BaseXMLConfiguration.class);
	
	/* --- Constants --- */
    /** The name of the schema file */
    protected final static String SCHEMA_FILE = "app_config.xsd";

    /* --- Properties --- */
    /** Log file name */
    private String logFileName;
    
    /** Set of wanted modules names */
    private Map<String, Integer> modulesEntries;
    
    public BaseXMLConfiguration(Module module) {
        super(module);

        this.modulesEntries = new LinkedHashMap<String, Integer>();
    }
    /* --- Accessors --- */
    public final String getLogFileName() {
        return logFileName;
    }
    
    public final Map<String, Integer> getModulesEntries() {
        return Collections.unmodifiableMap(modulesEntries);
    }

    /* --- Methods --- */
    /**
     * {@inheritDoc}
     */
    protected void setAllToDefault() {
    	// Does nothing here...
    }
    
    /**
     * {@inheritDoc}
     */
    public void parseConfig(Node config) {
    	try {
	        if (config == null) {
	            throw new IllegalArgumentException("config");
	        }
	
	        XPathReader reader = new XPathReader(config);
	        parseLogFile(reader);
    	} catch (Exception e) {
            logger.error("There were errors parsing XML configuration file : " +
                e.getMessage());
            throw new RuntimeException(e);
        }
    }
    
    /**
     * Parse "logFile" elements 
     * @throws XPathExpressionException 
     */
    private void parseLogFile(XPathReader reader) throws XPathExpressionException {
    	
    	String logFileNameText = reader.read("logFile",
                XPathConstants.STRING).toString();
    	
    	if (!logFileNameText.isEmpty()) {
    		logFileNameText = logFileNameText.replace("~", System.getProperty("user.home"));
    		
    		logFileName = logFileNameText;
    	}
    }

    /**
     * Export the configuration into an XML node
     */
    public Node export() {
        Document doc;

        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance()
                                                            .newDocumentBuilder();
            doc = builder.newDocument();

            Element element = doc.createElement(getModule().getModuleName());
            doc.appendChild(element);
            
            // Create the 'logFile' element
            Element logFileElem = doc.createElement("logFile");
            logFileElem.setTextContent(logFileName);
            element.appendChild(logFileElem);
        } catch (Exception e) {
            logger.error(
                "There were errors exporting XML configuration file : " +
                e.getMessage());
            throw new RuntimeException(e);
        }

        return doc;
    }

}
