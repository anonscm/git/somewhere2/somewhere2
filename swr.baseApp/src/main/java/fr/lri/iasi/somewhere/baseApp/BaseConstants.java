/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.baseApp;

/**
 * The Base contants to be used by Somewhere
 * @author Andre Fonseca
 *
 */
public final class BaseConstants {
	
    public static final String DEFAULT_CONFIG_PATH = "somewhere.xml";
    public static final String DEFAULT_LOG_PATH = "somewhere.log";
    public static final int GOOD_RESULT = 0;
    public static final int BAD_RESULT = 1;
    
    private BaseConstants() {
	}
}
