/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.baseApp.commands;

import fr.lri.iasi.somewhere.App;
import fr.lri.iasi.somewhere.baseApp.XMLConfigurationManager;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * A command to import a configuration file into the global configuration
 * 
 * @author Andre Fonseca
 */
public class ImportConfigurationFileCommand implements Command {
    
	/* --- Properties --- */
    /** Base App object */
    private final App app;

    public ImportConfigurationFileCommand(App app) {
        if (app == null) {
            throw new IllegalArgumentException("app");
        }

        this.app = app;
    }

    /* --- Accessors --- */
    /**
     * {@inheritDoc}
     */
    public String getName() {
        return "importConfigurationFile";
    }

    /**
     * {@inheritDoc}
     */
    public String getDescription() {
        return "Import a configuration file.";
    }

    /* --- Methods --- */
    /**
     * Import one or a list of configuration files.
     * @param parameters : list of configuration file paths.
     * @return the return status
     */
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;

        try {
            Set<String> wantedFiles = new HashSet<String>();

            // Retreiving files names
            if ((parameters != null) && (parameters.size() > 0)) {
                for (Object o : parameters) {
                    String s = o.toString();
                    wantedFiles.add(s);
                }
            }

            // Import configuration files
            for (String path : wantedFiles) {
                XMLConfigurationManager confManager = new XMLConfigurationManager(path);
                app.loadConfig(confManager);
            }

            res = new GenericReturnStatus("Successfully importing configuration.",
                    true, null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error importing configuration file...",
                    false, null);
        }

        return res;
    }
}
