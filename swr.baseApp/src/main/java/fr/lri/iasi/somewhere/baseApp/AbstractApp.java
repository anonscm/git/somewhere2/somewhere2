/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.baseApp;

import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.SomewhereException;
import fr.lri.iasi.somewhere.baseApp.commands.ChangeLogLevelCommand;
import fr.lri.iasi.somewhere.baseApp.commands.ImportConfigurationFileCommand;
import fr.lri.iasi.somewhere.baseApp.commands.ModulesListCommand;
import fr.lri.iasi.somewhere.baseApp.commands.ShowConfigurationCommand;
import fr.lri.iasi.somewhere.baseApp.configuration.BaseXMLConfiguration;
import fr.lri.iasi.somewhere.ui.UI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import ch.qos.logback.classic.LoggerContext;

import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


/**
 * The class which deals with the actions the user wants to execute,
 * and execute them according to the configuration
 * @author Andre Fonseca
 */
public abstract class AbstractApp implements fr.lri.iasi.somewhere.App,
    fr.lri.iasi.somewhere.Module {
    static Logger logger = LoggerFactory.getLogger(AbstractApp.class);

    /* --- Properties --- */
    /** Module Manager instance object */
    protected final Modules modules = Modules.getInstance();
    
    /** The default app loading order */
    protected final static Integer loadingOrder = 0;

    /** User Interface Manager */
    protected UI ui;
    
    /** The configuration for this module */
    protected final BaseXMLConfiguration configuration = new BaseXMLConfiguration(this);

    /** Configuration Manager */
    protected fr.lri.iasi.somewhere.ConfigurationManager configurationManager;

    /* --- Accessors --- */
    /**
     * {@inheritDoc}
     */
    public final UI getUI() {
        return ui;
    }

    /**
     * {@inheritDoc}
     */
    public final Set<Module> getModules() {
        return modules.getModules();
    }

    /**
     * {@inheritDoc}
     */
    public Module getModule(String name) {
        return modules.getModule(name);
    }

    /**
     * @return The module manager object
     */
    public Modules getModulesManager() {
        return modules;
    }
    
    /**
     * {@inheritDoc}
     */
    public Integer getLoadingOrder() {
    	return loadingOrder;
    }
    
    /* --- Mutators --- */
    /**
     * Loading order of an app cannot be changed.
     */
    public void setLoadingOrder(int order) {
    }
    
    /* --- Methods --- */
    /**
     * Add baseApp commands to the UI
     */
    protected void addBaseCommands() {
        ui.addCommand(new ImportConfigurationFileCommand(this));
        ui.addCommand(new ShowConfigurationCommand(this));
        ui.addCommand(new ModulesListCommand(this));
        ui.addCommand(new ChangeLogLevelCommand());
    }

    /**
     * {@inheritDoc}
     */
    public void loadConfig(
        fr.lri.iasi.somewhere.ConfigurationManager configurationManager) {
        logger.info("Analysing configuration ...");

        try {
            this.configurationManager = configurationManager;
            configurationManager.forwardConfig();
        } catch (Exception e) {
            logger.error("There were errors analysing configuration.");
            System.exit(BaseConstants.BAD_RESULT);
        }

        logger.info("Successfully analysed configuration.");
    }

    /**
     * Load the modules selected at compilation time
     */
    public final void loadModules() {
		try {
			ModuleManager manager = new ModuleManager();
			Set<String> classNamespaces = manager.fetchAllClasspathModuleClasses();
	        addModules(classNamespaces);
		} catch (SomewhereException e) {
			logger.error("There were errors loading somewhere modules: {}", e.getCause());
			System.exit(BaseConstants.BAD_RESULT);
		}
    }

    /**
     * {@inheritDoc}
     */
    public final void quit() {
    }

    /**
     * {@inheritDoc}
     */
    public final void quitApp() {
        quitApp(BaseConstants.GOOD_RESULT);
    }

    /**
     * Called when somewhere has to quit
     * @param result The exit status
     */
    private void quitApp(int result) {
        try {
            System.exit(result);
        } catch (Exception e) {
            logger.error("There were errors quitting somewhere.");
            System.exit(BaseConstants.BAD_RESULT);
        }
    }

    /**
     * {@inheritDoc}
     */
    public String getModuleName() {
        return "base";
    }

    /**
     * {@inheritDoc}
     */
    public void init() {
    	// Register shutdown thread
        Runtime.getRuntime().addShutdownHook(new ShutdownThread());
        
        // Set log properties
        updateLogFile();
        
        // Blocks the ui
        final ExecutorService blockUIService = Executors.newSingleThreadExecutor();
        blockUIService.submit(new Runnable() {
			public void run() {
				while(true) {
					try {
						Thread.sleep(5);
					} catch (InterruptedException e) {
						Thread.interrupted();
					}
				}
			}
		});
    }

    /**
     * {@inheritDoc}
     */
    public void importConfigXML(Node config) {
    	configuration.parseConfig(config);
    	
    	// Update log file
    	updateLogFile();
    }

    /**
     * {@inheritDoc}
     */
    public Node exportConfigXML() {
    	try {
            if (configuration == null) {
                DocumentBuilder builder = DocumentBuilderFactory.newInstance()
                                                                .newDocumentBuilder();
                Document doc = builder.newDocument();
                Element element = doc.createElement(getModuleName());
                doc.appendChild(element);

                return doc;
            }
            
            return configuration.export();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public boolean addModules(Set<String> classNamespaces) {
        boolean result = true;

        for (String classNamespace : classNamespaces) {
            try {
                Module module = null;

                if (classNamespace.equals(getClass().getName())) {
                    module = this;
                } else {
                    module = (Module) Class.forName(classNamespace)
                                           .getConstructor(fr.lri.iasi.somewhere.App.class)
                                           .newInstance(this);
                }

                if (module != null) {
                    modules.add(module);
                }
            } catch (Exception e) {            	
                logger.error("There were errors initializing module " +
                		classNamespace + " ! : ", e);
                result = false;
            }
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    public boolean removeModules(Set<String> modulesNames) {
        boolean result = true;

        for (String name : modulesNames) {
            if (!modules.isInTheList(name)) {
                logger.info("Uninitialize module " + name + " ...");

                try {
                    modules.remove(name);
                } catch (Exception e) {
                    result = false;
                }
            }
        }

        return result;
    }
    
    /**
     * Update the name of the output log file
     */
    public void updateLogFile() {
    	String logFileName = configuration.getLogFileName();
    	
    	// If log file configuration does not exists, set default file name
    	if (logFileName == null)
    		logFileName = BaseConstants.DEFAULT_LOG_PATH;

    	// Changing the log file output
    	final LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
    	lc.putProperty("logFile", logFileName);
    }

    /**
     * A thread to be run when the application quits, to make sure
     * everything is well cleaned up.
     */
    protected final class ShutdownThread extends Thread {
        public void run() {
            logger.info("Stopping somewhere modules...");

            boolean success = modules.removeAll();

            if (success) {
                logger.info("Successfully stopped somewhere modules...");
            } else {
                logger.error("There were errors stopping somewhere.");
            }
        }
    }
}
