/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.baseApp;

import java.net.URL;

import fr.lri.iasi.somewhere.Module;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathConstants;

/**
 * This class takes an XML configuration file and dispatch to modules all of
 * their config infos.
 * 
 * @author Andre Fonseca
 */
public class XMLConfigurationManager extends AbstractConfigurationManager {
	static Logger logger = LoggerFactory
			.getLogger(XMLConfigurationManager.class);

	/* --- Properties --- */
	/** The path of the XML Config file */
	private String path;

	/** The document of the XML configuration */
	protected Document doc;

	/**
	 * @param path
	 *            The path of the XML Config file
	 */
	public XMLConfigurationManager(String path) {
		if (path == null) {
			throw new IllegalArgumentException("path");
		}

		this.path = path;
		parseFile();
	}

	/* --- Methods --- */
	/**
	 * Parse XML configuration file to obtain corresponding Configuration
	 * properties
	 * 
	 * @return parsed document
	 */
	protected void parseFile() {
		logger.info("Parsing configuration file " + path + " ...");

		try {
			DocumentBuilderFactory domFactory = DocumentBuilderFactory
					.newInstance();
			domFactory.setNamespaceAware(true);

			DocumentBuilder builder = domFactory.newDocumentBuilder();
			doc = builder.parse(path);
		} catch (Exception e) {
			logger.error("There were errors parsing XML configuration file : "
					+ e.getMessage());
			throw new RuntimeException(e);
		}

		logger.info("Configuration file parsing successfully finished.");
	}

	/**
	 * Forward configuration parts associated specified module
	 * 
	 * @param module
	 *            Module to forward config to
	 */
	public void forwardConfig(Module module) {
		if (!modules.getModules().contains(module)) {
			throw new RuntimeException("module");
		}

		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		Node node = doc;

		try {
			XPathExpression expr = xpath.compile("somewhere/"
					+ module.getModuleName());

			node = (Node) expr.evaluate(doc, XPathConstants.NODE);

			if (node != null) {
				logger.info("Fowarding configuration for module "
						+ module.getModuleName() + " ...");

				if (!isSourceValid(node, module.getModuleName())) {
					throw new SAXException();
				}

				module.importConfigXML(node);
			}
		} catch (Exception e) {
			logger.info("Error when fowarding configuration to module "
					+ module.getModuleName() + " ...");
		}
	}

	/**
	 * Validate the xml source
	 * 
	 * @return if is valid
	 */
	protected boolean isSourceValid(Node config, String moduleName) {
		if (config == null) {
			throw new IllegalArgumentException("config");
		}

		// Lookup a factory for the W3C XML Schema language
		SchemaFactory factory = SchemaFactory
				.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

		// Compile the schema.
		URL schemaURL = getClass().getClassLoader().getResource(
				"META-INF/" + moduleName + "_config.xsd");
		Schema schema = null;

		try {
			schema = factory.newSchema(schemaURL);
		} catch (SAXException e) {
			logger.error("There was a problem parsing schema file '"
					+ moduleName + "_config.xsd'...");

			return false;
		}

		// Get a validator from the schema.
		Validator validator = schema.newValidator();

		try {
			// Create new doc for the config subtree
			DocumentBuilder builder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document doc = builder.newDocument();
			Element element = doc.createElement("somewhere");
			doc.appendChild(element);

			// Wrap the config subtree in the new doc in order to make the
			// validation
			config = doc.importNode(config, true);
			element.appendChild(config);

			// Check the document
			validator.validate(new DOMSource(doc));

			return true;
		} catch (Exception ex) {
			logger.error("XML source file is not valid...");

			return false;
		}
	}
}
