/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.baseApp.commands;

import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


/**
 * A command to list the loaded modules
 * 
 * @author Andre Fonseca
 */
public class ModulesListCommand implements Command {
	
    /* --- Properties --- */
    /** Base App object */
    private final fr.lri.iasi.somewhere.App app;

    public ModulesListCommand(fr.lri.iasi.somewhere.App app) {
        if (app == null) {
            throw new IllegalArgumentException("app");
        }

        this.app = app;
    }
    
    /* --- Accessors --- */
    /**
     * {@inheritDoc}
     */
    public String getName() {
        return "modulesList";
    }

    /**
     * {@inheritDoc}
     */
    public String getDescription() {
        return "List modules and give infos about them.";
    }

    /* --- Methods --- */
    /**
     * Outputs a list of loaded modules.
     * @param parameters : empty list
     * @return the return status
     */
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;

        try {
            Set<Module> loadedModules = app.getModules();
            Set<String> loadedModulesNames = new LinkedHashSet<String>();

            for (Module m : loadedModules) {
                loadedModulesNames.add(m.getModuleName());
            }

            Set<String> wantedModules = new HashSet<String>();
            wantedModules.addAll(loadedModulesNames);

            // Retreiving files names
            if ((parameters != null) && (parameters.size() > 0)) {
                wantedModules = new HashSet<String>();

                for (Object o : parameters) {
                    String s = o.toString();
                    wantedModules.add(s);
                }
            }

            // Print infos about commands on a string
            StringBuilder str = new StringBuilder();
            
            Iterator<String> modulesIterator = wantedModules.iterator();
            while (modulesIterator.hasNext()) {
            	String moduleName = modulesIterator.next();
            	if (loadedModulesNames.contains(moduleName)) {
                	str.append(moduleName);
                	if (modulesIterator.hasNext())
                		str.append("\n");
                }
            }

            res = new GenericReturnStatus(str.toString(), true, null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error retreiving infos about modules...",
                    false, null);
        }

        return res;
    }
}
