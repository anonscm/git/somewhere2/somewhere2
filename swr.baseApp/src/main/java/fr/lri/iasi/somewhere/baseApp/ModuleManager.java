/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.baseApp;

import java.lang.reflect.Modifier;
import java.net.JarURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.codehaus.classworlds.ClassWorld;
import org.codehaus.classworlds.DuplicateRealmException;

import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.SomewhereException;

/**
 * Responsible for indicate the modules objects on the classPath that should be loaded on the Somewhere.
 * 
 * @author Andre Fonseca
 *
 */
public class ModuleManager {
	static Logger logger = LoggerFactory.getLogger(ModuleManager.class);

	/** Manages classpaths from URLs */
	final ClassWorld classWorld = new ClassWorld();

	public ModuleManager() throws SomewhereException {
		try {
			this.classWorld.newRealm("core", getClass().getClassLoader());
		} catch (DuplicateRealmException e) {
			throw new SomewhereException(e);
		}
	}

	/**
	 * Load all modules from the classpath that match a given pattern.
	 * 
	 * @param pattern
	 * @param options
	 * @throws SomewhereException 
	 */
	public Set<String> fetchAllClasspathModuleClasses() throws SomewhereException {
		// Start the classpath search
		logger.debug("Starting classpath search... ");
		final Set<String> results = new LinkedHashSet<String>();

		// Get all classpath locations of the current classpath
		final List<URL> locations = findCurrentClassPath();

		// Process all locations
		for (URL location : locations) {
			final Collection<String> moduleClasses = findSubclassesFor(location,
					Module.class);

			logger.debug("Found " + moduleClasses.size() + " modules.");

			results.addAll(moduleClasses);
		}

		return results;
	}

	/**
	 * For one url on the ClassPath, finds out if it extends a specific class.
	 * @param a URL location on the ClassPath 
	 * @param superclass
	 * @return a list of all names of classes that extends the indicated superClass
	 * @throws SomewhereException 
	 */
	private Set<String> findSubclassesFor(URL location,
			Class<?> superclass) throws SomewhereException {

		final Set<String> results = new LinkedHashSet<String>();

		try {
			// Get all class in the indicated ClassPath
			final ClassLoader classLoader = this.classWorld.getRealm("core")
					.getClassLoader();
			final Collection<String> classNames = fetchClassNames(location
					.toURI());
			
			// For each class, see it extends the selected superClass
			for (String name : classNames) {
				final Class<?> c = Class.forName(name, false, classLoader);

				// No interfaces or abstract classes please
				if (c.isInterface()) continue;
				if (Modifier.isAbstract(c.getModifiers())) continue;

				if (superclass.isAssignableFrom(c)
						&& !superclass.getCanonicalName().equals(c.getCanonicalName()))
					results.add(name);
			}
		} catch (Exception e1) {
			throw new SomewhereException(e1);
		}

		return results;
	}

	/**
	 * Lists all top level class entries for the given URL (if the classPath belongs to fr.lri.iasi.somewhere).
	 * 
	 * @param the ClassPath uri
	 * @return the list of all names of classes that belongs to the indicated classPath
	 * @throws SomewhereException 
	 */
	private Set<String> fetchClassNames(URI uri) throws SomewhereException {
		final Set<String> results = new LinkedHashSet<String>();

		try {
			final JarURLConnection connection = (JarURLConnection) new URI(
					"jar:" + uri + "!/").toURL().openConnection();
			final JarFile jarFile = connection.getJarFile();

			final Enumeration<JarEntry> entries = jarFile.entries();

			while (entries.hasMoreElements()) {
				final JarEntry entry = entries.nextElement();

				if (entry.isDirectory()) continue;
				if (!entry.getName().startsWith("fr/lri/iasi/somewhere")) continue;
				if (!entry.getName().endsWith(".class")) continue;

				String name = entry.getName();
				name = name.replaceAll("/", ".");

				// Remove trailing .class
				if (name.endsWith("class")) {
					name = name.substring(0, name.length() - 6);
				}

				results.add(name);
			}
		} catch (Exception e) {
			throw new SomewhereException(e);
		}

		return results;
	}

	/**
	 * Get all locations of the current ClassPath
	 * @return a list of all urls on the ClassPath
	 */
	private List<URL> findCurrentClassPath() {
		// Get the System Classloader
		ClassLoader sysClassLoader = ClassLoader.getSystemClassLoader();

		// Get the URLs
		URL[] urls = ((URLClassLoader) sysClassLoader).getURLs();

		return Arrays.asList(urls);
	}

}
