/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.baseApp.commands;

import java.util.List;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

import fr.lri.iasi.somewhere.SomewhereException;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

/**
 * Responsible for changing the application's logger level at runtime.
 * 
 * @author Andre Fonseca
 *
 */
public class ChangeLogLevelCommand implements Command {

    /* --- Accessors --- */
    /**
     * {@inheritDoc}
     */
	@Override
	public String getName() {
		return "changeLogLevel";
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public String getDescription() {
		return "Change the root log level.";
	}

	@Override
	public ReturnStatus execute(List<Object> parameters) {
		ReturnStatus res = null;

        try {
        	// Verify there are enough parameters
            if (parameters.size() != 1) {
                res = new GenericReturnStatus("Syntax Error! \n Usage :" +
                        "\t" + getName() + " [ALL|TRACE|DEBUG|WARN|INFO]",
                        false, null);

                return res;
            }
            
            String level = (String) parameters.get(0);
            Level selectedLevel = Level.toLevel(level);
            if (selectedLevel == Level.ERROR || selectedLevel == Level.OFF)
            	throw new SomewhereException("Level not allowed. The possible options are : [ALL|TRACE|DEBUG|WARN|INFO]");
            
            Logger root = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
            root.setLevel(selectedLevel);

            res = new GenericReturnStatus("Successfully change log level.",
                    true, null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error changing the log level : " + e.getMessage(),
                    false, null);
        }

        return res;
	}

}
