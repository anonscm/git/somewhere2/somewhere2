/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.baseApp;

import fr.lri.iasi.somewhere.Module;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class is a base Configuration Manager class
 * @author Andre Fonseca
 */
public abstract class AbstractConfigurationManager implements fr.lri.iasi.somewhere.ConfigurationManager {
    static Logger logger = LoggerFactory.getLogger(AbstractConfigurationManager.class);

    /* --- Properties --- */
    /** The Modules manager */
    protected final Modules modules = Modules.getInstance();

    /* --- Methods --- */
    /**
     * Forward configuration parts associated with each modules
     */
    public void forwardConfig() {
        for (Module m : modules.getModules()) {
            forwardConfig(m);
        }
    }

    /**
     * Forward configuration parts associated specified module
     * @param module Module to forward config to
     */
    public abstract void forwardConfig(Module module);
}
