/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.baseApp.commands;

import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.ui.Command;
import fr.lri.iasi.somewhere.ui.GenericReturnStatus;
import fr.lri.iasi.somewhere.ui.ReturnStatus;

import org.w3c.dom.Node;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import java.util.List;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


/**
 * A command to print infos about the global configuration
 * 
 * @author Andre Fonseca
 */
public class ShowConfigurationCommand implements Command {
	
    /* --- Properties --- */
    /** Base App object */
    private final fr.lri.iasi.somewhere.App app;

    public ShowConfigurationCommand(fr.lri.iasi.somewhere.App app) {
        if (app == null) {
            throw new IllegalArgumentException("app");
        }

        this.app = app;
    }
    
    /* --- Accessors --- */
    /**
     * {@inheritDoc}
     */
    public String getName() {
        return "showConfiguration";
    }

    /**
     * {@inheritDoc}
     */
    public String getDescription() {
        return "Print infos about configuration.";
    }

    /* --- Methods --- */
    /**
     * Outputs all modules current configuration
     * @param parameters : empty list
     * @return the return status
     */
    public ReturnStatus execute(List<Object> parameters) {
        ReturnStatus res = null;

        try {
            StringBuilder str = new StringBuilder();

            // Retreive XML configs infos
            for (Module m : app.getModules()) {
            	str.append("\n");
            	str.append("# Configuration of module " + m.getModuleName() +
                " :\n");

                Node mconf = m.exportConfigXML();

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                prettyPrintXML(mconf, out);
                str.append(out.toString());
            }

            res = new GenericReturnStatus(str.toString(), true, null);
        } catch (Exception e) {
            res = new GenericReturnStatus("There was an error retreiving configuration...",
                    false, null);
        }

        return res;
    }

    /**
     * Pretty print an XML Node
     * @param node The node to pretty print
     * @param out Where to print to
     * @throws TransformerException 
     * @throws UnsupportedEncodingException 
     */
    protected final void prettyPrintXML(Node node, OutputStream out)
    		throws UnsupportedEncodingException, TransformerException {
        TransformerFactory transFactory = TransformerFactory.newInstance();
        transFactory.setAttribute("indent-number", 2);

        Transformer transformer = transFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount",
            "4");
        transformer.transform(new DOMSource(node),
            new StreamResult(new OutputStreamWriter(out, "utf-8")));
    }
}
