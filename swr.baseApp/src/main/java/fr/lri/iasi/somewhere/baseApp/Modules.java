/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.baseApp;

import fr.lri.iasi.somewhere.Module;
import fr.lri.iasi.somewhere.SomewhereException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Singleton that contains the list of modules of the project
 * 
 * @author Andre Fonseca
 */
public final class Modules {
    static Logger logger = LoggerFactory.getLogger(Modules.class);

    /* --- Properties --- */
    /** Singleton instance object */
    private volatile static Modules instance;

    /** Modules Map */
    private final Map<String, Module> modules = new ConcurrentHashMap<String, Module>();
    
    /** Indicates if the initialization route has been called */
    private boolean initialized;

    private Modules() {
    	initialized = false;
    }

    /* --- Accessors --- */
    public Set<Module> getModules() {
        return Collections.unmodifiableSet(new LinkedHashSet<Module>(modules.values()));
    }

    public Module getModule(String name) {
        return modules.get(name);
    }

    public Set<String> getModulesNames() {
        return modules.keySet();
    }
    
    public boolean isInitialized() {
        return initialized;
    }

    /* --- Methods --- */
    /**
     * Loads a module
     * @throws SomewhereException 
     */
    public void add(Module module) throws SomewhereException {
        if (module == null) {
            throw new IllegalArgumentException("module");
        }

        try {
            if (modules.containsKey(module.getModuleName())) {
                throw new Exception("" + module.getModuleName() +
                    " already added!");
            }

            modules.put(module.getModuleName(), module);
        } catch (Exception e) {
            logger.error("There was an error trying to add module " +
                module.getModuleName() + " : " + e);
            throw new SomewhereException(e);
        }
    }

    /**
     * Remove an added module
     * @param module Module Object to remove
     * @throws SomewhereException
     */
    public void remove(Module module) throws SomewhereException {
        if (module == null) {
            throw new IllegalArgumentException("module");
        }

        try {
            if (!modules.containsKey(module.getModuleName())) {
                throw new Exception("" + module.getModuleName() +
                    " not initialized!");
            }

            logger.info("Removing module " + module.getModuleName() + " ...");
            module.quit();
            modules.remove(module.getModuleName());
        } catch (Exception e) {
            logger.error("There was an error quitting module " +
                module.getModuleName() + " : " + e);

            if (modules.containsKey(module.getModuleName())) {
                modules.remove(module.getModuleName());
            }

            throw new SomewhereException(e);
        }
    }

    /**
     * Remove an added module by its name
     * @param name Name of the module
     * @throws SomewhereException 
     */
    public void remove(String name) throws SomewhereException {
        if (name == null) {
            throw new IllegalArgumentException("name");
        }

        try {
            if (!modules.containsKey(name)) {
                throw new Exception("" + name + " not initialized!");
            }

            logger.info("Removing module " + name + " ...");
            modules.get(name).quit();
            modules.remove(name);
        } catch (Exception e) {
            logger.error("There was an error quitting module " + name + " : " +
                e);

            if (modules.containsKey(name)) {
                modules.remove(name);
            }

            throw new SomewhereException(e);
        }
    }

    /**
     * Remove all added modules by the reverse loading order
     * @return success
     */
    public boolean removeAll() {
        boolean result = true;
        
        // Sorting by the reverse loading order
    	Comparator<Module> comparator = new ModuleComparator();
    	List<Module> sortedModules = new ArrayList<Module>(modules.values());
    	Collections.sort(sortedModules, comparator);
    	Collections.reverse(sortedModules);

    	// Unloading modules
        for (Module m : sortedModules) {
            try {
                remove(m);
            } catch (Exception e) {
                result = false;
            }
        }

        return result;
    }

    /**
     * @return a new instance of the singleton Modules
     */
    public static Modules getInstance() {
        if (instance == null) {
            instance = new Modules();
        }

        return instance;
    }

    /**
     * Verify if a module is in the list
     * @param name Module name
     * @return if it is in the list
     */
    public boolean isInTheList(String name) {
        return modules.containsKey(name);
    }

    /**
     *  Initialize the selected modules following its loading order
     */
    public void initAll() {
    	// Sorting by loading order
    	Comparator<Module> comparator = new ModuleComparator();
    	List<Module> sortedModules = new ArrayList<Module>(modules.values());
    	Collections.sort(sortedModules, comparator);
    	
    	// Initialize modules
        for (Module module : sortedModules) {
        	logger.info("Initializing module " + module.getModuleName() + " ...");
            module.init();
        }
        
        initialized = true;
        logger.info("All modules were sucessfully initialized!");
    }
    
    /**
     *  Used in order to compare modules by its loading order
     */
    static class ModuleComparator implements Comparator<Module>, Serializable {
		private static final long serialVersionUID = 5280330341931434895L;

		// Comparator interface requires defining compare method.
        public int compare(Module module1, Module module2) {
            return (module1.getLoadingOrder().compareTo(module2.getLoadingOrder()));
        }
    }
}
