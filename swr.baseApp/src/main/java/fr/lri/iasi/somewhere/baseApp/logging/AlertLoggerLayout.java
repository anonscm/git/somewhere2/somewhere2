/**
 * Somewhere2 ( https://sourcesup.renater.fr/projects/somewhere2/ ) - This file is part of Somewhere2
 * Copyright (C) 2012 - IASI - Philippe Chatalic, Andre Fonseca
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */
package fr.lri.iasi.somewhere.baseApp.logging;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.StackTraceElementProxy;
import ch.qos.logback.core.CoreConstants;
import ch.qos.logback.core.LayoutBase;

/**
 * Formats the events messages over the SL4J log.
 * 
 * @author Andre Fonseca
 *
 */
public class AlertLoggerLayout extends LayoutBase<ILoggingEvent> {

	@Override
	public String doLayout(ILoggingEvent event) {
		
		StringBuffer builder = new StringBuffer();
		
		builder.append("\n<" + event.getLevel() + " time= " + event.getTimeStamp() + ">");
		builder.append("\n<thread>" + event.getThreadName() + "</thread>");
		builder.append("\n<logger>" + event.getLoggerName() + "</logger>");
		builder.append("\n<message>" + event.getMessage() + "</message>");
		
		IThrowableProxy tp = event.getThrowableProxy();
		if (tp != null) {
			StackTraceElementProxy[] stepArray = tp.getStackTraceElementProxyArray();
			builder.append("\n<trace>");
			for (StackTraceElementProxy step : stepArray) {
				builder.append(CoreConstants.TAB);
				builder.append(step.toString());
				builder.append("\r\n");
			}
			builder.append("</trace>");
		}
		
		builder.append("\n</" + event.getLevel() + ">");
		
		return builder.toString();
	}

}
